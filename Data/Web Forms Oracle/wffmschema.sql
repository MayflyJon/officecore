/
CREATE TABLE "Form"(
	"Id" CHAR(32) NOT NULL,
	"FormItemId" CHAR(32) NOT NULL,
	"SessionId" CHAR(32) NOT NULL,
	"StorageName" CHAR(100) NULL,
	"Timestamp" TIMESTAMP(3) NOT NULL,
	"Data" NCLOB NULL,
CONSTRAINT "PK_Form" PRIMARY KEY  
(
	"Id"
)
)
/
CREATE  INDEX "IX_FormItemId" ON "Form" 
(
	"FormItemId"
)

/
CREATE  INDEX "IX_StorageName" ON "Form" 
(
	"StorageName"
)
/
CREATE TABLE "Field"(
	"Id" CHAR(32) NOT NULL,
	"FieldId" CHAR(32) NOT NULL,
	"FormId" CHAR(32) NOT NULL,
	"Value" NCLOB NULL,
	"Data" NCLOB NULL,
	"FieldName" NCLOB NULL,
CONSTRAINT "PK_Field" PRIMARY KEY  
(
	"Id"
))
/
CREATE  INDEX "IX_Field" ON "Field" 
(
	"FieldId"
)

/
CREATE  INDEX "IX_FormId" ON "Field" 
(
	"FormId"
)

/
CREATE TABLE "Pool"(
	"Id" CHAR(32) NOT NULL,
	"FieldId" CHAR(32) NOT NULL,
	"Occurrence" NUMBER(10,0) NOT NULL,
	"Value" NCLOB NULL,
CONSTRAINT "PK_Pool" PRIMARY KEY  
(
	"Id"
)
)
/
CREATE  INDEX "IX_PoolFieldId" ON "Pool" 
(
	"FieldId"
)
/
ALTER TABLE "Field"  
ADD CONSTRAINT "FK_FormId" FOREIGN KEY ("FormId") REFERENCES "Form" ("Id")
/