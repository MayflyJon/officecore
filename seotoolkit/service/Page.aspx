<%@ Page language="c#" %>
<%@ Import namespace="Sitecore.SeoToolkit.Service"%>

<script type="text/C#" runat="server">
  protected override void OnLoad(EventArgs e)
  {
     base.OnLoad(e);

     Response.Write(new PageService(FormHelper.GetFormData()).Respond());
  }
</script>