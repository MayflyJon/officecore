<%@ Page Language="c#" %>

<%@ Import Namespace="Sitecore.SeoToolkit.Service" %>

<script type="text/C#" runat="server">
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        PageProxyService service = new PageProxyService(this, Request.QueryString["url"]);
        service.CopyHeaders(Response);
        Response.Write(service.Respond());
    }
</script>

