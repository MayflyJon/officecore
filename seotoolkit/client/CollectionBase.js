var CollectionBase = Base.extend({
  constructor: function() {
    this.elements = new Array();
  },
  
  parse: function() {
    console.error('CollectionBase.parse is abstract');
  },
  
  renderReport: function() {
    console.error('CollectionBase.renderReport is abstract');
  },
  
  attachOverlays: function() {
    this.elements.each(function(element) {
      element.attachOverlays();
    });
  },
  
  length: function() {
    return this.elements.length;
  },
  
  valid: function() {
    return this.elements.findAll(function(element) {
      return element.isValid();
    });
  },
  
  invalid: function() {
    return this.elements.findAll(function(element) {
      return !element.isValid();
    });
  },
  
  errorCount: function() {
    return this.invalid().length;
  }
});