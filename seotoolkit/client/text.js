var SeoText = Base.extend({
  constructor: function() {
    this.container = $('seoTextPlaceholder');
  },

  parse: function() {
    this.container.innerHTML = seo.progressHTML;
    this.serviceLookup();
  },

  serviceLookup: function() {
    console.group('plain text service lookup');

    new Ajax.Request(seo.page.baseUrl + '/seotoolkit/service/page.aspx?url=' + seo.page.href, {
      postBody: seo.page.html(),
      onSuccess: function(transport) {
        console.log('plain text service call success');
        seo.page.text.onServiceResponse(transport.responseText);
      },
      onException: function(transport, error) {
        throw error;
      },
      onFailure: function(transport) {
        console.error('plain text service failure');
        this.container.innerHTML = seo.formatError(localizationDictionary["ERROR_ANALYZING_PAGE_TEXT"]);
      }.bind(this)
    });
    
    console.groupEnd();
  },

  onServiceResponse: function(text) {
    this.container.innerHTML = text;
  }
});