var SeoKeywords = Base.extend({
  constructor: function() {
    this.container = $('seoKeywordsPlaceholder');
  },

  parse: function() {
    this.serviceLookup();
  },
  
  attachEvents: function(container) {     
     $$('.keywordRow').each(function(row) {
      row.observe('mouseover', seo.page.keywords.onMouseOver.bindAsEventListener(row));
      row.observe('mouseout', seo.page.keywords.onMouseOut.bindAsEventListener(row));
    });
  },

  onMouseOver: function(evt) {
    /* this bound to 'tr.searchRow' */
    this.addClassName('keywordRowHover');
  },

  onMouseOut: function(evt) {
    /* this bound to 'tr.searchRow' */
    this.removeClassName('keywordRowHover');
  },
  
  onServiceResponse: function(text) {
    text = seo.util.preprocessJson(text);
    try {
      var json = eval(text);
    }
    catch (e) {
      this.container.innerHTML = seo.formatError(localizationDictionary["UNEXPECTED_SERVER_RESPONSE"]);
      console.error('error parsing server response:');
      console.log(text);
      return;
    }
    
    var empty = true;
   
    for(var i = 0; i <= 2; i++) {
      var keywordCollection = json[i];
      var report = '<table cellpadding="0" cellspacing="0">';

      var templateString = '<tr class="keywordRow">' ;
      templateString +=      '<td class="term">#{term}</td><td class="count">#{count}</td><td class="percentage">#{density}%</td>';
      templateString +=      '<td class="seoKeywordSearch">';
      templateString +=        '<a href="http://www.google.com/search?q=#{term}" target="_blank" title="' + 
        localizationDictionary["SEARCH_FOR"] + ' \'#{term}\' ' + localizationDictionary["IN_GOOGLE"] + '"><img src="' + seo.page.baseUrl + '/seotoolkit/client/img/google-icon.gif" /></a>';
      templateString +=        '<a href="http://search.yahoo.com/search?p=#{term}" target="_blank" title="' + 
        localizationDictionary["SEARCH_FOR"] + ' \'#{term}\' ' + localizationDictionary["IN_YAHOO"] + '"><img src="' + seo.page.baseUrl + '/seotoolkit/client/img/yahoo-icon.gif" /></a>';
      templateString +=        '<a href="http://search.msn.com/results.aspx?q=#{term}" target="_blank" title="' + 
        localizationDictionary["SEARCH_FOR"] + ' \'#{term}\' ' + localizationDictionary["IN_MSN"] + '"><img src="' + seo.page.baseUrl + '/seotoolkit/client/img/live-icon.gif" /></a>';
      templateString +=     '</td></tr>';

      keywordCollection.each(function(keyword) {
        report += new Template(templateString).evaluate(keyword);
        empty = false;
      });
      
      report += '</table>';    
      $('seoKeywords' + i).innerHTML = report;
      this.attachEvents($('seoKeywords' + i));
    }
    
    if (empty) {
      $('seoKeywordsPlaceholder').innerHTML = seo.formatNoElements(localizationDictionary["NO_STATISTICALLY_SIGNIFICANT_WORDS_ON_PAGE"]);
    }
  },

  serviceLookup: function() {
    console.group('keywords service lookup');
    seo.registerProgress('keywords service');
    

      var reques = new Ajax.Request(seo.page.baseUrl + '/seotoolkit/service/keywords.aspx', {
         postBody: seo.page.html(),
            onSuccess: function(transport) {
            console.log('keywords service call success');
            seo.page.keywords.onServiceResponse(transport.responseText);
            seo.unregisterProgress('keywords service');
         },
            onException: function(transport, error) {
            seo.unregisterProgress('keywords service');
            throw error;
         },
         onFailure: function(transport) {
            seo.unregisterProgress('keywords service');
            console.error('keywords service failure');
            this.container.innerHTML = seo.formatError(localizationDictionary["ERROR_ANALYZING_PAGE_KEYPHRASES"]);
         }.bind(this)
      });


    console.groupEnd();
  }
});