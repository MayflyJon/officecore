var ElementBase = Base.extend({
  constructor: function(domElement, prefix) {
    seo.assertElement(domElement, "domElement");

    this.domElement = domElement;
    this.controlID = seo.registerElement(this, prefix);
    
    this.util = new seoUtility();
  },

  scrollToElement: function() {
    var isRibbon = this.util.getCookie("sitecore_webedit_ribbon");
    var offsetY = this.domElement.offsetTop;

    if (isRibbon) {
      offsetY = offsetY - 150;
    }

    if (offsetY < 0)
      offsetY = 0;

    scroll(0, offsetY);
  },

  focus: function() {
    this.scrollToElement();
    this.enableHover();
    this.showTooltip(true);
  },

  enableHover: function() {
    this.domElement.addClassName('seoHover');
  },

  disableHover: function() {
    this.domElement.removeClassName('seoHover');
  },

  showTooltip: function() {
    if (this.tooltip == null) {
      var offset = this.cumulativeOffset();
      var tooltipLeft = offset[0] < 0 ? 0 : offset[0] + 'px';
      var tooltipTop = offset[1] + this.domElement.getHeight() + 'px';

      var tooltip = document.createElement('div');
      tooltip.className = 'seoTooltip';
      tooltip.style.left = tooltipLeft;
      tooltip.style.top = tooltipTop;
      tooltip.style.display = 'none';
      tooltip.innerHTML = this.renderer.getTooltipHTML(this);

      Event.observe(tooltip, 'click', this.hideTooltip.bindAsEventListener(this, true));

      document.body.appendChild(tooltip);
      seo.effects.tooltipAppear(tooltip);

      this.tooltip = tooltip;
    }
  },

  hideTooltip: function(evt, force) {
    if (this.tooltip != null) {
      this.tooltip.parentNode.removeChild(this.tooltip);
      this.tooltip = null;

      this.disableHover();

      if (evt != null) {
        Event.stop(evt);
      }
    }
  },

  attachOverlays: function() {
    console.error('attachOverlays is abstract');
  },

  onWindowResize: function(evt) {
    this._offset = null;
    if (this.overlay) {
      this.overlay.remove();
      this.overlay = null;
      this.attachOverlays();
    }
  },

  isValid: function() {
    return this.getErrors().length == 0;
  },

  getErrors: function() {
    return this.validators.invoke('getErrors', this).flatten();
  },

  cumulativeOffset: function() {
    if (!this._offset) {
      if (Prototype.Browser.IE) {
        this.ensureRelativeAncestorsHaveLayout();
      }

      this._offset = Position.cumulativeOffset(this.domElement);
    }

    return this._offset;
  },

  withinElement: function(x, y) {
    if (Position.includeScrollOffsets) {
      console.warn("falling back to prototype's Position.withingInclusingScrolloffsets");
      return Position.withinIncludingScrolloffsets(element, x, y);
    }

    this.offset = this.cumulativeOffset();

    return (y >= this.offset[1] &&
            y < this.offset[1] + this.domElement.offsetHeight &&
            x >= this.offset[0] &&
            x < this.offset[0] + this.domElement.offsetWidth);
  },

  ensureRelativeAncestorsHaveLayout: function() {
    if (!this._ancestorLayoutEnsured) {
      console.info('IE hasLayout hack - ensuring layout for relatively positioned ancestors');

      var element = this.domElement;
      while (element && element.nodeName != 'HTML') {
        if (Element.getStyle(element, 'position') == 'relative' && !element.currentStyle.hasLayout) {
          console.log('setting zoom property for: ');
          console.log(element);
          element.style.zoom = '1';
        }

        var offsetParent = Position.offsetParent(element);
        if (offsetParent == element) break;

        element = offsetParent;
      }

      this._ancestorLayoutEnsured = true;
    }
  },

  toString: function() {
    return this.controlID;
  }
});