var SeoLink = ElementBase.extend({
  constructor: function(element) {
    this.base(element, 'a');

//    if (typeof element == "object") {
//      this.text = element.text;
//    }
//    else {
      this.text = seo.util.innerText(this.domElement);
//    }

    this.href = element.href;
    this.type = this.getLinkType();
  },

  attachOverlays: function() {
    if (this.isValid()) return;

    var offset = this.cumulativeOffset();
    var overlayLeft = offset[0] + this.domElement.getWidth() + 'px';
    var overlayTop = offset[1] + this.domElement.getHeight() - 16 + 'px';

    var overlay = document.createElement('div');
    overlay.className = 'seoOverlay';
    overlay.style.left = overlayLeft;
    overlay.style.top = overlayTop;
    overlay.innerHTML = '<img src="' + seo.page.baseUrl + linkDeleteImageUrl + '" />';

    Event.observe(overlay, 'mouseover', this.showTooltip.bindAsEventListener(this));
    Event.observe(overlay, 'mouseout', this.hideTooltip.bindAsEventListener(this));

    document.body.appendChild(overlay);
    this.overlay = overlay;
  },

  showTooltip: function() {
    this.base();

    Event.observe(this.tooltip, 'mouseout', this.hideTooltip.bindAsEventListener(this, false));
  },

  hideTooltip: function(evt, force) {
    console.info('link hideTooltip');

    if (force || !this.within(Event.pointerX(evt), Event.pointerY(evt))) {
      this.base(evt, force);
    }
  },

  within: function(x, y) {
    var offset = this.cumulativeOffset();

    if (y < offset[1] || !this.tooltip) {
      return false;
    }
    else if (y < offset[1] + this.tooltip.getHeight()) {
      return (x > offset[0]) && (x < offset[0] + this.tooltip.getWidth() - 2);
    }
    else {
      return Position.within(this.tooltip, x, y);
    }
  },

  getLinkType: function() {
    var href = this.href.strip().toLowerCase();
    if (href.indexOf('#') > 0) {
      return 'anchor';
    }

    if (this.href && this.domElement.pathname && this.domElement.pathname.indexOf('//' < 0)) {
      return 'internal';
    }

    return 'external';
  },

  isWebLink: function() {
    return this.href.strip().indexOf('http') == 0;
  }
});