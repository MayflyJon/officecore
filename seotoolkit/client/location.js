/* location object - current and base url, url analysis, depth level, etc */

var SeoLocation = Base.extend({
  constructor: function() {
    /* Protocol/host of the Sitecore website, as in http://www.sitecore.net. Used to absolutize local links because of <base> tag injection during page proxying */  
    this.baseUrl = window.location.protocol + '//' + window.location.host;
       
    /* url of the page being analyzed, i.e. http://www.sitecore.net/products.aspx */
    if (window.location.search && window.location.search.indexOf('?url=') == 0) {
      this.path = new SeoUrl(window.location.search.substring(window.location.search.indexOf('?url=') + 5));
      this.proxyMode = true;    
    }
    else {
      this.path = new SeoUrl(window.location.href);
      this.proxyMode = false;
    }    
  },

  /*
     indicates that ids are used for navigation instead of proper url's. 
     /article.aspx?id=3543 - WRONG
     /article5434.aspx - WRONG
     /article/pigeons.aspx - RIGHT
  */
  isUrlId: function() {
    if (this.path.level > 1) return false;
  
    // look for id in query string params
    if (this.inspectQueryForId(this.path.queryString)) {
      return true;
    }
  
    // look for id in page name
    if (this.inspectPathForId(this.path.path)) {
      return true;
    }
    
    return false;    
  },
  
  inspectQueryForId: function(query) {
    if (!query) return false;
    
    var values = Object.values(query.toQueryParams());
    
    for(var i = 0; i < values.length; i++) {
      var value = values[i];
      
      if (parseInt(value) > 0) {
        return true;
      }
    }    
  },
  
  inspectPathForId: function(path) {
    for (var i = 0; i < path.length; i++) {
      var part = path.substring(i, path.length);
      
      if (parseInt(part) > 0) { 
        return true;
      }
    }   
  },

  /* outputs description string. Example: "firstLevel, secondLevel. Querystring: key=value" */
  formatPathInformation: function() {
    if (this.path.level == 0) return localizationDictionary["NONE_AT_FRONTPAGE"];

    var first = true;  
    var result = '';
    
    this.path.pathGroups.each(function(part) {
      if (first) first = false;
      else result += ', ';
      
      result += part;
    }.bind(this));
    
    if (this.path.query) {
      result += '<b>. ' + localizationDictionary["QUERYSTRING"] + '</b>: ' + this.path.query;
    }
    
    return result;    
  },
  
  toString: function() {
    return this.path.toString();  
  }
});