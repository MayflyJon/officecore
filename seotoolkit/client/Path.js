var SeoUrl = Base.extend({
  constructor: function(path) {
    this.rawUrl = unescape(path);
    try {
      this.parsePath();
      this.splitPath();
    }
    catch (e) {
      console.error(e);
    }
  },
  
  parsePath: function() {
    // protocol
    this.protocol = this.rawUrl.substring(0, this.rawUrl.indexOf('://'));
    var path = this.rawUrl.substring(this.rawUrl.indexOf('://') + 3)
    
    // hostname
    if (path.indexOf('/') > 0) {
      this.hostname = path.substring(0, path.indexOf('/'));
      path = path.substring(path.indexOf('/') + 1);
    }
    else {
      this.hostname = path;
    }
    
    // query string
    if (path.indexOf('?') >= 0) {
      this.parseQueryString(path.substring(path.indexOf('?') + 1, path.length));
      path = path.substring(0, path.indexOf('?'));
    }
    
    // remove extension
    if (path.lastIndexOf('.') >= 0) {
     path = path.substring(0, path.lastIndexOf('.'));
    }

    // finally, this is our relative path
    this.path = path;
  },
  
  parseQueryString: function(rawQueryString) {
    var result = '';
    var first = true;
  
    rawQueryString.split("&").each(function(queryPair) {
      var pair = queryPair.split('=');
      var key = pair[0];
      var value = pair[1];
      
      if (!key.startsWith('sc_')) {
        if (first) first = false;
        else result += '&';
        
        result += key + '=' + value;
      }
      
    }.bind(this));
    
    this.queryString = result;
  },
  
  splitPath: function() {
    if (this.path.length == 0) {
      this.level = 0;
      return;
    }
  
    this.pathGroups = this.path.split('/');
    this.level = this.pathGroups.length;
  },
  
  toString: function() {
    var result = '';
    
    if (this.protocol) {
      result += this.protocol + '://';
    }
    
    if (this.hostname) {
      result += this.hostname + '/';
    }
    
    if (this.path) {
      result += this.path;
    }
    
    if (this.queryString) {
      result += '?' + this.queryString;
    }
    
    return result;
  }
});