var SeoHeading = ElementBase.extend({
  constructor: function(domElement) {
    this.base(domElement, 'h');
    
    this.text = seo.util.innerText(domElement);
    this.level = parseInt(domElement.nodeName.substring(1, 2));
  }
});
