/* information lines, rendered on the first 'information' tab */  

var SeoInformation = Base.extend({
    title: new SeoInformationLine(localizationDictionary["TITLE"], function() {
    if (document.title) {
      this.text = document.title;
    }
    else {
      this.error = true;
      this.text = localizationDictionary["PAGE_TITLE_NOT_SET"];

      this.helpLink = 'http://sdn.sitecore.net/Products/Seo%20Toolkit/Possible%20Errors/Page/NoTitle.aspx';
    }
  }),

    url: new SeoInformationLine(localizationDictionary["URL"], function() {
    this.text = seo.page.location.toString();
  }),

    keywords: new SeoInformationLine(localizationDictionary["KEYWORDS"], function() {
    if (seo.page.meta.getValue("keywords")) {
      this.text = seo.page.meta.getValue("keywords");
    } 
    else {
      this.error = true;
      this.text = localizationDictionary["KEYWORDS_NOT_DEFINED"];
    }  
  }),

    urlKeywords: new SeoInformationLine(localizationDictionary["URL_KEYWORDS"], function() {
    if (seo.page.location.isUrlId()) {
      this.error = true;
      this.text = localizationDictionary["POSSIBLE_USE_NAVIGATION_IDS"] + ' ' + seo.page.location.formatPathInformation();
      this.helpLink = 'http://sdn.sitecore.net/Products/Seo%20Toolkit/Possible%20Errors/Page/Navigation%20ID.aspx';
    }
    else {
      this.text = seo.page.location.formatPathInformation();
    }
  }),

    description: new SeoInformationLine(localizationDictionary["DESCRIPTION"], function() {
    if (seo.page.meta.getValue("description")) {
      this.text = seo.page.meta.getValue("description");
    }
    else {
      this.error = true;
      this.text = localizationDictionary["DESCRIPTION_NOT_DEFINED"];
      this.helpLink = 'http://sdn.sitecore.net/Products/Seo%20Toolkit/Possible%20Errors/Page/NoDescription.aspx';
    } 
  }),

    language: new SeoInformationLine(localizationDictionary["LANGUAGE"], function() {
    var html = $$('html')[0];
    
    var language = html.getAttribute('lang') ? html.getAttribute('lang') : html.getAttribute('xml:lang');
    if (language) {
      this.text = language;
    }
    else {
      this.error = true;
      this.text = localizationDictionary["LANGUAGE_NOT_SET"];
      this.helpLink = 'http://sdn.sitecore.net/Products/Seo%20Toolkit/Possible%20Errors/Page/Language.aspx';
    }  
  }),

    charset: new SeoInformationLine(localizationDictionary["CHARACTER_SET"], function() {
    if (seo.page.charset) {
      this.text = seo.page.charset;
    }
    else {
      this.error = true;
      this.text = localizationDictionary["CHARACTER_SET_NOT_DEFINED"];
      this.helpLink = 'http://sdn.sitecore.net/Products/Seo%20Toolkit/Possible%20Errors/Page/Charset.aspx';
    }  
  }),

    metaTags: new SeoInformationLine(localizationDictionary["META_TAGS"], function() {
    this.click = 'tabController.showTab(\'seoMetaTab\')';

    this.text = seo.page.meta.length() + ' ' + localizationDictionary["META_TAGS_TOTAL"];
  }),

    images: new SeoInformationLine(localizationDictionary["IMAGES"], function() {
    this.click = 'tabController.showTab(\'seoImagesTab\')';
    
    var errorCount = seo.page.images.errorCount();
    
    if (errorCount > 0) {
      this.error = true;
      this.text = errorCount + ' ' + localizationDictionary["ERRORS"] + ', ';
    }
    else {
        this.text = localizationDictionary["NO_ERRORS"] + ' ';
    }
    
    this.text += seo.page.images.length() + ' ' + localizationDictionary["IMAGES_TOTAL"];
  }),

    links: new SeoInformationLine(localizationDictionary["LINKS"], function() {
    this.id = "seoLinksInfomationLine";
    
    this.click = 'tabController.showTab(\'seoLinksTab\')';

    if (seo.page.links.isFinishedCurrentJob)
    {
      var errorCount = seo.page.links.errorCount();
        
        if (errorCount > 0) {
          this.error = true;
          this.text = errorCount + ' ' + localizationDictionary["ERRORS"] + ', ';
        }
        else {
            this.text = localizationDictionary["NO_ERRORS"] + ' ';
        }
        this.text += seo.page.links.length() + ' ' + localizationDictionary["LINKS_TOTAL"];
    }
    else
    {
      this.text = localizationDictionary["LINKS_BEING_CHECKED"] + ', ';
      this.text += seo.page.links.length() + ' ' + localizationDictionary["LINKS_TOTAL"] + '...';
    }
  }),

    headings: new SeoInformationLine(localizationDictionary["HEADINGS"], function() {
    this.click = 'tabController.showTab(\'seoHeadingsTab\')';
    
    if (seo.page.headings.length() == 0) {
      this.error = true;
      this.text = localizationDictionary["NO_HEADINGS_FOUND_ON_PAGE"];
      this.helpLink = 'http://sdn.sitecore.net/Products/Seo%20Toolkit/Possible%20Errors/Page/NoHeadings.aspx';
    }
    else {
      this.text = localizationDictionary["NO_ERRORS"] + ' ' + seo.page.headings.length() + ' ' + localizationDictionary["HEADINGS_TOTAL"];
    }
  })
});