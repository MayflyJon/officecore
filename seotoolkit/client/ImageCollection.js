var SeoImageCollection = CollectionBase.extend({
  constructor: function() {
    this.base();
    
    SeoImage.prototype.renderer = new seoImageRenderer();
    
    SeoImage.prototype.validators = new Array();
    SeoImage.prototype.validators.push(new missingAltValidator());
    SeoImage.prototype.validators.push(new emptyAltValidator());
    
    this.activeTooltips = new Array();
    Event.observe(document, 'mousemove', this.onMouseMove.bindAsEventListener(this));
  },
  
  parse: function() {
    var documentImages = $A(document.images);
    
    for(var i = 0; i < documentImages.length; i++) {
      var image = Element.extend(documentImages[i]);
      
      if (image.up('#extender') == null && !seo.util.isWebEditImage(image)) {
        this.push(new SeoImage(image));
      }
    }
    
    console.log('%s images parsed', this.length());
  },
  
  push: function(image) {
    var original = this.elements.find(image.equals.bind(image));
    
    /* if there are duplicates, add duplicate */  
    if (original != null) {
      original.addDuplicate(image);
    }
    /* no duplicates, add new image */
    else {  
      this.elements.push(image);
    }
  },
  
  onMouseMove: function(evt) {
    var active = new Array();
    
    this.activeTooltips.each(function(image) {
      if (!image.within(Event.pointerX(evt), Event.pointerY(evt))) {
        image.hideTooltip();
      }
      else {
        active.push(image);
      }
    });
    
    this.activeTooltips = active;
  },
  
  renderReport: function(container) {
    var result = ''

    if (this.elements.length > 0) {
      this.invalid().each(function(image) {
        result += image.renderer.getReportHTML(image);
      });

      this.valid().each(function(image) {
        result += image.renderer.getReportHTML(image);
      });
    }
    else {
      result += seo.formatNoElements(localizationDictionary["NO_IMAGES_ON_PAGE"]);
    }
    
    container.innerHTML = result;
  }
});

/* validators */
function missingAltValidator() {}
missingAltValidator.prototype.getErrors = function(image) {
  var result = new Array();
  if (!image.domElement.hasAttribute("alt")) {
    result.push({ text: localizationDictionary["ALTERNATE_TEXT_NOT_SET"], helpLink: 'http://sdn.sitecore.net/Products/Seo%20Toolkit/Possible%20Errors/Images/Missing%20Alt%20Attribute.aspx' });
  }
  
  return result;
}

function emptyAltValidator() {}
emptyAltValidator.prototype.getErrors = function(image) {
  var result = new Array();
  
  if (image.domElement.hasAttribute("alt") && image.alt.length == 0 && image.isContentManaged()) {
    result.push({ text: localizationDictionary["ALTERNATE_TEXT_EMPTY"], helpLink: 'http://sdn.sitecore.net/Products/Seo%20Toolkit/Possible%20Errors/Images/Empty%20Alt%20Content%20Image.aspx' });
  }
  
  return result;
}