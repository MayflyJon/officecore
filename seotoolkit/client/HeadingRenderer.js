var SeoHeadingRenderer = Base.extend({
  getReportHTML: function(element) {
    var templateString = '<div class="reportRow seoHeading#{level}" onclick="seo.focus(\'#{controlID}\'); return false;">#{text}</div>'
    return new Template(templateString).evaluate(element);
  },
  
  getTooltipHTML: function(element) {
    var templateString = '';

    templateString += '<p class="tooltipHeader">' + localizationDictionary["HEADING"] + '#{level}</p>';
    templateString += '<div class="tooltipBody">'; 
    templateString += '' +
        '<p><span class="key">' + localizationDictionary["TEXT"] + ':</span><span class="value">#{text}</span></p>';
    templateString += '</div>';
    
    return new Template(templateString).evaluate(element);
  }
});