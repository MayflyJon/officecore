var SeoMetaTags = Base.extend({
  constructor: function() {
    this.metaTags = new Array();
  },

  parse: function() {
    $$('meta').each(function(tag) {
      this.metaTags.push(new SeoMetaTag(tag));
    }.bind(this));
  },

  length: function() {
    return this.metaTags.length;
  },

  getValue: function(tagName) {
    var tag = this.metaTags.find(function(tag) { return tag.name.toLowerCase() == tagName.toLowerCase(); });
    if (tag != null) {
      return tag.value;
    } 
  },

  renderReport: function(container) {
    seo.assertElement(container, "container");

    if (this.metaTags.length > 0) {
      this.metaTags.each(function(tag) {
        container.innerHTML += tag.toHTML();
      });
    }
    else {
      container.innerHTML = seo.formatNoElements(localizationDictionary["NO_META_TAGS_ON_PAGE"]);
    }  
  }
});

var SeoMetaTag = Base.extend({
  constructor: function(element) {
    this.domElement = element;
    
    this.name = element.name ? element.name : element.httpEquiv;
    this.value = element.content;
    this.scheme = element.scheme;
  },

  toHTML: function() {
    var template = new Template('<p class="meta"><span class="key">#{name}:</span><span class="value">#{value}</span></p>');
    return template.evaluate(this);
  }
});