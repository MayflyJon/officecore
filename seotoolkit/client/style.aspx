<%@ Page language="c#" %>
<%@ Import namespace="Sitecore.SeoToolkit"%>
<%@ Import namespace="Sitecore.SeoToolkit.Service"%>
<%@ Import Namespace="Sitecore.Resources" %>

<script type="text/C#" runat="server">
  protected override void OnLoad(EventArgs e)
  {
     base.OnLoad(e);
     Response.ContentType = "text/css";
  }
</script>

.webEdit_none
{
    height: 0px !important;
    visibility: hidden;
}

#extender {
  min-height: 350px;
  padding: 0;
  margin: 0 2px 0 2px;  
  clear: both;
  
  font-size: 11px;
  font-family: Tahoma, Serif;
  text-align: left;
  color: Black;
  background: white;
}

#extender img {
  border: none;
}

#extender a, .seoTooltip a {
  text-decoration: none;
  font-weight: normal;
  color: Black;
}

#extender p, .seoTooltip p {
  margin: 0;
  padding: 0;
}

#extender .seoNoElements {
  width: 100%;
  text-align: center;
  margin: 20px auto;    
}

#extender p.errorInfo, .seoTooltip p.errorInfo {
  color: Red;
  font-weight: bold;
  background: url(<%=Sitecore.Resources.Images.GetThemedImageSource("Applications/16x16/delete2.png", Sitecore.Web.UI.ImageDimension.id16x16)%>) no-repeat;
  background-position: 0px 50%;
  padding-left: 20px;
  line-height: 16px;
}

/* overlays */
.seoOverlay {
  position: absolute;
  background: transparent;
  font-size: 10px;
  
  opacity: .8;
}

/* toopltip */
.seoTooltip {
  position: absolute;
  background:#f4f1e8;
  border:1px solid #aca899;
  font-size: 11px;
  text-align: left;
  color: Black;
  cursor: pointer;
  z-index:999999;
}

.seoTooltip .tooltipHeader {
  background: #c1d2ee;
  padding: 2px 4px 2px 4px;
  border-bottom:1px solid #aca899;
  font-weight: bold;
}

.seoTooltip .errorHeader {
  background-color: #EDC5C0;
}

.seoTooltip .tooltipBody {
  padding: 4px;
}

.seoTooltip .validationErrors {
  margin-bottom: 4px;
  border-bottom: solid 1px #edc5c0;
}

.seoTooltip .key, #extender .key {
  font-weight: bold;
  margin-right: 5px;
}

.seoTooltip img {
  border: none;
}

/* hover effects */
a.seoHover {
  background-color: #d3d3d3;
}

.seoImageFrame {
  position: absolute;
  border: solid 1px black;
}

.seoInnerImageFrame {
  filter:progid:DXImageTransform.Microsoft.Alpha(opacity=20);

  height: 100%;
  width: 100%;
  
  opacity: .2;
}

/* report */
#extender p.information {
  margin-left: 4px;
  margin-bottom: 2px;
  padding-left: 20px;
  line-height: 16px;
  
  background-position: 0px 50%;
  background: url(<%=Sitecore.Resources.Images.GetThemedImageSource("Applications/16x16/check.png", Sitecore.Web.UI.ImageDimension.id16x16)%> ) no-repeat;
}

#extender p.seoError {
  background: url(<%=Sitecore.Resources.Images.GetThemedImageSource("Applications/16x16/delete2.png", Sitecore.Web.UI.ImageDimension.id16x16)%> ) no-repeat;
}

#extender p.information a .key {
  text-decoration: underline;
}

#extender p.seoError .key{
  color: Red;
}

#extender .reportRow {
  cursor: pointer;
  margin-bottom: 4px;
  clear: left;
}

#extender .reportRow:hover {
  background-color: #d9d9d9;
}

#extender .reportRow:hover .imageContainer {
  border: solid 1px black;
}

#extender .seoProgress {
  text-align: center;
  margin: 25px 0 50px 0px;
}

.seoProgressTooltip {
  position: absolute;
  top: 25px;
  line-height: 24px;  
  font-size: 12px;
  font-family: Tahoma, Serif;
}

.seoProgressTooltip img {
  float: left;
}

/* report search engines */
#extender #seoSearchEngines {
  padding: 0 0 0 4px;
}

#extender #seoSearchEngines h2 {
  margin: 0 0 5px 0;
  padding: 0;
  font-size: 12px;
}

#extender #seoSearchEngines a:hover {
  text-decoration: underline;
}

#extender ul {
  padding: 0;
  margin: 8px 0 4px 10px;  
}

#extender ul {
  list-style-type:none;
}

#extender ul li {
  margin-bottom: 2px;
  height: 16px;
  line-height: 16px;
}

#extender ul li img {
  float:left;
  margin-right: 4px;
}

/* report images */
#extender .imageRow .imageContainer {
  height: 49px;
  width: 48px;
  line-height: 48px;
  font-size:1px;
  float: left;
  text-align: center;
  border: solid 1px #d3d3d3;  
  margin: 2px 5px;
}

#extender .imageRow img.imageThumbnail {
  vertical-align: middle;
}

#extender .imageRow .reportContainer {
  float: left;
}

/* report links */
#extender .link {
  background-repeat: no-repeat;
  background-position: 2px 2px;
  padding-left: 20px;
  padding-bottom: 4px;
}

#extender .linkProcessing {
  background-image: url(<%=Sitecore.Resources.Images.GetThemedImageSource("Network/16x16/link_view.png", Sitecore.Web.UI.ImageDimension.id16x16)%>);
  background-position: 2px 2px;
}

#extender .linkProcessed {
  background: url(<%=Sitecore.Resources.Images.GetThemedImageSource("Network/16x16/link.png", Sitecore.Web.UI.ImageDimension.id16x16)%>) no-repeat;
  background-position: 2px 2px;
}

#extender .link_anchor {
  background-image: url(<%=Sitecore.Resources.Images.GetThemedImageSource("Core3/16x16/undo.png", Sitecore.Web.UI.ImageDimension.id16x16)%>);
}

#extender .link_external {
  background-image: url(<%=Sitecore.Resources.Images.GetThemedImageSource("Network/16x16/earth.png", Sitecore.Web.UI.ImageDimension.id16x16)%>);
}

/* report meta tags */
#extender p.meta {
  margin-left: 4px;
  margin-bottom: 4px;
}

/* report text view*/
#seoTextPlaceholder {
  padding: 4px;
  font-size: 105%;
}

#seoTextPlaceholder .seoTextLink {
  text-decoration: underline;  
  padding: 0 2px;
}

#seoTextPlaceholder .seoTextHeading {
  font-weight: bold;
  display: block;
}

#seoTextPlaceholder .seoTexth1 {
  font-size: 140%;
  margin: 10px 0 5px 0;
}

#seoTextPlaceholder .seoTexth2 {
  font-size: 120%;
}

#seoTextPlaceholder .seoTexth3 {
  font-size: 110%;
}

/* report keywords*/
#seoKeywords {
  width: 100%;
  text-align: left;
  padding-left: 4px;
}

#seoKeywords tbody {
  font-size: 11px;
}

#seoKeywords tr {
  vertical-align: top;  
}

#seoKeywords tr.keywordRow {
  height: 18px;
}

#seoKeywords td.count {
  padding: 0 4px 0 16px;
}

#seoKeywords td.percentage {
  padding: 0 12px 0 12px;
}

#seoKeywords tr.keywordRow img {
  visibility: hidden;
  padding-right: 4px;
}

#seoKeywords tr.keywordRowHover img {
  visibility: visible !important;
}

/* report headings */
#seoHeadingReport {
  margin-top: 4px;
}

#seoHeadingReport .reportRow  {
  height: 20px;
  line-height: 20px;
  margin: 0;  
  padding: 0 0 0 18px;
  background: url(<%=Sitecore.Resources.Images.GetThemedImageSource("Applications/16x16/bullet_triangle_green.png", Sitecore.Web.UI.ImageDimension.id16x16)%>) no-repeat;
  background-position: 0 50%;
}

#seoHeadingReport div a {
  vertical-align: middle;
  margin-top: 4px;
}

#seoHeadingReport .seoHeading1 {
  margin-left: 4px;
}

#seoHeadingReport .seoHeading2 {
  margin-left: 20px;
}

#seoHeadingReport .seoHeading3 {
  margin-left: 36px;
}

/* tabs */
/* adapted from css tabs by Joshua Kaufman: http://unraveled.com/projects/css_tabs/ */
/* */
ul#tabnav {
  font-size: 11px;
  list-style-type: none;
  padding: 0 0 24px 24px;
  border-bottom: 1px solid #a0a0a0;
  margin: 0;  
}

ul#tabnav li {
  float: left;
  height: 21px;
  background-color: #4e4e4e;
  margin: 2px 2px 0 2px;
  border: 1px solid #a0a0a0;
}

ul#tabnav li.active {
  border-bottom: 1px solid #fff;
  background-color: #fff;
}
ul#tabnav li a {
  color: #aaaaaa;
}
ul#tabnav li.active a {
  color: black;
}

#tabnav a {
  float: left;
  display: block;
  color: #666;
  text-decoration: none;
  padding: 4px;
}

#tabnav a:hover {
  background: #fff;
  color: Black;
}

#tabcontents {
  padding-top: 8px;
  border: solid 1px #a0a0a0;
  border-top: none;
  margin: 0 0 0 0;
  width: auto;
}

/* toolbutton */
#extender .seoAdminButtons {
  margin-top: 24px;
}

#extender .seoToolButton {
  margin: 0px 0 4px 4px;
  padding: 4px;
  border: 1px solid #CCCCCC;
  width: 19em;
  cursor: pointer;
}

#extender .seoToolButton:hover {
  background-color: #BFE4FF;
  border: 1px solid #6096BF;
}

#extender .seoToolButton img {
  margin-right: 2px;
  float:left;
}

#extender .seoToolButton span {
  font-size: 12px; 
  line-height: 24px;
}

/* log error */
#extender .seoLogError {
  font-weight: bold;
}

#extender .seoLogError img {
  margin-right:5px; 
  vertical-align:middle;
}