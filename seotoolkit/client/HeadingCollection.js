var SeoHeadingCollection = CollectionBase.extend({
  constructor: function() {
    this.base();
    
    SeoHeading.prototype.renderer = new SeoHeadingRenderer();
  },
  
  parse: function() {
    $$('h1', 'h2', 'h3', 'h4', 'h5', 'h6').each(function(element) {
      if (element.up('#extender') == null) {
        this.elements.push(new SeoHeading(element));
      }
    }.bind(this));
    
    console.log('%s headings parsed', this.length());
  },
  
  renderReport: function(container) {
    if (this.elements.length > 0) {
      var result = '';
      this.elements.each(function(element) {
        result += element.renderer.getReportHTML(element);
      });
      
      container.innerHTML = result;
    }
    else {
        container.innerHTML = seo.formatNoElements(localizationDictionary["NO_HEADINGS_ON_PAGE"]);
    }
  }
});
