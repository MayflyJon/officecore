function seoTabs() {
  Event.observe(window, 'load', this.initialize);
  this.tabs = new Array();
}

seoTabs.prototype.initialize = function() {
  $('tabnav').getElementsBySelector('a').each(function(element) {
    Event.observe(element, 'click', tabController.click.bindAsEventListener(tabController));
  });
}

seoTabs.prototype.click = function(evt) {
  this.showTab(Event.element(evt).parentNode);
  Event.stop(evt);
}

seoTabs.prototype.showTab = function(tab) {
  tab = $(tab);

  if (this.tabs.length == 0) {    
    $('tabcontents').immediateDescendants().each(function(element) {
      this.tabs.push(element);
    }.bind(this));
  }
 
  $('tabnav').immediateDescendants().each(function(element) {
    element.removeClassName('active');
  });
  
  this.tabs.each(function(tab) {
    Element.hide(tab);
  });

  tab.addClassName('active');
  Element.show(tab.getAttribute('tab'));  
}

var tabController = new seoTabs();