<%@ Control Language="C#" Inherits="System.Web.UI.UserControl" %>
<%@ Import Namespace="Sitecore.SeoToolkit" %>
<div style="display: none" id="enginesContent">
    <div id="seoSearchEngines">
        <h2>
            <%=Sitecore.SeoToolkit.Localization.ResourcesManager.Localize(Sitecore.SeoToolkit.Localization.ResourcesManager.GetResourceValue("INCOMING_LINKS"))%>
        </h2>
        <p>
            <%=Sitecore.SeoToolkit.Localization.ResourcesManager.Localize(Sitecore.SeoToolkit.Localization.ResourcesManager.GetResourceValue("PAGE_LINKING_TO"))%><b>                
            <%=SeoController.PageUrl %>
            </b>(<%=Sitecore.SeoToolkit.Localization.ResourcesManager.Localize(Sitecore.SeoToolkit.Localization.ResourcesManager.GetResourceValue("OPEN_IN_WINDOW"))%>):</p>
        <ul>
            <li>
               <img src="<%= SeoPath.SeoToolKit %>/img/google-icon.gif" 
                    alt="<%=Sitecore.SeoToolkit.Localization.ResourcesManager.Localize(Sitecore.SeoToolkit.Localization.ResourcesManager.GetResourceValue("INCOMING_LINKS_IN_GOOGLE"))%>" /><a
                    href="http://www.google.com/search?q=link:<%= SeoController.PageUrl %>" target="_blank">
                    <%=Sitecore.SeoToolkit.Localization.ResourcesManager.Localize(Sitecore.SeoToolkit.Localization.ResourcesManager.GetResourceValue("IN_GOOGLE"))%></a></li>
            <li>
               <img src="<%= SeoPath.SeoToolKit %>/img/yahoo-icon.gif" 
                    alt="<%=Sitecore.SeoToolkit.Localization.ResourcesManager.Localize(Sitecore.SeoToolkit.Localization.ResourcesManager.GetResourceValue("INCOMING_LINKS_IN_YAHOO"))%>" /><a
                    href="http://search.yahoo.com/search?p=link:<%= SeoController.PageUrl %>" target="_blank">
                    <%=Sitecore.SeoToolkit.Localization.ResourcesManager.Localize(Sitecore.SeoToolkit.Localization.ResourcesManager.GetResourceValue("IN_YAHOO"))%></a></li>
            <li>
               <img src="<%= SeoPath.SeoToolKit %>/img/live-icon.gif" 
                    alt="<%=Sitecore.SeoToolkit.Localization.ResourcesManager.Localize(Sitecore.SeoToolkit.Localization.ResourcesManager.GetResourceValue("INCOMING_LINKS_IN_WINDOWS_LIVE_SEARCH"))%>" /><a
                    href="http://search.msn.com/results.aspx?q=link:<%= SeoController.PageUrl %>"
                    target="_blank">
                   <%=Sitecore.SeoToolkit.Localization.ResourcesManager.Localize(Sitecore.SeoToolkit.Localization.ResourcesManager.GetResourceValue("IN_MSN"))%></a></li>
        </ul>
    </div>
</div>
