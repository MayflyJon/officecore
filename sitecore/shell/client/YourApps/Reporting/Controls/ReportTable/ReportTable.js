﻿define(["sitecore"], function (Sitecore) {
  var model = Sitecore.Definitions.Models.ControlModel.extend({
    initialize: function (options) {
        this._super();

        this.get("items", null);
        this.get("title", null);

    }
  });

  var view = Sitecore.Definitions.Views.ControlView.extend({
    initialize: function (options) {
        this._super();

        this.model.on("change", this.render, this);
    },
    render: function () {

        var items = this.model.get("items");
        if (!items && !IsGuid(items))
            return;

        var $el = $(this.el);

        //Create Elements
        $(this.el).html("<div class='reportTable'><div class='reportTableLeft'></div><div class='reportTableRight'></div></div>");

    }
  });

  Sitecore.Factories.createComponent("ReportTable", model, view, ".sc-ReportTable");
});
