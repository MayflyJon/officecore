﻿
require.config({
    baseUrl: "/sitecore/shell/client/Speak/Assets",
    paths: {
        async: "lib/core/deps/async/async"
    },
    shim: {
        'async': { deps: ['jquery'] }
    }
});

define(["sitecore", "jquery", "async", "async!http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0!onscriptload"], function (Sitecore) {
    var model = Sitecore.Definitions.Models.ControlModel.extend({
    initialize: function (options) {
        this._super();

        this.set("items", null);
        this.set("retryAttempts", null);
        
    }
  });

  var view = Sitecore.Definitions.Views.ControlView.extend({
    initialize: function (options) {
        this._super();
        
        // Initialize the map
        this.mapReadyTest();

        this.model.on("change", this.mapReadyTest, this);
        
    },
    mapReadyTest: function () {
        if (window.Microsoft && window.Microsoft.Maps && window.Microsoft.Maps.Location) {
            //Instantiate the map
            var elementId = $(this.el);
            this.map = new Microsoft.Maps.Map(document.getElementById(elementId['0'].id), {
                credentials: "AlPXVkGNwPEoywnPfgs8o5za9URuJEUB2oOMBj5z3BGxi9UdnT0MO1I7NnXrsR4K",
                mapTypeId: Microsoft.Maps.MapTypeId.road,
                enableClickableLogo: false,
                enableSearchLogo: false,
                showDashboard: false,
                zoom: 3
            });
            
            this.setMap();
        }
        else {
            setTimeout(mapReadyTest, 100);
        }
    },
    setMap: function () {
        
        var items = this.model.get("items");
        if (_.isNull(items) || _.isUndefined(items))
            return;
        
        $.each(items, $.proxy(function(index, value) {

            //Make url
            var url = makeBingRequestUrl(value.Country, value.Region);

            //Set retries
            var retries = this.model.get("retryAttempts");
            value.Retries = _.isNull(retries) || _.isUndefined(retries) ? 3 : retries;

            //Make the request
            this.makeRequest(url, value);

        }, this));
        
    },
    makeRequest: function (url, requestObject) {
        $.ajax({
            url: url,
            dataType: "jsonp",
            data: {
                key: "AlPXVkGNwPEoywnPfgs8o5za9URuJEUB2oOMBj5z3BGxi9UdnT0MO1I7NnXrsR4K"
            },
            jsonp: "jsonp",
            success: $.proxy(function (data) {
                this.successHandler(data, requestObject);
            }, this)
        });
    },
    successHandler: function (data, requestObject) {

            if (data &&
                   data.resourceSets &&
                   data.resourceSets.length > 0 &&
                   data.resourceSets[0].resources &&
                   data.resourceSets[0].resources.length > 0) {

                //Get coordinates and place pin
                var loc = new Microsoft.Maps.Location(data.resourceSets[0].resources[0].point.coordinates[0], data.resourceSets[0].resources[0].point.coordinates[1]);
                var pin = new Microsoft.Maps.Pushpin(loc, { text: requestObject.Visits });
                
                // Create the infobox for the pushpin
                this.pinInfobox = new Microsoft.Maps.Infobox(pin.getLocation(),
                    {
                        title: requestObject.Country + " - " + requestObject.Region,
                        description: 'Visits: ' + requestObject.Visits,
                        visible: false,
                        offset: new Microsoft.Maps.Point(0, 15)
                    });

                // Add handler for the pushpin click event.
                Microsoft.Maps.Events.addHandler(pin, 'click', this.displayInfobox);

                // Hide the infobox when the map is moved.
                Microsoft.Maps.Events.addHandler(this.map, 'viewchange', this.hideInfobox);


                this.map.entities.push(pin);
                // London
                var lon = new Microsoft.Maps.Location(51.5072, 0.1275);
                this.map.setView({ center: lon });

            }
            else {//The location could not be geocoded
                
                if(requestObject.Retries > 0) {
                    
                    //Decrement retries
                    requestObject.Retries -= 1;
                    
                    //Get url
                    var url = makeBingRequestUrl(requestObject.Country, requestObject.Region);
                    this.makeRequest(url, requestObject);

                }
            }

    },
    displayInfoBox: function () {
        this.pinInfobox.setOptions({ visible: true });
    },
    hideInfoBox: function () {
        this.pinInfobox.setOptions({ visible: true });
    }
  });

  Sitecore.Factories.createComponent("BingMaps", model, view, ".sc-BingMaps");
});


function makeBingRequestUrl(countryCode, regionCode) {
    //Create Bing Maps REST Services request to geocode the address provided by the user
    return "http://dev.virtualearth.net/REST/v1/Locations/" + countryCode + "/" + regionCode;
}
