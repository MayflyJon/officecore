﻿define(["sitecore"], function (Sitecore) {
    var model = Sitecore.Definitions.Models.ControlModel.extend({
        initialize: function (options) {
            this._super();

            this.set("items", []);
            this.set("selectedItems", []);
            this.set("selectedItem", null);
            this.set("selectedItemId", null);

            this.running = false;
            this.set("isBusy", false);

            this.on("change:site", this.waitForChange, this);
        },
        waitForChange: function () {

            if (_.isUndefined(this.running) || this.running === false) {
                this.running = true;
                this.timeOut = $.proxy(setTimeout(_.bind(this.refresh, this), 100), this);
            }

        },
        refresh: function () {
            this.set("isBusy", true);
            var url = "/sitecore/shell/~/analytics/reports/reports.ashx?report=SiteSwitcher";

            //Run AJAX command to get data
            $.ajax({
                url: url,
                method: 'GET',
                context: this,
                success: function (data) {

                    //Set the data
                    $.proxy(this.setData(data), this);

                },
                error: function (xhr, status, error) {
                    console.log("Error encountered: " + error);
                },
                complete: function (xhr, status) {
                    this.set("isBusy", false);
                }
            });
        },
        setData: function (data) {
            if (_.isUndefined(data))
                return;

            var array = data.split("\n");

            var results = new Array();
            var headerArray = new Array();
            var faceted;

            //Iterate results
            $.each(array, function (index, value) {
                if (_.isNull(value) || _.isUndefined(value) || value === "")
                    return false;

                var resultObject = {};

                // We don't care about the header, so skip the first
                if (index > 0) {
                    resultObject["site"] = value;
                    results.push(resultObject);
                }

            });

            this.set("items", results);
            this.running = false;
        }
    });

    var view = Sitecore.Definitions.Views.ControlView.extend({
        events: {
            "change #siteSwitcher": "siteSelected"
        },
        initialize: function (options) {
            this._super();

            this.listenTo(this.model, 'change:items', this.render);

            if (!this.model.get("site"))
                this.model.set("site", "website");
        },
        render: function () {
            $("#siteSwitcher").empty();
            var data = this.model.get("items");
            if (!_.isNull(data) && !_.isUndefined(data)) {
                $.each(data, function (index, value) {
                    $("#siteSwitcher").append("<option value=" + value.site + ">" + value.site + "</option>");
                });
            }
        },
        siteSelected: function () {
            this.model.set("site", $("#siteSwitcher option:selected").text());
        }
    });

    Sitecore.Factories.createComponent("SiteSwitcher", model, view, ".sc-SiteSwitcher");
});
