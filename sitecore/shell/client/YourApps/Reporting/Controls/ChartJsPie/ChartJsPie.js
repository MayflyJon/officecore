﻿require.config({
    baseUrl: "/sitecore/shell/client/Speak/Assets",
    paths: {
        chartjs: "lib/ui/deps/devExtreme/dx.chartjs",
        globalize: "lib/ui/deps/devExtreme/globalize.min"
    },
    shim: {
        'globalize': { exports: "Globalize" },
        'chartjs': { deps: ['jquery', 'underscore', 'globalize'], exports: "DevExpress" }
    }
});

define(["sitecore", "underscore", "chartjs"], function (Sitecore) {
  var model = Sitecore.Definitions.Models.ControlModel.extend({
    initialize: function (options) {
        this._super();
        
        this.set("items", null);
        this.set("facet", null);
        this.set("valueField", null);
        this.set("argumentField", null);
        this.set("enabled", true);
        this.set("title", "");
        this.set("width", null);
        this.set("height", null);

        this.on("change", this.prepareData, this);
    },
    prepareData: function () {

        var enabled = this.get("enabled");
        if (!enabled)
            return;

        var results = this.get("items");
        if (!results)
            return;
        
        var facet = this.get("facet");
        if (facet) {
            results = _.filter(results, function (item) {
                var value = _.values(item);
                return value[0] == facet;
            });
        }
        //Set the chart data
        this.chartData = results;
    }
    
  });

  var view = Sitecore.Definitions.Views.ControlView.extend({
    initialize: function (options) {
        this._super();
        this.listenTo(this.model, 'change', this.render);

    },
      render: function () {
          $(this.el).empty();
          $(this.el).removeClass("pieBlock");
          var enabled = this.model.get("enabled");
          if (!enabled)
              return;

          var facet = this.model.get("facet");
          var argumentField = this.model.get("argumentField");

          var chartId = this.model.attributes.name + "Chart";

          //Get data
          var chartData = this.model.chartData;
          var data = chartData;

          if (!_.isNull(data) && !_.isUndefined(data)) {

              //Create Elements
              $(this.el).append("<div id='" + chartId + "'></div>");
              var chartElement = $("#" + chartId);
              $(this.el).removeClass("pieBlock").addClass("pieBlock");
              
              var width = _.isNull(this.model.get("width")) ? "100" : this.model.get("width");
              //$(this.el).css("width", width + "%");
              
              var valueField = this.model.get("valueField");
              if(_.isNull(valueField) || _.isUndefined(valueField)) {
                  valueField = "Value";
              }

              var title = !_.isNull(facet) && facet !== "" ? facet : !_.isNull(this.model.get("title")) && this.model.get("title") !== "" ? this.model.get("title") : "Pie Chart";

              var chartParams = {
                  dataSource: data,
                  series: {
                      argumentField: argumentField,
                      valueField: valueField,
                      minSegmentSize: 5
                  },
                  title: title,
                  tooltip: {
                      enabled: true,
                      percentPrecision: 2,
                      customizeText: function (value) {
                          return "<b>" + value.percentText + "</b><br/>" +
                              value.value + "<br/>";
                      },
                      font: {
                          size: 18,
                      }
                  },
                  loadingIndicator: {
                      backgroundColor: 'firebrick',
                      font: {
                          weight: 700,
                          size: 16
                      }
                  },
                  legend: {
                      columnCount: 1,
                      horizontalAlignment: 'right',
                      verticalAlignment: 'top'
                  },
                  incidentOccured: $.proxy(function (message) {
                      if (this.model.chartData.length === 0) {
                          chartElement.append('<span>The chart rendering is complete</span>');
                      }
                  }, this)
              };

              var sizeParam = {};
              var width = this.model.get("width");
              var height = this.model.get("height");
              if (!_.isNull(width))
                  sizeParam["width"] = width;
              if (!_.isNull(height))
                  sizeParam["height"] = height;
              
              if (!_.isNull(width) || !_.isNull(height))
                  chartParams["size"] = sizeParam;

              chartElement.dxPieChart(chartParams);
              
              if(data.length === 0) {
                  chartElement.append('<span style=\"float:left; margin-left: 15%;  margin-top:-25%; font-weight: bold;\">No data found in range</span>');
              }
              
          }
      }
  });

  Sitecore.Factories.createComponent("ChartJsPie", model, view, ".sc-ChartJsPie");
});

function get_random_color(alpha) {
    var r = function () { return Math.floor(Math.random() * 256); };
    return "rgba(" + r() + "," + r() + "," + r() + "," + alpha + ")";
}
function roundToTwo(num) {
    return +(Math.round(num + "e+2") + "e-2");
}