﻿require.config({
    baseUrl: "/sitecore/shell/client/Speak/Assets",
    paths: {
        momentjs: "lib/core/deps/momentjs/moment"
    },
    shim: {
        'momentjs': { deps: ['jquery', 'underscore'] }
    }
});

define(["sitecore", "jquery", "underscore", "momentjs"], function (Sitecore) {
    var model = Sitecore.Definitions.Models.ControlModel.extend({
        initialize: function (options) {
            this._super();

            this.set("items", null);
            this.set("reportName", null);
            this.set("startDate", null);
            this.set("endDate", null);
            this.set("filters", null);
            this.set("pageSize", null);
            this.set("sortColumn", null);
            this.set("site", null);
            this.set("isBusy", false);
            this.set("translateGuids", "True");
            this.set("translateUserAgents", "False");

            //Watch for changes on the key attributes
            this.on("change:startDate change:endDate change:sortColumn change:site", this.waitForChange, this);

            // Container to hold our promises
            this.promises = [];
            this.newItems = [];
            this.returnValues = [];
            this.running = false;

        },
        waitForChange: function () {

            if (_.isUndefined(this.running) || this.running === false) {
                this.running = true;
                this.timeOut = $.proxy(setTimeout(_.bind(this.refresh, this), 100), this);
            }

        },
        updateGuids: function (originalItems) {

            var resultsArray = originalItems;
            if (!resultsArray) {
                this.running = false;
                return;
            }

            //Iterate results
            $.each(resultsArray, $.proxy(function (index, value) {

                //Iterate over inner values
                $.each(value, $.proxy(function (innerIndex, innerValue) {

                    var propertyName = innerIndex;

                    this.promises.push(RetrieveItem(innerValue, "master", "itemName", propertyName).done($.proxy(function (guid, fieldName, returnValue) {

                        var finalResults = null;
                        var newResults = originalItems;

                        if (IsGuid(guid) && _.isNull(returnValue))
                            returnValue = "Item Not Found";

                        //Make sure this isn't already added
                        var existingValues = this.returnValues;
                        if ((_.isUndefined(_.find(existingValues, function (itemValue) { return itemValue == returnValue; })) && !_.isNull(returnValue)) || returnValue === "Item Not Found") {

                            this.returnValues.push(returnValue);

                            for (var i = 0; i < newResults.length; i++) {
                                if (newResults[i][fieldName] == guid) {
                                    finalResults = newResults[i];

                                    if (!_.isNull(returnValue)) {
                                        finalResults[fieldName] = returnValue.replace(/\b./g, function (m) { return m.toUpperCase(); });
                                    }

                                    finalResults['ID'] = guid;

                                    //Push into array
                                    this.newItems.push(finalResults);
                                    break;
                                }
                            }

                        }

                    }, this)));

                }, this));

            }, this));

            // Apply entire array of promises to a $.when listener
            $.when.apply($, this.promises).then($.proxy(function () {

                //Sort Array
                var sortColumn = this.get("sortColumn");
                var completedList;
                if (sortColumn) {
                    completedList = _.sortBy(this.newItems, function (o) { return parseInt(o[sortColumn]) * -1; });
                    this.newItems = completedList;
                }

                this.set("items", this.newItems);
                this.set("isBusy", false);
                this.running = false;

            }, this));

        },
        refresh: function () {
            this.set("isBusy", true);
            this.newItems = [];
            this.returnValues = [];

            var reportName = this.get("reportName");
            if (!reportName) {
                this.running = false;
                return;
            }
                

            var siteName = this.get("site");
            if (!siteName)
                siteName = "website";

            var startDate = Date.parse(this.get("startDate"));
            if (!startDate || isNaN(startDate)) {
                this.set("items", null);
                this.running = false;
                return;
            }

            var endDate = Date.parse(this.get("endDate"));
            if (!endDate || isNaN(endDate)) {
                this.set("items", null);
                this.running = false;
                return;
            }

            var filters = this.get("filters");

            //Get the url for the report
            var url = queryBuilder(reportName, getFormattedDate(startDate), getFormattedDate(endDate), siteName, filters);
            var translateUserAgents = this.get("translateUserAgents");
            //Run AJAX command to get data
            $.ajax({
                url: url,
                method: 'GET',
                context: this,
                success: function (data) {
                    
                },
            }).then(function (data) {
                if (translateUserAgents === "True") {
                    if (_.isUndefined(data)) {
                        this.running = false;
                        return;
                    }

                    $.ajax({
                        url: '/sitecore/shell/client/yourapps/reporting/service/uas.svc/checkuas',
                        type: 'GET',
                        context: this,
                        data: { s: data },
                        success: function (data) {
                            //Set the data
                            $.proxy(this.setData(data.d), this);

                        },
                        complete: function (data) {
                        }
                    });
                }
                else {
                    $.proxy(this.setData(data), this);
                }
            });

        },
        setData: function (data) {
            if (_.isUndefined(data)) {
                this.running = false;
                return;
            }

            var array = data.split("\n");

            var results = new Array();
            var headerArray = new Array();
            var intRegex = /^\+?(0|[1-9]\d*)$/;
            var decimalRegex = /^(\d+\.?\d{0,9}|\.\d{0,9})$/;
            var arrayHasGuids = false;
            var translateGuids = this.get("translateGuids");

            //Iterate results
            $.each(array, $.proxy(function (index, value) {
                if (value == "")
                    return false;

                var valueArray = value.split(";");
                var resultObject = {};

                $.each(valueArray, $.proxy(function (innerIndex, innerValue) {

                    if (index == 0) {
                        headerArray[innerIndex] = innerValue.replace(/(\r\n|\n|\r)/gm, "");
                    } else {
                        var takenValue = innerValue;

                        if (headerArray[innerIndex] === "Date") {
                            var dateString = takenValue;
                            if (dateString.length > 10)
                                dateString = dateString.substr(0, 10);
                            var day = moment(dateString, "DD/MM/YYYY");
                            var daytest = day.toDate();
                            takenValue = moment(daytest).format('YYYY-MM-DD');;
                        } else {
                            if (isNumber(takenValue)) {
                                if (takenValue.indexOf(".") > -1 || takenValue.indexOf(",") > -1) {
                                    takenValue = parseFloat(takenValue);
                                }
                                else {
                                    takenValue = parseInt(takenValue);
                                }
                            }
                        }

                        //See if this is a GUID
                        if (IsGuid(takenValue))
                            arrayHasGuids = true;

                        //Set a default
                        takenValue = takenValue === "" ? "unknown" : takenValue;

                        //Create a property on the object based on the header
                        resultObject[headerArray[innerIndex]] = takenValue;
                        //Object.defineProperty(resultObject, headerArray[innerIndex], { value: takenValue });
                    }
                }, this));

                //Push results to array
                results.push(resultObject);
            }, this));

            var finalResults;
            var sortColumn = this.get("sortColumn");
            if (!_.isNull(sortColumn)) {
                finalResults = _.sortBy(results, function (o) { return o[sortColumn] * -1; });
            } else {
                finalResults = results;
            }

            //Get page size
            var pageSize = this.get("pageSize");
            var newFinalResults;
            if ((pageSize === null || !intRegex.test(pageSize)) && pageSize != 0) {
                pageSize = 5;
            }

            //Clean and slice array
            var cleanFinalResults = _.filter(finalResults, function (num) { return !_.isEmpty(num); });
            if (pageSize != 0) {
                newFinalResults = cleanFinalResults.slice(0, pageSize);
            } else {
                newFinalResults = cleanFinalResults;
            }

            if (arrayHasGuids && (translateGuids === "True")) {
                //Set items to array
                this.updateGuids(newFinalResults);
            } else {
                this.set("items", newFinalResults);
                this.set("isBusy", false);
            }
            
            this.running = false;

        }
    });

    var view = Sitecore.Definitions.Views.ControlView.extend({
        initialize: function (options) {
            this._super();

            this.model.set("translateUserAgents", this.$el.data("scTranslateuseragents"));
        }
    });

    Sitecore.Factories.createComponent("ReportListData", model, view, ".sc-ReportListData");
});

//Format a date to be accepted by the handler
function getFormattedDate(date) {
    var dateFormatted = new Date(date);
    var dateStringified = dateFormatted.getFullYear() + "-" + (dateFormatted.getMonth() + 1) + "-" + dateFormatted.getDate();

    return dateStringified;
}

//Build the handler URL from parameters
function queryBuilder(reportName, startDate, endDate, siteName, filters) {

    var sb = new StringBuilder();
    sb.append("/sitecore/shell/~/analytics/reports/reports.ashx?report=" + reportName);

    sb.append("&sites=" + siteName);

    if (startDate != null)
        sb.append('&startDate=' + startDate);

    if (endDate != null)
        sb.append('&endDate=' + endDate);

    if (filters != null)
        sb.append(filters);

    return sb.toString();
}

function isNumber(n) {
    return !isNaN(parseInt(n)) && isFinite(n);
}