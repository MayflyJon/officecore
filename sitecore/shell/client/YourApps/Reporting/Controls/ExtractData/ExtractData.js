﻿define(["sitecore", "underscore"], function (Sitecore) {
  var model = Sitecore.Definitions.Models.ControlModel.extend({
    initialize: function (options) {
        this._super();

        this.set("items", null);
    }
  });

  var view = Sitecore.Definitions.Views.ControlView.extend({
    initialize: function (options) {
        this._super();

        $("#" + this.model.attributes.name).click($.proxy(function() {
            var items = this.model.get("items");
            if (_.isNull(items)) 
                return;
            

            var csvContent = "data:text/csv;charset=utf-8,";

            csvContent += ConvertToCSV(items);
                
            var encodedUri = encodeURI(csvContent);
            var link = document.createElement("a");
            link.setAttribute("href", encodedUri);
            link.setAttribute("download", this.model.attributes.name + ".csv");

            link.click(); // This will download the data file named "my_data.csv".
            
        }, this));

        this.model.on("change:items", $.proxy(function () {
            
            var items = this.model.get("items");
            if (!items || items.length === 0) {
                $("#" + this.model.attributes.name).hide();
                return;
            } else {
                $("#" + this.model.attributes.name).show();
            }

        }, this));
        

    }
  });

  Sitecore.Factories.createComponent("ExtractData", model, view, ".sc-ExtractData");
});

function ConvertToCSV(objArray) {
    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    var str = '';
    

    var headerLine = '';
    for (var headerIndex in array[0]) {
        if (headerLine != '') headerLine += ',';
        headerLine += headerIndex;
    }
    str += headerLine + '\r\n';

    for (var i = 0; i < array.length; i++) {
        var line = '';
        for (var index in array[i]) {
            if (line != '') line += ',';

            line += array[i][index];
        }

        str += line + '\r\n';
    }

    return str;
}
