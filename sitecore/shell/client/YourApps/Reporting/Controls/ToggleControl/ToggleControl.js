﻿define(["sitecore"], function (_sc) {
    var model = _sc.Definitions.Models.ControlModel.extend({
        initialize: function (options) {
            this._super();
        },
    });

    var view = _sc.Definitions.Views.ControlView.extend({
        events: {
            "click #toggleControl": "changed"
        },
        initialize: function (options) {
            this._super();
        },
        changed: function (event) {
            $('#help').toggle("slow");
        }
    });

    _sc.Factories.createComponent("ToggleControl", model, view, ".sc-ToggleControl");
});
