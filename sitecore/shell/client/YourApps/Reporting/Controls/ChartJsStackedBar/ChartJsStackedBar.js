﻿require.config({
    baseUrl: "/sitecore/shell/client/Speak/Assets",
    paths: {
        chartjs: "lib/ui/deps/devExtreme/dx.chartjs",
        globalize: "lib/ui/deps/devExtreme/globalize.min"
    },
    shim: {
        'globalize': { exports: "Globalize" },
        'chartjs': { deps: ['jquery', 'underscore', 'globalize'], exports: "DevExpress" }
    }
});

define(["sitecore", "underscore", "chartjs"], function (Sitecore) {
  var model = Sitecore.Definitions.Models.ControlModel.extend({
    initialize: function (options) {
        this._super();
        this.set("items", null);
        this.set("title", null);
        this.set("argumentField", null);
        this.set("valueFields", null);
        
        this.on("change:items", this.prepareData, this);
    },

      prepareData: function () {

          var results = this.get("items");
          if (!results)
              return;

          //Set the chart data
          this.chartData = results;
      }
  });

  var view = Sitecore.Definitions.Views.ControlView.extend({
    initialize: function (options) {
        this._super();
        
        this.listenTo(this.model, 'change', this.render);
    },
    render: function () {
        //Get data
        var data = this.model.chartData;
        if (!data)
            return;

        var argumentField = this.model.get("argumentField");
        if (!argumentField)
            return;
        
        var valueFieldsRaw = this.model.get("valueFields");
        if (!valueFieldsRaw)
            return;

        var valueFields = $.parseJSON(valueFieldsRaw);
        var series = [];
        $.each(valueFields, function(index, value) {

            series.push({
                valueField: value.value,
                name: value.name
            });

        });

        var title = this.model.get("title");
        var chartId = this.model.attributes.name;
        var toolTipId = this.model.attributes.name + "Tooltip";
        var toolTipValue = toolTipId + "Value";
        var chartType = this.model.get("chartType");

        title = _.isNull(title) || _.isUndefined(title) ? "Stacked Chart" : title;

        if (!_.isNull(data) && !_.isUndefined(data)) {
            $("#" + chartId).dxChart({
                dataSource: data,
                commonAxisSettings: {
                    grid: {
                        opacity: 0.3
                    },
                    label: {
                        overlappingBehavior: { mode: 'rotate', rotationAngle: -90 }
                    }
                },
                commonSeriesSettings: {
                    argumentField: argumentField,
                    type: 'StackedBar'
                },
                series: series,
                tooltip: {
                    enabled: true
                },
                title:
                {
                    text: title
                },
                legend: {
                    visible: true,
                    verticalAlignment: "top",
                    horizontalAlignment: "center",
                    paddingTopBottom: 0,
                    itemTextPosition: 'right'
                },
                rotated: true,
                valueAxis: {
                    title: 'Visits'
                }
            });
        }
    }
  });

  Sitecore.Factories.createComponent("ChartJsStackedBar", model, view, ".sc-ChartJsStackedBar");
});
