﻿require(["jasmineEnv"], function (jasmineEnv) {
  var setupTests = function () {
    "use strict";

    describe("Given a ChartJsStackedBar model", function () {
      var component = new Sitecore.Definitions.Models.ChartJsStackedBar();

      describe("when I create a ChartJsStackedBar model", function () {
        it("it should have an 'items' property that allows injection of items into the chart", function () {
          expect(component.get("items")).toBeDefined();
        });

        it("it should set 'isVisible' to true by default", function () {
          expect(ChartJsStackedBar.get("isVisible")).toBe(true);
        });

        it("it should have a 'toggle' function that either shows or hides the ChartJsStackedBar component depending on the 'isVisible' property", function () {
          expect(component.toggle).toBeDefined();
        });
      });
    });
  };

  runTests(jasmineEnv, setupTests);
});