﻿define(["sitecore", "underscore"], function (Sitecore) {
  var model = Sitecore.Definitions.Models.ControlModel.extend({
    initialize: function (options) {
        this._super();
        
        this.set("items", null);
        this.set("itemsSource1", null);
        this.set("itemsSource2", null);
        this.set("IncludeSecondSource", null);
        this.set("selectedFilterItems", null);
        //Watch for changes on the key attributes
        this.on("change:itemsSource1 change:itemsSource2", this.waitForChange, this);
    },
    waitForChange: function () {

        if (_.isUndefined(this.running) || this.running === false) {
            this.running = true;
            this.timeOut = $.proxy(setTimeout($.proxy(this.refresh, this), 0), this);
        }

    },
    refresh: function () {

        //Get arrays
        var itemArray = this.get("itemsSource1");
        var secondSource = this.get("itemsSource2");
        var includeSecondSource = this.get("IncludeSecondSource");
        var selectedFilterItems = this.get("selectedFilterItems");

        //Check arrays
        if (!itemArray)
            return;
        
        if (!selectedFilterItems)
            return;

        //Check arrays
        if (includeSecondSource && !_.isNull(secondSource) && !_.isUndefined(secondSource)) {
        
            $.each(itemArray, function(index, value) {
                if (!_.isUndefined(secondSource[index]) && !_.isNull(secondSource[index])) {

                    var trafficTypeNameArray = _.filter(selectedFilterItems, function(item) {
                        return item.value == secondSource[index].trafficType;
                    });
                    var trafficTypeName = trafficTypeNameArray[0].name;
                    
                    if (!_.isNull(secondSource[index])) {
                        value[trafficTypeName + "%ValueSecondary"] = secondSource[index]["%Value"];
                        value[trafficTypeName + "%VisitsSecondary"] = secondSource[index]["%Visits"];
                    }

                    $.each(itemArray[index].results, function(innerIndex, innerValue) {

                        if ((!_.isNull(secondSource[index]) && !_.isNull(secondSource[index].results[innerIndex])) && (!_.isUndefined(secondSource[index]) && !_.isUndefined(secondSource[index].results[innerIndex]))) {
                            innerValue[trafficTypeName + "ValueSecondary"] = secondSource[index].results[innerIndex].Value;
                            innerValue[trafficTypeName + "VisitsSecondary"] = secondSource[index].results[innerIndex].Visits;
                        }

                    });
                }
            });
            
        }

        //Add to items
        var newItems = $.extend({}, itemArray);
        newItems.timeStamp = new Date();
        this.set("items", newItems);
        this.running = false;
    }
  });

  var view = Sitecore.Definitions.Views.ControlView.extend({
    initialize: function (options) {
      this._super();
    }
  });

  Sitecore.Factories.createComponent("ReportDataAggregator", model, view, ".sc-ReportDataAggregator");
});
