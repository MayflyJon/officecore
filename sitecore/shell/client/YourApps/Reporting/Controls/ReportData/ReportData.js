﻿define(["sitecore", "jquery", "underscore"], function (Sitecore) {
    var model = Sitecore.Definitions.Models.ControlModel.extend({
        initialize: function (options) {
            this._super();

            this.set("items", null);
            this.set("itemsTotal", null);
            this.set("reportName", null);
            this.set("startDate", null);
            this.set("endDate", null);
            this.set("facet", [{ name: 'All', value: totalTrafficType }]);
            this.set("Enabled", true);
            this.set("isBusy", null);
            this.set("datedReport", false);
            this.set("nonParsedItems", null);
            this.set("site", null);
            this.running = false;
            
            //Watch for changes on the key attributes
            this.on("change:reportName change:startDate change:endDate change:facet change:site", this.waitForChange, this);

        },
        waitForChange: function () {

            if (_.isUndefined(this.running) || this.running === false) {
                this.running = true;
                this.timeOut = $.proxy(setTimeout(_.bind(this.refresh, this), 100), this);
                this.endTimeOut = $.proxy(setTimeout(_.bind(this.removeTimeOut, this), 200), this);
            }

        },
        removeTimeOut: function () {
            this.running = false;
        },
        refresh: function () {

            //Set busy indicator
            this.set("isBusy", true);
            var enabled = this.get("Enabled");
            if (!enabled)
                return;

            var reportName = this.get("reportName");
            if (!reportName)
                return;

            if (_.isUndefined(this.get("startDate")) || this.get("startDate") === null) {
                this.set("items", null);
                return;
            }
                
            if (_.isUndefined(this.get("endDate")) || this.get("endDate") === null) {
                this.set("items", null);
                return;
            }

            var startDate = Date.parse(this.get("startDate").replace(/-/g, "/"));
            if (!startDate || isNaN(startDate)) {
                this.set("items", null);
                return;
            }

            var endDate = Date.parse(this.get("endDate").replace(/-/g, "/"));
            if (!endDate || isNaN(endDate)) {
                this.set("items", null);
                return;
            }

            var siteName = this.get("site");
            if (!siteName)
                siteName = "website";

            //Get the url for the report
            var url = queryBuilder(reportName, getFormattedDate(startDate), getFormattedDate(endDate), siteName);

            //Run AJAX command to get data
            $.ajax({
                url: url,
                method: 'GET',
                context: this,
                success: function (data) {

                    //Set the data    
                    $.proxy(this.setData(data), this);

                },
                error: function (data) {
                    this.set("items", null);
                }
            });

        },
        setData: function (data) {

            if (_.isUndefined(data))
                return;

            var array = data.split("\n");

            var results = new Array();
            var headerArray = new Array();
            var faceted;
            var intRegex = /^\d+$/;

            //Iterate results
            $.each(array, function (index, value) {
                if (_.isNull(value) || _.isUndefined(value) || value === "")
                    return false;

                var valueArray = value.split(";");

                var resultObject = {};

                $.each(valueArray, function (innerIndex, innerValue) {
                    if (index == 0) {
                        headerArray[innerIndex] = innerValue.replace(/(\r\n|\n|\r)/gm, "");
                    } else {
                        var takenValue = innerValue;

                        if (intRegex.test(takenValue)) {
                            takenValue = parseInt(takenValue);
                        }
                        //Create a property on the object based on the header
                        var header = headerArray[innerIndex];
                        resultObject[header] = takenValue;
                    }
                });
                results.push(resultObject);
            });

            this.set("nonParsedItems", results);

            //Iterate and total values and visits
            var totalArray = _.groupBy(results, "Date");
            var totalItems = [];
            $.each(totalArray, function (index, value) {

                var itemObject = {
                    Date: totalArray[index][0].Date
                };

                var obj = value[0];
                for (var property in obj) {

                    if (property != "Date" && property != "FacetId") {

                        var iterationResult = _.reduce(_.pluck(value, property), function (memo, num) { return parseInt(memo) + parseInt(num); }, 0);
                        var parsedResult = isInt(iterationResult) ? parseInt(iterationResult) : parseFloat(iterationResult);

                        itemObject[property] = parsedResult;

                    }
                }

                totalItems.push(itemObject);
            });

            //Get faceted list
            var finalList = results;
            var finalCleanList = [];

            var facetsArray = this.get("facet");
            var facets = _.pluck(facetsArray, 'value');
            if (facets.length > 0) {
                $.each(facets, function (index, value) {

                    var finalObject;
                    if (value === totalTrafficType) {
                        finalObject = addFacetList(totalItems, totalItems, value);
                    } else {
                        var facetList = _.filter(finalList, function (item) {
                            return parseInt(item.FacetId) == value;
                        });
                        finalObject = addFacetList(facetList, totalItems, value);
                    }

                    finalCleanList.push(finalObject);

                });
            } else {
                var totalsObject = addFacetList(totalItems, totalItems, totalTrafficType);
                finalCleanList.push(totalsObject);
            }
            
            this.set("itemsTotal", addFacetList(totalItems, totalItems, totalTrafficType));

            //See if list is faceted, but no facet selected
            this.set("items", finalCleanList);

            //Set busy indicator
            this.running = false;
            this.set("isBusy", false);
        }
    });

    var view = Sitecore.Definitions.Views.ControlView.extend({
        initialize: function (options) {
            this._super();
        }
    });

    Sitecore.Factories.createComponent("ReportData", model, view, ".sc-ReportData");
});

function addFacetList(facetList, totalItems, trafficType) {

    var facetResultList = [];

    $.each(facetList, function (index, value) {

        var itemObject = {
            Date: value.Date,
            Value: value.Value,
            Visits: value.Visits,
            Visitors: value.Visitors
        };

        facetResultList.push(itemObject);
    });

    var finalObject = {
        trafficType: trafficType,
        results: facetResultList
    };

    for (var property in totalItems[0]) {

        if (property != "Date" && property != "FacetId") {

            var totalPluck = _.filter(_.pluck(totalItems, property), function (item) {
                return !_.isUndefined(item);
            });
            var filterPluck = _.filter(_.pluck(finalObject.results, property), function (item) {
                return !_.isUndefined(item);
            });
            var totalResult = _.reduce(totalPluck, function (memo, num) { return parseInt(memo) + parseInt(num); }, 0);
            var filterResult = _.reduce(filterPluck, function (memo, num) { return parseInt(memo) + parseInt(num); }, 0);
            var totalParsedResult = isInt(totalResult) ? parseInt(totalResult) : parseFloat(totalResult);
            var filterParsedResult = isInt(filterResult) ? parseInt(filterResult) : parseFloat(filterResult);

            finalObject["%" + property] = filterParsedResult > 0 ? parseFloat((filterParsedResult / totalParsedResult) * 100).toFixed(2) : 0;

        }
    }

    return finalObject;

}

//Format a date to be accepted by the handler
function getFormattedDate(date) {
    var dateFormatted = new Date(date);
    var dateStringified = dateFormatted.getFullYear() + "-" + (dateFormatted.getMonth() + 1) + "-" + dateFormatted.getDate();

    return dateStringified;
}

function splitDate(date) {
    if (_.isUndefined(date))
        return date;

    var dateString = date.toString();

    var year = dateString.substring(0, 4);
    var month = dateString.substring(4, 6);
    var day = dateString.substring(6, 8);

    return day + "-" + month + "-" + year;
}

//Build the handler URL from parameters
function queryBuilder(reportName, startDate, endDate, siteName) {

    var sb = new StringBuilder();
    sb.append("/sitecore/shell/~/analytics/reports/reports.ashx?report=" + reportName);

    sb.append("&sites=" + siteName);

    if (startDate != null)
        sb.append('&startDate=' + startDate);

    if (endDate != null)
        sb.append('&endDate=' + endDate);

    return sb.toString();
}

function isInt(n) { return n % 1 === 0; }
