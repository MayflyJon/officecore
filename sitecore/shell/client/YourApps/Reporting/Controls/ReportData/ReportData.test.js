﻿require(["jasmineEnv"], function (jasmineEnv) {
  var setupTests = function () {
    "use strict";

    describe("Given a ReportData model", function () {
      var component = new Sitecore.Definitions.Models.ReportData();

      describe("when I create a ReportData model", function () {
        it("it should have a 'isVisible' property that determines if the ReportData component is visible or not", function () {
          expect(component.get("isVisible")).toBeDefined();
        });

        it("it should set 'isVisible' to true by default", function () {
          expect(ReportData.get("isVisible")).toBe(true);
        });

        it("it should have a 'toggle' function that either shows or hides the ReportData component depending on the 'isVisible' property", function () {
          expect(component.toggle).toBeDefined();
        });
      });
    });
  };

  runTests(jasmineEnv, setupTests);
});