﻿require.config({
    baseUrl: "/sitecore/shell/client/Speak/Assets",
    paths: {
        momentjs: "lib/core/deps/momentjs/moment",
        daterangepicker: 'lib/ui/deps/daterangepicker/daterangesource'
    },
    shim: {
        'momentjs': { deps: ['jquery', 'underscore'] },
        'daterangepicker': { deps: ['jquery', 'jqueryui'] }
    }
});

define(["sitecore", "jquery", "underscore", "daterangepicker", "momentjs"], function (Sitecore) {
  var model = Sitecore.Definitions.Models.ControlModel.extend({
    initialize: function (options) {
        this._super();

        var localFromDate = localStorage.SitecoreReportStartDate;
        var localToDate = localStorage.SitecoreReportToDate;

        var warnMessage = "<div class='alert alert-warning alert-dismissible'><button type='button' class='close' onclick='hide();'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button><strong>Whoops!</strong> When selecting over 1 month of data it might take a while to show the reports!</div>";
        //var localFromDate2 = _.isUndefined(localStorage.SitecoreReportStartDate2) || _.isNull(localStorage.SitecoreReportStartDate2) ? null : Date.parse(localStorage.SitecoreReportStartDate2);
        //var localToDate2 = _.isUndefined(localStorage.SitecoreReportToDate2) || _.isNull(localStorage.SitecoreReportToDate2) ? null :  Date.parse(localStorage.SitecoreReportToDate2);
        //var dateRangeComparison = localStorage.SitecoreReportRangeComparison === "true" ? true : false;
        var fromDate;
        var toDate;
        //var fromDate2 = !_.isNaN(localFromDate2) ? new Date(localFromDate2) : null;
        //var toDate2 = !_.isNaN(localToDate2) ? new Date(localToDate2) : null;
        
        if (_.isUndefined(localFromDate) || _.isUndefined(localToDate)) {
            var date = new Date();
            toDate = new Date();
            date.setMonth(date.getMonth() - 1);
            fromDate = date;
        } else {
            var parsedFrom = localFromDate.split(/[ ,]+/);
            var parsedTo = localToDate.split(/[ ,]+/);
            fromDate = new Date(parsedFrom[1] + ' ' + parsedFrom[0] + ', ' + parsedFrom[2]);
            toDate = new Date(parsedTo[1] + ' ' + parsedTo[0] + ', ' + parsedTo[2]);;

            var total = (toDate.getFullYear() - fromDate.getFullYear()) * 12 + (toDate.getMonth() - fromDate.getMonth())

            //if (total > 0) {
            //    $(".sc-applicationContent-main").append(warnMessage);

            //    $(".alert").show("slide", { direction: "up" });
            //    setTimeout(function () {
            //        $(".alert").hide("slide", { direction: "up" })
            //    }, 7500);
            //}
            //else {
            //    $(".alert").hide("slide", { direction: "up" });
            //}
        }
        
        this.set("fromDate", getFormattedDate(fromDate));
        this.set("toDate", getFormattedDate(toDate));
        
        this.$current_target = null;
        this.$dropdown = null;
        this.$datepicker = null;
        this.$parameters = null;
        this.$daterangePreset= null;
        this.$parameter1 = null;
        this.$aggregation = null;
        this.$aggregationWrap = null;
        this.$enableComparison = null;

        onPreset = null;
        //if (_.isNull(fromDate2) || _.isNull(toDate2)) {
            this.default_options = {

                aggregations: ['-', 'daily', 'weekly', 'monthly', 'yearly'],
                values: {
                    dr1from: fromDate.getDate() + ' ' + fromDate.getMonthName(true) + ' ' + fromDate.getFullYear(),
                    dr1to: toDate.getDate()  + ' ' + toDate.getMonthName(true) + ' ' + toDate.getFullYear(),
                    aggregation: 'daily',
                    comparisonEnabled: false,
                    daterangePreset: "custom",
                    comparisonPreset: "custom"
                }

            };
        //} else {
        //    this.default_options = {

        //        aggregations: ['-', 'daily', 'weekly', 'monthly', 'yearly'],
        //        values: {
        //            dr1from: fromDate.getDate() - 1 + ' ' + fromDate.getMonthName(true) + ' ' + fromDate.getFullYear(),
        //            dr1to: toDate.getDate() - 1 + ' ' + toDate.getMonthName(true) + ' ' + toDate.getFullYear(),
        //            dr2from: fromDate2.getDate() - 1 + ' ' + fromDate2.getMonthName(true) + ' ' + fromDate2.getFullYear(),
        //            dr2to: toDate2.getDate() - 1 + ' ' + toDate2.getMonthName(true) + ' ' + toDate2.getFullYear(),
        //            aggregation: 'daily',
        //            comparisonEnabled: dateRangeComparison,
        //            daterangePreset: "custom",
        //            comparisonPreset: "custom"
        //        }

        //    };
        //}

        this.default_aggregation = 'daily';
        this.db = {

            aggregations: {
                '-': {
                    title: "Inherit",
                    presets: []
                },
                'hourly': {
                    title: "Hourly",
                    presets: ['custom', 'lastdays']
                },
                'daily': {
                    title: "Daily",
                    presets: ['custom', 'lastdays'] // 'yesterday', 'today']
                },
                'weekly': {
                    title: "Weekly",
                    presets: ['custom', 'lastweeks']
                },
                'monthly': {
                    title: "Monthly",
                    presets: ['custom', 'lastmonths']
                },
                'quarterly': {
                    title: "Quarterly",
                    presets: ['custom', 'lastquarters']
                },
                'yearly': {
                    title: "Yearly",
                    presets: ['custom', 'lastyears']
                },
                'whole': {
                    title: "Whole period",
                    presets: ['custom', 'lastdays', 'lastweeks', 'lastmonths', 'lastquarters', 'lastyears']
                }
            },

            date_presets: {
                'custom': {
                    title: "Custom",
                    dates: function () { return null; }
                },
                //'today': {
                //    title: "Today",
                //    dates: function () {
                //        var dates = [];
                //        dates[0] = ((new Date()).setHours(0, 0, 0, 0)).valueOf();
                //        dates[1] = new Date(dates[0]).setHours(23, 59, 59, 0).valueOf();
                //        return dates;
                //    }
                //},
                //'yesterday': {
                //    title: "Yesterday",
                //    dates: function () {
                //        var dates = [];
                //        dates[0] = ((new Date()).setHours(0, 0, 0, 0)).valueOf() - 24 * 3600 * 1000;
                //        dates[1] = new Date(dates[0]).setHours(23, 59, 59, 0).valueOf();
                //        return dates;
                //    }
                //},
                'lastdays': {
                    title: "Last Day(s)",
                    parameters: true,
                    defaults: {
                        parameter1: 7
                    },
                    dates: $.proxy(function () {
                        var days = this.getParameter1();
                        var dates = [];

                        var today = new Date();
                        dates[0] = new Date(today).setDate(today.getDate() - days).valueOf();
                        dates[1] = new Date(today);
                        dates[1].setDate(today.getDate() - 1);
                        dates[1].setHours(23, 59, 59, 0).valueOf();

                        return dates;
                    },this)
                },
                'lastweeks': {
                    title: "Last Week(s)",
                    parameters: true,
                    defaults: {
                        parameter1: 2
                    },
                    dates: $.proxy(function () {
                        var dates = [];
                        var weeks = this.getParameter1();

                        var monday = this.getMonday(new Date());
                        monday.setDate(monday.getDate() - (7 * weeks));
                        dates[0] = monday.valueOf();
                        var sunday = new Date(monday);
                        sunday.setDate(sunday.getDate() + 6 + (7 * (weeks - 1)));
                        sunday.setHours(23, 59, 59, 0);
                        dates[1] = sunday.valueOf();

                        return dates;
                    },this)
                },
                'lastmonths': {
                    title: "Last Month(s)",
                    parameters: true,
                    defaults: {
                        parameter1: 3
                    },
                    dates: $.proxy(function () {
                        var months = this.getParameter1();
                        var dates = [];

                        var lastOfMonth = new Date().setDate(0);
                        var firstOfMonth = new Date(lastOfMonth);
                        firstOfMonth.setDate(1);
                        firstOfMonth.setMonth(firstOfMonth.getMonth() - months + 1);
                        dates[0] = firstOfMonth.valueOf();
                        dates[1] = lastOfMonth.valueOf();

                        return dates;
                    },this)
                },
                'lastquarters': {
                    title: "Last Quarters(s)",
                    parameters: true,
                    defaults: {
                        parameter1: 2
                    },
                    dates: $.proxy(function () {
                        // TODO: fix -- works as months now
                        var months = this.getParameter1() * 3;
                        var dates = [];

                        var lastOfMonth = new Date().setDate(0);
                        var firstOfMonth = new Date(lastOfMonth);
                        firstOfMonth.setDate(1);
                        firstOfMonth.setMonth(firstOfMonth.getMonth() - months + 1);
                        dates[0] = firstOfMonth.valueOf();
                        dates[1] = lastOfMonth.valueOf();

                        return dates;
                    },this)
                },
                'lastyears': {
                    title: "Last Year(s)",
                    parameters: true,
                    defaults: {
                        parameter1: 1
                    },
                    dates: $.proxy(function () {
                        var years = this.getParameter1();
                        var dates = [];

                        var lastOfYear = new Date();
                        lastOfYear.setDate(0);
                        lastOfYear.setMonth(-1);

                        var firstOfYear = new Date(lastOfYear);
                        firstOfYear.setDate(1);
                        firstOfYear.setMonth(-12 * (years - 1));
                        dates[0] = firstOfYear.valueOf();
                        dates[1] = lastOfYear.valueOf();

                        return dates;
                    },this)
                }
            }

        };
        
        //var to = new Date();
        //var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 14);

        this.set("compareRange", false);
        
        var $this = $("#DateRangesWidget");
        var data = $this.data('DateRangesWidget');
        $this.data('test', this);

        // initialize data in dom element
        if (!data) {
            var effective_options = $.extend({}, this.default_options, options);
            $this.data('DateRangesWidget', {
                options: effective_options
            });

        }
        this.createElements($this);
        this.updateDateField($this);

    },
    refreshForm: function () {
        var lastSel = this.$datepicker.DatePickerGetLastSel();

        if ($('.comparison-preset', this.$dropdown).val() != 'custom') {
            lastSel = lastSel % 2;
            this.$datepicker.DatePickerSetLastSel(lastSel);
        }
        $('.dr', this.$dropdown).removeClass('active');
        $('.dr[lastSel=' + lastSel + ']', this.$dropdown).addClass('active');

        var dates = this.$datepicker.DatePickerGetDate()[0];

        var newFrom = dates[0].getDate() + ' ' + dates[0].getMonthName(true) + ' ' + dates[0].getFullYear();
        var newTo = dates[1].getDate() + ' ' + dates[1].getMonthName(true) + ' ' + dates[1].getFullYear();

        var oldFrom = $('.dr1.from', this.$dropdown).val();
        var oldTo = $('.dr1.to', this.$dropdown).val();

        if (newFrom != oldFrom || newTo != oldTo) {
            $('.dr1.from', this.$dropdown).val(newFrom);
            $('.dr1.to', this.$dropdown).val(newTo);
        }

        $('.dr1.from_millis', this.$dropdown).val(dates[0].getTime());
        $('.dr1.to_millis', this.$dropdown).val(dates[1].getTime());

        if (dates[2]) {
            $('.dr2.from', this.$dropdown).val(dates[2].getDate() + ' ' + dates[2].getMonthName(true) + ' ' + dates[2].getFullYear());
        }
        if (dates[3]) {
            $('.dr2.to', this.$dropdown).val(dates[3].getDate() + ' ' + dates[3].getMonthName(true) + ' ' + dates[3].getFullYear());
        }
    },
    createElements: function ($target) {

        // modify div to act like a dropdown
        $target.html(
            '<span class="btn aggregation"></span>' +
            '<div class="date-range-field">' +
                '<span class="main"></span>' +
                //'<span class="comparison-divider"> Cmp to: </span>'+
                '<span class="comparison"></span>' +
                '<a href="#">&#9660;</a>' +
            '</div>'
        );

        // only one dropdown exists even though multiple widgets may be on the page
        if (!this.$dropdown) {
            this.$dropdown = $(
            '<div id="datepicker-dropdown">' +
                '<div class="date-ranges-picker"></div>' +
                '<div class="date-ranges-form">' +
                    '<div class="aggregation-wrap">' +
                        'Aggregation:' +
                        '<select class="aggregation">' +
                        '</select>' +
                    '</div>' +
                    '<div class="main-daterange">' +
                        '<div>' +
                            'Date Range:' +
                            '<select class="daterange-preset">' +
                            '</select>' +
                            '<span class="parameters">' +
                                '<input type="number" class="daterange-preset-parameter1" />' +
                            '</span>' +
                        '</div>' +
                        '<input type="text" class="dr dr1 from" lastSel="0" /> - <input type="text" class="dr dr1 to" lastSel="1" />' +
                        '<input type="hidden" class="dr dr1 from_millis" lastSel="2" /><input type="hidden" class="dr dr1 to_millis" lastSel="3" />' +
                    '</div>' +
                    '<div class="comparison">' +
                        '<input type="checkbox" class="enable-comparison" /> Compare to: ' +
                        '<select class="comparison-preset compareselect">' +
                            '<option value="custom">Custom</option>' +
                            '<option value="previousperiod" selected="selected">Previous period</option>' +
                            '<option value="previousyear">Previous year</option>' +
                        '</select>' +
                    '</div>' +
                    '<div class="comparison-daterange">' +
                        '<input type="text" class="dr dr2 from" lastSel="2" /> - <input type="text" class="dr dr2 to" lastSel="3" />' +
                        '<input type="hidden" class="dr dr2 from_millis" lastSel="2" /><input type="hidden" class="dr dr2 to_millis" lastSel="3" />' +
                '</div>' +
                    '<div class="btn-group">' +
                    '<button class="btn btn-mini" id="button-ok">Apply</button>' +
                    '<button class="btn btn-mini" id="button-cancel">Cancel</button>' +
                    '</div>' +
                '</div>' +
            '</div>');
            this.$dropdown.appendTo($('body'));
            this.$aggregation = $('.aggregation', this.$dropdown);
            this.$aggregationWrap = $('.aggregation-wrap', this.$dropdown);

            this.$datepicker = $('.date-ranges-picker', this.$dropdown);

            this.$daterangePreset = $('.daterange-preset', this.$dropdown);
            this.$parameters = $('.parameters', this.$dropdown);
            this.$parameter1 = $('.daterange-preset-parameter1', this.$dropdown);

            this.$enableComparison = $('.enable-comparison', this.$dropdown);
            this.$comparisonPreset = $('.comparison-preset', this.$dropdown);

            // TODO: inherit options from DRW options
            
            this.$datepicker.DatePicker({
                mode: 'range',
                starts: 1,
                calendars: 3,
                inline: true,
                onChange: $.proxy(function (dates, el, options) {
                    // user clicked on datepicker
                    this.setDaterangePreset('custom');
                }, this)
            });

            /**
             * Handle change of aggregation.
             */
            this.$aggregation.change($.proxy(function () {
                this.populateDateRangePresets();
            }, this));


            /**
             * Handle change of datePreset
             */
            this.$daterangePreset.change($.proxy(function () {
                var date_preset = this.getDaterangePreset();
                if (date_preset.parameters) {
                    if (!$.isNumeric(this.getParameter1())) {
                        this.setParameter1(date_preset.defaults.parameter1);
                    }
                    this.$parameters.show();
                } else {
                    this.$parameters.hide();
                }
                $('.dr1', this.$dropdown).prop('disabled', (this.$daterangePreset.val() == 'custom' ? false : true));

                this.recalculateDaterange();
            }, this));

            this.$parameter1.change($.proxy(function () {
                var p1 = this.getParameter1();
                if (!$.isNumeric(p1) || p1 < 1)
                    this.setParameter1(1);
                this.recalculateDaterange();
            }, this));

            /**
             * Handle enable/disable comparison.
             */
            this.$enableComparison.change($.proxy(function () {
                this.setComparisonEnabled($(this.$enableComparison).is(':checked'));
                //this.set("CompareRange", $(this.$enableComparison).is(':checked'));
            }, this));

            /**
             * Handle change of comparison preset.
             */
            this.$comparisonPreset.change($.proxy(function () {
                this.recalculateComparison();
            }, this));

            /**
             * Handle clicking on date field.
             */
            $('.dr', this.$dropdown).click($.proxy(function () {
                // set active date field for datepicker
                this.$datepicker.DatePickerSetLastSel($(this.$dropdown).attr('lastSel'));
                //internal.refreshForm(); // don't refresh
            }, this));

            /**
             * Handle clicking on OK button.
             */
            $('#button-ok', this.$dropdown).click($.proxy(function () {
                this.retractDropdown(this.$current_target);
                this.saveValues(this.$current_target);
                this.updateDateField(this.$current_target);
                this.set("compareRange", $(this.$enableComparison).is(':checked'));
                this.recalculateComparison();

                var dates = this.$datepicker.DatePickerGetDate()[0];
                var newFrom = dates[0];//.getDate().getMonth();
                var newTo = dates[1];//.getDate().getMonth();

                //return Math.floor((utc2 - utc1) / _MS_PER_DAY);
                var total = (newTo.getFullYear() - newFrom.getFullYear()) * 12 + (newTo.getMonth() - newFrom.getMonth())

                //if (total > 0) {
                //    $(".sc-applicationContent-main").append(this.warnMessage);
                    
                //    $(".alert").show("slide", { direction: "up" });
                //    setTimeout(function () {
                //        $(".alert").hide("slide", { direction: "up" })
                //    }, 7500);
                //}
                //else {
                //    $(".alert").hide("slide", { direction: "up" });
                //}
                return false;
            }, this));

            /**
             * Handle clicking on OK button.
             */
            $('#button-cancel', this.$dropdown).click($.proxy(function () {
                var $this = $(this.$dropdown);
                this.retractDropdown(this.$current_target);
                return false;
            }, this));

        }

        /**
         * Handle expand/retract of dropdown.
         */
        $target.bind('click', $.proxy(function () {
            //var $this = $(this);
            if ($target.hasClass('DRWClosed')) {
                this.expandDropdown($target);
            } else {
                this.retractDropdown($target);
            }
            return false;
        }, this));

        $target.addClass('DRWInitialized');
        $target.addClass('DRWClosed');
    },
    recalculateDaterange: function () {

        var date_preset = this.getDaterangePreset();
        var dates = this.$datepicker.DatePickerGetDate()[0];

        // TODO: remove
        if (date_preset != undefined) {
            var d = date_preset.dates();
            if (d != null) {
                dates[0] = d[0];
                dates[1] = d[1];
            }
            this.$datepicker.DatePickerSetDate(dates);
        }
        this.recalculateComparison();
        /*
        $('.main-daterange input.dr', $dropdown).prop('disabled', ($this.val() == 'custom' ? false : true));

        $('.comparison-preset', $dropdown).change();
        internal.refreshForm(); // should do only one refresh call
        */
    },
    recalculateComparison: function () {
        var dates = this.$datepicker.DatePickerGetDate()[0];
        var comparison = $(this.$enableComparison).is(':checked');
        var comparisonPreset = this.getComparisonPreset();
        
        if(dates.length < 2 &&  comparison) {
            dates[0] = moment(this.get("fromDate")).toDate();
            dates[1] = moment(this.get("toDate")).toDate();
        }

        if (dates.length >= 2) {
            
            switch (comparisonPreset) {
                case 'previousperiod':
                    var days = parseInt((dates[1] - dates[0]) / (24 * 3600 * 1000));
                    dates[2] = new Date(dates[0]).setDate(dates[0].getDate() - (days + 1));
                    dates[3] = new Date(dates[1]).setDate(dates[1].getDate() - (days + 1));
                    break;
                case 'previousyear':
                    dates[2] = new Date(dates[0]).setFullYear(dates[0].getFullYear(dates[0]) - 1);
                    dates[3] = new Date(dates[1]).setFullYear(dates[1].getFullYear(dates[1]) - 1);
                    break;
            }
            this.$datepicker.DatePickerSetDate(dates);
            $('.comparison-daterange input.dr', this.$dropdown).prop('disabled', (comparisonPreset == 'custom' ? false : true));
            this.refreshForm();
        }
    },
    populateAggregations: function (aggregations) {
        var $select = $('select.aggregation', this.$dropdown);

        $select.html('');
        $.each(aggregations, $.proxy(function (i, aggregation) {
            $select.append($("<option/>", {
                value: aggregation,
                text: this.db.aggregations[aggregation].title
            }));
        }, this));
        this.populateDateRangePresets();
    },
      /**
   * Loads values from target element's data to controls.
   */
    loadValues: function ($target) {

        var values = $target.data('DateRangesWidget').options.values;

        // handle initial values
        $('.dr1.from', this.$dropdown).val(values.dr1from);
        $('.dr1.from', this.$dropdown).change();
        $('.dr1.to', this.$dropdown).val(values.dr1to);
        $('.dr1.to', this.$dropdown).change();
        $('.dr2.from', this.$dropdown).val(values.dr2from);
        $('.dr2.from', this.$dropdown).change();
        $('.dr2.to', this.$dropdown).val(values.dr2to);
        $('.dr2.to', this.$dropdown).change();

        this.$aggregation.val(values.aggregation);
        this.$aggregation.change();

        if (values.daterangePreset) {
            this.$daterangePreset.val(values.daterangePreset);
            this.$daterangePreset.change();
        }

        this.$parameter1.val(values.parameter1);
        this.$parameter1.change();

        this.$enableComparison.prop('checked', values.comparisonEnabled);
        this.$enableComparison.change();

        this.$comparisonPreset.val(values.comparisonPreset);
        this.$comparisonPreset.change();
    },
      /**
   * Stores values from controls to target element's data.
   */
    saveValues: function ($target) {
        var data = $target.data('DateRangesWidget');
        var values = data.options.values;
        values.aggregation = this.getAggregation();
        values.daterangePreset = this.getDaterangePresetVal();
        values.parameter1 = this.getParameter1();
        values.dr1from = $('.dr1.from', this.$dropdown).val();
        values.dr1to = $('.dr1.to', this.$dropdown).val();
        values.dr1from_millis = $('.dr1.from_millis', this.$dropdown).val();
        values.dr1to_millis = $('.dr1.to_millis', this.$dropdown).val();

        values.comparisonEnabled = this.getComparisonEnabled();
        values.comparisonPreset = this.getComparisonPreset();
        if (values.comparisonEnabled) {
            values.dr2from = $('.dr2.from', this.$dropdown).val();
            values.dr2to = $('.dr2.to', this.$dropdown).val();
        } else {
            values.dr2from = moment(new Date()).format("DD MMMM YYYY");
            values.dr2to = moment(new Date()).format("DD MMMM YYYY");
        }

        values.dr2from_millis = $('.dr2.from_millis', this.$dropdown).val();
        values.dr2to_millis = $('.dr2.to_millis', this.$dropdown).val();
        $target.data('DateRangesWidget', data);

        if ($target.data().DateRangesWidget.options.onChange)
            $target.data().DateRangesWidget.options.onChange(values);

        localStorage.SitecoreReportStartDate = values.dr1from;
        localStorage.SitecoreReportToDate = values.dr1to;
        //localStorage.SitecoreReportStartDate2 = values.dr2from;
        //localStorage.SitecoreReportToDate2 = values.dr2to;
        //localStorage.SitecoreReportRangeComparison = values.comparisonEnabled;

    },
      /**
   * Updates target div with data from target element's data
   */
    updateDateField: function ($target) {
        var values = $target.data("DateRangesWidget").options.values;
        if (values.aggregation) {
            $('span.aggregation', $target).text(values.aggregation);
            $('span.aggregation', $target).show();
        } else {
            $('span.aggregation', $target).hide();
        }

        if (values.dr1from && values.dr1to) {
            $('span.main', $target).text(values.dr1from + ' - ' + values.dr1to);

        } else if (values.daterangePreset) {
            var dates = db.date_presets[values.daterangePreset].dates();
            $('span.main', $target).text(dates[0] + ' - ' + dates[1]);

        }
        else {
            $('span.main', $target).text('N/A');
        }
        
        this.set("fromDate", getFormattedDate(values.dr1from));
        this.set("toDate", getFormattedDate(values.dr1to));
        
        if (values.comparisonEnabled && values.dr2from && values.dr2to) {
            $('span.comparison', $target).text(values.dr2from + ' - ' + values.dr2to);
            $('span.main', $target).css({ top: -5 });
            $('span.comparison', $target).css({ top: -10 });
            $('span.comparison', $target).show();
            $('span.comparison-divider', $target).show();
            this.set("fromDate2", getFormattedDate(values.dr2from));
            this.set("toDate2", getFormattedDate(values.dr2to));
        } else {
            $('span.comparison-divider', $target).hide();
            $('span.main', $target).css({ top: 0 });
            $('span.comparison', $target).css({ top: 0 });
            $('span.comparison', $target).hide();
            this.set("fromDate2", null);
            this.set("toDate2", null);
        }
        
        return true;
    },

    getAggregation: function () {
        return this.$aggregation.val();
    },

    getDaterangePresetVal: function () {
        return this.$daterangePreset.val();
    },

    getDaterangePreset: function () {
        return this.db.date_presets[this.$daterangePreset.val()];
    },

    setDaterangePreset: function (value) {
        this.$daterangePreset.val(value);
        this.$daterangePreset.change();
    },

    getParameter1: function () {
        return parseInt(this.$parameter1.val());
    },

    setParameter1: function (value) {
        this.$parameter1.val(value);
    },

    setComparisonEnabled: function (enabled) {
        this.$datepicker.DatePickerSetMode(enabled ? 'tworanges' : 'range');
        //this.set("compareRange", enabled);
    },

    getComparisonEnabled: function () {
        return this.$enableComparison.prop('checked');
    },

    getComparisonPreset: function () {
        return this.$comparisonPreset.val();
    },
    populateDateRangePresets: function () {
        var aggregation = this.getAggregation();
        if (!aggregation)
            aggregation = this.default_aggregation;
        var main_presets_keys = this.db.aggregations[aggregation].presets;

        var $other_presets = $('<optgroup/>', { label: 'Other presets' });
        var valueBackup = this.$daterangePreset.val();

        this.$daterangePreset.html('');

        // add main presets
        $.each(main_presets_keys, $.proxy(function (i, main_preset_key) {
            var date_preset = this.db.date_presets[main_preset_key];
            if (date_preset == undefined) throw 'Invalid preset "' + main_preset_key + '".';
            this.$daterangePreset.append($("<option/>", {
                value: main_preset_key,
                text: date_preset.title
            }));
        }, this));

        // add other presets
        $.each(this.db.date_presets, $.proxy(function (preset_key, date_preset) {
            if ($.inArray(preset_key, main_presets_keys) == -1) {
                $other_presets.append($("<option/>", {
                    value: preset_key,
                    text: date_preset.title
                }));
            }
        }, this));
        this.$daterangePreset.append($other_presets);

        this.$daterangePreset.val(valueBackup);
    },
    expandDropdown: function ($target) {
        var options = $target.data("DateRangesWidget").options;
        this.$current_target = $target;
        // init aggregations
        if (options.aggregations.length > 0) {
            this.populateAggregations(options.aggregations);
            this.$aggregationWrap.show();
        } else {
            this.$aggregationWrap.hide();
        }

        this.loadValues($target);

        // retract all other dropdowns
        $('.DRWOpened').each($.proxy(function () {
            this.retractDropdown($(this));
        },this));

        var leftDistance = $target.offset().left;
        var rightDistance = $(document).width() - $target.offset().left - $target.width();
        this.$dropdown.show();
        if (rightDistance > leftDistance) {
            // align left edges
            this.$dropdown.offset({
                left: $target.offset().left,
                top: $target.offset().top + $target.height() - 1
            });
            this.$dropdown.css('border-radius', '0 5px 5px 5px')
        } else {
            // align right edges
            var fix = parseInt(this.$dropdown.css('padding-left').replace('px', '')) +
                parseInt(this.$dropdown.css('padding-right').replace('px', '')) +
                parseInt(this.$dropdown.css('border-left-width').replace('px', '')) +
                parseInt(this.$dropdown.css('border-right-width').replace('px', ''))
            this.$dropdown.offset({
                left: $target.offset().left + $target.width() - this.$dropdown.width() - fix,
                top: $target.offset().top + $target.height() - 1
            });
            this.$dropdown.css('border-radius', '5px 0 5px 5px')
        }

        // switch to up-arrow
        $('.date-range-field a', $target).html('&#9650;');
        //$('.date-range-field', $target).css({ borderBottomLeftRadius: 0, borderBottomRightRadius: 0 });
        $('.date-range-field a', $target).css({ borderBottomRightRadius: 0 });
        $target.addClass('DRWOpened');
        $target.removeClass('DRWClosed');


        // refresh
        this.recalculateDaterange();
    },
    retractDropdown: function ($target) {
        this.$dropdown.hide();
        $('.date-range-field a', $target).html('&#9660;');
        //$('.date-range-field', $target).css({ borderBottomLeftRadius: 5, borderBottomRightRadius: 5 });
        $('.date-range-field a', $target).css({ borderBottomRightRadius: 5 });
        $target.addClass('DRWClosed');
        $target.removeClass('DRWOpened');
    },

    getMonday: function (d) {
        d = new Date(d);
        var day = d.getDay();
        var diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
        return new Date(d.setDate(diff));
    }
  });

  var view = Sitecore.Definitions.Views.ControlView.extend({
    initialize: function (options) {
      this._super();
    }
  });

  Sitecore.Factories.createComponent("DateRangePicker", model, view, ".sc-DateRangePicker");
});

Date.prototype.getMonthName = function() {
    var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"];
    return monthNames[this.getMonth()];
};

function getMonthFromString(mon) {
    return new Date(Date.parse(mon + " 1, 2012")).getMonth() + 1;
}
