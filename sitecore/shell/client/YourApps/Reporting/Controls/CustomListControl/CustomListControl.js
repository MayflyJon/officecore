﻿/// <reference path="../../../../../../assets/vendors/underscore/underscore.js" />
/// <reference path="../../../../../../assets/lib/dist/sitecore.js" />
/// <reference path="../../../../../../assets/vendors/Backbone/backbone.js" />

require.config({
    paths: {
        "Scrollbar": "lib/ui/behaviors/Scrollbar",
    },
    shim: {
        'Scrollbar': { deps: ['sitecore'] }
    }
});

define(["sitecore", "Scrollbar", "underscore", "/-/speak/v1/client/analytics.js"], function (_sc) {
    /**
    * Detail List View
    * Composite View which uses:
    * - One Header view
    * - One Footer view
    * - One Row view
    */
    var Header = Backbone.LayoutManager.extend({
        template: "header",
        initialize: function (options) {
            this.parent = options.parent;
        },

        tagName: "tr"
    });

    var Footer = Backbone.LayoutManager.extend({
        template: "footer",
        initialize: function (options) {
            this.parent = options.parent;
        },
        tagName: "tr"
    });

    var Row = Backbone.LayoutManager.extend({
        template: "row",
        tagName: "tr",
        events: { "click": "select" },
        initialize: function (options) {
            this.parent = options.parent;
        },
        afterRender: function () {
            this.sync();
        },
        select: function (e) {
            if (this.$el.hasClass("active")) {
                this.trigger("unselected", this.$el, this.model);
            } else {
                this.trigger("selected", this.$el, this.model);
            }
        }

    });

    var DetailList = Backbone.LayoutManager.extend({
        template: "main",
        events: {
            "click .sc-table-sortable": "sort"
        },
        selectRow: function (row, rowModel) {
            this.$el.find(".active").removeClass("active");
            row.addClass("active");


            this.model.set("selectedItem", rowModel);
            this.model.set("selectedItemId", rowModel.get("itemId"));
        },
        initialize: function (options) {
            this.parent = options.parent;
            this.setView(".sc-table thead", new Header({ parent: this.parent }));
            this.setView(".sc-table-footer", new Footer({ parent: this.parent }));
            this.sortingParameters = [];
            this.model.on("change:selectedItemId", function () {
                this.model.get("selectedItemId") == null ? this.unselectRow() : $.noop();
            }, this);
        },
        unselectRow: function (row, rowModel) {
            this.$el.find(".active").removeClass("active");

            this.model.set("selectedItemId", "");
            this.model.set("selectedItem", "");
        },
        insertRow: function (model) {
            if (this.hasEmpty) {
                this.removeEmpty();
                this.hasEmpty = false;
            }

            var row = new Row({ parent: this.parent, model: model, serialize: model.viewModel });
            this.insertView(".sc-table tbody", row);
            row.on("selected", this.selectRow, this);
            row.on("unselected", this.unselectRow, this);
        },
        beforeRender: function () {
            this.collection.each(this.insertRow, this);
        },
        afterRender: function () {
            var numberRow = this.$el.find(".sc-table thead tr th").length;
            if (this.collection.length === 0) {
                this.renderEmpty(numberRow);
            }
            this.formatFooter(numberRow);
        },
        removeEmpty: function () {
            this.$el.find(".empty").remove();
        },
        sort: function (evt) {
            var $source = $(evt.currentTarget),
                parameter = $source.data("sc-sort-name"),
                columnName = $source.data("sc-name"),
                sortConfiguration = "",
                isExisting;

            //check if it exist
            isExisting = _.find(this.sortingParameters, function (param) {
                return param.columnName == columnName;
            });

            if (isExisting) {
                if (isExisting.direction === "d") {
                    isExisting.direction = "a";
                    $source.removeClass("up");
                } else {
                    isExisting.direction = "d";
                    $source.addClass("up");
                }
            } else {
                $source.removeClass("up");
                this.sortingParameters.push({ columnName: columnName, name: parameter, direction: "d" });
            };

            sortConfiguration = _.reduce(this.sortingParameters, function (memo, conf) { return memo + "|" + conf.direction + conf.name; }, "");
            sortConfiguration = sortConfiguration.substring(1, sortConfiguration.length);

            this.model.set("sorting", sortConfiguration);
        },
        renderEmpty: function (numberRow) {
            var emptytext = this.model.get("empty") || "",
                html = "<tr class='empty'><td colspan='" + numberRow + "'>{0}</td></tr>";

            html = html.replace("{0}", emptytext);
            this.$el.find(".sc-table tbody").html(html);
            this.hasEmpty = true;
        },
        formatFooter: function (numberRow) {
            //this.$el.find(".sc-table tfoot tr td").attr("colspan", numberRow);
        },
        afterRender: function () {
            _.each(this.sortingParameters, function (obj) {
                var $header = this.$el.find("[data-sc-name='" + obj.columnName + "']");
                if (obj.direction === "a") {
                    $header.addClass("up");
                } else {
                    $header.removeClass("up");
                }
            }, this);
        }
    });

    /**
    * Icon List View
    * Composite View which uses:
    * - Icon view
    */

    var Icon = Backbone.LayoutManager.extend({
        events: {
            "click": "select"
        },
        template: "icon",
        tagName: "div",
        getImage: function () {
            //find Thumbail
            if (this.model.get("__Thumbnail")) {
                var shityHtml = this.model.get("__Thumbnail"),
                    extractId = new RegExp(/mediaid="{(.*?)}"/),
                    id = shityHtml.match(extractId)[1],
                    db = this.model.get("itemUri").databaseUri.databaseName;
                id = id.replace(/-/g, "");

                return "/~/media/" + id + ".ashx?bc=Transparent&thn=1t&h=130&w=130&db=" + db;
            } else {
                return this.model.get("$mediaurl");
            }
        },
        className: "sc-iconList-wrap",
        afterRender: function () {
            this.model.set("image", this.getImage());
            this.sync();
        },
        select: function (e) {
            e.preventDefault();
            if (this.$el.hasClass("active")) {
                this.trigger("unselected", this.$el, this.model);
            } else {
                this.trigger("selected", this.$el, this.model);
            }
        }
    });

    var IconList = Backbone.LayoutManager.extend({
        template: "iconList",
        initialize: function (options) {
            this.parent = options.parent;
        },
        insertRow: function (model) {
            if (this.hasEmpty) {
                this.removeEmpty();
                this.hasEmpty = false;
            }
            var icon = new Icon({ model: model, serialize: model.toJSON() });
            icon.on("selected", this.selectIcon, this);
            icon.on("unselected", this.unselectIcon, this);
            this.insertView(icon);
        },
        beforeRender: function () {
            this.collection.each(this.insertRow, this);
        },
        selectIcon: function (icon, iconModel) {
            this.$el.find(".active").removeClass("active");
            icon.addClass("active");


            this.model.set("selectedItem", iconModel);
            this.model.set("selectedItemId", iconModel.get("itemId"));
        },
        unselectIcon: function (icon, iconModel) {
            this.$el.find(".active").removeClass("active");

            this.model.set("selectedItem", "");
            this.model.set("selectedItemId", "");
        },
        afterRender: function () {
            if (this.collection.length === 0) {
                this.renderEmpty();
            }
        },
        removeEmpty: function () {
            this.$el.find(".empty").remove();
        },
        renderEmpty: function (numberRow) {
            var emptyText = this.model.get("empty") || "";
            this.$el.html("<div class='empty'>" + emptyText + "</div>");
            this.hasEmpty = true;
        },
        totalScroll: function () {
            this.collection.each(this.insertRow, this);
        }
    });

    /**
    * ListControl is a Composite view
    * It is wrapper in order to switch the view Mode
    * Sorting parameter is a pipe separated string.
    * prefixed with a, it will be ascending
    * prefixed with d, it will be descending
    */
    var ListControl = Backbone.LayoutManager.extend({
        template: "listControl",
        initialize: function (options) {
            this.parent = options.parent;
            this.model.on("change:items", this.setItemRows, this);
            this.model.on("change:newItems", this.refresh, this);
            this.model.on("change:view", this.setViewModel, this);
            this.setViewModel();
            this.rowItems = [];

            this.promises = [];
        },
        setItemRows: function () {

            var items = this.model.get("items");
            if (_.isNull(items) || _.isUndefined(items))
                return;

            _.each(this.model.get("items"), $.proxy(function (item) {


                if (item.Country && (item.Country == 'N/A') || item.Country == 'unknown') {
                    item.Country = 'Unknown';
                    this.rowItems.push(item);
                }
                else if (item.Country && item.Country.length === 2) {
                    this.promises.push(this.makeRequest(item));
                } else {
                    if (IsGuid) {
                        this.rowItems.push(item);
                    }
                    else {
                        this.rowItems.push(addCommas(item));
                    }
                }

            }, this));

            $.when.apply($, this.promises).then($.proxy(function () {
                this.model.set("newItems", null);
                this.model.set("newItems", this.rowItems);
                this.rowItems = [];

            }, this));
        },
        refresh: function () {

            this.collection.reset();
            _.each(this.model.get("newItems"), function (item) {

                $.each(item, function (key, value) {
                    if (!IsGuid(value)) {
                        item[key] = addCommas(value);
                    }
                });

                // creating new item from json
                if (item instanceof this.collection.model) {
                    itemModel = item;
                } else {
                    var itemModel = new this.collection.model(item);
                }

                // apply formating
                itemModel.viewModel.formatValue = function (name, fromat) {
                    var formatDate = "";

                    var val = typeof this[name]() != 'undefined' ? this[name]() : '';
                    var tempValue = '';
                    var additionalValues;
                    if (this.$formatedFields) {
                        additionalValues = this.$formatedFields();
                    }
                    if (fromat && fromat == "short") {
                        if (additionalValues) {
                            tempValue = additionalValues[name].shortDateValue;
                            if (tempValue) val = tempValue;
                        }
                    }
                    else if (fromat && fromat == "long") {
                        if (additionalValues) {
                            tempValue = additionalValues[name].longDateValue;
                            if (tempValue) val = tempValue;
                        }
                    }
                    else if (fromat) {
                        if (_sc.Helpers.date.isISO(val)) {
                            var dateConverter = _sc.Converters.get("date");
                            tempValue = dateConverter.toStringWithFormat(val, fromat);
                        }
                        if (tempValue) val = tempValue;
                    }
                    return val;
                };

                this.collection.add(itemModel);
            }, this);

            var parent = this.parent;
            this.render().done(function () {
                parent.afterRender();
                parent.trigger("didRender");
            });

        },
        makeRequest: function (item) {
            var dfd = $.Deferred();
            $.ajax({
                url: "http://dev.virtualearth.net/REST/v1/Locations/" + item.Country,
                dataType: "jsonp",
                data: {
                    key: "AvDDZnfZA-SbaAan_Z5mBL8PfU-bTWQ4YoLxGAT6F-NusUq8y6QKj8M5GIxyAEvy"
                },
                jsonp: "jsonp",
                success: $.proxy(function (data) {
                    if (data && data.resourceSets[0] && data.resourceSets[0].resources[0] && data.resourceSets[0].resources[0].name) {
                        item.Country = data.resourceSets[0].resources[0].name;

                        this.rowItems.push(item);

                    }
                }, this),
                complete: $.proxy(function () {
                    //Resolve deffered
                    dfd.resolve();
                }, this),
            });

            return dfd.promise();
        },
        setViewModel: function () {
            var view = this.model.get("view");

            if (!view || view.toLowerCase() == "DetailList".toLowerCase()) {
                this.$el.empty();
                this.setView(new DetailList({ model: this.model, parent: this.parent, collection: this.collection }));
            }
            else if (view.toLowerCase() == "IconList".toLowerCase()) {
                this.$el.empty();
                this.setView(new IconList({ model: this.model, parent: this.parent, collection: this.collection }));
            }
            else {
                throw "Unknown view mode: " + view;
            }
            this.render();
        }
    });

    /**
    * Collection
    * will be shared among all the Composite view
    */
    var SC_Collection = Backbone.Collection.extend({
        model: _sc.Definitions.Models.Model
    });

    var controlModel = _sc.Definitions.Models.ControlModel.extend({
        initialize: function (options) {
            this._super();
            this.set("items", []);

            this.set("selectedItem", "");
            this.set("selectedItemId", "");
            this.set("empty", "");
        }
    });

    /**
    * ControlView
    * Object which allows us to exposed the Composite
    */
    var controlView = _sc.Definitions.Views.ControlView.extend({
        listen: _.extend({}, _sc.Definitions.Views.ControlView.prototype.listen, {
            "detailList:$this": "setDetailListView",
            "icon:$this": "setIconView"
        }),
        initialize: function (options) {
            this._super();
            this.model.set("empty", this.$el.data("sc-empty")); //the empty should be move in the Composite
            this.model.set("view", this.$el.data("sc-viewmode"));
            this.model.set("height", this.$el.data("sc-height"));
            this.model.set("sorting", "");
            this.listControl = new ListControl({ model: this.model, parent: this, collection: this.collection, app: this.app });
            this.$el.empty().append(this.listControl.el);
            this.listControl.render();

        },
        setDetailListView: function () {
            this.model.set("view", "DetailList");
        },
        setIconView: function () {
            this.model.set("view", "IconList");
        }
    });

    _sc.Factories.createComponent("ListControl", controlModel, controlView, ".sc-listcontrol", SC_Collection);
});