﻿require.config({
    baseUrl: "/sitecore/shell/client/Speak/Assets",
    paths: {
        chartjs: "lib/ui/deps/devExtreme/dx.chartjs",
        globalize: "lib/ui/deps/devExtreme/globalize.min"
    },
    shim: {
        'globalize': { exports: "Globalize" },
        'chartjs': { deps: ['jquery', 'underscore', 'globalize'], exports: "DevExpress" }
    }
});

define(["sitecore", "underscore", , "jquery", "chartjs"], function (Sitecore) {
    var model = Sitecore.Definitions.Models.ControlModel.extend({
        initialize: function (options) {
            this._super();
            this.set("items", null);
            this.set("rotated", true);
            this.set("title", null);
            this.set("chartType", null);

            this.on("change:items", this.prepareData, this);
        },

        prepareData: function () {

            var results = this.get("items");
            if (!results)
                return;

            //Set the chart data
            this.chartData = results;
        }
    });

    var view = Sitecore.Definitions.Views.ControlView.extend({
        initialize: function (options) {
            this._super();

            this.listenTo(this.model, 'change', this.render);

        },
        render: function () {
            //Get data
            var data = this.model.chartData;
            if (!data)
                return;

            var rotated = this.model.get("rotated") === 'true';
            var title = this.model.get("title");
            var chartId = this.model.attributes.name;
            var toolTipId = this.model.attributes.name + "Tooltip";
            var toolTipValue = toolTipId + "Value";
            var chartType = this.model.get("chartType");

            title = _.isNull(title) || _.isUndefined(title) ? "Bar Chart" : title;

            if (!_.isNull(data) && !_.isUndefined(data)) {
                $("#" + chartId).dxChart({
                    dataSource: data,
                    commonAxisSettings: {
                        grid: {
                            opacity: 0.3
                        },
                        label: {
                            overlappingBehavior: { mode: 'rotate', rotationAngle: -90 }
                        }
                    },
                    commonSeriesSettings: {
                        argumentField: 'Text',
                        type: 'bar',
                        label: {
                            visible: true,
                            connector: { visible: true },
                            format: 'largeNumber'
                        }
                    },
                    series: [{
                        name: '',
                        valueField: 'ValuePerVisit'
                    }],
                    rotated: rotated,
                    tooltip: {
                        enabled: true
                    },
                    title:
                    {
                        text: title
                    },
                    legend: {
                        visible: false
                    },
                    loadingIndicator: {
                        backgroundColor: 'firebrick',
                        font: {
                            weight: 700,
                            size: 16
                        }
                    },
                });
            }
        }
    });

    Sitecore.Factories.createComponent("ChartJsBar", model, view, ".sc-ChartJsBar");
});