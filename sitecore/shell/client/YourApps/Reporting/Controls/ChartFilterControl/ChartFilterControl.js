﻿require.config({
    baseUrl: "/sitecore/shell/client/Speak/Assets",
    paths: {
        chartjs: "lib/ui/deps/devExtreme/dx.chartjs",
        globalize: "lib/ui/deps/devExtreme/globalize.min"
    },
    shim: {
        'globalize': { exports: "Globalize" },
        'chartjs': { deps: ['jquery', 'underscore', 'globalize'], exports: "DevExpress" }
    }
});

define(["sitecore", "underscore", "jquery", "jqueryui", "chartjs"], function (Sitecore) {
    var model = Sitecore.Definitions.Models.ControlModel.extend({
        initialize: function (options) {
            this._super();
            this.dropIndex = 1;
            this.set("filterItems", null);
            this.set("sourceItems", null);
            this.set("selectedFilterItems", [{ name: 'All', value: totalTrafficType }]);
            this.set("selectedSeries", ['Visits', 'Value']);
            this.set("compareRange", false);
        }
    });

    var view = Sitecore.Definitions.Views.ControlView.extend({
        initialize: function (options) {
            this._super();

            $('div[data-sc-id=\"' + this.model.attributes.name + '\"] .filterChevronButton').on("click", $.proxy(this.toggleFilters, this));
            
            $('#ValueSeries').click($.proxy(function(event) {
                var oldSeries = _.clone(this.model.get("selectedSeries"));

                var index = oldSeries.indexOf(event.target.value);
                if (index > -1) {
                    oldSeries.splice(index, 1);
                } else {
                    oldSeries.push(event.target.value);
                }

                this.setSeries(oldSeries);
            }, this));
            $('#VisitsSeries').click($.proxy(function (event) {
                var oldSeries = _.clone(this.model.get("selectedSeries"));

                var index = oldSeries.indexOf(event.target.value);
                if (index > -1) {
                    oldSeries.splice(index, 1);
                } else {
                    oldSeries.push(event.target.value);
                }

                this.setSeries(oldSeries);
            },this));

            this.model.on("change:filterItems", this.refreshFilters, this);
            this.model.on("change:sourceItems", this.updateCharts, this);

            $(".droppable").droppable({
                drop: $.proxy(function (ev, ui) {

                    this.filterDrop(ui.draggable);

                }, this)
            });
            
            $('body').append(createFilterItem('All', '100.00%', totalTrafficType));
            this.makeDraggable('#filterItem' + totalTrafficType);
            this.filterDrop($('#filterItem' + totalTrafficType));
            //$('#filterItem' + totalTrafficType).click($.proxy(function () {

            //    this.clearFilter($('#filterItem' + totalTrafficType));

            //}, this));

        },
        filterDrop: function (ui) {

            var target = $('div[data-droptarget="' + this.model.dropIndex + '"]');
            ui.draggable("disable", 1);
            var draggableId = ui.attr("id");

            $('#' + draggableId).appendTo(target);
            $('#' + draggableId).removeAttr("style");

            //remove droppable classes
            $(target).removeClass("droppable");
            $(target).removeAttr("style");

            var clearItemElements = $(".filterItemClear");
            var clearButton = $(target).find(clearItemElements)[0];
            $(clearButton).click($.proxy(function (event) {

                this.clearFilter(event.target);

            }, this));

            this.model.dropIndex += 1;
            this.selectedFilters();
            
        },
        setSeries: function (series) {

            this.model.set("selectedSeries", series);

        },
        clearFilter: function (eventElement) {

            var dataDrop = $(eventElement).parents('.filterItemDroppable')[0];
            var dropTarget = $(dataDrop).data('droptarget');

            if (!_.isNull(dropTarget) && !_.isUndefined(dropTarget)) {
            
                var parentItemWrapper = $('div[data-droptarget="' + dropTarget + '"]');

                var filterItem = $(parentItemWrapper).children('.filterItem')[0];
                var trafficType = $(filterItem).attr("data-traffictype");
                $(filterItem).appendTo('.filterSelectContent');
                $('#gaugeChartText' + trafficType).empty();
                //this.makeDraggable($(filterItem));
                $(filterItem).draggable('enable', 1);

                if (dropTarget < 5) {
                    this.moveSelectedFilters(dropTarget + 1);
                }

                this.model.dropIndex -= 1;
            }
        },
        toggleFilters: function () {

            $('.filterSelectWrapper').toggleClass('filterHidden');
            $('.filterChevronButton').toggleClass("filterChevronOff").toggleClass("filterChevronOn");

        },
        moveSelectedFilters: function (start) {

            if (start > 4) {
                start = 4;
            }

            for (var i = parseInt(start) ; i < 5; i++) {
                var parentItemWrapper = $('div[data-droptarget="' + i + '"]');
                if (parentItemWrapper[0].innerHTML != "") {

                    var filterItem = $(parentItemWrapper).children('.filterItem')[0];
                    $(filterItem).appendTo('div[data-droptarget="' + (i - 1) + '"]');

                }
            }

            for (var j = 1; j < 5; j++) {
                var itemWrapper = $('div[data-droptarget="' + j + '"]');
                if (itemWrapper[0].innerHTML === "") {
                    $(itemWrapper).addClass("droppable");
                } else {
                    $(itemWrapper).removeClass("droppable");
                }
            }

            this.selectedFilters();
        },
        selectedFilters: function () {

            var selectedFilters = [];
            for (var j = 1; j < 5; j++) {
                var itemWrapper = $('div[data-droptarget="' + j + '"]');
                if (itemWrapper[0].innerHTML != "") {
                    var selectedFilterItem = $(itemWrapper).children('.filterItem')[0];
                    var trafficType = $(selectedFilterItem).attr("data-traffictype");
                    var trafficTypeName = $(selectedFilterItem).attr("data-traffictypename");

                    var selectedFilter = {
                        name: trafficTypeName,
                        value: parseInt(trafficType)
                    };
                    selectedFilters.push(selectedFilter);
                }
            }
            this.model.set("selectedFilterItems", selectedFilters);

        },
        refreshFilters: function () {

            $('.filterSelectContent').empty();

            var items = this.model.get("filterItems");
            if (_.isNull(items) || _.isUndefined(items))
                return;

            $.each(items, $.proxy(function (index, value) {

                $('.filterSelectContent').append(createFilterItem(value.$displayName, '', value.Value));
                this.makeDraggable("#filterItem" + value.Value);

            }, this));

        },
        makeDraggable: function (target) {

            $(target).draggable({
                snap: ".droppable",
                snapMode: "inner",
                drag: function () {
                    $('.droppable').css({ background: '#B6FFB6', border: '2px solid #92CC92' });
                },
                stop: function (ev, ui) {

                    $('.droppable').css({ background: 'white', border: 'none' });

                },
                revert: function (event, ui) {
                    // on older version of jQuery use "draggable"
                    // $(this).data("draggable")
                    // on 2.x versions of jQuery use "ui-draggable"
                    // $(this).data("ui-draggable")
                    $(this).data("uiDraggable").originalPosition = {
                        top: 0,
                        left: 0
                    };
                    // return boolean
                    return !event;
                    // that evaluate like this:
                    // return event !== false ? false : true;
                },
                revertDuration: 3,

            });
            
        },
        updateCharts: function () {
            var items = this.model.get("sourceItems");
            if (_.isNull(items) || _.isUndefined(items))
                return;

            var selectedItemsArray = this.model.get("selectedFilterItems");
            if (_.isNull(selectedItemsArray) || _.isUndefined(selectedItemsArray))
                return;
            var selectedItems = _.pluck(selectedItemsArray, 'value');

            $.each(selectedItems, $.proxy(function (index, value) {

                var objectTotal = _.find(items, function (obj) { return obj.trafficType === value; });

                var series = this.model.get("selectedSeries");
                var compareRange = this.model.get("compareRange");
                var colour = getPallete(index, series, compareRange);
                
                var filterObject = _.filter(selectedItemsArray, function (item) {
                    return item.value === value;
                });
                var filterName = filterObject[0].name;

                
                var values = [];
                var totalText = "";
                if (compareRange) {
                    values.push(parseInt(objectTotal["%Visits"]), parseInt(objectTotal[filterName + "%VisitsSecondary"]));
                    var currentPerc = parseFloat(objectTotal["%Visits"]);
                    var previousPerc = parseFloat(objectTotal[filterName + "%VisitsSecondary"]);
                    var total = (currentPerc - previousPerc).toFixed(2);
                    
                    if (total !== "NaN") {
                        if (total >= 0) {
                            $("#gaugeChartText" + value).css({ color: 'green' });
                            totalText = "+" + total + "% change";
                        } else {
                            $("#gaugeChartText" + value).css({ color: 'red' });
                            totalText = total + "% change";
                        }
                    }

                } else {
                    values.push(parseInt(objectTotal["%Visits"]));
                    if (!_.isUndefined(objectTotal["%Visits"])) {
                        totalText = objectTotal["%Visits"] + " %";
                    } else {
                        totalText = "0 %";
                    }
                    $("#gaugeChartText" + value).css({ color: 'black' });
                }

                if (!_.isUndefined(objectTotal) && !_.isNull(objectTotal)) {
                    $('#gaugeChart' + value).dxBarGauge({
                        startValue: 0,
                        endValue: 100,
                        values: values,
                        label: {
                            visible: false
                        },
                        palette: colour,
                        geometry: {
                            startAngle: 270,
                            endAngle: 275
                        },
                        barSpacing: 2
                    });
                    $('#gaugeChartText' + value).html(totalText);
                    
                }


            }, this));

        }


    });

    function createFilterItem(name, percentage, value) {

        var sb = new StringBuilder();
        sb.append('<div class="filterItem" id="filterItem' + value);
        sb.append('" data-traffictype="' + value);
        sb.append('" data-traffictypename="' + name);
        sb.append('"><div class="filterItemGauge"><div class="filterItemGaugeChart" id="gaugeChart' + value);
        sb.append('"></div></div><div class="filterItemText"><span>' + name);
        sb.append('</span><p id="gaugeChartText' + value);
        sb.append('">' + percentage);
        sb.append('</p></div><div class="filterItemClear"><img src="/sitecore/shell/client/Speak/Assets/img/cross.png" alt="close" /></div></div>');

        return sb.toString();
    }
    
    function getPallete(filterIndex, series, compareRange) {

        var colour = [];
        if (compareRange) {
            var colourIndex;
            if (filterIndex === 0) {
                colourIndex = filterIndex;
            } else {
                colourIndex = (filterIndex * filterIndex) + (series.length * 2);
                if(isOdd(colourIndex)) {
                    colourIndex -= 1;
                }
            }
            

            colour.push(chartPalette[colourIndex]);
            colour.push(chartPalette[colourIndex + 1]);

        } else {
            if (series.length > 1) {
                colour.push(chartPalette[(filterIndex * 2)]);
            } else {
                colour.push(chartPalette[filterIndex]);
            }
        }
        
        return colour;
        
    }

    Sitecore.Factories.createComponent("ChartFilterControl", model, view, ".sc-ChartFilterControl");
});
