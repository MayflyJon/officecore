﻿require(["jasmineEnv"], function (jasmineEnv) {
  var setupTests = function () {
    "use strict";

    describe("Given a ChartJsBubble model", function () {
      var component = new Sitecore.Definitions.Models.ChartJsBubble();

      describe("when I create a ChartJsBubble model", function () {
        it("it should have a 'isVisible' property that determines if the ChartJsBubble component is visible or not", function () {
          expect(component.get("isVisible")).toBeDefined();
        });

        it("it should set 'isVisible' to true by default", function () {
          expect(ChartJsBubble.get("isVisible")).toBe(true);
        });

        it("it should have a 'toggle' function that either shows or hides the ChartJsBubble component depending on the 'isVisible' property", function () {
          expect(component.toggle).toBeDefined();
        });
      });
    });
  };

  runTests(jasmineEnv, setupTests);
});