﻿require.config({
    baseUrl: "/sitecore/shell/client/Speak/Assets",
    paths: {
        chartjs: "lib/ui/deps/devExtreme/dx.chartjs",
        globalize: "lib/ui/deps/devExtreme/globalize.min"
    },
    shim: {
        'globalize': { exports: "Globalize" },
        'chartjs': { deps: ['jquery', 'underscore', 'globalize'], exports: "DevExpress" }
    }
});

define(["sitecore", "underscore", "chartjs"], function (Sitecore) {
  var model = Sitecore.Definitions.Models.ControlModel.extend({
    initialize: function (options) {
        this._super();
        
        //Make the items null
        this.set("items", null);
        this.set("title", "");
        
        this.on("change:items", this.prepareData, this);
    },
    prepareData: function () {
        
        var results = this.get("items");
        if (!results)
            return;
        
        //Set the chart
        this.chartData = results;
    }
  });

  var view = Sitecore.Definitions.Views.ControlView.extend({
    initialize: function (options) {
        this._super();

        $(this.el).dxChart();
        $(this.el).dxChart('instance').showLoadingIndicator();
        
        this.model.on("change", this.render, this);
    },
    render: function () {

        var newData = this.model.chartData;
        if (_.isNull(newData) || _.isUndefined(newData))
            return;

        var chartId = this.model.attributes.name + "Chart";
        var title = this.model.get("title");
        
        var axisArray = [];
        var chartParams;

        var timeAxis = {
            name: 'TimeAxis',
            min: 0,
            title: 'Time on Site (s)',
            label: { format: 'decimal' }
        };

        axisArray.push(timeAxis);
        
        chartParams = {
            dataSource: newData,
            commonSeriesSettings: {
                type: 'bubble',
                tagField: 'Value',
                label: {
                    visible: true,
                    format: 'largeNumber'
                },
                maxLabelCount: 9,
                ignoreEmptyPoints: true
            },
            valueAxis: axisArray,
            argumentAxis: {
                title: 'PageViews',
                label: { format: 'decimal', precision: 0.1 }
            },
            tooltip: {
                enabled: true,
                shared: true,
                argumentFormat: 'monthAndYear',
                customizeText: function (value, test) {
                    return "<b>" + value.seriesName + " Value</b><br/>" +
                        this.point.tag + "<br/>";
                    //value.points[2].seriesName + ": " + value.points[2].value;
                },
                font: {
                    size: 14
                }
            },
            legend: {
                visible: true,
                verticalAlignment: "top",
                horizontalAlignment: "center",
                paddingTopBottom: 0,
                itemTextPosition: 'right'
            },
            series: [{ argumentField: 'UnsubscribedPageViews', valueField: 'TimeOnSite', sizeField: 'Value', name: 'Unsubcribed' },
            { argumentField: 'SubscribedPageViews', valueField: 'TimeOnSite', sizeField: 'Value', name: 'Subscribed' }],
            title: title,
            palette: 'analyticsPalette',
            size: {
                height: 400,
            }
        };

        $(this.el).dxChart(chartParams);

    }
  });

  Sitecore.Factories.createComponent("ChartJsBubble", model, view, ".sc-ChartJsBubble");
});
