﻿require.config({
    baseUrl: "/sitecore/shell/client/Speak/Assets",
    paths: {
        globalize: "lib/ui/deps/devExtreme/globalize.min",
        webappjs: "lib/ui/deps/devExtreme/dx.webappjs"
    },
    shim: {
        'globalize': { exports: "Globalize" },
        'webappjs': { deps: ['jquery', 'underscore', 'globalize'] }
    }
});

define(["sitecore", "underscore", "jquery", "webappjs"], function (Sitecore) {
  var model = Sitecore.Definitions.Models.ControlModel.extend({
    initialize: function (options) {
        this._super();

        this.set("items", null);
        this.set("compareRangeItems", null);
        this.set("compareRange", null);
        this.set("configurationItem", null);
        this.set("selectedItem", null);
        this.set("configItem", null);
        this.set("dataSeries", null);
        this.setSecondItems = false;
        
        this.on("change:configurationItem change:compareRangeItems", this.configure, this);

    },
    configure: function () {

        var configurationItemId = this.get("configurationItem");
        if (_.isNull(configurationItemId) || _.isUndefined(configurationItemId))
            return;

        this.setSecondItems = false;

        RetrieveSimpleItem(configurationItemId, "core", $.proxy(function (item) {

            var compareRange = this.get("compareRange");
            var showFilterRow;
            if (compareRange) {
                showFilterRow = false;
            } else {
                showFilterRow = item.showFilterRow === "1" ? true : false;
            }

            var configItem = {
                allowColumnReordering: item.allowColumnReordering === "1" ? true : false,
                columnChooser: item.columnChooser === "1" ? true : false,
                pagerRowSize: parseInt(item.pagerRowSize),
                selectionMode: item.selectionMode,
                showFilterRow: showFilterRow,
                showGroupPanel: item.showGroupPanel === "1" ? true : false,
                showPager: item.showPager === "1" ? true : false,
                showSearchPanel: item.showSearchPanel === "1" ? true : false,
                sortingMode: item.sortingMode,
                showPageSizeSelector: item.showPageSizeSelector === "1" ? 'auto' : false,
                allowCompareRange: item.allowCompareRange === "1" ? true : false,
            };

            this.set("configItem", configItem);

        }, this));
        

        RetreiveItemChildren(configurationItemId, "core", $.proxy(function (item) {

            var dataColumns = [];
            var compareRange = this.get("compareRange");
            
            $.each(item, $.proxy(function(index, value) {

                var column = {
                    allowGrouping: value.allowGrouping === "1" ? true : false,
                    dataField: value.dataField,
                    format: value.format,
                    headerText: value.headerText,
                    precision: value.precision,
                    sortOrder: value.sortOrder,
                    visible: value.visible === "1" ? true : false,
                    width: value.width,
                    allowFiltering: value.allowFiltering,
                    caption: value.headerText,
                };

                if(compareRange) {
                    column.groupIndex = value.groupIndex;
                }

                dataColumns.push(column);

            }, this));
            
            if(compareRange) {

                var column = {
                    allowGrouping: false,
                    dataField: "Source",
                    visible: true,
                    caption: "Range"
                };
                dataColumns.push(column);
            }

            this.set("dataSeries", dataColumns);

        }, this));
        
    }
  });

  var view = Sitecore.Definitions.Views.ControlView.extend({
    initialize: function (options) {
        this._super();
        this.model.on("change:items", this.setItemRows, this);
        this.model.on("change:newItems change:dataSeries", $.proxy(this.RenderGrid, this));

        this.rowItems = [];

        this.promises = [];
    },
    RenderGrid: function () {
        
        var data = this.model.get("items");
        if (_.isNull(data) || _.isUndefined(data))
            return;
        
        var configItem = this.model.get("configItem");
        if (_.isNull(configItem) || _.isUndefined(configItem))
            return;

        var dataColumns = this.model.get("dataSeries");
        if (_.isNull(dataColumns) || _.isUndefined(dataColumns))
            return;

        var compareRange = this.model.get("compareRange");

        _.each(data, $.proxy(function (item) {


            if (item.Country && (item.Country == 'N/A')) {
                item.Country = 'unknown';
                this.rowItems.push(item);
            }
            else if (item.Country && item.Country.length === 2) {
                this.promises.push(this.makeRequest(item));
            } else {
                if (IsGuid) {
                    this.rowItems.push(item);
                }
                else {
                    this.rowItems.push(addCommas(item));
                }
            }
            
            if (compareRange && configItem.allowCompareRange) {
                item.Source = "Current";
            }

        }, this));
        
        if (compareRange && configItem.allowCompareRange && !this.model.setSecondItems) {
            var secondData = this.model.get("compareRangeItems");
            if (!_.isNull(secondData) && !_.isUndefined(secondData)) {
                _.each(secondData, $.proxy(function(item) {

                    item.Source = "Previous";
                    data.push(item);
                    
                }, this));

                this.model.setSecondItems = true;
            }
        } else if(compareRange && !configItem.allowCompareRange) {
            dataColumns = _.without(dataColumns, _.findWhere(dataColumns, { dataField: "Source" }));
        }

        $(this.el).dxDataGrid({
            dataSource: data,
            columns: dataColumns,
            columnChooser: { enabled: configItem.columnChooser },
            allowColumnReordering: configItem.allowColumnReordering,
            sorting: { mode: configItem.sortingMode },
            groupPanel: { visible: configItem.showGroupPanel },
            pager: { showPageSizeSelector: configItem.showPageSizeSelector },
            paging: { pageSize: configItem.pagerRowSize },
            //stateStoring: {
            //    enabled: true,
            //    type: 'localStorage',
            //    storageKey: 'storage'
            //},
            editing: {
                editEnabled: false,
            },
            filterRow: { visible: configItem.showFilterRow },
            searchPanel: { visible: configItem.showSearchPanel },
            selection: { mode: configItem.selectionMode },
            hoverStateEnabled: true,
            loadPanel: { enabled: true },
            rowClick: $.proxy(function (row) {

                this.model.set("selectedItem", row.key);
                //window.location.replace('/sitecore/client/Your Apps/Reporting/Pages/GoalPage?goal=' + row.key.ID);

            }, this)
        });

    },
    setItemRows: function () {

        var items = this.model.get("items");
        if (_.isNull(items) || _.isUndefined(items))
            return;

        _.each(this.model.get("items"), $.proxy(function (item) {


            if (item.Country && (item.Country == 'N/A')) {
                item.Country = 'unknown';
                this.rowItems.push(item);
            }
            else if (item.Country && item.Country.length === 2) {
                this.promises.push(this.makeRequest(item));
            } else {
                if (IsGuid) {
                    this.rowItems.push(item);
                }
                else {
                    this.rowItems.push(addCommas(item));
                }
            }

        }, this));

        $.when.apply($, this.promises).then($.proxy(function () {
            this.model.set("newItems", null);
            this.model.set("newItems", this.rowItems);
            this.rowItems = [];

        }, this));
    },
    makeRequest: function (item) {
        var dfd = $.Deferred();
        $.ajax({
            url: "http://dev.virtualearth.net/REST/v1/Locations/" + item.Country,
            dataType: "jsonp",
            data: {
                key: "AvDDZnfZA-SbaAan_Z5mBL8PfU-bTWQ4YoLxGAT6F-NusUq8y6QKj8M5GIxyAEvy"
            },
            jsonp: "jsonp",
            success: $.proxy(function (data) {
                if (data && data.resourceSets[0] && data.resourceSets[0].resources[0] && data.resourceSets[0].resources[0].name) {
                    item.Country = data.resourceSets[0].resources[0].name;

                    this.rowItems.push(item);

                }
            }, this),
            complete: $.proxy(function () {
                //Resolve deffered
                dfd.resolve();
            }, this),
        });

        return dfd.promise();
    },
  });

  Sitecore.Factories.createComponent("DataGrid", model, view, ".sc-DataGrid");
});
