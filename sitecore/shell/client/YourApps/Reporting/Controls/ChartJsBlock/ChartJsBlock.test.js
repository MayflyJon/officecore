﻿/// <reference path="/sitecore/shell/client/YourApps/Reporting/Controls/ChartJsBlock/ChartJsBlock.js" />
/// <reference path="/tests/jasmine.ui.runner.test.js" />
/// <reference path="/tests/jasmineEnv.js" />

require(["jasmineEnv"], function (jasmineEnv) {
  var setupTests = function () {
    "use strict";

    describe("Given a ChartJsBlock model", function () {
      var component = new Sitecore.Definitions.Models.ChartJsBlock();

      describe("when I create a ChartJsBlock model", function () {
        it("it should have a 'isVisible' property that determines if the ChartJsBlock component is visible or not", function () {
          expect(component.get("isVisible")).toBeDefined();
        });

        it("it should set 'isVisible' to true by default", function () {
          expect(ChartJsBlock.get("isVisible")).toBe(true);
        });

        it("it should have a 'toggle' function that either shows or hides the ChartJsBlock component depending on the 'isVisible' property", function () {
          expect(component.toggle).toBeDefined();
        });
      });
    });
  };

  runTests(jasmineEnv, setupTests);
});