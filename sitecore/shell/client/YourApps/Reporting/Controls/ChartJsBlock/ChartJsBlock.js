﻿require.config({
    baseUrl: "/sitecore/shell/client/Speak/Assets",
    paths: {
        chartjs: "lib/ui/deps/devExtreme/dx.chartjs",
        globalize: "lib/ui/deps/devExtreme/globalize.min"
    },
    shim: {
        'globalize': { exports: "Globalize" },
        'chartjs': { deps: ['jquery', 'underscore', 'globalize'], exports: "DevExpress" }
    }
});

define(["sitecore", "underscore", , "jquery", "chartjs"], function (Sitecore) {
  var model = Sitecore.Definitions.Models.ControlModel.extend({
    initialize: function (options) {
        this._super();
        this.set("items", null);
        this.set("compareItems", null);
        this.set("compareRange", null);
        this.set("valueColumn", null);
        this.set("text", null);
        this.set("enabled", true);
        this.set("lineColour", null);
        this.set("calculateAverage", null);
        this.set("appendText", null);
        this.set("useSingleTotalValue", null);
        this.set("total", null);
        
        this.on("change:items change:compareItems", this.prepareData, this);
        
    },
    prepareData: function () {
        
        var enabled = this.get("enabled");
        if (!enabled)
            return;

        var results = this.get("items");
        if (!results)
            return;
        
        var compareRange = this.get("compareRange");

        if(compareRange) {
            var compareItems = this.get("compareItems");
            var valueColumn = this.get("valueColumn");
            
            if (!_.isNull(compareItems) && !_.isUndefined(compareItems)) {

                $.each(compareItems, function (index, value) {

                    if (!_.isNull(results[index]) && !_.isUndefined(results[index])) {
                        results[index][valueColumn + "Secondary"] = value[valueColumn];
                    }

                });

            }
        }
        
        //Set the chart data
        this.chartData = results;
    }

  });

  var view = Sitecore.Definitions.Views.ControlView.extend({
    initialize: function (options) {
        this._super();
        
        _.bindAll(this, 'beforeRender', 'render', 'afterRender');
        this.render = _.wrap(this.render, function (render) {
            this.beforeRender();
            render();
            this.afterRender();
            return this;
        });
        this.listenTo(this.model, 'change', $.proxy(this.render, this));

        
        //$(this.el).dxChart();
        //$(this.el).dxChart('instance').showLoadingIndicator();

        
    },
    beforeRender: function () {

    },
    render: function () {
        
        $(this.el).empty();
        $(this.el).removeClass("d3Block");
        var enabled = this.model.get("enabled");
        if (!enabled)
            return;
        
        //Get data
        var data = this.model.chartData;
        if (!data) 
            return;           
        
        var compareRange = this.model.get("compareRange");
        if (compareRange) {
            $('.chartBlockLoading').css("height", "260px");
        } else {
            $('.chartBlockLoading').css("height", "200px");
        }

        var seriesArray = [];
        var text = this.model.get("text");
        var valueColumn = this.model.get("valueColumn");
        var lineColour = this.model.get("lineColour");
        var useSingleTotalValue = this.model.get("useSingleTotalValue");
        var chartId = this.model.attributes.name + "Chart";
        var toolTipId = this.model.attributes.name + "Tooltip";
        var toolTipDate = toolTipId + "Date";
        var toolTipValue = toolTipId + "Value";
        var secondaryTotal;
        
        //Get total or average
        var valueData = _.pluck(data, valueColumn);
        var calcAverage = this.model.get("calculateAverage");
        var total;
        if (!_.isNull(useSingleTotalValue) && !_.isUndefined(useSingleTotalValue)) {
            if (data.length > 0) {
                total = data[0][useSingleTotalValue];
            } else {
                total = 0;
            }
        } else if (!_.isNull(calcAverage)) {
            total = _.reduce(valueData, function (memo, num) { return parseInt(memo) + parseInt(num); }, 0) / valueData.length;
            total = parseFloat(Math.round(total * 100) / 100).toFixed(2);
        } else {
            total = _.reduce(valueData, function (memo, num) { return parseInt(memo) + parseInt(num); }, 0);
        }
        
        this.model.set("total", total);

        //Add first series
        seriesArray.push({
            name: valueColumn,
            valueField: valueColumn,
            color: "#125687"
        });
        
        if(compareRange) {
            //Add second series
            seriesArray.push({
                name: valueColumn + "Secondary",
                valueField: valueColumn + "Secondary",
                type: "spline",
                color: "#EF8200"
            });
            
            var compareTotal;
            var secondaryValueData = _.pluck(data, valueColumn + "Secondary");
            if (!_.isNull(calcAverage)) {
                secondaryTotal = _.reduce(_.filter(secondaryValueData, function (num) { return !_.isUndefined(num) }), function (memo, num) { return parseInt(memo) + parseInt(num); }, 0) / valueData.length;
                secondaryTotal = parseFloat(Math.round(secondaryTotal * 100) / 100).toFixed(2);
                compareTotal = parseFloat(total - secondaryTotal).toFixed(2);
            } else {
                secondaryTotal = _.reduce(_.filter(secondaryValueData, function (num) { return !_.isUndefined(num)}), function (memo, num) { return parseInt(memo) + parseInt(num); }, 0);
                var newTotal = parseFloat(total);
                var newSecondaryTotal = parseFloat(secondaryTotal);
                var calc1 = (newTotal - newSecondaryTotal);
                var calc2 = calc1 / newSecondaryTotal;
                compareTotal = parseFloat(calc2 * 100).toFixed(2);
            }
            
            secondaryTotal = addCommas(secondaryTotal);
            
        }
        
        var appendText = this.model.get("appendText");
        if (!_.isNull(appendText)) {
            total = total + appendText;
            if(compareRange) {
                secondaryTotal = secondaryTotal + appendText;
            }
        }
        
        text = _.isNull(text) || _.isUndefined(text) ? "No Data" : text;
        total = _.isNull(total) || _.isUndefined(total) ? " " : total;
        if (total === "NaN%") {
            total = "0%";
        }
        
        if (!_.isNull(data) && !_.isUndefined(data)) {
            
            //Create Elements
            if (compareRange) {
                var colour;
                if(calcAverage) {
                    colour = compareTotal < 0 ? "green" : "red";
                } else {
                    colour = compareTotal > 0 ? "green" : "red";
                }
                
                var sign = compareTotal < 0 ? "" : "+";
                compareTotal = compareTotal === "NaN" || compareTotal === "Infinity" ? 0 : compareTotal;
                total = total === "NaN" ? 0 : total;
                secondaryTotal = secondaryTotal === "NaN" ? 0 : secondaryTotal;
                
                var htmlString = "<h4>" + text + "</h4>" +
                    "<h2 style='color:" + colour + "'>" + sign + addCommas(compareTotal) + "%</h2>" +
                    "<div class='chartBlockRangeCurrent'></div><h5 style='margin-left: 15px;'>Current: " + addCommas(total) + "</h5>" +
                    "<div class='chartBlockRangePrevious'></div><h5 style='margin-left: 15px; margin-top:-15px;'>Previous " + addCommas(secondaryTotal) + "</h5>";
                $(this.el).html(htmlString);

            } else {
                $(this.el).html("<h4>" + text + "</h4><h2>" + addCommas(total) + "</h2>");
            }
            
            $(this.el).append("<div id='" + chartId + "' class='blockheight'></div>");
            $(this.el).removeClass("d3Block").addClass("d3Block");
            
            $("#" + chartId).dxChart({
                dataSource: data,
                commonSeriesSettings: {
                    argumentField: 'Date',
                    type: 'splineArea',
                    point: {
                        size: 1,
                        hoverStyle: { size: 4 }
                    }
                },
                tooltip: {
                    enabled: true,
                    argumentFormat: 'monthAndYear',
                    customizeText: function (value) {
                        return value.seriesName + ": " + value.value + "<br/>";
                    },
                    font: {
                        size: 14
                    }
                },
                legend: {
                    visible: false
                },
                commonAxisSettings: {
                    grid: { visible: false },
                    label: { visible: false }
                },
                series: seriesArray
            });

        }
    },
    afterRender: function () {
        //hide loader
        $("#" + this.model.attributes.name).prev().hide();
    }
  });

  Sitecore.Factories.createComponent("ChartJsBlock", model, view, ".sc-ChartJsBlock");
});