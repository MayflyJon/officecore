﻿require.config({
    baseUrl: "/sitecore/shell/client/Speak/Assets",
    paths: {
        signalrhubs: "lib/core/deps/signalR-hubs/signalr-init",
        signalr: "lib/core/deps/signalR/jquery.signalR-2.0.3.min"
    },
    shim: {
        'signalrhubs': { deps: ['jquery'], exports: "SignalR" },
        'signalr': { deps: ['jquery'] }
    }
});

define(["sitecore", "underscore", "signalr", "signalrhubs"], function (Sitecore) {
  var model = Sitecore.Definitions.Models.ControlModel.extend({
    initialize: function (options) {
        this._super();
        
        this.set("hubName", null);
        this.set("hub", null);
        this.set("started", false);
        this.set("activeCount", 0);

        var items = [];
        for (i = 0; i < 61; i++) {
            items.push({ second: i, count: 0 });
        }
        this.set("items", items);
        var updateArray = setInterval($.proxy(this.updateArray, this), 1000);
        
        this.on("change:hubName", $.proxy(this.waitForHubs, this));
        
    },
    waitForHubs: function () {
        if ($.connection.visitsHub === undefined) {
            setTimeout(this.waitForHubs, 100);
        } else {
            this.setHub();
        }
    },
    setHub: function () {
        var hubName = this.get("hubName");
        if (hubName === "" || hubName === undefined)
            throw "Missing hubName";
        var hub = $.connection[hubName];
        if (hub === null)
            throw "Could not find hub with name " + hubName;
        hub.timeStamp = new Date();
        this.set("hub", hub);

        this.addClientHandler("broadcastMessage", $.proxy(this.recieveMessage, this));
        this.start();
    },
    addClientHandler: function (broadcast, handler) {
        if (this.get("started"))
            throw "You cannot add a client handler after hub connection started";
        var hub = this.get("hub");
        
        if (!_.isUndefined(hub)) {
            hub.client[broadcast] = handler;
        }
    },
    start: function () {
        if (this.get("started"))
            return;
        var hub = this.get("hub");
        var m = this;
        
        $.connection.hub.logging = true;
        $.connection.hub.start(this).done(function () {
            m.set("server", hub.server);
            m.set("started", true);
            m.trigger("ready");
        });
    },
    recieveMessage: function (message) {

        var visitCount = this.get("activeCount");

        switch (message.Message) {
            case "new":
                visitCount += 1;
                break;
            case "leaving":
                visitCount -= 1;
                break;
            case "first":
                visitCount = message.Value;
                break;
        }

        visitCount = visitCount < 0 ? 0 : visitCount;
        var items = _.clone(this.get("items"));
        for (var i in items) {
            if (items[i].second == 0) {
                items[i].count +=1;
                break; 
            }
        }

        this.set("items", items);
        this.set("activeCount", visitCount);
    },
    updateArray: function () {

        var items = _.clone(this.get("items"));
        var activeCount = this.get("activeCount");

        items.unshift({ second: 0, count: activeCount });
        for (var i = 0; i < 60; i++) {
            items[i].second = i;
        }
        items.pop();

        this.set("items", items);

    }
    
  });

  var view = Sitecore.Definitions.Views.ControlView.extend({
    initialize: function (options) {
        this._super();
        //var hubName = this.$el.attr("data-sc-hubname");
        //this.model.setHub(hubName);
    }
  });

  Sitecore.Factories.createComponent("SignalRVisitsHub", model, view, ".sc-SignalRVisitsHub");
});
