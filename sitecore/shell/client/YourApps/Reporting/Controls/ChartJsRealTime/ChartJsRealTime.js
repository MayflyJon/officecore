﻿require.config({
    baseUrl: "/sitecore/shell/client/Speak/Assets",
    paths: {
        chartjs: "lib/ui/deps/devExtreme/dx.chartjs",
        globalize: "lib/ui/deps/devExtreme/globalize.min"
    },
    shim: {
        'globalize': { exports: "Globalize" },
        'chartjs': { deps: ['jquery', 'underscore', 'globalize'], exports: "DevExpress" }
    }
});

define(["sitecore", "underscore", "chartjs"], function (Sitecore) {
  var model = Sitecore.Definitions.Models.ControlModel.extend({
    initialize: function (options) {
        this._super();

        this.set("items", null);

        this.on("change", $.proxy(this.prepareData, this), this);
        
    },
    prepareData: function () {

        this.chartData = this.get("items");

    }
  });

  var view = Sitecore.Definitions.Views.ControlView.extend({
    initialize: function (options) {
        this._super();
        
        this.model.on("change", $.proxy(this.render, this), this);
    },
    render: function () {

        var title = this.$el.attr("data-sc-chartTitle");
        var valueAxisTitle = this.$el.attr("data-sc-valueAxisTitle");
        var argumentAxisTitle = this.$el.attr("data-sc-argumentAxisTitle");
        var valueField = this.$el.attr("data-sc-valueField");
        var argumentField = this.$el.attr("data-sc-argumentField");
        
        $(this.el).dxChart({
            dataSource: this.model.chartData,
            commonAxisSettings: {
                grid: {
                    opacity: 0.3
                },
                label: {
                    overlappingBehavior: { mode: 'rotate', rotationAngle: -90 }
                }
            },
            commonSeriesSettings: {
                argumentField: argumentField,
                point: {
                    size: 0,
                    hoverStyle: { size: 4 }
                }
            },
            argumentAxis: {
                title: argumentAxisTitle,
                min: 0,
                max: 60,
                valueMarginsEnabled: true
            },
            valueAxis: {
                name: 'VisitAxis',
                position: 'left',
                min: 0,
                minValueMargin: 1,
                maxValueMargin: 1,
                tickInterval: 1,
                title: valueAxisTitle
            },
            legend: {
                visible: false,
            },
            series: {
                type: 'line',
                name: "VisitCount",
                valueField: valueField,
                axis: 'VisitAxis'
            },
            title: title,
            palette: 'analyticsPalette',
            size: {
                height: 250,
            },
            animation: { enabled: false }
        });

    }
  });

  Sitecore.Factories.createComponent("ChartJsRealTime", model, view, ".sc-ChartJsRealTime");
});
