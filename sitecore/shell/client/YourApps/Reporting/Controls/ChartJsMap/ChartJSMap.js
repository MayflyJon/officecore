﻿require.config({
    baseUrl: "/sitecore/shell/client/Speak/Assets",
    paths: {
        chartjsWorld: "lib/ui/deps/devExtreme/world",
        chartjs: "lib/ui/deps/devExtreme/dx.chartjs",
        globalize: "lib/ui/deps/devExtreme/globalize.min"
    },
    shim: {
        'chartjsWorld': { deps: ['jquery', 'chartjs'] },
        'globalize': { exports: "Globalize" },
        'chartjs': { deps: ['jquery', 'underscore', 'globalize'], exports: "DevExpress" }
    }
});

define(["sitecore", "underscore", "jquery", "chartjs", "chartjsWorld"], function (Sitecore) {
  var model = Sitecore.Definitions.Models.ControlModel.extend({
    initialize: function (options) {
        this._super();
        
        this.set("items", null);
        this.set("retryAttempts", null);
        this.set("markers", null);
        this.markerItems = [];
        
        // Container to hold our promises
        this.promises = [];
        
        this.minLon = -135;
        this.maxLon = -65;
        this.minLat = 20;
        this.maxLat = 60;
        
        _.bindAll(this, 'setMap', 'makeRequest', 'successHandler');
        this.on("change:items", this.setMap, this);
    },
    setMap: function () {

        this.markerItems = [];
        var items = this.get("items");
        if (_.isNull(items) || _.isUndefined(items))
            return;

        $.each(items, $.proxy(function (index, value) {

            //Make url
            var url = makeBingRequestUrl(value.Country, value.Region);

            //Set retries
            var retries = this.get("retryAttempts");
            value.Retries = _.isNull(retries) || _.isUndefined(retries) ? 3 : retries;

            //Make the request
            this.promises.push(this.makeRequest(url, value, value.Visits));

        }, this));

        // Apply entire array of promises to a $.when listener
        $.when.apply($, this.promises).then($.proxy(function () {

            //Sort Array
            this.set("markers", null);
            this.set("markers", this.markerItems);

        }, this));

    },
    makeRequest: function (url, requestObject, locationVisits) {
        
        // Create our deferred
        var dfd = $.Deferred();
        
        $.ajax({
            url: url,
            dataType: "jsonp",
            data: {
                key: "AlPXVkGNwPEoywnPfgs8o5za9URuJEUB2oOMBj5z3BGxi9UdnT0MO1I7NnXrsR4K"
            },
            jsonp: "jsonp",
            success: $.proxy(function (data) {
                this.successHandler(data, requestObject, locationVisits);
                //Resolve deffered
                dfd.resolve();
            }, this),
            error: $.proxy(function () {
                //Resolve deffered
                dfd.resolve();
            }, this)
        });
        
        //Return deffered promise
        return dfd.promise();
    },
    successHandler: function (data, requestObject, locationVisits) {

        if (data &&
            data.resourceSets &&
            data.resourceSets.length > 0 &&
            data.resourceSets[0].resources &&
            data.resourceSets[0].resources.length > 0) {
            //Set markers

            var coords = data.resourceSets[0].resources[0].geocodePoints[0].coordinates.toString().split(",");
            
            this.minLon = parseFloat(coords[1]) < this.minLon ? parseFloat(coords[1]) - 30 : this.minLon;
            this.maxLon = parseFloat(coords[1]) > this.maxLon ? parseFloat(coords[1]) + 30 : this.maxLon;
            this.minLat = parseFloat(coords[0]) < this.minLat ? parseFloat(coords[1]) - 30 : this.minLat;
            this.maxLat = parseFloat(coords[0]) > this.maxLat ? parseFloat(coords[1]) + 30 : this.maxLat;
            
            var markerItem = {
                coordinates: [coords[1], coords[0]],
                attributes: { name: data.resourceSets[0].resources[0].name, visits: locationVisits }
            };

            this.markerItems.push(markerItem);
        }
    }
  });

  var view = Sitecore.Definitions.Views.ControlView.extend({
    initialize: function (options) {
      this._super();
      this.model.on('change:markers', this.render, this);

    },
    render: function () {

        var markers = this.model.get("markers");
        var chartId = this.model.attributes.name + "Chart";
        
        //Create Elements
        $(this.el).append("<div id='" + chartId + "'></div>");

        var i = 0;
        
        $("#" + chartId).dxVectorMap({
            markers: markers,
            mapData: world,
            areaSettings: {
                color: 'gainsboro',
                hoveredColor: 'darkgrey'
            },
            markerSettings: {
                customize: function (m) {
                    return {
                        text: m.attributes.name
                    };
                },
                font: {
                    size: 13
                },
                selectedColor: 'dodgerblue'
            },
            tooltip: {
                enabled: true,
                customizeText: function (element) {
                    return element.attribute('visits');
                }
            },
            bounds: {
                minLon: this.model.minLon,
                maxLon: this.model.maxLon,
                minLat: this.model.minLat,
                maxLat: this.model.maxLat
            },
            loadingIndicator: {
                backgroundColor: 'lightcyan',
                font: {
                    weight: 700,
                    size: 16
                }
            }
        });
          
        this.running = false;
      }
  });

  Sitecore.Factories.createComponent("ChartJSMap", model, view, ".sc-ChartJSMap");
});

function makeBingRequestUrl(countryCode, regionCode) {
    //Create Bing Maps REST Services request to geocode the address provided by the user
    return "http://dev.virtualearth.net/REST/v1/Locations/" + countryCode + "/" + regionCode;
}
