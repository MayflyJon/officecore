﻿require(["jasmineEnv"], function (jasmineEnv) {
  var setupTests = function () {
    "use strict";

    describe("Given a ChartJSMap model", function () {
      var component = new Sitecore.Definitions.Models.ChartJSMap();

      describe("when I create a ChartJSMap model", function () {
        it("it should have a 'isVisible' property that determines if the ChartJSMap component is visible or not", function () {
          expect(component.get("isVisible")).toBeDefined();
        });

        it("it should set 'isVisible' to true by default", function () {
          expect(ChartJSMap.get("isVisible")).toBe(true);
        });

        it("it should have a 'toggle' function that either shows or hides the ChartJSMap component depending on the 'isVisible' property", function () {
          expect(component.toggle).toBeDefined();
        });
      });
    });
  };

  runTests(jasmineEnv, setupTests);
});