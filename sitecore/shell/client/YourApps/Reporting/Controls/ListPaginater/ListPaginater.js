﻿define(["sitecore"], function (Sitecore) {
    var model = Sitecore.Definitions.Models.ControlModel.extend({
        initialize: function (options) {
            this._super();

            this.set("items", null);
            this.set("sourceItems", null);
            //this.set("pageSize", 5);
            this.set("startIndex", 0);
            this.set("rowSize", parseInt($('#rowSize option:selected').val()));

            this.on("change:sourceItems change:startIndex change:rowSize", this.prepareData, this);

        },
        prepareData: function () {

            var items = this.get("sourceItems");
            if (_.isNull(items) || _.isUndefined(items))
                return;

            var pageSize =  this.get("rowSize");
            if (_.isNull(pageSize) || _.isUndefined(pageSize))
                pageSize = 5;

            var startIndex = this.get("startIndex");
            var test = parseInt(startIndex) + parseInt(pageSize);

            var finalItems = _.toArray(items).slice(startIndex, parseInt(startIndex) + parseInt(pageSize));

            this.set("items", finalItems);

            $('#pages').remove();
            $('<div id="pages"/>').html('Showing page ' + Math.ceil((startIndex + 1) / pageSize) + ' of ' + Math.ceil(items.length / pageSize)).appendTo($('.controls'));
        }

    });

    var view = Sitecore.Definitions.Views.ControlView.extend({
        events: {
            "click button": "changed",
            "change select": "changeRows"
        },
        initialize: function (options) {
            this._super();
        },
        changeRows: function (event) {
            var textarea = $(event.target);
            var size = textarea[0].value;

            this.model.set("rowSize", parseInt(size));
            this.model.set("startIndex", 0);
        },
        changed: function (event) {
            var textarea = $(event.target);
            var target = textarea[0].dataset.target;

            var startIndex = parseInt(this.model.get("startIndex"));
            var pageSize = parseInt(this.model.get("rowSize"));
            var sourceItems = this.model.get("sourceItems");
            var itemLength = sourceItems.length - 1;

            if (target === "next") {
                if ((startIndex + pageSize) < itemLength) {
                    this.model.set("startIndex", startIndex + pageSize);
                }
            } else if (target === "prev") {
                if (startIndex >= pageSize) {
                    this.model.set("startIndex", startIndex - pageSize);
                }
            } else if (target === "first") {
                this.model.set("startIndex", 0);
            } else if (target === "last") {
                var lastPage = Math.ceil(itemLength / pageSize);
                this.model.set("startIndex", (lastPage - 1) * pageSize);
            }
        }
    });

    Sitecore.Factories.createComponent("ListPaginater", model, view, ".sc-ListPaginater");
});
