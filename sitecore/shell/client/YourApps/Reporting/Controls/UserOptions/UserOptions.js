﻿define(["sitecore"], function (Sitecore) {
    var model = Sitecore.Definitions.Models.ControlModel.extend({
        initialize: function (options) {
            this._super();

            this.open = false;
        }
    });

    var view = Sitecore.Definitions.Views.ControlView.extend({
        events: {
            "click .resetAll": "resetAll",
            "click .optionsMenu": "showMenu",
        },
        initialize: function (options) {
            this._super();
            var self = this;

            $(document).click(function () {
                //$(".optionsMenu ul").css("display", "none");
                //this.open = false;
                $('.optionsMenu ul').each(function () {
                    if ($(this).css('display') == 'block') {
                        $(this).css('display', 'none');
                        self.model.open = false;
                    }
                });
            });
        },
        resetAll: function () {
            localStorage.clear();
            location.reload();
        },
        showMenu: function () {
            event.stopPropagation();

            if (this.model.open) {
                $(".optionsMenu ul").css("display", "none");
            }
            else {
                $(".optionsMenu ul").css("display", "block");
            }

            this.model.open = this.model.open === false;
        },
    });

    Sitecore.Factories.createComponent("UserOptions", model, view, ".sc-UserOptions");
});
