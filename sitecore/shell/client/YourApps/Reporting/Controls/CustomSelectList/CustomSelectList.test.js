﻿require(["jasmineEnv"], function (jasmineEnv) {
  var setupTests = function () {
    "use strict";

    describe("Given a CustomSelectList model", function () {
      var component = new Sitecore.Definitions.Models.CustomSelectList();

      describe("when I create a CustomSelectList model", function () {
        it("it should have a 'isVisible' property that determines if the CustomSelectList component is visible or not", function () {
          expect(component.get("isVisible")).toBeDefined();
        });

        it("it should set 'isVisible' to true by default", function () {
          expect(CustomSelectList.get("isVisible")).toBe(true);
        });

        it("it should have a 'toggle' function that either shows or hides the CustomSelectList component depending on the 'isVisible' property", function () {
          expect(component.toggle).toBeDefined();
        });
      });
    });
  };

  runTests(jasmineEnv, setupTests);
});