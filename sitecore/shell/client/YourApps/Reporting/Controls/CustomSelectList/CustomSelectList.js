﻿define(["sitecore"], function (Sitecore) {
  var model = Sitecore.Definitions.Models.ControlModel.extend({
    initialize: function (options) {
        this._super();
        
        this.set("items", []);
        this.set("selectedItems", []);
        this.set("selectedItem", null);
        this.set("selectedItemId", null);
        this.set("displayFieldName", null);
        this.set("valueFieldName", null);
        this.set("selectedValue", null);
        
    }
  });

  var view = Sitecore.Definitions.Views.ControlView.extend({
    initialize: function (options) {
        this._super();

        this.model.on("change", this.render, this);
        
        //Set text value on change
        $(this.el).on("change", $.proxy(function () {

            this.model.set("selectedValue", $(this.el).val());

        }, this));
    },
    render: function () {
        
        var items = this.model.get("items");
        if (!items || items.length == 0)
            return;

        //Append all option
        $(this.el).append("<option value=''>All</option>");

        $.each(items, $.proxy(function (index, value) {

            //Get fields
            var itemValue = value.FacetId;
            var itemDisplay = value.FacetName;

            //Append html
            $(this.el).append("<option value='" + itemValue + "'>" + itemDisplay + "</option>");

        }, this));
        
    }
  });

  Sitecore.Factories.createComponent("CustomSelectList", model, view, ".sc-CustomSelectList");
});
