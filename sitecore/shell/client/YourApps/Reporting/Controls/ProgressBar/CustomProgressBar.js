﻿require.config({
    baseUrl: "/sitecore/shell/client/Speak/Assets",
    paths: {
        nprogress: "lib/ui/deps/nprogress/nprogress"
    },
    shim: {
        'nprogress': { deps: ['jquery'], exports: "NProgress" }
    }
});

define(["sitecore", "jquery", "nprogress"], function (Sitecore) {
  var model = Sitecore.Definitions.Models.ControlModel.extend({
    initialize: function (options) {
        this._super();
                
    }
  });

  var view = Sitecore.Definitions.Views.ControlView.extend({
    initialize: function (options) {
      this._super();
    }
  });

  Sitecore.Factories.createComponent("CustomProgressBar", model, view, ".sc-CustomProgressBar");
});
