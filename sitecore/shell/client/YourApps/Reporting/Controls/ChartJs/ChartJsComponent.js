﻿require.config({
    baseUrl: "/sitecore/shell/client/Speak/Assets",
    paths: {
        chartjs: "lib/ui/deps/devExtreme/dx.chartjs",
        globalize: "lib/ui/deps/devExtreme/globalize.min"
    },
    shim: {
        'globalize': { exports: "Globalize" },
        'chartjs': { deps: ['jquery', 'underscore', 'globalize'], exports: "DevExpress" }
    }
});

define(["sitecore", "underscore", "chartjs"], function (Sitecore) {
    var model = Sitecore.Definitions.Models.ControlModel.extend({
        initialize: function (options) {
            this._super();

            //Make the items null
            this.set("items", null);
            this.set("compareRange", null);
            this.set("title", "");
            this.set("selectedFilters", null);
            this.set("selectedSeries", null);

            //Listen for changes to data
            this.on("change:items change:compareRange change:selectedFilters change:selectedSeries", this.prepareData, this);
        },
        prepareData: function () {
            var results = this.get("items");
            if (!results)
                return;

            var filtersArray = this.get("selectedFilters");
            if (!filtersArray)
                return;
            var filters = _.pluck(filtersArray, 'value');

            var validFilters = _.filter(results, $.proxy(function (item) {
                var test = ($.inArray(item.trafficType, filters) > -1);
                return ($.inArray(item.trafficType, filters) > -1);
            }, this));


            var compareRange = this.get("compareRange");

            var chartResults = [];

            var mergedResults = [];
            $.each(validFilters, function (index, value) {

                var filterObject = _.filter(filtersArray, function (item) {
                    return item.value === value.trafficType;
                });
                if (!_.isNull(filterObject[0]) && !_.isUndefined(filterObject[0]))
                {
                    var filterName = filterObject[0].name;

                    $.each(value.results, function (resultsIndex, resultsValue) {

                        var existingDate = findIndexByKeyValue(chartResults, 'Date', resultsValue.Date);
                        if (!_.isNull(existingDate)) {

                            chartResults[existingDate][filterName + "Visits"] = resultsValue.Visits;
                            chartResults[existingDate][filterName + "Value"] = resultsValue.Value;
                            
                            if(compareRange) {
                                chartResults[existingDate][filterName + "VisitsSecondary"] = resultsValue[filterName + "VisitsSecondary"];
                                chartResults[existingDate][filterName + "ValueSecondary"] = resultsValue[filterName + "ValueSecondary"];
                            }

                        }
                        else {
                            var newObject = {
                                Date: resultsValue.Date
                            };
                            newObject[filterName + "Visits"] = resultsValue.Visits;
                            newObject[filterName + "Value"] = resultsValue.Value;
                            
                            if (compareRange) {
                                newObject[filterName + "VisitsSecondary"] = resultsValue[filterName + "VisitsSecondary"];
                                newObject[filterName + "ValueSecondary"] = resultsValue[filterName + "ValueSecondary"];
                            }
                            chartResults.push(newObject);
                        }
                    });

                }

            });

            //Set the chart
            this.chartData = chartResults;
            this.compareRange = this.get("compareRange");
        }
    });

    var view = Sitecore.Definitions.Views.ControlView.extend({
        initialize: function (options) {
            this._super();

            this.listenTo(this.model, "change", this.render);
            

        },
        render: function () {

            var newData = this.model.chartData;
            if (_.isNull(newData) || _.isUndefined(newData))
                return;

            var filtersArray = this.model.get("selectedFilters");
            if (!filtersArray)
                return;

            var selectedSeries = this.model.get("selectedSeries");
            if (!selectedSeries)
                return;

            //Reformat dates
            $.each(newData, function (index, value) {

                var date = moment(value.Date, "YYYYMMDD");
                var dateTest = date.format("DD MMM YY");
                if (dateTest != "Invalid date") {
                    value.Date = date.format("DD MMM YY");
                } 

            });

            var compareRange = this.model.compareRange;
            var legendId = this.model.attributes.name + "legend";
            var chartId = this.model.attributes.name + "Chart";
            var title = this.model.get("title");

            var seriesArray = [];
            var axisArray = [];
            var panes = [];
            var chartParams;

            var visitsAxis = {
                name: 'visitsAxis',
                min: 0
            };
            var valueAxis = {
                name: 'valueAxis',
                position: 'right',
                min: 0
            };
            axisArray.push(visitsAxis);
            axisArray.push(valueAxis);

            $.each(filtersArray, function (index, value) {

                $.each(selectedSeries, function (innerIndex, innerValue) {

                    var axis = innerValue === 'Visits' ? 'visitsAxis' : 'valueAxis';
                    var type = innerValue === 'Visits' ? 'spline' : 'splinearea';
                        
                    var filterSeries = {
                        type: type,
                        name: value.name + " " + innerValue,
                        valueField: value.name + innerValue,
                        axis: axis
                    };
                    seriesArray.push(filterSeries);
                        
                    if (!_.isNull(compareRange) && !_.isUndefined(compareRange) && compareRange === true) {

                        var secondaryFilterSeries = {
                            type: type,
                            name: value.name + " " + innerValue + " Secondary",
                            valueField: value.name + innerValue + "Secondary",
                            axis: axis
                        };
                        seriesArray.push(secondaryFilterSeries);
                    }

                });

            });

            chartParams = {
                dataSource: newData,
                commonAxisSettings: {
                    grid: {
                        opacity: 0.3
                    },
                    label: {
                        overlappingBehavior: { mode: 'rotate', rotationAngle: -90 }
                    }
                },
                commonSeriesSettings: {
                    argumentField: 'Date',
                    point: {
                        size: 6,
                        hoverStyle: { size: 4 }
                    }
                },
                panes: {
                    name: 'bottomPane'
                },
                valueAxis: axisArray,
                tooltip: {
                    enabled: true,
                    shared: true,
                    argumentFormat: 'monthAndYear',
                    customizeText: function (value, test) {
                        return "<b>" + value.argumentText + "</b><br/>" +
                            value.seriesName + ": " + value.value + "<br/>";
                        //value.points[2].seriesName + ": " + value.points[2].value;
                    },
                    font: {
                        size: 14
                    }
                },
                legend: {
                    visible: true,
                    verticalAlignment: "top",
                    horizontalAlignment: "center",
                    paddingTopBottom: 0,
                    itemTextPosition: 'right'

                },
                series: seriesArray,
                title: title,
                palette: 'analyticsPalette',
                size: {
                    height: 250,
                }
            };

            $(this.el).dxChart(chartParams);

        }
    });

    Sitecore.Factories.createComponent("ChartJs", model, view, ".sc-ChartJs");
});
