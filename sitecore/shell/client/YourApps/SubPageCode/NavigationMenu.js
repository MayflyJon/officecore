﻿define(["sitecore", "jquery", "underscore"], function (Sitecore) {
    var NavigationMenu = Sitecore.Definitions.App.extend({
        initialized: function () {
            
            var bodies = $(".sc-accordion ").get();

            for (index = 0; index < bodies.length; ++index) {

                var item = bodies[index];
                var buttonGroup = $(item).find(".sc-hyperlinkbuttonsgroup")[0];
                var buttonHtml = buttonGroup.innerHTML;
                var liIndex = buttonHtml.indexOf("li");
                if (liIndex < 0) {

                    var chevron = $(item).find(".sc-accordion-header-chevron")[0];
                    chevron.style.display = 'none';
                    $(chevron).parent().append("<div style=\"width:10px;\"></div>");
                }

            };

      }
  });
    
    return NavigationMenu;
   
});


