﻿require.config({
    baseUrl: "/sitecore/shell/client/Speak/Assets",
    paths: {
        nprogress: "lib/ui/deps/nprogress/nprogress",
        analytics: "lib/core/deps/sitecoreAnalytics/analytics"
    },
    shim: {
        'nprogress': { deps: ['jquery'], exports: "NProgress" },
        'analytics': { deps: ['jquery', 'nprogress'] }
    }
});

define(["sitecore", "jquery", "underscore", "analytics"], function (Sitecore) {
    var LanguageSubPage = Sitecore.Definitions.App.extend({
        initialized: function () {
            this.insertRendering("{12514198-F1D6-4D6D-AE40-1E9B51B51B89}", { $el: $(".sc-applicationContent-navigation") }, $.proxy(function (subApp) {

                var bodies = $(".sc-accordion ").get();

                for (index = 0; index < bodies.length; ++index) {

                    var item = bodies[index];
                    var buttonGroup = $(item).find(".sc-hyperlinkbuttonsgroup")[0];
                    var buttonHtml = buttonGroup.innerHTML;
                    var liIndex = buttonHtml.indexOf("li");

                    if (liIndex < 0) {
                        var chevron = $(item).find(".sc-accordion-header-chevron")[0];
                        chevron.style.display = 'none';
                        $(chevron).parent().append("<div style=\"width:10px;\"></div>");
                    }
                    toggleAccordion();

                };

            }, this));

            this.PieValueSelect.on("change", function () {
                var rawItem = this.PieValueSelect.get("selectedItem");

                if (rawItem != null) {
                    this.LanguagePie.set("valueField", $.trim(rawItem.itemName));
                }
                else {
                    // Fall back to Value field if not set
                    this.LanguagePie.set("valueField", "Value");
                }
            }, this);

            var items = this.PieValueSelect.get("items");
            // TODO: temporary fix for 7.1 - 7.2 update 2 differences
            if (_.isEmpty(items)) {
                this.PieValueSelect.set("isVisible", false);
            }
            else {
                this.LanguagePie.set("valueField", $.trim(items[0].itemName));
            }
        },
    });

    return LanguageSubPage;
});
