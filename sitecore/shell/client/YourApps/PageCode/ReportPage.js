﻿require.config({
    baseUrl: "/sitecore/shell/client/Speak/Assets",
    paths: {
        nprogress: "lib/ui/deps/nprogress/nprogress",
        analytics: "lib/core/deps/sitecoreAnalytics/analytics",
    },
    shim: {
        'nprogress': { deps: ['jquery'], exports: "NProgress" },
        'analytics': { deps: ['jquery', 'nprogress'] }
    }
});

define(["sitecore", "jquery", "underscore", "analytics"], function (Sitecore) {
    var StartPage = Sitecore.Definitions.App.extend({
        initialized: function () {

            this.ChartData2.on("change:items", $.proxy(function() {

                this.ChartFilterControl1.trigger("change");
            }, this));

            this.VisitsChartBlock.on("change:total", $.proxy(function () {
                var total = this.VisitsChartBlock.get("total");
                if (!_.isUndefined(total) && !_.isNull(total)) {
                    this.TotalVisits.set("text", addCommas(total));
                }

            }, this));
            
            this.VisitorChartBlock.on("change:total", $.proxy(function () {
                var total = this.VisitorChartBlock.get("total");
                if (!_.isUndefined(total) && !_.isNull(total)) {
                    this.TotalVisitors.set("text", addCommas(total));
                }

            }, this));
            
            this.ValueChartBlock.on("change:total", $.proxy(function () {
                var total = this.ValueChartBlock.get("total");
                if (!_.isUndefined(total) && !_.isNull(total)) {
                    this.TotalValue.set("text", addCommas(total));
                }

            }, this));

            //Handle user clicking on goal
            this.TopGoalsDataGrid.on("change:selectedItem", function () {

                var rawGoalItem = this.TopGoalsDataGrid.get("selectedItem");
                if (rawGoalItem) {
                    var goalId = rawGoalItem.ID;

                    window.location.href = '/sitecore/client/Your Apps/Reporting/Pages/GoalPage?goal=' + goalId;
                }

            }, this);

            //Handle user clicking on campaign
            this.TopCampaignsDataGrid.on("change:selectedItem", function () {

                var rawCampaignItem = this.TopCampaignsDataGrid.get("selectedItem");
                if (rawCampaignItem) {
                    var campaignId = rawCampaignItem.ID;

                    window.location.href = '/sitecore/client/Your Apps/Reporting/Pages/CampaignPage?campaign=' + campaignId;
                }

            }, this);

            this.insertRendering("{12514198-F1D6-4D6D-AE40-1E9B51B51B89}", { $el: $(".sc-applicationContent-navigation") }, $.proxy(function (subApp) {

                var bodies = $(".sc-accordion ").get();

                for (index = 0; index < bodies.length; ++index) {

                    var item = bodies[index];
                    var buttonGroup = $(item).find(".sc-hyperlinkbuttonsgroup")[0];
                    var buttonHtml = buttonGroup.innerHTML;
                    var liIndex = buttonHtml.indexOf("li");

                    if (liIndex < 0) {
                        var chevron = $(item).find(".sc-accordion-header-chevron")[0];
                        chevron.style.display = 'none';
                        $(chevron).parent().append("<div style=\"width:10px;\"></div>");
                    }
                    toggleAccordion();

                };

            }, this));

            this.prepareData();

            $('div[data-sc-id="ChartBarRow"').append("<div class='clearfix'></div>");
        },
        prepareData: function () {
            // ChartBarRow
            $("div[data-sc-id='ChartBarRow']").attr("data-col", "1");
            $("div[data-sc-id='ChartBarRow']").attr("data-row", "1"); // TODO: Need to get row from cookie/localStorage/db/something else
            $("div[data-sc-id='ChartBarRow']").attr("data-sizex", "6");
            $("div[data-sc-id='ChartBarRow']").attr("data-sizey", "3");
            // TopGoalsContainer
            $("div[data-sc-id='TopGoalsContainer']").attr("data-col", "1");
            $("div[data-sc-id='TopGoalsContainer']").attr("data-row", "4");
            $("div[data-sc-id='TopGoalsContainer']").attr("data-sizex", "6");
            $("div[data-sc-id='TopGoalsContainer']").attr("data-sizey", "7");
            // TopCampaignsContainer
            $("div[data-sc-id='TopCampaignsContainer']").attr("data-col", "1");
            $("div[data-sc-id='TopCampaignsContainer']").attr("data-row", "10");
            $("div[data-sc-id='TopCampaignsContainer']").attr("data-sizex", "6");
            $("div[data-sc-id='TopCampaignsContainer']").attr("data-sizey", "7");
            
        },

    });


    return StartPage;

});

function getGridLocations(name) {
    var n = name + "=";
    var cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
        var c = cookies[i].trim();
        if (c.indexOf(n) == 0) {
            return JSON.parse(c.substring(n.length, c.length));
        }
    }

    return [{ "id": "1", "col": 1, "row": 1, "size_x": 1, "size_y": 1 }, { "id": "2", "col": 2, "row": 1, "size_x": 1, "size_y": 1 }, { "id": "3", "col": 3, "row": 1, "size_x": 1, "size_y": 1 }, { "id": "4", "col": 4, "row": 1, "size_x": 1, "size_y": 1 }, { "id": "5", "col": 5, "row": 1, "size_x": 1, "size_y": 1 }, { "id": "6", "col": 6, "row": 1, "size_x": 1, "size_y": 1 }];
}
