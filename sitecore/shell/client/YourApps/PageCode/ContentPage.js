﻿define(["sitecore", "jquery", "underscore", "analytics"], function (Sitecore) {
    var ContentPage = Sitecore.Definitions.App.extend({
        initialized: function () {
            //this.ChartBarSelect.on("change", function () {
            //    var rawItem = this.ChartBarSelect.get("selectedItem");
            //    this.ChartBarData.set("sortColumn", $.trim(rawItem.itemName));
            //}, this);
            var self = this;
            this.ProfileMatchReportData.on("change:items", $.proxy(function() {
                var profileItems = this.ProfileMatchReportData.get("items");
                var profileVisits = _(profileItems).pluck('Visits');
                var profileVisitTotal = _.reduce(profileVisits, function(memo, num) {
                    return memo + num;
                }, 0);
                this.TotalProfiles.set("text", addCommas(profileVisitTotal));
                $("div[data-sc-id='ProfileTableSelectorColumn']").empty();
                
                var profilePagesItems = this.ProfileTopPagesData.get("items");
                if (!_.isNull(profilePagesItems) && !_.isUndefined(profilePagesItems) && profilePagesItems.length > 0) {
                    this.profileReport(profileItems[0].ProfileName);
                }
                
                $.each(profileItems, $.proxy(function (index, value) {
                    
                    var sb = new StringBuilder();
                    sb.append("<button type='button');'>");
                    sb.append(value['ProfileName']);
                    sb.append("</button>");

                    var button = $(sb.toString());

                    button.on("click", self.setProfile.bind(self));
                    
                    $("div[data-sc-id='ProfileTableSelectorColumn']").append(button);
                    
                    }, this));
            }, this));
            
            this.PatternCardMatchData.on("change:items", $.proxy(function () {
                var patternItems = this.PatternCardMatchData.get("items");
                var patternVisits = _(patternItems).pluck('Visits');
                var patternVisitTotal = _.reduce(patternVisits, function (memo, num) {
                    return memo + num;
                }, 0);
                this.TotalPatternCards.set("text", addCommas(patternVisitTotal));
                
                $("div[data-sc-id='PatternsTableSelectorColumn']").empty();
                
                var patternPagesItems = this.PatternTopPagesData.get("items");
                if (!_.isNull(patternPagesItems) && !_.isUndefined(patternPagesItems) && patternPagesItems.length > 0) {
                    this.patternReport(patternPagesItems[0].patternlabel);
                }

                $.each(patternItems, $.proxy(function (index, value) {

                    var sb = new StringBuilder();
                    sb.append("<button type='button');'>");
                    sb.append(value['PatternLabel']);
                    sb.append("</button>");

                    var button = $(sb.toString());

                    button.on("click", self.setPattern.bind(self));

                    $("div[data-sc-id='PatternsTableSelectorColumn']").append(button);

                }, this));
            }, this));
                        
            this.TotalProfilesPatternsData.on("change:items", $.proxy(function () {
                var totalItems = this.TotalProfilesPatternsData.get("items");
                var totalVisits = _(totalItems).pluck('Visits');
                var totalVisitTotal = _.reduce(totalVisits, function (memo, num) {
                    return memo + num;
                }, 0);
                this.TotalVisits.set("text", addCommas(totalVisitTotal));
                
                var totalVisitors = _(totalItems).pluck('Visitors');
                var totalVisitorTotal = _.reduce(totalVisitors, function (memo, num) {
                    return memo + num;
                }, 0);
                this.TotalVisitors.set("text", addCommas(totalVisitorTotal));

                var totalProfiles = _(totalItems).pluck('Profiles');
                var totalProfilesTotal = _.reduce(totalProfiles, function (memo, num) {
                    return memo + num;
                }, 0);
                
                var totalPatterns = _(totalItems).pluck('Patterns');
                var totalPatternsTotal = _.reduce(totalPatterns, function (memo, num) {
                    return memo + num;
                }, 0);

                var totalProfilePieObject = [{
                        VisitType: 'No Profile',
                        Visits: (totalVisitTotal - totalProfilesTotal)
                    },
                    {
                        VisitType: 'Profile',
                        Visits: totalProfilesTotal
                    }];
                this.VisitsAgainstProfilesPie.set("items", totalProfilePieObject);
                
                var totalPatternPieObject = [{
                        VisitType: 'No Pattern',
                        Visits: (totalVisitTotal - totalPatternsTotal)
                    },
                    {
                        VisitType: 'Pattern',
                        Visits: totalProfilesTotal
                    }];
                this.VisitsAgainstPatternsPie.set("items", totalPatternPieObject);
                
            }, this));

            
            this.ProfileTopPagesData.on("change:items", $.proxy(function () {
                
                var profileItems = this.ProfileMatchReportData.get("items");
                if (!_.isNull(profileItems) && !_.isUndefined(profileItems) && profileItems.length > 0) {
                    this.profileReport(profileItems[0].ProfileName);
                }
                
                
            }, this));
            
            this.PatternTopPagesData.on("change:items", $.proxy(function () {
                
                var patternPagesItems = this.PatternTopPagesData.get("items");
                if (!_.isNull(patternPagesItems) && !_.isUndefined(patternPagesItems) && patternPagesItems.length > 0) {
                    this.patternReport(patternPagesItems[0].patternlabel);
                }

            }, this));
            
            this.insertRendering("{12514198-F1D6-4D6D-AE40-1E9B51B51B89}", { $el: $(".sc-applicationContent-navigation") }, $.proxy(function (subApp) {

                var bodies = $(".sc-accordion ").get();

                for (index = 0; index < bodies.length; ++index) {

                    var item = bodies[index];
                    var buttonGroup = $(item).find(".sc-hyperlinkbuttonsgroup")[0];
                    var buttonHtml = buttonGroup.innerHTML;
                    var liIndex = buttonHtml.indexOf("li");
                    if (liIndex < 0) {
                        
                        var chevron = $(item).find(".sc-accordion-header-chevron")[0];
                        chevron.style.display = 'none';
                        $(chevron).parent().append("<div style=\"width:10px;\"></div>");
                    }
                    toggleAccordion();
                };
                
            }, this));
            
        },
        setProfile: function (profileName) {
            var app = this;

            this.profileReport(profileName.currentTarget.innerText);
        },
        profileReport: function (profile) {
            
            if (_.isNull(profile) || profile === "")
                return;
            
            var profileItems = this.ProfileTopPagesData.get("items");
            if (_.isNull(profileItems) || _.isUndefined(profileItems))
                return;

            var filteredProfileItems = _.filter(profileItems, function(item) {
                return item.profilename === profile;
            });
            this.ProfileTable.set("items", filteredProfileItems.slice(0, 10));

        },
        setPattern: function (patternName) {
            var app = this;

            this.patternReport(patternName.currentTarget.innerText);
        },
        patternReport: function (pattern) {

            if (_.isNull(pattern) || pattern === "")
                return;

            var patternItems = this.PatternTopPagesData.get("items");
            if (_.isNull(patternItems) || _.isUndefined(patternItems))
                return;

            var filteredPatternItems = _.filter(patternItems, function (item) {
                return item.patternlabel === pattern;
            });
            this.PatternTable.set("items", filteredPatternItems.slice(0, 10));

        }
    });
    
    return ContentPage;

});
