﻿require.config({
    baseUrl: "/sitecore/shell/client/Speak/Assets",
    paths: {
        nprogress: "lib/ui/deps/nprogress/nprogress",
        analytics: "lib/core/deps/sitecoreAnalytics/analytics"
    },
    shim: {
        'nprogress': { deps: ['jquery'], exports: "NProgress" },
        'analytics': { deps: ['jquery', 'nprogress'] }
    }
});

define(["sitecore", "jquery", "underscore", "analytics"], function (Sitecore) {
    var SubPage = Sitecore.Definitions.App.extend({
        initialized: function () {
            var self = this;

            this.insertRendering("{12514198-F1D6-4D6D-AE40-1E9B51B51B89}", { $el: $(".sc-applicationContent-navigation") }, $.proxy(function (subApp) {

                var bodies = $(".sc-accordion ").get();
                 
                for (index = 0; index < bodies.length; ++index) {

                    var item = bodies[index];
                    var buttonGroup = $(item).find(".sc-hyperlinkbuttonsgroup")[0];
                    var buttonHtml = buttonGroup.innerHTML;
                    var liIndex = buttonHtml.indexOf("li");
                    if (liIndex < 0) {
                        
                        var chevron = $(item).find(".sc-accordion-header-chevron")[0];
                        chevron.style.display = 'none';
                        $(chevron).parent().append("<div style=\"width:10px;\"></div>");
                    }
                    toggleAccordion();

                };
                
            }, this));

            this.PatternTopPagesData.on("change:items", $.proxy(function () {
                var patternPagesItems = this.PatternTopPagesData.get("items");
                if (!_.isNull(patternPagesItems) && !_.isUndefined(patternPagesItems) && patternPagesItems.length > 0) {
                    this.patternReport(patternPagesItems[0].patternlabel);
                    this.setVisibility(true);
                } else {
                    this.setVisibility(false);
                }
            }, this));

            this.PatternCardMatchData.on("change:items", $.proxy(function () {
                var patternItems = this.PatternCardMatchData.get("items");

                $("div[data-sc-id='PatternsTableSelectorColumn']").empty();

                var patternPagesItems = this.PatternTopPagesData.get("items");
                if (!_.isNull(patternPagesItems) && !_.isUndefined(patternPagesItems) && patternPagesItems.length > 0) {
                    this.patternReport(patternPagesItems[0].patternlabel);
                    this.setVisibility(true);
                } else {
                    this.setVisibility(false);
                }

                $.each(patternItems, $.proxy(function (index, value) {

                    var sb = new StringBuilder();
                    sb.append("<button type='button');'>");
                    sb.append(value['PatternLabel']);
                    sb.append("</button>");

                    var button = $(sb.toString());

                    button.on("click", self.setPattern.bind(self));

                    $("div[data-sc-id='PatternsTableSelectorColumn']").append(button);

                }, this));
            }, this));
            
        },
        setVisibility: function(isVisible) {
            this.NoData.set("isVisible", !isVisible);
            if (isVisible) {
                $("div[data-sc-id='PatternsTableSelectorColumn']").show();
                $("div[data-sc-id='PatternTable']").show();
            } else {
                $("div[data-sc-id='PatternsTableSelectorColumn']").hide();
                $("div[data-sc-id='PatternTable']").hide();
            }
        },
        setPattern: function (patternName) {
            var app = this;

            this.patternReport(patternName.currentTarget.innerText);
        },
        patternReport: function (pattern) {

            if (_.isNull(pattern) || pattern === "")
                return;

            var patternItems = this.PatternTopPagesData.get("items");
            if (_.isNull(patternItems) || _.isUndefined(patternItems))
                return;

            var filteredPatternItems = _.filter(patternItems, function (item) {
                return item.patternlabel === pattern;
            });
            this.PatternTable.set("items", filteredPatternItems);

        }
    });
    
    return SubPage;

});
