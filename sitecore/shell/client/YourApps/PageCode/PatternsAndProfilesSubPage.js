﻿require.config({
    baseUrl: "/sitecore/shell/client/Speak/Assets",
    paths: {
        nprogress: "lib/ui/deps/nprogress/nprogress",
        analytics: "lib/core/deps/sitecoreAnalytics/analytics"
    },
    shim: {
        'nprogress': { deps: ['jquery'], exports: "NProgress" },
        'analytics': { deps: ['jquery', 'nprogress'] }
    }
});

define(["sitecore", "jquery", "underscore", "analytics"], function (Sitecore) {
    var SubPage = Sitecore.Definitions.App.extend({
        initialized: function () {
            
            this.insertRendering("{12514198-F1D6-4D6D-AE40-1E9B51B51B89}", { $el: $(".sc-applicationContent-navigation") }, $.proxy(function (subApp) {

                var bodies = $(".sc-accordion ").get();

                for (index = 0; index < bodies.length; ++index) {

                    var item = bodies[index];
                    var buttonGroup = $(item).find(".sc-hyperlinkbuttonsgroup")[0];
                    var buttonHtml = buttonGroup.innerHTML;
                    var liIndex = buttonHtml.indexOf("li");
                    if (liIndex < 0) {
                        
                        var chevron = $(item).find(".sc-accordion-header-chevron")[0];
                        chevron.style.display = 'none';
                        $(chevron).parent().append("<div style=\"width:10px;\"></div>");
                    }
                    toggleAccordion();

                };
                
            }, this));

            this.TotalProfilesPatternsData.on("change:items", $.proxy(function () {
                var totalItems = this.TotalProfilesPatternsData.get("items");
                var totalVisits = _(totalItems).pluck('Visits');
                var totalVisitTotal = _.reduce(totalVisits, function (memo, num) {
                    return memo + num;
                }, 0);
                this.TotalVisits.set("text", addCommas(totalVisitTotal));

                var totalVisitors = _(totalItems).pluck('Visitors');
                var totalVisitorTotal = _.reduce(totalVisitors, function (memo, num) {
                    return memo + num;
                }, 0);
                this.TotalVisitors.set("text", addCommas(totalVisitorTotal));

                var totalProfiles = _(totalItems).pluck('Profiles');
                var totalProfilesTotal = _.reduce(totalProfiles, function (memo, num) {
                    return memo + num;
                }, 0);
                this.TotalProfiles.set("text", addCommas(totalProfilesTotal));
                
                var totalPatterns = _(totalItems).pluck('Patterns');
                var totalPatternsTotal = _.reduce(totalPatterns, function (memo, num) {
                    return memo + num;
                }, 0);
                this.TotalPatternCards.set("text", addCommas(totalPatternsTotal));
                
                var totalProfilePieObject = [{
                    VisitType: 'No Profile',
                    Visits: (totalVisitTotal - totalProfilesTotal)
                },
                    {
                        VisitType: 'Profile',
                        Visits: totalProfilesTotal
                    }];
                this.VisitsAgainstProfilesPie.set("items", totalProfilePieObject);

                var totalPatternPieObject = [{
                    VisitType: 'No Pattern',
                    Visits: (totalVisitTotal - totalPatternsTotal)
                },
                    {
                        VisitType: 'Pattern',
                        Visits: totalProfilesTotal
                    }];
                this.VisitsAgainstPatternsPie.set("items", totalPatternPieObject);

            }, this));

            $('li[data-tab-id="{BFD59846-242E-421F-9952-0BFD62567723}"]').click(function() {
                var pieChart = $('#ChartJsStackedBar1').dxChart('instance');
                var renderOptions = {
                    force: true,
                    animate: false,
                    asyncSeriesRendering: false
                };
                pieChart.render(renderOptions);
            });

            
        }
    });
    
    return SubPage;

});
