﻿require.config({
    baseUrl: "/sitecore/shell/client/Speak/Assets",
    paths: {
        nprogress: "lib/ui/deps/nprogress/nprogress",
        analytics: "lib/core/deps/sitecoreAnalytics/analytics"
    },
    shim: {
        'nprogress': { deps: ['jquery'], exports: "NProgress" },
        'analytics': { deps: ['jquery', 'nprogress'] }
    }
});

define(["sitecore", "analytics"], function (Sitecore) {
  var BounceRateListPage = Sitecore.Definitions.App.extend({
      initialized: function () {

          this.set("fromDate", null);
          this.set("toDate", null);

          this.on("change:fromDate change:toDate", this.resetDates, this);
      },
      resetDates: function () {

          this.BounceRateFromDate.set("text", this.get("fromDate"));
          this.BounceRateToDate.set("text", this.get("toDate"));
          
      }
  });
    
  return BounceRateListPage;
});
