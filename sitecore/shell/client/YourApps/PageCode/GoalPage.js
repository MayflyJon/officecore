﻿require.config({
    baseUrl: "/sitecore/shell/client/Speak/Assets",
    paths: {
        nprogress: "lib/ui/deps/nprogress/nprogress",
        analytics: "lib/core/deps/sitecoreAnalytics/analytics"
    },
    shim: {
        'nprogress': { deps: ['jquery'], exports: "NProgress" },
        'analytics': { deps: ['jquery', 'nprogress'] }
    }
});

define(["sitecore", "jquery", "underscore", "analytics"], function (Sitecore) {
  var GoalPage = Sitecore.Definitions.App.extend({
      initialized: function () {
          
          //Get Goal GUID from querystring
          var guid = loadPageVar("goal");
          
          this.set("Goal", null);
          this.GoalSplitVisitsData.set("filters", "&campaignId=" + guid);

          this.setData(guid);


          this.insertRendering("{12514198-F1D6-4D6D-AE40-1E9B51B51B89}", { $el: $(".sc-applicationContent-navigation") }, $.proxy(function (subApp) {

              var bodies = $(".sc-accordion ").get();

              for (index = 0; index < bodies.length; ++index) {

                  var item = bodies[index];
                  var buttonGroup = $(item).find(".sc-hyperlinkbuttonsgroup")[0];
                  var buttonHtml = buttonGroup.innerHTML;
                  var liIndex = buttonHtml.indexOf("li");
                  if (liIndex < 0) {

                      var chevron = $(item).find(".sc-accordion-header-chevron")[0];
                      chevron.style.display = 'none';
                      $(chevron).parent().append("<div style=\"width:10px;\"></div>");
                  }
                  toggleAccordion();

              };

          }, this));
      },
      setData: function (guid) {
          
          //Attempt to get the goal item
          RetrieveSimpleItem(guid, "master", $.proxy(function (goalItem) {

              if(goalItem) {
                  this.set("Goal", goalItem);
                  this.GoalIcon.set("imageUrl", goalItem["$mediaurl"]);
                  this.GoalTitle.set("text", goalItem.itemName.replace(/\b./g, function (m) { return m.toUpperCase(); }));
                  this.GoalEVP.set("text", "Points: " + goalItem["Points"]);
                  this.GoalDescription.set("text", goalItem["Description"]);
                  this.GoalDetailListTitle.set("text", goalItem.itemName.replace(/\b./g, function (m) { return m.toUpperCase(); }));
              }

          },this));
      }
  });
    
  function loadPageVar(sVar) {
      return unescape(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + escape(sVar).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
  }

  return GoalPage;
});
