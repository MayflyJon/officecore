﻿define(["sitecore", "jquery", "underscore", "/-/speak/v1/client/analytics.js"], function (Sitecore) {
    var SubPage = Sitecore.Definitions.App.extend({
        initialized: function () {

            this.insertRendering("{12514198-F1D6-4D6D-AE40-1E9B51B51B89}", { $el: $(".sc-applicationContent-navigation") }, $.proxy(function (subApp) {

                var bodies = $(".sc-accordion ").get();

                for (index = 0; index < bodies.length; ++index) {

                    var item = bodies[index];
                    var buttonGroup = $(item).find(".sc-hyperlinkbuttonsgroup")[0];
                    var buttonHtml = buttonGroup.innerHTML;
                    var liIndex = buttonHtml.indexOf("li");

                    if (liIndex < 0) {
                        var chevron = $(item).find(".sc-accordion-header-chevron")[0];
                        chevron.style.display = 'none';
                        $(chevron).parent().append("<div style=\"width:10px;\"></div>");
                    }

                };

            }, this));

        },
    });

    return SubPage;
});
