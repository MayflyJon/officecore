﻿require.config({
    baseUrl: "/sitecore/shell/client/Speak/Assets",
    paths: {
        nprogress: "lib/ui/deps/nprogress/nprogress",
        analytics: "lib/core/deps/sitecoreAnalytics/analytics"
    },
    shim: {
        'nprogress': { deps: ['jquery'], exports: "NProgress" },
        'analytics': { deps: ['jquery', 'nprogress'] }
    }
});

define(["sitecore", "analytics"], function (Sitecore) {
  var CampaignPage = Sitecore.Definitions.App.extend({
      initialized: function () {
          //Get Goal GUID from querystring
          var guid = loadPageVar("campaign");

          this.set("Campaign", null);
          this.CampaignGoalsListData.set("filters", "&campaignId=" + guid);
          this.CampaignReferringSitesData.set("filters", "&campaignId=" + guid);

          this.getData(guid);

          this.insertRendering("{12514198-F1D6-4D6D-AE40-1E9B51B51B89}", { $el: $(".sc-applicationContent-navigation") }, $.proxy(function (subApp) {

              var bodies = $(".sc-accordion ").get();

              for (index = 0; index < bodies.length; ++index) {

                  var item = bodies[index];
                  var buttonGroup = $(item).find(".sc-hyperlinkbuttonsgroup")[0];
                  var buttonHtml = buttonGroup.innerHTML;
                  var liIndex = buttonHtml.indexOf("li");
                  if (liIndex < 0) {

                      var chevron = $(item).find(".sc-accordion-header-chevron")[0];
                      chevron.style.display = 'none';
                      $(chevron).parent().append("<div style=\"width:10px;\"></div>");
                  }
                  toggleAccordion();
              };

          }, this));
          
    },
    getData: function (guid) {
          
        //Attempt to get the goal item
        RetrieveSimpleItem(guid, "master", $.proxy(function (campaignItem) {

            if (campaignItem) {
                this.set("Campaign", campaignItem);

                //Set items on the page
                this.setItems(campaignItem);
                
            }
            
        },this));
    },
      setItems: function (campaign) {

          this.CampaignImage.set("imageUrl", campaign["$mediaurl"]);
          this.HeaderTitle.set("text", campaign.itemName.replace(/\b./g, function (m) { return m.toUpperCase(); }));
          var campaignlink = "sc_camp=" + campaign.itemId.replace(/-/g, "").replace("{", "").replace("}", "");
          this.LinkUrl.set("text", "Campaign Url: " + campaignlink);
      }
  });
    
  function loadPageVar(sVar) {
      return unescape(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + escape(sVar).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
  }

  return CampaignPage;
});


