﻿require.config({
    baseUrl: "/sitecore/shell/client/Speak/Assets",
    paths: {
        nprogress: "lib/ui/deps/nprogress/nprogress",
        analytics: "lib/core/deps/sitecoreAnalytics/analytics"
    },
    shim: {
        'nprogress': { deps: ['jquery'], exports: "NProgress" },
        'analytics': { deps: ['jquery', 'nprogress'] }
    }
});

define(["sitecore", "jquery", "underscore", "analytics"], function (Sitecore) {
    var SubPage = Sitecore.Definitions.App.extend({
        initialized: function () {

            var self = this;

            this.insertRendering("{12514198-F1D6-4D6D-AE40-1E9B51B51B89}", { $el: $(".sc-applicationContent-navigation") }, $.proxy(function (subApp) {

                var bodies = $(".sc-accordion ").get();

                for (index = 0; index < bodies.length; ++index) {

                    var item = bodies[index];
                    var buttonGroup = $(item).find(".sc-hyperlinkbuttonsgroup")[0];
                    var buttonHtml = buttonGroup.innerHTML;
                    var liIndex = buttonHtml.indexOf("li");
                    if (liIndex < 0) {

                        var chevron = $(item).find(".sc-accordion-header-chevron")[0];
                        chevron.style.display = 'none';
                        $(chevron).parent().append("<div style=\"width:10px;\"></div>");
                    }
                    toggleAccordion();

                };

            }, this));

            this.ProfileTopPagesData.on("change:items", $.proxy(function () {

                var profileItems = this.ProfileMatchReportData.get("items");
                if (!_.isNull(profileItems) && !_.isUndefined(profileItems) && profileItems.length > 0) {
                    this.profileReport(profileItems[0].ProfileName);
                }


            }, this));

            this.ProfileMatchReportData.on("change:items", $.proxy(function () {
                var profileItems = this.ProfileMatchReportData.get("items");
                var profileVisits = _(profileItems).pluck('Visits');
                
                $("div[data-sc-id='ProfileTableSelectorColumn']").empty();

                var profilePagesItems = this.ProfileTopPagesData.get("items");
                if (!_.isNull(profilePagesItems) && !_.isUndefined(profilePagesItems) && profilePagesItems.length > 0) {
                    this.profileReport(profileItems[0].ProfileName);
                }

                $.each(profileItems, $.proxy(function (index, value) {

                    var sb = new StringBuilder();
                    sb.append("<button type='button');'>");
                    sb.append(value['ProfileName']);
                    sb.append("</button>");

                    var button = $(sb.toString());

                    button.on("click", self.setProfile.bind(self));

                    $("div[data-sc-id='ProfileTableSelectorColumn']").append(button);

                }, this));

            }, this));
            
        },
        setProfile: function (profileName) {
            var app = this;

            this.profileReport(profileName.currentTarget.innerText);
        },
        profileReport: function (profile) {

            if (_.isNull(profile) || profile === "")
                return;

            var profileItems = this.ProfileTopPagesData.get("items");
            if (_.isNull(profileItems) || _.isUndefined(profileItems))
                return;

            var filteredProfileItems = _.filter(profileItems, function (item) {
                return item.profilename === profile;
            });
            this.ProfileTable.set("items", filteredProfileItems);

        },
    });

    return SubPage;

});
