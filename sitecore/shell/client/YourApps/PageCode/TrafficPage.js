﻿require.config({
    baseUrl: "/sitecore/shell/client/Speak/Assets",
    paths: {
        nprogress: "lib/ui/deps/nprogress/nprogress",
        analytics: "lib/core/deps/sitecoreAnalytics/analytics"
    },
    shim: {
        'nprogress': { deps: ['jquery'], exports: "NProgress" },
        'analytics': { deps: ['jquery', 'nprogress'] }
    }
});

define(["sitecore", "jquery", "underscore", "analytics"], function (Sitecore) {
    var TrafficPage = Sitecore.Definitions.App.extend({
        initialized: function () {
            this.NewReturningVisitorListData.set("pageSize", 100);

            this.insertRendering("{12514198-F1D6-4D6D-AE40-1E9B51B51B89}", { $el: $(".sc-applicationContent-navigation") }, $.proxy(function (subApp) {

                var bodies = $(".sc-accordion ").get();

                for (index = 0; index < bodies.length; ++index) {

                    var item = bodies[index];
                    var buttonGroup = $(item).find(".sc-hyperlinkbuttonsgroup")[0];
                    var buttonHtml = buttonGroup.innerHTML;
                    var liIndex = buttonHtml.indexOf("li");
                    if (liIndex < 0) {

                        var chevron = $(item).find(".sc-accordion-header-chevron")[0];
                        chevron.style.display = 'none';
                        $(chevron).parent().append("<div style=\"width:10px;\"></div>");
                    }
                    
                    toggleAccordion();

                };

            }, this));
        },
    });

    return TrafficPage;
});


// Initializes a new instance of the StringBuilder class
// and appends the given value if supplied
function StringBuilder(value) {
    this.strings = new Array("");
    this.append(value);
};

//Format a date to be accepted by the handler
function getFormattedDate(date) {
    var dateFormatted = new Date(date);
    var dateStringified = dateFormatted.getFullYear() + "-" + (dateFormatted.getMonth() + 1) + "-" + dateFormatted.getDate();

    return dateStringified;
}

// Appends the given value to the end of this instance.
StringBuilder.prototype.append = function (value) {
    if (value) {
        this.strings.push(value);
    }
};

// Clears the string buffer
StringBuilder.prototype.clear = function () {
    this.strings.length = 1;
};

// Converts this instance to a String.
StringBuilder.prototype.toString = function () {
    return this.strings.join("");
};

//Check for valid GUID
function IsGuid(value) {
    return /^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/.test(value);
}




