﻿require.config({
    baseUrl: "/sitecore/shell/client/Speak/Assets",
    paths: {
        nprogress: "lib/ui/deps/nprogress/nprogress",
        analytics: "lib/core/deps/sitecoreAnalytics/analytics"
    },
    shim: {
        'nprogress': { deps: ['jquery'], exports: "NProgress" },
        'analytics': { deps: ['jquery', 'nprogress'] }
    }
});

define(["sitecore", "jquery", "underscore", "analytics"], function (Sitecore) {
    var ContentPage = Sitecore.Definitions.App.extend({
        initialized: function () {
            var self = this;

            this.EngagementDistributionDataGrid.on("change:selectedItem", $.proxy(function() {

                var selectedItem = this.EngagementDistributionDataGrid.get("selectedItem");
                var items = this.EngagementGoalsData.get("items");
                
                if (_.isNull(selectedItem) || _.isUndefined(selectedItem))
                    return;

                if (_.isNull(items) || _.isUndefined(items))
                    return;
                
                var newItems = _.filter(items, function (item) {
                    return item.DECILE.trim() == selectedItem.DECILE.trim();
                });


                this.EngagementGoals.set("items", newItems);
            }, this));

            this.EngagementDistributionDataGridHistory.on("change:selectedItem", $.proxy(function () {

                var selectedItem = this.EngagementDistributionDataGridHistory.get("selectedItem");
                var items = this.EngagementGoalsDataHistory.get("items");

                if (_.isNull(selectedItem) || _.isUndefined(selectedItem))
                    return;

                if (_.isNull(items) || _.isUndefined(items))
                    return;

                var newItems = _.filter(items, function (item) {
                    return item.DECILE.trim() == selectedItem.DECILE.trim();
                });


                this.EngagementGoalsHistory.set("items", newItems);
            }, this));

            this.EngagementGoalsData.on("change:items", $.proxy(this.ChangeGridSelection, this));
            this.EngagementGoalsDataHistory.on("change:items", $.proxy(this.ChangeHistoryGridSelection, this));
            this.EngagementDistributionDataGrid.on("change:items", $.proxy(this.ChangeGridSelection, this));
            this.EngagementDistributionDataGridHistory.on("change:items", $.proxy(this.ChangeHistoryGridSelection, this));
            
            this.insertRendering("{12514198-F1D6-4D6D-AE40-1E9B51B51B89}", { $el: $(".sc-applicationContent-navigation") }, $.proxy(function (subApp) {

                var bodies = $(".sc-accordion ").get();

                for (index = 0; index < bodies.length; ++index) {

                    var item = bodies[index];
                    var buttonGroup = $(item).find(".sc-hyperlinkbuttonsgroup")[0];
                    var buttonHtml = buttonGroup.innerHTML;
                    var liIndex = buttonHtml.indexOf("li");
                    if (liIndex < 0) {
                        
                        var chevron = $(item).find(".sc-accordion-header-chevron")[0];
                        chevron.style.display = 'none';
                        $(chevron).parent().append("<div style=\"width:10px;\"></div>");
                    }
                    toggleAccordion();
                };
                
            }, this));
            
        },
        ChangeGridSelection: function () {
            
            var items = this.EngagementGoalsData.get("items");
            if (_.isNull(items) || _.isUndefined(items))
                return;
            
            var selectedItem = this.EngagementDistributionDataGrid.get("selectedItem");
            if (_.isNull(selectedItem) || _.isUndefined(selectedItem)) {
                var items = this.EngagementDistributionDataGrid.get("items");
                if (!_.isNull(items) && !_.isUndefined(items)) {
                    selectedItem = items[0];
                }
            }

            if (!_.isNull(selectedItem) && !_.isUndefined(selectedItem)) {
                this.EngagementDistributionDataGrid.set("selectedItem", selectedItem);
                $('div[data-sc-id="EngagementDistributionDataGrid"]').dxDataGrid('instance').selectRows(selectedItem);
            }
            
        },
        ChangeHistoryGridSelection: function () {

            var items = this.EngagementGoalsDataHistory.get("items");
            if (_.isNull(items) || _.isUndefined(items))
                return;

            var selectedItem = this.EngagementDistributionDataGridHistory.get("selectedItem");
            if (_.isNull(selectedItem) || _.isUndefined(selectedItem)) {
                var items = this.EngagementDistributionDataGridHistory.get("items");
                if (!_.isNull(items) && !_.isUndefined(items)) {
                    selectedItem = items[0];
                }
            }

            if (!_.isNull(selectedItem) && !_.isUndefined(selectedItem)) {
                this.EngagementDistributionDataGridHistory.set("selectedItem", selectedItem);
                $('div[data-sc-id="EngagementDistributionDataGridHistory"]').dxDataGrid('instance').selectRows(selectedItem);
            }

        }
    });
    
    return ContentPage;

});
