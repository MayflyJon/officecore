﻿var id;
var calledResize = 0;
var totalTrafficType = 999;
var chartPalette;

$(function () {
    
    $(window).on('resize', $.proxy(mediaQuery, this));

    var startButton = $('.sc-globalHeader-startButton').parent();
    if (!_.isUndefined(startButton)) {
        $(startButton).removeClass("span3");
        $(startButton).css({ 'position': 'absolute', 'left': '10px' });
    }

    var loginInfo = $('.sc-globalHeader-loginInfo').parent();
    if (!_.isUndefined(loginInfo)) {
        $(loginInfo).removeClass("span3");
        $(loginInfo).css({ 'position': 'absolute', 'right': '10px' });
    }

    id = resizeNavigation();

    chartPalette = ['#EF8200', '#125687', '#008ED6', '#86BE2B', '#52DEBF', '#9887CC', '#E6C1A1', '#B2E6A1', '#95AA6F', '#CC70B8', '#31AFBD', '#BD3F31', '#31BD36', '#BD31B8', '#BD8C31', '#3162BD'];
    var analyticsPalette = {
        simpleSet: chartPalette, // for dxChart, dxPieChart and dxBarGauge 
    };
    

    if (typeof DevExpress !== "undefined") {
        if (!_.isUndefined(DevExpress.viz)) {
            DevExpress.viz.core.registerPalette('analyticsPalette', analyticsPalette);
        }
    }
    
    $(document).ajaxStart(function () {
        if (typeof NProgress !== "undefined") {
            NProgress.start();
        }
    });


    $(document).ajaxStop(function () {
        if (typeof NProgress !== "undefined") {
            NProgress.done();
        }

        if ($("div.sc-Gridster").length > 0) {
            gridster = $("div.sc-Gridster").gridster({
                widget_margins: [10, 10],
                widget_base_dimensions: [215, 65],
                widget_selector: "div",
                resize: {
                    enabled: false
                },
                serialize_params: function ($w, wgd) {
                    return {
                        id: $w.prop('id'),
                        col: $w.data().col,
                        row: $w.data().row,
                        size_x: $w.data().sizex,
                        size_y: $w.data().sizey,
                    };
                },
                avoid_overlapped_widgets: true,
            }).data("gridster");
        }
    });
    
});

function toggleAccordion() {
    
    if (window.location.hash) {
        var page = window.location.hash.substring(1);

        var pageAccordion = $('div[data-sc-id="' + page + 'Accordion"]');
        var pageChevron = $(pageAccordion).find('.sc-accordion-header-chevron a');
        if (!_.isUndefined(pageChevron)) {
            $(pageChevron).trigger("click");
        }
    }

}

function mediaQuery() {

    var viewportWidth = $(window).width();
    var viewportHeight = $(window).height();

    if (viewportWidth < 992) {
        resizeNavigation();
    } 
}

function resizeNavigation() {
    return window.setInterval(function () {

        var mainHeight = $('.sc-applicationContent-main').css('height');
        var navHeight = $('.sc-applicationContent-navigation').css('height');
        var viewportHeight = $(window).height() - 50; // -50 because of header

        mainHeight = parseInt(mainHeight.substring(0, mainHeight.length - 2));
        navHeight = parseInt(navHeight.substring(0, navHeight.length - 2));

        if ((mainHeight <= navHeight)) {
            calledResize += 1;
            if ((viewportHeight < 600) || (mainHeight < 600)) {
                viewportHeight = viewportHeight < 600 ? 600 : viewportHeight;
                $('.sc-applicationContent-navigation').css({ height: viewportHeight });
            }

        } else {
            var newNavHeight = viewportHeight > mainHeight ? viewportHeight : mainHeight;
            $('.sc-applicationContent-navigation').css({ height: newNavHeight });
        }

        if (calledResize >= 8) {
            window.clearInterval(id);
            calledResize = 0;
        }

    }, 500);
};

function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }

    return x1 + x2;
}

function RetrieveSimpleItem(guid, databaseName, callback) {


    if (IsGuid(guid)) {

        var newGuid;
        if (guid.indexOf("{") > -1) {
            newGuid = guid;
        } else {
            newGuid = "{" + guid + "}";
        }

        // Get the URI's for the database and item
        var databaseUri = new _sc.Definitions.Data.DatabaseUri(databaseName);
        var itemUri = new _sc.Definitions.Data.ItemUri(databaseUri, newGuid);

        //Get database
        var database = new _sc.Definitions.Data.Database(itemUri.getDatabaseUri());

        //Get item
        database.getItem(newGuid, function (item) {

            //Return item
            if (callback && typeof (callback) === "function") {
                callback(item);
            } else {
                return item;
            }

        });
    }

}

function RetreiveItemChildren(guid, databaseName, callback) {


    if (IsGuid(guid)) {

        var newGuid;
        if (guid.indexOf("{") > -1) {
            newGuid = guid;
        } else {
            newGuid = "{" + guid + "}";
        }

        // Get the URI's for the database and item
        var databaseUri = new _sc.Definitions.Data.DatabaseUri(databaseName);
        var itemUri = new _sc.Definitions.Data.ItemUri(databaseUri, newGuid);

        //Get database
        var database = new _sc.Definitions.Data.Database(itemUri.getDatabaseUri());

        //Get item
        database.getChildren(newGuid, function (item) {

            //Return item
            if (callback && typeof (callback) === "function") {
                callback(item);
            } else {
                return item;
            }

        });
    }

}

function RetrieveItem(guid, databaseName, returnField, headerName, callback) {

    // Create our deferred
    var dfd = $.Deferred();

    if (guid === '00000000-0000-0000-0000-000000000000') {
        //Resolve deffered
        dfd.resolve(guid, headerName, "No Item Set");
    } else if (IsGuid(guid)) {

        var newGuid;
        if (guid.indexOf("{") > -1) {
            newGuid = guid;
        } else {
            newGuid = "{" + guid + "}";
        }

        // Get the URI's for the database and item
        var databaseUri = new _sc.Definitions.Data.DatabaseUri(databaseName);
        var itemUri = new _sc.Definitions.Data.ItemUri(databaseUri, newGuid);

        //Get database
        var database = new _sc.Definitions.Data.Database(itemUri.getDatabaseUri());
        var returnValue;

        //Get item
        database.getItem(newGuid, function (item) {

            //Return just the field if specified, else everything
            if (item && returnField != null) {
                returnValue = item[returnField];

            } else {
                returnValue = item;
            }

            //Resolve deffered
            dfd.resolve(guid, headerName, returnValue);

        });
    } else {
        //Resolve deffered
        dfd.resolve(guid, headerName, null);
    }

    //Return deffered promise
    return dfd.promise();

}

//Check for valid GUID
function IsGuid(value) {
    return /^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/.test(value);
}

// Initializes a new instance of the StringBuilder class
// and appends the given value if supplied
function StringBuilder(value) {
    this.strings = new Array("");
    this.append(value);
};

// Appends the given value to the end of this instance.
StringBuilder.prototype.append = function (value) {
    if (value) {
        this.strings.push(value);
    }
};

// Clears the string buffer
StringBuilder.prototype.clear = function () {
    this.strings.length = 1;
};

// Converts this instance to a String.
StringBuilder.prototype.toString = function () {
    return this.strings.join("");
};

function HeaderSelected(evt) {
    var $source = $(evt.currentTarget);
    var i = $source.index();
    if (i === 0) {
        i++;
    }

    var keys = _.keys(_sc.app.NewReturningVisitorListData.get("items")[0]);
    var selectedKey = keys[i];

    _sc.app.NewReturningVisitorPieChart.set("valueField", selectedKey);
};

//Format a date to be accepted by the handler
function getFormattedDate(date) {
    var dateFormatted = new Date(date);
    var dateStringified = dateFormatted.getFullYear() + "-" + (dateFormatted.getMonth() + 1) + "-" + dateFormatted.getDate();

    return dateStringified;
}

function findIndexByKeyValue(obj, key, value) {
    for (var i = 0; i < obj.length; i++) {
        if (obj[i][key] == value) {
            return i;
        }
    }
    return null;
}

function isOdd(n) {
    return isNumber(n) && (Math.abs(n) % 2 == 1);
}