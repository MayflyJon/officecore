﻿function Dialog(options)
{
  this.address = options.address;
  this.height = options.height;
  this.name = options.name;
  this.parameters = options.parameters;
  this.width = options.width;
}

Dialog.prototype.getHandle = function()
{
  var parameterData = JSON.stringify(this.parameters);
  var request = { action: "getDialogHandle", parameters: parameterData };
  var response = sendRequest(request);

  return response;
};

Dialog.prototype.getPosition = function()
{
  var left = (window.screen.width - this.width) / 2;
  var top = (window.screen.height - this.height) / 2;

  return { left: left, top: top };
};

Dialog.prototype.getUrl = function()
{
  if (!this.parameters)
  {
    return this.address;
  }

  var handle = this.getHandle();
  var url = this.address + "?handle=" + handle;

  return url;
};

Dialog.prototype.show = function()
{
  var url = this.getUrl();

  var position = this.getPosition();

  var options =
    "width=" + this.width + "," +
    "height=" + this.height + "," +
    "left=" + position.left + "," +
    "top=" + position.top + "," +
    "resizable=0," +
    "location=0," +
    "toolbar=0," +
    "menubar=0," +
    "status=0," +
    "directories=0";

  return window.open(url, this.name, options);
};

Dialog.prototype.showModal = function()
{
  var url = this.getUrl();

  var position = this.getPosition();

  var options =
    "dialogWidth: " + this.width + "px; " +
    "dialogHeight: " + this.height + "px; " +
    "dialogLeft: " + position.left + "px; " +
    "dialogTop: " + position.top + "px; " +
    "resizable: no; status: no";

  return showModalDialog(url, this.name, options);
};