﻿/*
 * The URL Remapper.
 */

function UrlRemapper()
{
  this.createRibbon();

  this.linkPanel = jQuery("#LinkPanel");
  this.linkPane = jQuery("#LinkPane");
  this.linkHost = jQuery("#LinkHost");
  this.browseButton = jQuery("#BrowseButton");
  this.followButton = jQuery("#FollowButton");
  this.downloadButton = jQuery("#DownloadButton");
  this.tabPanel = jQuery("#TabPanel");
  this.previewFrame = jQuery("#PreviewFrame");
  this.htmlTab = jQuery("#HtmlTab");

  this.currentLink = null;
  this.rejectedLinks = [];

  this.scaleUi();

  if (info == null)
  {
    return;
  }

  this.links = [];
  this.refresh();

  var owner = this;
  jQuery(window).resize(function(event) { owner.scaleUi(); });
  this.browseButton.click(function(event) { owner.browseButtonClick(); });
  this.followButton.click(function(event) { owner.followButtonClick(); });
  this.downloadButton.click(function(event) { owner.downloadButtonClick(); });

  this.tabButtons = jQuery("#TabButtons").find(".TabButton");

  for (var i = 0; i < this.tabButtons.length; i++)
  {
    jQuery(this.tabButtons[i]).click(function(event) { owner.tabButtonClick(event); });
  }
}

UrlRemapper.prototype.browseButtonClick = function()
{
  var text = Texts.SelectTheItemThatYouWantToLink;
  var rootItem = "/sitecore/Content";
  var item = this.selectItem(text, rootItem, rootItem, false);

  if (item == null)
  {
    return;
  }

  if (!item.path)
  {
    throw "Item path not resolved.";
  }

  var newValue = this.getItemUrl(item.path);
  jQuery("#TextBox_" + this.currentLink.Id).val(newValue);
  this.mapLink(this.currentLink.Id, newValue);
  this.ribbon.refresh();
}

UrlRemapper.prototype.createLinkControl = function()
{
  var links = info.Links;

  var html = "";

  for (var i = 0; i < links.length; i++)
  {
    var link = links[i];

    html += '<div class="TextBoxPanel">';
    html += '<input id="TextBox_' + link.Id + '" type="text" value="' + link.OldValue + '"/>';
    html += '</div>';
  }

  this.linkHost.html(html);

  var textBoxes = jQuery("[id^='TextBox_']");
  var owner = this;

  for (var i = 0; i < links.length; i++)
  {
    var textBox = jQuery(textBoxes[i]);
    textBox.focus(function(event) { owner.textBoxFocus(event); });
    textBox.keyup(function(event) { owner.textBoxKeyUp(event); });
  }
}

UrlRemapper.prototype.createPreview = function()
{
  this.previewFrame.attr("src", info.PreviewUri);
}

UrlRemapper.prototype.createRibbon = function()
{
  var owner = this;

  this.ribbon = new Ribbon();

  this.ribbon.addButton(
    {
      name: Texts.Open,
      type: "Large",
      icon: 'Core3/24x24/open_document.png',
      tooltip: Texts.OpenALayoutOrSublayout,
      enable: function() { return true; },
      click: function() { owner.open(); }
    }
  );

  this.ribbon.addButton(
    {
      name: Texts.Save,
      type: "Large",
      icon: 'Applications/24x24/disk_blue.png',
      tooltip: Texts.SaveAnyChanges,
      enable: function() { return owner.hasChanges(); },
      click: function() { owner.save(); }
    }
  );

  this.ribbon.refresh();
}

UrlRemapper.prototype.createSourceView = function()
{
  var source = this.getNewSource(true);
  source = source.escapeHtml();
  source = source.replaceAll("\t", " &nbsp;");
  source = source.replaceAll("  ", " &nbsp;");
  source = this.highlightLinks(source, true);
  source = this.highlightLinks(source, false);

  var lines = source.split("\n");
  var view = "<table border='0' cellpadding='0' cellspacing='0'><tbody>";

  for (var i = 0; i < lines.length; i++)
  {
    view += "<tr><td class='LineNumber'>" + (i + 1) + "</td><td>" + lines[i] + "</td></tr>";
  }

  view += "</tbody></table>";

  this.htmlTab.html(view);
}

UrlRemapper.prototype.downloadButtonClick = function()
{
  var link = this.getLink(this.currentLink.Id);

  this.suspendUi();

  var response = sendRequest({ action: "downloadResource", uri: link.OldValue });

  this.resumeUi();

  if (!response)
  {
    alert(Texts.ThisWebResourceIsNotDownloadable);
  }
  else
  {
    link.NewValue = response;
  }

  link.NotDownloadable = true;

  this.refresh();

  if (response)
  {
    jQuery("#TextBox_" + this.currentLink.Id).val(response);
  }
}

UrlRemapper.prototype.enableElement = function(element, enable)
{
  if (enable)
  {
    element.removeAttr("disabled");

  }
  else
  {
    element.attr("disabled", "disabled");
  }
}

UrlRemapper.prototype.extractLinks = function()
{
  var result = [];

  for (var i = 0; i < info.Links.length; i++)
  {
    var link = info.Links[i];

    for (var j = 0; j < link.Indexes.length; j++)
    {
      result.push(
        {
          Index: link.Indexes[j],
          NewValue: link.NewValue,
          OldValue: link.OldValue
        });
    }
  }

  result.sort(
    function(a, b)
    {
      if (a.Index > b.Index)
      {
        return 1;
      }

      if (a.Index < b.Index)
      {
        return -1;
      }

      return 0;
    });

  return result;
}

UrlRemapper.prototype.followButtonClick = function()
{
  var link = this.getLink(this.currentLink.Id);

  if (link.IsNotAvailable === undefined)
  {
    this.suspendUi();

    var response = sendRequest({ action: "isWebResourceAvailable", uri: link.OldValue });

    this.resumeUi();

    link.IsNotAvailable = response == "0";
  }

  if (link.IsNotAvailable)
  {
    this.enableElement(this.followButton, false);
    alert(Texts.ThisWebResourceIsNotAvailable);
    return;
  }

  var width = 1024;
  var height = 768;
  var left = (screen.width - width) / 2;
  var top = (screen.height - height) / 2;
  var parameters = "width=" + width + ",height=" + height + ",left=" + left + ",top=" + top + ",resizable=1";

  if (this.oldLinkWindow)
  {
    this.oldLinkWindow.close();
  }

  this.oldLinkWindow = window.open(link.OldValue, "OldLinkWindow", parameters);
}

UrlRemapper.prototype.getItemUrl = function(path)
{
  var url = path;
  var search = /\/sitecore\/content/i;
  var index = url.search(search);

  if (index == 0)
  {
    url = url.replace(search, "");
  }

  search = /\.aspx$/i;
  index = url.search(search);

  if (index < 0)
  {
    url += ".aspx";
  }

  return url;
}

UrlRemapper.prototype.getLink = function(linkId)
{
  for (var i = 0; i < info.Links.length; i++)
  {
    if (info.Links[i].Id == linkId)
    {
      return info.Links[i];
    }
  }

  throw "Link (" + linkId + ") not found.";
}

UrlRemapper.prototype.getLinkId = function(control)
{
  return control.id.substring(control.id.lastIndexOf("_") + 1);
}

UrlRemapper.prototype.getNewSource = function(insertMarkers)
{
  var result = "";
  var start = 0;

  for (var i = 0; i < this.links.length; i++)
  {
    var link = this.links[i];
    var index = link.Index;

    result += info.Source.substring(start, index);

    if (insertMarkers)
    {
      result += (link.NewValue == null)
        ? "[UnmappedLink]" + link.OldValue + "[/UnmappedLink]"
        : "[MappedLink]" + link.NewValue + "[/MappedLink]";
    }
    else
    {
      result += (link.NewValue == null) ? link.OldValue : link.NewValue;
    }

    start = index + link.OldValue.length;
  }

  result += info.Source.substring(start);

  return result;
}

UrlRemapper.prototype.getRemappedLinks = function()
{
  var result = [];

  for (var i = 0; i < info.Links.length; i++)
  {
    if (info.Links[i].NewValue)
    {
      result.push(info.Links[i].NewValue);
    }
  }

  return result;
}

UrlRemapper.prototype.hasChanges = function()
{
  return this.getRemappedLinks().length > 0;
}

UrlRemapper.prototype.highlightLinks = function(source, isMapped)
{
  var result = "";
  var start = 0;

  var tag1 = isMapped ? "[MappedLink]" : "[UnmappedLink]";
  var tag2 = isMapped ? "[/MappedLink]" : "[/UnmappedLink]";

  while (true)
  {
    var index1 = source.indexOf(tag1, start);

    if (index1 < 0)
    {
      result += source.substring(start);
      break;
    }

    var index2 = index1 + tag1.length;
    var index3 = source.indexOf(tag2, index2);

    if (index3 < 0)
    {
      throw "Link has no end tag.";
    }

    var link = source.substring(index2, index3);
    var className = "";

    if (link == this.selectedLink)
    {
      className = " class='Selected'";
    }
    else if (isMapped)
    {
      className = " class='Mapped'";
    }

    link = '<a ' + className + 'href="' + link + '" target="_blank">' + link + '</a>';
    result += source.substring(start, index1) + link;
    start = index3 + tag2.length;
  }

  return result;
}

UrlRemapper.prototype.highlightUnmappedLink = function(linkId)
{
  this.selectedLink = this.getLink(linkId).OldValue;
  this.refreshTabs();
}

UrlRemapper.prototype.linkPaneScroll = function(event)
{
  var scrollTop = jQuery(event.target).scrollTop();
  this.unmappedLinkPane.scrollTop(scrollTop);
  this.linkPane.scrollTop(scrollTop);
}

UrlRemapper.prototype.mapLink = function(linkId, value)
{
  var link = this.getLink(linkId);
  link.NewValue = (value && (value != link.OldValue)) ? value : null;
  this.ribbon.refresh();
  this.links = this.extractLinks();
  this.refreshTabs();
}

UrlRemapper.prototype.open = function()
{
  if (this.hasChanges() && !confirm(Texts.AllTheChangesYouMadeBeforeWillBeDiscarded))
  {
    return;
  }

  var text = Texts.SelectALayoutOrSublayout;
  var rootItem = "/sitecore/Content";
  var item = this.selectItem(text, rootItem, rootItem, true);

  if (item == null)
  {
    return;
  }

  var errorMessage = sendRequest({
    action: "getUrlRemapperOpenItemError",
    itemId: item.id,
    language: Context.language
  });

  if (errorMessage)
  {
    window.alert(errorMessage);
    return;
  }

  location.href = "UrlRemapper.aspx?id=" + encodeURIComponent(item.id);
}

UrlRemapper.prototype.refresh = function()
{
  this.ribbon.refresh();
  this.links = this.extractLinks();
  this.createLinkControl();
  this.refreshButtons();
  this.refreshTabs();
}

UrlRemapper.prototype.refreshButtons = function()
{
  this.enableElement(this.browseButton, this.currentLink);
  this.enableElement(this.followButton, this.currentLink && !this.currentLink.IsNotAvailable);
  this.enableElement(this.downloadButton, this.currentLink && !this.currentLink.NotDownloadable);
}

UrlRemapper.prototype.refreshTabs = function()
{
  this.createPreview();
  this.createSourceView();
}

UrlRemapper.prototype.resumeUi = function()
{
  if (this.suspendPanel)
  {
    this.suspendPanel.remove();
  }

  if (this.suspendIcon)
  {
    this.suspendIcon.remove();
  }
}

UrlRemapper.prototype.save = function()
{
  info.RemappedLinks = this.getRemappedLinks();
  info.Source = this.getNewSource(false);

  sendRequest({ action: "saveRemappedLinks", info: JSON.stringify(info) });

  alert(Texts.TheChangesWereSavedSuccessfully);
  location.reload(true);
}

UrlRemapper.prototype.scaleUi = function()
{
  var width;
  var height;

  var pageTab = jQuery(".Tab.Visible");

  this.linkPane.hide();
  pageTab.hide();

  width = this.linkPanel.width();
  height = this.linkPanel.height();
  this.linkPane.css("width", width + "px");
  this.linkPane.css("height", height + "px");

  width = this.tabPanel.width();
  height = this.tabPanel.height();
  pageTab.css("width", width + "px");
  pageTab.css("height", height + "px");

  this.linkPane.show();
  pageTab.show();
}

UrlRemapper.prototype.selectItem = function(text, rootItemPath, selectedItemPath, showRoot)
{
  var request =
  {
    action: "getSelectItemDialogUrl",
    root: rootItemPath,
    selected: selectedItemPath,
    showRoot: showRoot ? "1" : "0",
    text: text
  };

  var dialogUrl = sendRequest(request);
  var dialogStyle = "dialogWidth: 450px; dialogHeight: 600px; resizable: yes";
  var itemId = showModalDialog(dialogUrl, "SelectItemDialog", dialogStyle);

  if (itemId == null)
  {
    return null;
  }

  var itemPath = sendRequest({ action: "getItemPath", itemId: itemId });

  return { id: itemId, path: itemPath };
}

UrlRemapper.prototype.showLinkButtons = function(event, show)
{
  var sender = jQuery(event.currentTarget);
  var visibility = show ? "visible" : "hidden";
  var senderId = sender.attr("id");
  var position = senderId.indexOf("_");
  var linkId = senderId.substring(position + 1);
  var goToButton = jQuery("#GoToButton_" + linkId);
  var downloadButton = jQuery("#DownloadButton_" + linkId);
  goToButton.css("visibility", visibility);
  downloadButton.css("visibility", visibility);
}

UrlRemapper.prototype.suspendUi = function()
{
  this.suspendPanel = jQuery("<div class='SuspendPanel'/>");
  this.suspendIcon = jQuery("<div class='SuspendIcon'/>");
  jQuery(document.body).append(this.suspendPanel);
  jQuery(document.body).append(this.suspendIcon);
}

UrlRemapper.prototype.tabButtonClick = function(event)
{
  var sender = jQuery(event.target);
  var senderId = sender.attr("id");

  for (var i = 0; i < this.tabButtons.length; i++)
  {
    var tabButton = jQuery(this.tabButtons[i]);
    var tabButtonId = tabButton.attr("id");

    if (tabButtonId == senderId)
    {
      tabButton.addClass("Active");
    }
    else
    {
      tabButton.removeClass("Active");
    }
  }

  var tabs = this.tabPanel.find(".Tab");

  for (var i = 0; i < tabs.length; i++)
  {
    var tab = jQuery(tabs[i]);
    var tabId = tab.attr("id");

    if (tabId.replace("Tab", "") == senderId.replace("TabButton", ""))
    {
      tab.show();
      tab.addClass("Visible");
    }
    else
    {
      tab.hide();
      tab.removeClass("Visible");
    }
  }

  this.scaleUi();
}

UrlRemapper.prototype.textBoxFocus = function(event)
{
  var textBox = event.target;
  var linkId = this.getLinkId(textBox);

  if (this.currentLink && (this.currentLink.Id != linkId))
  {
    jQuery("#TextBox_" + this.currentLink.Id).parent().removeClass("Selected");
  }

  jQuery(textBox).parent().addClass("Selected");

  this.currentLink = this.getLink(linkId);
  this.refreshButtons();
  this.highlightUnmappedLink(linkId);
}

UrlRemapper.prototype.textBoxKeyUp = function(event)
{
  var sender = event.target;
  var linkId = this.getLinkId(sender);
  this.mapLink(linkId, sender.value);
}

UrlRemapper.prototype.unmappedLinkClick = function(event)
{
  this.highlightUnmappedLink(this.getLinkId(event.target));
}

/*
 * String extensions.
 */

String.prototype.escapeHtml = function()
{
  var text = this;
  var find = ["&", "<", ">", "\""];
  var replace = ["&amp;", "&lt;", "&gt;", "&quot;"];

  for (var i = 0; i < find.length; i++)
  {
    var search = new RegExp(find[i], "gi");

    if (search.test(text))
    {
      text = text.replace(search, replace[i]);
    }
  }

  return text;
}

String.prototype.replaceAll = function(search, replace)
{
  return this.split(search).join(replace);
}

jQuery(document).ready(function(event) { new UrlRemapper(); });