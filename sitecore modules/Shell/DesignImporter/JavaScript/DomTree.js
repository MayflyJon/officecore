﻿function DomTree(host)
{
  this.ignoredElements = ["BASE", "LINK", "PARAM", "SCRIPT", "STYLE", "#comment"];
  this.nodeIdPrefix = idPrefix + "DomTree_Node_";
  this.host = host;
  this.replacedElements = [];
}

DomTree.prototype.addReplacedElement = function(elementId, isText)
{
  for (var i = 0; i < this.replacedElements.length; i++)
  {
    if (this.replacedElements[i].elementId == elementId)
    {
      return;
    }
  }

  this.replacedElements.push({ elementId: elementId, isText: isText });
};

DomTree.prototype.clear = function()
{
  if (this.control)
  {
    this.control.remove();
  }

  this.element = null;
  this.control = null;
};

DomTree.prototype.collapse = function(elementId)
{
  var node = jQuery("#" + this.nodeIdPrefix + elementId);
  var children = node.find(".Children:first");

  if (children.is(":hidden"))
  {
    return;
  }

  node.find(".ToggleButton:first").attr("src", "Images/DomTree/Expand.png");
  children.hide();
};

DomTree.prototype.create = function(element)
{
  this.element = element;
  this.control = jQuery(this.renderElement(element));

  this.host.html("");
  this.host.append(this.control);

  var owner = this;
  this.control.click(function(event) { owner.onClick(event); });
};

DomTree.prototype.expand = function(elementId, expandParents)
{
  var node = jQuery("#" + this.nodeIdPrefix + elementId);

  if (node.find("label:first").hasClass("Replaced"))
  {
    return;
  }

  var children = node.find(".Children:first");

  if (children.is(":hidden"))
  {
    node.find(".ToggleButton:first").attr("src", "Images/DomTree/Collapse.png");
    children.show();
  }

  if (!expandParents)
  {
    return;
  }

  var parentNode = node.parents(".Node:first");

  if (parentNode.length)
  {
    var parentElementId = parentNode[0].id.replace(this.nodeIdPrefix, "");
    this.expand(parentElementId, true);
  }
};

DomTree.prototype.getNodeId = function(element)
{
  var isText = element.nodeName == "#text";

  if (!isText && !element.id)
  {
    throw "Element has no ID.";
  }

  var elementId = isText ? element.parentNode.id : element.id;

  return this.nodeIdPrefix + elementId;
};

DomTree.prototype.isElementIgnored = function(element)
{
  if (element.nodeName.toLowerCase() == "#text")
  {
    return false;
  }

  for (var i = 0; i < this.ignoredElements.length; i++)
  {
    if (element.nodeName == this.ignoredElements[i])
    {
      return true;
    }
  }

  if (element.id == coverId)
  {
    return true;
  }

  if (element.className == "Selection")
  {
    return true;
  }

  if (!element.id)
  {
    return true;
  }

  return false;
};

DomTree.prototype.markElementAsReplaced = function(elementId, isText, mark)
{
  var nodeId = "#" + this.nodeIdPrefix + elementId;
  var node = jQuery(nodeId);

  if (isText)
  {
    node = node.find(nodeId);
  }

  var label = node.find("label:first");
  var hasChildren = node.find(".Children:first").length > 0;

  if (mark && !label.hasClass("Replaced"))
  {
    this.addReplacedElement(elementId, isText);

    label.addClass("Replaced");
    this.collapse(elementId);

    if (hasChildren)
    {
      node.find(".ToggleButton").addClass("Hidden");
    }
  }

  if (!mark && label.hasClass("Replaced"))
  {
    this.removeReplacedElement(elementId);

    label.removeClass("Replaced");
    this.expand(elementId);

    if (hasChildren)
    {
      node.find(".ToggleButton").removeClass("Hidden");
    }
  }
};

DomTree.prototype.onClick = function(event)
{
  var element = event.target;
  var nodeName = element.nodeName.toLowerCase();
  var className = element.className;

  if (className == "ToggleButton")
  {
    this.onToggleButtonClick(event);
  }
  else if ((nodeName == "label") || (className.indexOf("Element") >= 0))
  {
    this.onLabelClick(event);
  }
};

DomTree.prototype.onLabelClick = function(event)
{
  var label = jQuery(event.target);
  var node = label.parents(".Node:first");

  this.selectNode(node);

  if (this.nodeClickHandler)
  {
    var owner = this.nodeClickHandler.owner;
    var methodName = this.nodeClickHandler.methodName;
    var elementId = node[0].id.replace(this.nodeIdPrefix, "");
    owner[methodName](elementId);
  }
};

DomTree.prototype.onToggleButtonClick = function(event)
{
  var toggleButton = jQuery(event.target);
  var node = toggleButton.parents(".Node:first");
  var elementId = node[0].id.replace(this.nodeIdPrefix, "");

  if (node.find(".Children:first").is(":hidden"))
  {
    this.expand(elementId, false);
  }
  else
  {
    this.collapse(elementId);
  }
};

DomTree.prototype.refreshElement = function(element)
{
  var nodeId = this.nodeIdPrefix + element[0].id;
  var node = this.control.find("#" + nodeId);

  if (!node.length)
  {
    return;
  }

  var html = this.renderElement(element[0]);
  var newNode = jQuery(html);
  node.replaceWith(newNode);

  this.refreshReplacedElements();
};

DomTree.prototype.refreshReplacedElements = function()
{
  for (var i = 0; i < this.replacedElements.length; i++)
  {
    var elementId = this.replacedElements[i].elementId;
    var isText = this.replacedElements[i].isText;
    this.markElementAsReplaced(elementId, isText, true);
  }
};

DomTree.prototype.removeReplacedElement = function(elementId)
{
  for (var i = 0; i < this.replacedElements.length; i++)
  {
    if (this.replacedElements[i].elementId == elementId)
    {
      this.replacedElements.splice(i, 1);

      return;
    }
  }
};

DomTree.prototype.renderChildren = function(element)
{
  var html = "";
  var children = element.childNodes;

  for (var i = 0; i < children.length; i++)
  {
    html += this.renderElement(children[i]);
  }

  if (html)
  {
    html = "<div class='Children'>" + html + "</div>";
  }

  return html;
};

DomTree.prototype.renderElement = function(element)
{
  if (this.isElementIgnored(element))
  {
    return "";
  }

  var labelHtml = this.renderLabel(element);

  if (!labelHtml)
  {
    return "";
  }

  var childrenHtml = this.renderChildren(element);
  var toggleButtonHtml = this.renderToggleButton(childrenHtml ? true : false);
  var nodeId = this.getNodeId(element);

  var html =
    "<div class='Node' id='" + nodeId + "'>" +
    "<table><tbody><tr>" +
    "<td>" + toggleButtonHtml + "</td>" +
    "<td>" + labelHtml + "</td>" +
    "</tr></tbody></table>" +
    childrenHtml +
    "</div>";

  if (element == this.element)
  {
    html = "<div class='DomTree'>" + html + "</div>";
  }

  return html;
};

DomTree.prototype.renderLabel = function(element)
{
  var nodeName = element.nodeName.toLowerCase();

  switch (nodeName)
  {
    case "#text":
    {
      var text = jQuery.trim(element.nodeValue);

      if (!text)
      {
        return null;
      }

      var maxLength = 25;

      if (text.length > maxLength)
      {
        text = text.substring(0, maxLength) + "...";
      }

      return "<label class='Text'>" + jQuery("<div></div>").text(text).html() + "</label>";
    }

    default:
    {
      var className = (nodeName.toLowerCase() == "body") ? "Element Protected" : "Element";
      var html = "<span class='" + className + "'>" + nodeName + "</span>";

      if (element.id && jQuery.trim(element.id) && (element.id.indexOf(idPrefix) < 0))
      {
        html = element.id + " (" + html + ")";
      }

      return "<label>" + html + "</label>";
    }
  }
};

DomTree.prototype.renderToggleButton = function(hasChildren)
{
  var className = "ToggleButton";

  if (!hasChildren)
  {
    className += " Hidden";
  }

  return "<img class='" + className + "' src='Images/DomTree/Expand.png'/>";
};

DomTree.prototype.select = function(elementId)
{
  if (!elementId)
  {
    this.selectNode(null);
    return;
  }

  var nodeId = this.nodeIdPrefix + elementId;
  var node = jQuery("#" + nodeId);
  this.selectNode(node);
  this.host.scrollTo(node);
};

DomTree.prototype.selectNode = function(node)
{
  if (this.selectedNodeLabel)
  {
    this.selectedNodeLabel.removeClass("Selected");
  }

  if (!node)
  {
    return;
  }

  this.selectedNodeLabel = node.find("label:first");
  this.selectedNodeLabel.addClass("Selected");
};