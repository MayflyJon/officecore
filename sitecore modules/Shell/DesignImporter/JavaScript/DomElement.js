﻿function DomElement(element)
{
  this.element = element;
}

DomElement.prototype.cleanupHtml = function(html)
{
  var result = html;

  result = result.replace(new RegExp('<div.*id="?' + coverId + '"?.*>.*</div>', "gi"), "");
  result = result.replace(new RegExp('<div.*class="?' + fieldRendererClass + '"?.*>.*</div>', "gi"), "");
  result = result.replace(new RegExp('<div.*class="?' + hoverSelectionClass + '"?.*>.*</div>', "gi"), "");
  result = result.replace(new RegExp('<div.*class="?' + placeholderClass + '"?.*>.*</div>', "gi"), "");
  result = result.replace(new RegExp('<div.*class="?' + selectionClass + '"?.*>.*</div>', "gi"), "");
  result = result.replace(new RegExp(' jQuery[0-9]+="?[0-9]+"?', "gi"), "");

  return result;
};

DomElement.prototype.getInnerHtml = function(cleanIds)
{
  var html = jQuery(this.element).html();

  if (cleanIds)
  {
    html = this.removeGeneratedIds(html);
  }

  return this.cleanupHtml(html);
};

DomElement.prototype.getOffset = function()
{
  var element = jQuery(this.element);
  var offset = element.offset();

  if (!jQuery.browser.msie)
  {
    return offset;
  }

  var documentElement = element.parents("html");
  offset.left += documentElement.scrollLeft();
  offset.top += documentElement.scrollTop();

  return offset;
};

DomElement.prototype.getOuterHtml = function(cleanIds)
{
  var html;

  if (this.element.outerHTML)
  {
    html = this.element.outerHTML;
  }
  else
  {
    var name = this.element.nodeName;

    html = "<" + name;

    var attributes = this.element.attributes;
    var length = attributes.length;

    for (var i = 0; i < length; i++)
    {
      var attribute = attributes[i];

      if (attribute.specified)
      {
        var name = attribute.name;
        var value = attribute.value;
        html += " " + name + '="' + value + '"';
      }
    }

    html += ">";
    html += this.element.innerHTML;
    html += "</" + name + ">";
  }

  if (cleanIds)
  {
    html = this.removeGeneratedIds(html);
  }

  return this.cleanupHtml(html);
};

DomElement.prototype.getRect = function()
{
  var offset = this.getOffset();
  var size = this.getSize();

  var rect =
  {
    left: offset.left,
    top: offset.top,
    width: size.width,
    height: size.height
  };

  return rect;
};

DomElement.prototype.getSize = function()
{
  var element = jQuery(this.element);

  var size =
  {
    width: element.outerWidth(),
    height: element.outerHeight()
  };

  return size;
};

DomElement.prototype.removeGeneratedIds = function(html)
{
  return html.replace(new RegExp(' id="?' + idPrefix + '[0-9]*"?', "gi"), "");
};