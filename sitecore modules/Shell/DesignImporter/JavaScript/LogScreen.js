﻿/*
 * The Log Screen.
 */

function LogScreen()
{
  this.messages = [];
  this.errors = 0;
  this.warnings = 0;
  this.informationMessages = 0;
  this.showErrors = true;
  this.showWarnings = true;
  this.showInformationMessages = true;
  this.lastReceivedMessageId = -1;
  this.pingInterval = 100;

  this.errorButton = jQuery("#ErrorButton");
  this.warningButton = jQuery("#WarningButton");
  this.informationButton = jQuery("#InformationButton");
  this.grid = jQuery("#Grid");
  this.gridHeaders = jQuery("#GridHeaders");
  this.scrollPanel = jQuery("#ScrollPanel");

  var owner = this;

  jQuery(window).resize(function(event) { owner.scaleUi(); });
  this.errorButton.click(function(event) { owner.onTabButtonClick(event); });
  this.warningButton.click(function(event) { owner.onTabButtonClick(event); });
  this.informationButton.click(function(event) { owner.onTabButtonClick(event); });

  this.scaleUi();

  this.timer = setInterval(function(event) { owner.ping(); }, this.pingInterval);
}

LogScreen.prototype.alignGrid = function()
{
  var cell1 = this.gridHeaders.find("td.Time:first");
  var cell2 = jQuery("#GridData").find("td.Time:first");
  var width = cell2.outerWidth();
  cell1.width(width - 12);
};

LogScreen.prototype.displayLogMessages = function(response)
{
  var newMessages = JSON.parse(response);
  var count = newMessages.length;

  if (count == 0)
  {
    return;
  }

  var lastMessage = newMessages[count - 1];
  var lastMessageId = lastMessage.Id;
  this.lastReceivedMessageId = lastMessageId;

  for (var i = 0; i < newMessages.length; i++)
  {
    var message = newMessages[i];
    this.messages.push(message);
    var type = message.Type;

    switch (type)
    {
      case "Error":
        this.errors++;
        break;

      case "Warning":
        this.warnings++;
        break;

      case "Information":
        this.informationMessages++;
        break;
    }

    this.errorButton.html(this.errors + " " + Texts.Errors);
    this.warningButton.html(this.warnings + " " + Texts.Warnings);
    this.informationButton.html(this.informationMessages + " " + Texts.Messages);
  }

  this.refreshGrid();
};

LogScreen.prototype.getDisplayMessages = function()
{
  var result = [];

  for (var i = 0; i < this.messages.length; i++)
  {
    var message = this.messages[i];
    var type = message.Type;

    if (((type == "Error") && this.showErrors) ||
        ((type == "Warning") && this.showWarnings) ||
        ((type == "Information") && this.showInformationMessages))
    {
      result.push(message);
    }
  }

  return result;
};

LogScreen.prototype.onTabButtonClick = function(event)
{
  var button = event.target;
  var buttonId = button.id;

  switch (buttonId)
  {
    case "ErrorButton":
    {
      this.showErrors = !this.showErrors;
      var className = "TabButton" + (this.showErrors ? " Selected" : "");
      this.errorButton.attr("class", className);
      break;
    }

    case "WarningButton":
    {
      this.showWarnings = !this.showWarnings;
      var className = "TabButton" + (this.showWarnings ? " Selected" : "");
      this.warningButton.attr("class", className);
      break;
    }

    case "InformationButton":
    {
      this.showInformationMessages = !this.showInformationMessages;
      var className = "TabButton" + (this.showInformationMessages ? " Selected" : "");
      this.informationButton.attr("class", className);
      break;
    }
  }

  this.refreshGrid();
};

LogScreen.prototype.ping = function()
{
  var owner = this;
  var request = { action: "getLogMessages", id: owner.lastReceivedMessageId };
  var onSuccess = function(response) { owner.displayLogMessages(response); };
  sendRequest(request, onSuccess);
};

LogScreen.prototype.refreshGrid = function()
{
  var displayMessages = this.getDisplayMessages();

  var html = '<table id="GridData"><tbody>';

  for (var i = 0; i < displayMessages.length; i++)
  {
    var message = displayMessages[i];
    var time = message.Time;
    var type = message.Type;
    var description = message.Description;

    time = time.split(" ").join("&nbsp;");
    description = description.split(" ").join("&nbsp;");

    html +=
      "<tr>" +
      '<td class="Time">' + time + "</td>" +
      '<td class="' + type + '">' + description + "</td>" +
      "</tr>";
  }

  html += "</tbody></table>";

  this.scrollPanel.html(html);
  this.scrollPanel[0].scrollTop = this.scrollPanel[0].scrollHeight;
  this.alignGrid();
};

LogScreen.prototype.scaleUi = function()
{
  var gridHeight = this.grid.outerHeight();
  var gridHeadersHeight = this.gridHeaders.outerHeight();
  var scrollPanelHeight = gridHeight - gridHeadersHeight;
  this.scrollPanel.height(scrollPanelHeight - 20);
};

jQuery(document).ready(function(event) { new LogScreen(); });