/*
 * The Ajax utilities.
 */

function sendRequest(request, onSuccess)
{
  var async = (onSuccess !== undefined) ? true : false;

  var parameters = "";

  for (var parameterName in request)
  {
    if (parameterName === undefined)
    {
      continue;
    }

    var parameterValue = request[parameterName];

    if (parameters)
    {
      parameters += "&";
    }

    parameters += encodeURIComponent(parameterName);
    parameters += "=";
    parameters += encodeURIComponent(parameterValue);
  }

  var result;

  jQuery.ajax(
  {
    url: "WebHandler.ashx",
    data: parameters,
    type: "post",
    async: async,
    success: function(response)
    {
      if (async)
      {
        onSuccess(response);
      }

      result = response;
    }
  });

  return result;
}