﻿(function ($)
{
  var check = false;
  var isRelative = true;

  $.elementFromPoint = function(documentElement, x, y)
  {
    if(!documentElement.elementFromPoint)
    {
      return null;
    }

    if (!check)
    {
      var scrollTop;
      var scrollLeft;

      if ((scrollTop = $(documentElement).scrollTop()) > 0)
      {
        isRelative = documentElement.elementFromPoint(0, scrollTop + $(window).height() - 1) == null;
      }
      else if ((scrollLeft = $(documentElement).scrollLeft()) > 0)
      {
        isRelative = (documentElement.elementFromPoint(scrollLeft + $(window).width() - 1, 0) == null);
      }

      check = (scrollLeft > 0) || (scrollTop > 0);
    }

    if (!isRelative)
    {
      x += $(documentElement).scrollLeft();
      y += $(documentElement).scrollTop();
    }

    return documentElement.elementFromPoint(x, y);
  }

})(jQuery);

function DesignImporter()
{
  this.pageFrame = jQuery("#PageFrame");
  this.waitFrame = jQuery("#WaitFrame");
  this.tabPanel = jQuery("#Tabs");
  this.treeTab = jQuery("#TreeTab");
  this.textTab = jQuery("#TextTab");
  this.htmlTab = jQuery("#HtmlTab");
  this.previewTab = jQuery("#PreviewTab");
  this.previewFrame = jQuery("#PreviewFrame");
  this.logTab = jQuery("#LogTab");

  new PageExplorer();

  this.domTree = new DomTree(this.treeTab);
  this.domTree.nodeClickHandler = { owner: this, methodName: "onTreeNodeClick" };

  var owner = this;
  jQuery(window).resize(function(event) { owner.windowResize(); });
  this.pageFrame.load(function(event) { owner.onPageLoaded(); });

  this.resetState();
  this.createRibbon();
  this.windowResize();
}


DesignImporter.prototype.canCrop = function()
{
  var element = this.selectedElement;

  if (!element)
  {
    return false;
  }

  if (element[0].nodeName.toLowerCase() == "body")
  {
    return false;
  }

  var elementId = element[0].id;

  for (var i = 0; i < this.fieldRenderers.length; i++)
  {
    var id = this.fieldRenderers[i].ElementId;

    if (id == elementId)
    {
      continue;
    }

    if (element.find("#" + id).length == 0)
    {
      return false;
    }
  }

  for (var i = 0; i < this.placeholders.length; i++)
  {
    var id = this.placeholders[i].ElementId;

    if (id == elementId)
    {
      continue;
    }

    if (element.find("#" + id).length == 0)
    {
      return false;
    }
  }

  return true;
};

DesignImporter.prototype.canRemove = function()
{
  var element = this.selectedElement;

  if (!element)
  {
    return false;
  }

  if (this.isElementProtected(element))
  {
    return false;
  }

  var elementId = element[0].id;

  for (var i = 0; i < this.fieldRenderers.length; i++)
  {
    var id = this.fieldRenderers[i].ElementId;

    if (id == elementId)
    {
      return false;
    }

    if (element.find("#" + id).length > 0)
    {
      return false;
    }
  }

  for (var i = 0; i < this.placeholders.length; i++)
  {
    var id = this.placeholders[i].ElementId;

    if (id == elementId)
    {
      return false;
    }

    if (element.find("#" + id).length > 0)
    {
      return false;
    }
  }

  return true;
};

DesignImporter.prototype.canReplace = function()
{
  var element = this.selectedElement;

  if (!element)
  {
    return false;
  }

  if (this.isElementProtected(element))
  {
    return false;
  }

  var elementId = element[0].id;

  for (var i = 0; i < this.fieldRenderers.length; i++)
  {
    var id = this.fieldRenderers[i].ElementId;

    if (id == elementId)
    {
      return false;
    }

    if (element.parents("#" + id).length > 0)
    {
      return false;
    }

    if (element.find("#" + id).length > 0)
    {
      return false;
    }
  }

  for (var i = 0; i < this.placeholders.length; i++)
  {
    var id = this.placeholders[i].ElementId;

    if (id == elementId)
    {
      return false;
    }

    if (element.parents("#" + id).length > 0)
    {
      return false;
    }

    if (element.find("#" + id).length > 0)
    {
      return false;
    }
  }

  return true;
};

DesignImporter.prototype.coverPage = function()
{
  var cover = this.getById(coverId);

  if (cover.length)
  {
    cover.remove();
  }

  var page = this.getPageDocument();

  this.pageCover = jQuery("<div></div>");
  this.pageCover.css("background", "black");
  this.pageCover.css("filter", "alpha(opacity = 0)");
  this.pageCover.css("height", page.height() + "px");
  this.pageCover.css("left", "0");
  this.pageCover.css("opacity", "0");
  this.pageCover.css("position", "absolute");
  this.pageCover.css("top", "0");
  this.pageCover.css("width", page.width() + "px");
  this.pageCover.css("z-index", "1000000000");
  this.pageCover.attr("id", coverId);

  var owner = this;
  this.pageCover.click(function(event) { owner.onPageClick(event); });
  this.pageCover.mousemove(function(event) { owner.onPageMouseMove(event); });
  this.pageCover.mouseout(function(event) { owner.onPageMouseOut(event); });

  this.getPageBody().append(this.pageCover);
};

DesignImporter.prototype.createRibbon = function()
{
  var owner = this;

  var openButton =
  {
    name: Texts.Open,
    type: "Large",
    icon: "Core3/24x24/open_document.png",
    tooltip: Texts.OpenAWebPage,
    enable: function() { return true; },
    click: function() { owner.openWebsite(); }
  };

  var importButton =
  {
    name: Texts.Import,
    type: "Large",
    icon: "Applications/24x24/disk_blue.png",
    tooltip: Texts.SaveAnyChanges,
    enable: function() { return owner.isPageLoaded(); },
    click: function() { owner.save(); }
  };

  var insertFieldButton =
  {
    name: Texts.Field,
    type: "Large",
    icon: "Software/24x24/element_add.png",
    tooltip: Texts.ReplaceSelectionWithAField,
    enable: function() { return owner.canReplace(); },
    click: function() { owner.insertField(); }
  };

  var insertPlaceholderButton =
  {
    name: Texts.Placeholder,
    type: "Large",
    icon: "Software/24x24/element_add.png",
    tooltip: Texts.ReplaceSelectionWithAPlaceholder,
    enable: function() { return owner.canReplace(); },
    click: function() { owner.insertPlaceholder(); }
  };

  var removeButton =
  {
    name: Texts.Delete,
    type: "Small",
    icon: "Applications/16x16/delete2.png",
    tooltip: Texts.DeleteTheSelection,
    enable: function() { return owner.canRemove(); },
    click: function() { owner.remove(); }
  };

  var cropButton =
  {
    name: Texts.Crop,
    type: "Small",
    icon: "Software/16x16/elements_selection_delete.png",
    tooltip: Texts.DeleteAllTheElementsExceptThisElement,
    enable: function() { return owner.canCrop(); },
    click: function() { owner.crop(); }
  };

  var undoButton =
  {
    name: Texts.Undo1,
    type: "Large",
    icon: "Applications/24x24/undo.png",
    tooltip: Texts.Undo2,
    enable: function() { return owner.actions.hasUndo(); },
    click: function() { owner.undo(); }
  };

  var redoButton =
  {
    name: Texts.Redo1,
    type: "Large",
    icon: "Applications/24x24/redo.png",
    tooltip: Texts.Redo2,
    enable: function() { return owner.actions.hasRedo(); },
    click: function() { owner.redo(); }
  };

  var selectNoneButton =
  {
    name: Texts.SelectNone1,
    type: "Large",
    icon: "Software/24x24/element_selection_delete.png",
    tooltip: Texts.SelectNone2,
    enable: function() { return owner.selectedElement != null; },
    click: function() { owner.selectNone(); }
  };

  var previewButton =
  {
    name: Texts.Preview,
    type: "Large",
    icon: "Applications/24x24/view.png",
    tooltip: Texts.OpenImportedPageInPreview,
    enable: function() { return owner.previewUri != null; },
    click: function() { owner.openPreview(); }
  };

  var remapButton =
  {
    name: Texts.Remap,
    type: "Large",
    icon: "Network/24x24/link_edit.png",
    tooltip: Texts.OpenUrlRemapper,
    enable: function() { return owner.urlRemapperUri != null; },
    click: function() { return scForm.invoke("designimporter:remap(id=" + owner.urlRemapperUri + ")"); }
  };

  var pageEditorButton =
  {
    name: Texts.PageEditor,
    type: "Large",
    icon: "Applications/24x24/document_edit.png",
    tooltip: Texts.OpenImportedPageInPageEditor,
    enable: function() { return owner.pageEditorUri != null; },
    click: function() { owner.openPageEditor(); }
  };

  this.ribbon = new Ribbon();
  this.ribbon.addButton(openButton);
  this.ribbon.addButton(importButton);
  this.ribbon.addButton(insertFieldButton);
  this.ribbon.addButton(insertPlaceholderButton);
  this.ribbon.addButton(removeButton);
  this.ribbon.addButton(cropButton);
  this.ribbon.addButton(undoButton);
  this.ribbon.addButton(redoButton);
  this.ribbon.addButton(selectNoneButton);
  this.ribbon.addButton(previewButton);
  this.ribbon.addButton(remapButton);
  this.ribbon.addButton(pageEditorButton);
  this.ribbon.refresh();
};

DesignImporter.prototype.crop = function()
{
  var element = this.selectedElement;
  var elementId = element[0].id;
  var body = this.getPageBody();

  var html1 = body.html();
  body.empty();
  body.append(element);
  var html2 = body.html();

  var action =
  {
    elementId: elementId,
    html1: html1,
    html2: html2,
    name: "Crop"
  };

  this.actions.add(action);

  this.domTree.refreshElement(body);
  this.highlightReplacedElements();
  this.goToElement(elementId);
  this.ribbon.refresh();
  this.coverPage();
};

DesignImporter.prototype.getById = function(id)
{
  return this.getPageDocument().find("[id='" + id + "']");
};

DesignImporter.prototype.getByClass = function(className)
{
  return this.getPageDocument().find("[class='" + className + "']");
}

DesignImporter.prototype.getElementHtml = function(elementId)
{
  var element = this.getPageDocument().find("#" + elementId);

  return element.length ? new DomElement(element[0]).getOuterHtml(true) : "";
};

DesignImporter.prototype.getFieldNames = function()
{
  var result = [];

  for (var i = 0; i < this.fieldRenderers.length; i++)
  {
    result.push(this.fieldRenderers[i].FieldName);
  }

  return result;
};

DesignImporter.prototype.getImportingMode = function()
{
  return this.importingMode ? this.importingMode : "layout";
};

DesignImporter.prototype.getImportingName = function()
{
  if (this.importingName)
  {
    return this.importingName;
  }

  var name;

  var title = this.page.getTitle();

  if (title)
  {
    name = title;
  }
  else
  {
    name = this.openUrl;

    if (name.lastIndexOf("/") == name.length - 1)
    {
      name = name.substring(0, name.length - 1);
    }

    var index1 = name.lastIndexOf("/");

    if (index1 < 0)
    {
      throw "Wrong URI of the page.";
    }

    index1++;

    var index2 = name.lastIndexOf(".");

    if (index2 < index1)
    {
      index2 = name.length;
    }

    name = name.substring(index1, index2);
  }

  if (!this.invalidItemNameChars)
  {
    this.invalidItemNameChars = sendRequest({ action: "getInvalidItemNameChars" });
  }

  for (var i = 0; i < this.invalidItemNameChars.length; i++)
  {
    name = name.split(this.invalidItemNameChars.charAt(i)).join("");
  }

  this.importingName = name;

  return name;
};

DesignImporter.prototype.getLogMessages = function()
{
  var request = { action: "getLogMessages", id: -1 };
  var response = sendRequest(request);
  this.logMessages = JSON.parse(response);
  this.refreshLogTab();
};

DesignImporter.prototype.getPageBody = function()
{
  return this.getPageDocument().find("body");
};

DesignImporter.prototype.getPageData = function()
{
  if (!this.importingName)
  {
    throw "Importing name not defined.";
  }

  if (!this.importingMode)
  {
    throw "Importing mode not defined.";
  }

  var html;

  switch (this.importingMode)
  {
    case 'layout':
      html = this.page.getSource();
      break;

    case 'sublayout':
      html = this.page.getBodyHtml();
      break;
  }

  var baseUri = this.page.getUri();
  var name = this.importingName;
  var mode = this.importingMode;
  var fieldRenderers = this.fieldRenderers;
  var placeholders = this.placeholders;
  var sessionId = this.sessionId;

  var pageData =
  {
    Name: name,
    Mode: mode,
    BaseUri: baseUri,
    Html: html,
    FieldRenderers: fieldRenderers,
    Placeholders: placeholders,
    SessionId: sessionId
  };

  return JSON.stringify(pageData);
};

DesignImporter.prototype.getPageDocument = function()
{
  return this.pageFrame.contents();
};

DesignImporter.prototype.getPlaceholderKeys = function()
{
  var result = [];

  for (var i = 0; i < this.placeholders.length; i++)
  {
    result.push(this.placeholders[i].Key);
  }

  return result;
};

DesignImporter.prototype.getPreviewHtml = function()
{
  if (!this.selectedElement)
  {
    return "";
  }

  // Removing scripts.
  var element = this.selectedElement.clone();
  var scripts = element.find("script");

  for (var i = 0; i < scripts.length; i++)
  {
    jQuery(scripts[i]).remove();
  }

  var html = '<base href="' + this.openUrl + '"/>';
  html += new DomElement(element[0]).getOuterHtml(true);

  return html;
};

DesignImporter.prototype.goToElement = function(elementId)
{
  if (!elementId)
  {
    throw "Element ID not specified.";
  }

  this.domTree.expand(elementId, true);
  this.domTree.select(elementId);
  this.selectElement(elementId);
};

DesignImporter.prototype.highlightElement = function(parameters)
{
  var className = parameters.className;
  var color = parameters.color;
  var elementId = parameters.elementId;

  var element = this.getById(elementId);
  var domElement = new DomElement(element[0]);
  var elementBounds = domElement.getRect();
  var x = elementBounds.left;
  var y = elementBounds.top;
  var w = elementBounds.width;
  var h = elementBounds.height;

  var lineBounds =
  [
    { x: x, y: y, w: w, h: 1 },
    { x: x + w - 1, y: y, w: 1, h: h },
    { x: x, y: y + h - 1, w: w, h: 1 },
    { x: x, y: y, w: 1, h: h }
  ];

  var body = this.getPageBody();
  var lines = [];

  for (var i = 0; i < lineBounds.length; i++)
  {
    var bound = lineBounds[i];

    var line = jQuery("<div></div>");
    line.css("background", color);
    line.css("height", bound.h + "px");
    line.css("font-size", "0");
    line.css("left", bound.x + "px");
    line.css("position", "absolute");
    line.css("top", bound.y + "px");
    line.css("width", bound.w + "px");
    line.css("z-index", "2000000000");

    if (className)
    {
      line.attr("class", className);
    }

    body.append(line);
    lines.push(line);
  }
};

DesignImporter.prototype.highlightFieldRenderers = function()
{
  this.getByClass(fieldRendererClass).remove();

  for (var i = 0; i < this.fieldRenderers.length; i++)
  {
    var fieldRenderer = this.fieldRenderers[i];
    var elementId = fieldRenderer.ElementId;

    var parameters =
    {
      className: fieldRendererClass,
      color: "red",
      elementId: elementId
    };

    this.highlightElement(parameters);
  }
};

DesignImporter.prototype.highlightPlaceholder = function(elementId, key)
{
  var host = location.host;
  var backgroundImage = "http://" + host + "/sitecore modules/Shell/DesignImporter/Images/EmptyPlaceholderBackground.png";

  var selection = jQuery("<div></div>");
  selection.attr("class", placeholderClass);
  selection.attr("id", elementId + "_Placeholder");
  selection.css("background-attachment", "scroll");
  selection.css("background-color", "transparent");
  selection.css("background-image", "url('" + backgroundImage + "')");
  selection.css("background-position", "0 0");
  selection.css("background-repeat", "repeat");
  selection.css("padding", "12px");
  selection.css("text-align", "center");
  selection.html(key);

  this.getById(elementId).before(selection);

  return selection;
};

DesignImporter.prototype.highlightPlaceholders = function()
{
  this.getByClass(placeholderClass).remove();

  for (var i = 0; i < this.placeholders.length; i++)
  {
    var placeholder = this.placeholders[i];
    var elementId = placeholder.ElementId;
    var key = placeholder.Key;
    this.highlightPlaceholder(elementId, key);
  }
}

DesignImporter.prototype.highlightReplacedElements = function()
{
  this.highlightPlaceholders();
  this.highlightFieldRenderers();
};

DesignImporter.prototype.highlightSelectedElement = function()
{
  this.getByClass(selectionClass).remove();

  if (!this.selectedElement)
  {
    return;
  }

  var element = this.selectedElement;
  var elementId = element[0].id;

  for (var i = 0; i < this.placeholders.length; i++)
  {
    var id = this.placeholders[i].ElementId;

    if (id == elementId)
    {
      elementId = elementId + "_Placeholder";
      break;
    }
  }

  var parameters =
  {
    className: selectionClass,
    color: "#4B6996",
    elementId: elementId
  };

  this.highlightElement(parameters);

  if (this.scrollToSelectedElement)
  {
    this.pageFrame.scrollTo(element);
    this.scrollToSelectedElement = false;
  }
};

DesignImporter.prototype.insertField = function()
{
  var element = this.selectedElement;
  var elementId = element[0].id;
  var isIrreplaceable = this.isElementIrreplaceable(element);
  var isText = this.domTree.selectedNodeLabel.hasClass("Text");
  var tagName = isText ? "#text" : element[0].tagName;
  var text = element.text();
  var html = isText ? text : this.getElementHtml(elementId);
  var previewHtml = this.getPreviewHtml();
  var fieldNames = this.getFieldNames();

  var parameters =
  {
    fieldNames: fieldNames,
    html: html,
    previewHtml: previewHtml,
    tagName: tagName,
    text: text
  };

  var dialogOptions =
  {
    address: "InsertField.aspx",
    height: 535,
    name: "InsertFieldDialog",
    parameters: parameters,
    width: 600
  };

  var dialog = new Dialog(dialogOptions);
  var result = dialog.showModal();

  if (!result)
  {
    return;
  }

  var replaceContent = isIrreplaceable || isText || result.replaceContent;
  var content = replaceContent ? element.html() : new DomElement(element[0]).getOuterHtml(false);
  var markText = isText || result.replaceContent;

  var fieldRenderer =
  {
    Content: content,
    ElementId: elementId,
    FieldName: result.fieldName,
    FieldType: result.fieldType,
    ReplaceContent: replaceContent
  };

  this.fieldRenderers.push(fieldRenderer);

  var action =
  {
    fieldRenderer: fieldRenderer,
    isText: markText,
    name: "Insert Field"
  };

  this.actions.add(action);

  this.domTree.markElementAsReplaced(elementId, markText, true);
  this.highlightReplacedElements();

  this.ribbon.refresh();
};

DesignImporter.prototype.insertPlaceholder = function()
{
  var keys = this.getPlaceholderKeys();
  var parameters = { keys: keys };

  var dialogOptions =
  {
    address: "InsertPlaceholder.aspx",
    height: 80,
    name: "InsertPlaceholderDialog",
    parameters: parameters,
    width: 275
  };

  var dialog = new Dialog(dialogOptions);
  var key = dialog.showModal();

  if (key == null)
  {
    return;
  }

  var isText = this.domTree.selectedNodeLabel.hasClass("Text");
  var element = this.selectedElement;
  var elementId = element[0].id;

  var placeholder =
  {
    ElementId: elementId,
    Key: key,
    ReplaceContent: isText
  };

  this.placeholders.push(placeholder);

  var action =
  {
    name: "Insert Placeholder",
    placeholder: placeholder
  };

  this.actions.add(action);

  element.hide();

  this.domTree.markElementAsReplaced(elementId, isText, true);
  this.highlightReplacedElements();
  this.goToElement(elementId);
  this.ribbon.refresh();
};

DesignImporter.prototype.isElementIrreplaceable = function(element)
{
  return jQuery.inArray(element[0].nodeName.toLowerCase(), Settings.irreplaceableElements) >= 0;
};

DesignImporter.prototype.isElementProtected = function(element)
{
  var protectedElements = ["html", "head", "body"];

  for (var i = 0; i < protectedElements.length; i++)
  {
    if (element[0].nodeName.toLowerCase() == protectedElements[i])
    {
      return true;
    }
  }

  return false;
};

DesignImporter.prototype.isPageConsistent = function()
{
  return this.getPageBody().length > 0;
}

DesignImporter.prototype.isPageLoaded = function()
{
  if (this.pageLoaded != null)
  {
    return this.pageLoaded;
  }

  var url = this.getPageDocument()[0].URL;

  this.pageLoaded =
    !url.match(/DesignImporter\/404.aspx$/) &&
    !url.match(/DesignImporter\/NoPage.aspx$/);

  return this.pageLoaded;
};

DesignImporter.prototype.onPageClick = function(event)
{
  if (!this.isPageLoaded())
  {
    return;
  }

  var x = event.clientX;
  var y = event.clientY;
  var page = this.pageFrame.contents();

  this.pageCover.hide();
  var element = jQuery.elementFromPoint(page[0], x, y);
  this.pageCover.show();

  if (!element)
  {
    return;
  }

  if (!element.id)
  {
    return;
  }

  if (element.className == "Selection")
  {
    return;
  }

  var elementId = (element.className == placeholderClass)
    ? element.id.replace("_Placeholder", "") : element.id;

  this.goToElement(elementId);
};

DesignImporter.prototype.onPageLoaded = function()
{
  this.stopWaitAnimation();

  this.resetState();
  this.ribbon.refresh();
  this.refreshTextTab();
  this.refreshHtmlTab();
  this.refreshPreviewTab();
  this.refreshLogTab();

  if (!this.isPageLoaded())
  {
    return;
  }

  if (!this.isPageConsistent())
  {
    alert(Texts.TheWebPageCannotBeLoadedBecauseThePageStructureIsInconsistent);
    this.pageFrame.attr("src", "NoPage.aspx");
    return;
  }

  this.setIds();
  this.page = new Page(this.pageFrame);
  this.refreshDom();
  this.coverPage();
};

DesignImporter.prototype.onPageMouseMove = function(event)
{
  if (!this.isPageLoaded())
  {
    return;
  }

  this.pageCover.hide();
  var x = event.clientX;
  var y = event.clientY;
  var page = this.pageFrame.contents();
  var element = jQuery.elementFromPoint(page[0], x, y);
  this.pageCover.show();

  if (!element || !element.id)
  {
    return;
  }

  this.getByClass(hoverSelectionClass).remove();

  var parameters =
  {
    className: hoverSelectionClass,
    color: "magenta",
    elementId: element.id
  };

  this.highlightElement(parameters);
};

DesignImporter.prototype.onPageMouseOut = function(event)
{
  this.getByClass(hoverSelectionClass).remove();
};

DesignImporter.prototype.onTreeNodeClick = function(elementId)
{
  this.scrollToSelectedElement = true;
  this.selectElement(elementId);
};

DesignImporter.prototype.openPageEditor = function()
{
  this.showDialog(this.pageEditorUri, "PageEditor");
};

DesignImporter.prototype.openPreview = function()
{
  this.showDialog(this.previewUri, "Preview");
};

DesignImporter.prototype.openUrlRemapper = function()
{
  this.showDialog(this.urlRemapperUri, "UrlRemapper");
};

DesignImporter.prototype.openWebsite = function()
{
  var dialog = new Dialog({
    address: "Open.aspx",
    name: "OpenDialog",
    parameters: null,
    width: 460,
    height: 70
  });

  var url = dialog.showModal();

  if (url == null)
  {
    return;
  }

  this.openUrl = url;
  this.sessionId = sendRequest({ action: "getSessionId" });
  this.isDesignSaved = false;
  this.domTree.clear();
  this.startWaitAnimation();
  this.pageFrame.attr('src', 'WebHandler.ashx?action=getPage&uri=' + encodeURIComponent(url));
};

DesignImporter.prototype.redo = function()
{
  if (!this.actions.hasRedo())
  {
    return;
  }

  var action = this.actions.getRedoAction();

  switch (action.name)
  {
    case "Crop":
      this.redoCrop(action);
      return;

    case "Insert Field":
      this.redoInsertField(action);
      return;

    case "Insert Placeholder":
      this.redoInsertPlaceholder(action);
      return;

    case "Remove":
      this.redoRemove(action);
      return;
  }
};

DesignImporter.prototype.redoCrop = function(action)
{
  var elementId = action.elementId;
  var body = this.getPageBody();
  var html = action.html2;

  body[0].innerHTML = html;

  this.ribbon.refresh();
  this.domTree.refreshElement(body);
  this.highlightReplacedElements();
  this.goToElement(elementId);
  this.coverPage();
};

DesignImporter.prototype.redoInsertField = function(action)
{
  var fieldRenderer = action.fieldRenderer;
  var elementId = fieldRenderer.ElementId;
  var body = this.getPageBody();
  var isText = action.isText;

  this.fieldRenderers.push(fieldRenderer);
  this.domTree.markElementAsReplaced(elementId, isText, true);

  this.ribbon.refresh();
  this.highlightReplacedElements();
  this.goToElement(elementId);
};

DesignImporter.prototype.redoInsertPlaceholder = function(action)
{
  var placeholder = action.placeholder;
  var elementId = placeholder.ElementId;
  var element = this.getById(elementId);
  var isText = placeholder.ReplaceContent;

  this.placeholders.push(placeholder);
  element.hide();

  this.domTree.markElementAsReplaced(elementId, isText, true);
  this.highlightReplacedElements();
  this.goToElement(elementId);
  this.ribbon.refresh();
};

DesignImporter.prototype.redoRemove = function(action)
{
  var parentId = action.parentId;
  var parent = this.getById(parentId);
  var html = action.html2;

  parent.html(html);
  this.domTree.refreshElement(parent);

  this.ribbon.refresh();
  this.highlightReplacedElements();
  this.goToElement(parentId);
  this.coverPage();
};

DesignImporter.prototype.refreshDom = function()
{
  var body = this.getPageDocument().find("body")[0];

  if (!body.id) // Fix for IE: something removes ID of BODY element.
  {
    body.id = idPrefix + "1";
  }

  this.domTree.create(body);
};

DesignImporter.prototype.refreshHtmlTab = function()
{
  var element = this.selectedElement;
  var html = element ? this.getElementHtml(element[0].id) : "";
  this.htmlTab.text(html);
};

DesignImporter.prototype.refreshLogTab = function()
{
  jQuery("#LogMessages").remove();

  var noLogMessagePanel = jQuery("#NoLogMessages");

  if (!this.logMessages)
  {
    noLogMessagePanel.show();
    return;
  }

  noLogMessagePanel.hide();

  var html = '<table id="LogMessages"><tbody>';

  for (var i = 0; i < this.logMessages.length; i++)
  {
    var message = this.logMessages[i];
    var description = message.Description;
    var time = message.Time;
    var type = message.Type;

    time = time.split(" ").join("&nbsp;");
    description = description.split(" ").join("&nbsp;");

    html +=
      "<tr>" +
      '<td class="Time">' + time + "</td>" +
      '<td class="' + type + '">' + description + "</td>" +
      "</tr>";
  }

  html += "</tbody></table>";

  this.logTab.append(jQuery(html));
};

DesignImporter.prototype.refreshPreviewTab = function()
{
  var preview = this.previewFrame.contents()[0];
  var html = this.getPreviewHtml();

  preview.open();
  preview.writeln(html);
  preview.close();
};

DesignImporter.prototype.refreshTextTab = function()
{
  this.textTab.text(this.selectedElement ? this.selectedElement.text() : "");
};

DesignImporter.prototype.remove = function()
{
  var element = this.selectedElement;
  var elementId = element[0].id;
  var parent = element.parent();
  var parentId = parent[0].id;

  var html1 = parent.html();
  element.remove();
  var html2 = parent.html();

  var action =
  {
    elementId: elementId,
    html1: html1,
    html2: html2,
    name: "Remove",
    parentId: parentId
  };

  this.actions.add(action);

  this.ribbon.refresh();
  this.highlightReplacedElements();
  this.domTree.refreshElement(parent);
  this.goToElement(parentId);
  this.coverPage();
};

DesignImporter.prototype.reset = function()
{
  location.href = "DesignImporter.aspx";
};

DesignImporter.prototype.resetState = function()
{
  this.pageLoaded = null;
  this.actions = new ActionQueue();
  this.fieldRenderers = [];
  this.placeholders = [];
  this.selectedElement = null;
  this.scrollToSelectedElement = false;

  this.importingName = null;
  this.importingMode = null;

  this.previewUri = null;
  this.pageEditorUri = null;
  this.urlRemapperUri = null;

  this.logMessages = null;
};

DesignImporter.prototype.save = function()
{
  if (!this.isPageLoaded())
  {
    return;
  }

  var name = this.getImportingName();
  var mode = this.getImportingMode();

  var parameters =
  {
    name: name,
    mode: mode
  };

  if (hideSaveOptions)
  {
    parameters.hideSaveOptions = true;
  }

  var dialogOptions =
  {
    address: "Save.aspx",
    name: "SaveDialog",
    parameters: parameters,
    width: 370,
    height: 150
  };

  var dialog = new Dialog(dialogOptions);
  var result = dialog.showModal();

  if (!result)
  {
    return;
  }

  this.pageEditorUri = null;
  this.previewUri = null;
  this.urlRemapperUri = null;
  this.ribbon.refresh();

  this.importingName = result.name;
  this.importingMode = result.mode;

  var isImported = sendRequest({ action: "isImported", name: result.name }) == "1";

  if (isImported && !window.confirm(Texts.TheWebPageHasAlreadyBeenImported))
  {
    return;
  }

  dialogOptions =
  {
    address: "LogScreen.aspx",
    name: "LogScreen",
    width: 800,
    height: 600
  };

  dialog = new Dialog(dialogOptions);
  this.logScreen = dialog.show();

  var owner = this;
  var pageData = this.getPageData();
  var request = { action: "save", pageData: pageData };
  var onSuccess = function(response) { owner.saveComplete(response); }
  sendRequest(request, onSuccess);
};

DesignImporter.prototype.saveComplete = function(response)
{
  if (this.logScreen)
  {
    this.logScreen.close();
  }

  this.getLogMessages();

  if (response == "failed")
  {
    alert(Texts.FailedTheWebPageHasNotBeenImported);
    return;
  }

  if (response != "null")
  {
    var itemId = response;
    this.pageEditorUri = "/?sc_itemid=" + itemId + "&sc_mode=edit";
    this.previewUri = "/?sc_itemid=" + itemId + "&sc_mode=preview";
    this.urlRemapperUri = "/sitecore modules/Shell/DesignImporter/UrlRemapper.aspx?id=" + itemId;
    this.ribbon.refresh();
  }

  this.isDesignSaved = true;
  alert(Texts.DownloadSuccessful);
};

DesignImporter.prototype.selectElement = function(elementId)
{
  if (!elementId)
  {
    return;
  }

  this.selectedElement = this.getPageDocument().find("#" + elementId);

  this.highlightSelectedElement();

  this.ribbon.refresh();
  this.refreshTextTab();
  this.refreshHtmlTab();
  this.refreshPreviewTab();
};

DesignImporter.prototype.selectNone = function()
{
  this.selectedElement = null;
  this.highlightSelectedElement();

  this.ribbon.refresh();
  this.domTree.select(null);
  this.refreshTextTab();
  this.refreshHtmlTab();
  this.refreshPreviewTab();
};

DesignImporter.prototype.setId = function(element)
{
  if ((element.nodeName == "#text") || (element.nodeName == "#comment"))
  {
    return;
  }

  if (!element.id)
  {
    element.id = idPrefix + this.nextId++;
  }

  for (var i = 0; i < element.childNodes.length; i++)
  {
    this.setId(element.childNodes[i]);
  }
};

DesignImporter.prototype.setIds = function()
{
  this.nextId = 0;
  this.setId(this.getPageBody()[0]);
};

DesignImporter.prototype.showDialog = function(uri, windowName)
{
  var settings =
    "width=800,height=600,resizable=1,scrollbars=1,location=1," +
    "menubar=0,toolbar=0,directories=0,status=0,fullscreen=0";

  window.open(uri, windowName, settings);
};

DesignImporter.prototype.startWaitAnimation = function()
{
  // We need collapse page iframe rather than hide it,
  // because hiding affects execution of JavaScript code of displayed page.
  this.pageFrame.width(0);
  this.pageFrame.height(0);

  if (!this.waitAnimation)
  {
    var panel = jQuery("<div></div>");
    panel.css("background-image", "url('Images/Spinner.gif')");
    panel.css("height", "66px");
    panel.css("left", "50%");
    panel.css("margin-left", "-33px");
    panel.css("margin-top", "-33px");
    panel.css("position", "absolute");
    panel.css("top", "50%");
    panel.css("width", "66px");

    this.waitFrame.contents().find("body").append(panel);
    this.waitAmination = panel;
  }

  this.waitFrame.show();
};

DesignImporter.prototype.stopWaitAnimation = function()
{
  this.waitFrame.hide();
  this.pageFrame.css("width", "100%");
  this.pageFrame.css("height", "100%");
};

DesignImporter.prototype.undo = function()
{
  if (!this.actions.hasUndo())
  {
    return;
  }

  var action = this.actions.getUndoAction();

  switch (action.name)
  {
    case "Crop":
      this.undoCrop(action);
      return;

    case "Insert Field":
      this.undoInsertField(action);
      return;

    case "Insert Placeholder":
      this.undoInsertPlaceholder(action);
      return;

    case "Remove":
      this.undoRemove(action);
      return;
  }
};

DesignImporter.prototype.undoCrop = function(action)
{
  var elementId = action.elementId;
  var body = this.getPageBody();
  var html = action.html1;

  body[0].innerHTML = html;

  this.ribbon.refresh();
  this.domTree.refreshElement(body);
  this.highlightReplacedElements();
  this.goToElement(elementId);
  this.coverPage();
};

DesignImporter.prototype.undoInsertField = function(action)
{
  var fieldRenderer = action.fieldRenderer;
  var elementId = fieldRenderer.ElementId;
  var isText = action.isText;

  this.fieldRenderers.pop();
  this.domTree.markElementAsReplaced(elementId, isText, false);

  this.ribbon.refresh();
  this.highlightReplacedElements();
  this.goToElement(elementId);
};

DesignImporter.prototype.undoInsertPlaceholder = function(action)
{
  var placeholder = action.placeholder;
  var elementId = placeholder.ElementId;
  var element = this.getById(elementId);
  var isText = placeholder.ReplaceContent;

  this.placeholders.pop();
  element.show();

  this.domTree.markElementAsReplaced(elementId, isText, false);
  this.highlightReplacedElements();
  this.goToElement(elementId);
  this.ribbon.refresh();
};

DesignImporter.prototype.undoRemove = function(action)
{
  var elementId = action.elementId;
  var parentId = action.parentId;
  var parent = this.getById(parentId);
  var html = action.html1;

  parent.html(html);
  this.domTree.refreshElement(parent);

  this.ribbon.refresh();
  this.highlightReplacedElements();
  this.goToElement(elementId);
  this.coverPage();
};

DesignImporter.prototype.windowResize = function()
{
  var tabPanelHeight = (this.tabPanel.height() - 14) + "px";
  this.treeTab.css("height", tabPanelHeight);
  this.textTab.css("height", tabPanelHeight);
  this.htmlTab.css("height", tabPanelHeight);
  this.previewTab.css("height", tabPanelHeight);
  this.logTab.css("height", tabPanelHeight);

  this.highlightSelectedElement();
  this.highlightReplacedElements();
  this.coverPage();
};