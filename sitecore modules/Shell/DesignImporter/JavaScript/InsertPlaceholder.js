﻿function InsertPlaceholderDialog()
{
  window.returnValue = null;

  this.keyTextBox = jQuery("#KeyTextBox");
  this.errorMessage = jQuery("#ErrorMessage");
  this.okButton = jQuery("#OkButton");
  this.cancelButton = jQuery("#CancelButton");

  var owner = this;
  this.keyTextBox.keyup(function(event) { owner.refresh(); });
  this.okButton.click(function(event) { owner.onOkButtonClick(); });
  this.cancelButton.click(function(event) { window.close(); });

  this.refresh();
}

InsertPlaceholderDialog.prototype.isKeyInUse = function(key)
{
  for (var i = 0; i < parameters.keys.length; i++)
  {
    if (parameters.keys[i].toLowerCase() == key.toLowerCase())
    {
      return true;
    }
  }

  return false;
};

InsertPlaceholderDialog.prototype.onOkButtonClick = function()
{
  window.returnValue = this.keyTextBox.val();
  window.close();
};

InsertPlaceholderDialog.prototype.refresh = function()
{
  var key = this.keyTextBox.val();

  if (this.isKeyInUse(key))
  {
    this.setErrorMessage(Texts.KeyIsAlreadyInUse);

    return;
  }

  var owner = this;

  var onRequestComplete = function(responseText)
  {
    owner.setErrorMessage(responseText == "0" ? Texts.KeyIsNotValid : "");
  };

  sendRequest({ action: "isItemNameValid", name: key }, onRequestComplete);
};

InsertPlaceholderDialog.prototype.setErrorMessage = function(errorMessage)
{
  this.errorMessage.html(errorMessage);
  this.okButton[0].disabled = errorMessage != "";
};

jQuery(document).ready(function(event) { new InsertPlaceholderDialog(); });