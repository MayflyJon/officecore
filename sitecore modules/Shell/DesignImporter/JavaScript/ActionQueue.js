﻿/*
 * The action queue.
 */

function ActionQueue()
{
  this.actions = [];
  this.cursor = -1;
}

ActionQueue.prototype.add = function(action)
{
  this.cursor++;
  this.actions.splice(this.cursor, (this.actions.length - this.cursor));
  this.actions[this.cursor] = action;
};

ActionQueue.prototype.getRedoAction = function()
{
  if ((this.cursor + 1) >= this.actions.length)
  {
    return;
  }

  return this.actions[++this.cursor];
};

ActionQueue.prototype.getUndoAction = function()
{
  if (this.cursor < 0)
  {
    return;
  }

  return this.actions[this.cursor--];
};

ActionQueue.prototype.hasRedo = function()
{
  return (this.cursor + 1) < this.actions.length;
};

ActionQueue.prototype.hasUndo = function()
{
  return this.cursor > -1;
};