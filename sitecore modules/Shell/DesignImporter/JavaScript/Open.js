﻿function OpenDialog()
{
  window.returnValue = null;

  this.urlTextBox = jQuery("#UrlTextBox");
  this.okButton = jQuery("#OkButton");
  this.cancelButton = jQuery("#CancelButton");

  var owner = this;
  this.urlTextBox.keyup(function(event) { owner.onUrlTextBoxKeyUp(event); });
  this.okButton.click(function(event) { owner.onOkButtonClick(); });
  this.cancelButton.click(function(event) { window.close(); });

  this.loadUrl();
  this.refresh();
}

OpenDialog.prototype.loadUrl = function(url)
{
  var url = sendRequest({ action: "getWebPageUrl" });

  this.urlTextBox.val(url);
  this.urlTextBox.selectRange(0, url.length);
  this.urlTextBox[0].focus();
};

OpenDialog.prototype.fixUrl = function(url)
{
  if (url.toLowerCase().indexOf("http://") < 0 &&
      url.toLowerCase().indexOf("https://") < 0)
  {
    return "http://" + url;
  }

  return url;
};

OpenDialog.prototype.onOkButtonClick = function()
{
  var url = this.fixUrl(this.urlTextBox.val());

  sendRequest({ action: "saveWebPageUrl", webPageUrl: url });

  window.returnValue = url;
  window.close();
};

OpenDialog.prototype.onUrlTextBoxKeyUp = function(event)
{
  if ((event.keyCode == 13) && this.validate())
  {
    this.onOkButtonClick();

    return;
  }

  this.refresh();
};

OpenDialog.prototype.refresh = function()
{
  if (this.validate())
  {
    this.okButton.removeAttr("disabled");
  }
  else
  {
    this.okButton.attr("disabled", "disabled");
  }
};

OpenDialog.prototype.validate = function()
{
  return jQuery.trim(this.urlTextBox.val());
};

jQuery(document).ready(function(event) { new OpenDialog(); });