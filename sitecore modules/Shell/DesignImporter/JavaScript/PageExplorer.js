﻿function PageExplorer()
{
  this.control = jQuery("#PageExplorer");
  this.tabButtons = this.control.find(".TabButton");
  this.tabs = this.control.find(".Tab");

  var owner = this;
  this.tabButtons.click(function(event) { owner.onTabButtonClick(event); });
  this.tabButtons.mouseout(function(event) { owner.onTabButtonMouseOut(); });
  this.tabButtons.mouseover(function(event) { owner.onTabButtonMouseOver(event); });
}

PageExplorer.prototype.onTabButtonClick = function(event)
{
  var tabButton = jQuery(event.target);
  var mode = tabButton[0].id.replace("TabButton", "");

  this.control.find(".TabButton.Active").removeClass("Active");
  tabButton.removeClass("Hover");
  tabButton.addClass("Active");

  this.tabs.hide();
  this.control.find("#" + mode + "Tab").show();
};

PageExplorer.prototype.onTabButtonMouseOut = function()
{
  this.tabButtons.removeClass("Hover");
};

PageExplorer.prototype.onTabButtonMouseOver = function(event)
{
  var tabButton = jQuery(event.target);

  if (!tabButton.hasClass("Active"))
  {
    tabButton.addClass("Hover");
  }
};