﻿function Ribbon()
{
  this.classNames =
  [
    "scRibbonToolbarLargeButton",
    "scRibbonToolbarLargeButtonDisabled",
    "scRibbonToolbarSmallButton",
    "scRibbonToolbarSmallButtonDisabled"
  ];

  this.buttons = [];
  this.isLoaded = false;
}

Ribbon.prototype.addButton = function(button)
{
  this.buttons.push(button);
};

Ribbon.prototype.createDisabledButton = function(button)
{
  var className = (button.type == "Large") ?
    "scRibbonToolbarLargeButton" :
    "scRibbonToolbarSmallButton";
  var className1 = className + "Disabled";
  var className2 = className + "IconGrayed";
  var className3 = className + "IconDisabled";
  var header = button.name;
  var tooltip = button.tooltip;
  var iconPath = this.getDisabledIcon(button.icon);

  var span1 = jQuery("<span></span>");
  span1.attr("class", className1);
  span1.attr("title", tooltip);

  var span2 = jQuery("<span></span>");
  span2.attr("class", className2);

  var img = jQuery("<img/>");
  img.attr("alt", tooltip);
  img.attr("border", "0");
  img.attr("class", className3);
  img.attr("src", iconPath);

  var span3 = jQuery("<span></span>");
  span3.attr("class", "header");
  span3.html(header);

  span1.append(span2);
  span1.append(span3);
  span2.append(img);

  return span1;
};

Ribbon.prototype.createEnabledButton = function(button)
{
  var className = (button.type == "Large") ?
    "scRibbonToolbarLargeButton" :
    "scRibbonToolbarSmallButton";
  var iconClassName = className + "Icon";
  var tooltip = button.tooltip;
  var path = "/temp/IconCache/" + button.icon;
  var name = button.name;

  var a = jQuery("<a></a>");
  a.attr("class", className);
  a.attr("href", "#");
  a.attr("title", tooltip);

  var img = jQuery("<img/>");
  img.attr("alt", tooltip);
  img.attr("border", "0");
  img.attr("class", iconClassName);
  img.attr("src", path);

  var span = jQuery("<span></span>");
  span.attr("class", "header");
  span.html(name);

  a.append(img);
  a.append(span);

  if (button.click)
  {
    a.click(button.click);
  }

  return a;
};

Ribbon.prototype.getButtonElement = function(button)
{
  for (var i = 0; i < this.classNames.length; i++)
  {
    var className = this.classNames[i];
    var buttonElements = jQuery("." + className);

    for (var j = 0; j < buttonElements.length; j++)
    {
      var buttonElement = jQuery(buttonElements[j]);
      var header = buttonElement.find(".header");

      if (header.html() == button.name)
      {
        return buttonElement;
      }
    }
  }

  throw "Button '" + button.name + "' not found.";
};

Ribbon.prototype.getDisabledIcon = function(icon)
{
  var temp = icon.split("/");
  var size = temp[temp.length - 2];
  var name = temp[temp.length - 1];
  name = name.split(".");
  var extension = name[1];
  name = name[0];

  return "/temp/" + name + "_disabled" + size + "." + extension;
};

Ribbon.prototype.load = function()
{
  for (var i = 0; i < this.buttons.length; i++)
  {
    var button = this.buttons[i];
    var element = this.getButtonElement(button);
    var isEnabled = element[0].className.indexOf("Disabled") < 0;

    if (isEnabled)
    {
      button.isEnabled = true;
      button.enabledButton = element;
      button.disabledButton = this.createDisabledButton(button);
      button.disabledButton.hide();
      button.enabledButton.before(button.disabledButton);
    }
    else
    {
      button.isEnabled = false;
      button.enabledButton = this.createEnabledButton(button);
      button.disabledButton = element;
      button.enabledButton.hide();
      button.disabledButton.before(button.enabledButton);
    }
  }

  this.isLoaded = true;
};

Ribbon.prototype.refresh = function()
{
  if (!this.isLoaded)
  {
    this.load();
  }

  for (var i = 0; i < this.buttons.length; i++)
  {
    this.refreshButton(this.buttons[i]);
  }
};

Ribbon.prototype.refreshButton = function(button)
{
  var enable = button.enable();

  if (enable && button.isEnabled)
  {
    return;
  }

  if (!enable && !button.isEnabled)
  {
    return;
  }

  if (enable)
  {
    button.isEnabled = true;
    button.enabledButton.css("display", "block");
    button.disabledButton.css("display", "none");
  }
  else
  {
    button.isEnabled = false;
    button.enabledButton.css("display", "none");
    button.disabledButton.css("display", "block");
  }
};