﻿function SaveDialog()
{
  window.returnValue = null;

  this.nameTextBox = jQuery("#NameTextBox");
  this.okButton = jQuery("#OkButton");
  this.cancelButton = jQuery("#CancelButton");

  this.hideSaveOptions();
  this.fillForm();
  this.assignEventHandlers();
  this.refresh();
}

SaveDialog.prototype.assignEventHandlers = function()
{
  var owner = this;
  this.nameTextBox.keyup(function(event) { owner.refresh(); });
  this.okButton.click(function(event) { owner.okButtonClick(); });
  this.cancelButton.click(function(event) { window.close(); });
};

SaveDialog.prototype.fillForm = function()
{
  this.nameTextBox.val(parameters.name);
  jQuery("input[type='radio'][value='" + parameters.mode + "']").attr("checked", "checked");
};

SaveDialog.prototype.hideSaveOptions = function()
{
  if (!parameters.hideSaveOptions)
  {
    return;
  }

  jQuery("#Radio1")[0].disabled = true;
  jQuery("#Radio2")[0].disabled = true;
};

SaveDialog.prototype.okButtonClick = function()
{
  var name = this.nameTextBox.val();
  var mode = jQuery("input[type='radio']:checked").val();

  window.returnValue =
  {
    name: name,
    mode: mode
  };

  window.close();
};

SaveDialog.prototype.refresh = function()
{
  var owner = this;

  var onRequestComplete = function(responseText)
  {
    owner.okButton[0].disabled = responseText == "0";
  };

  sendRequest({ action: "isItemNameValid", name: this.nameTextBox.val() }, onRequestComplete);
};

jQuery(document).ready(function(event) { new SaveDialog(); });