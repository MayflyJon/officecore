﻿function Page(pageFrame)
{
  this.pageFrame = pageFrame;
  this.pageDocument = jQuery(this.pageFrame.contents());
}

Page.prototype.getDoctype = function()
{
  var nodes = this.pageDocument[0].childNodes;

  for (var i = 0; i < nodes.length; i++)
  {
    var node = nodes[i];

    if (jQuery.browser.msie)
    {
      if ((node.nodeType == 8) && (node.text.toLowerCase().indexOf("<!doctype") == 0))
      {
        return node.text;
      }
    }
    else
    {
      if (node.nodeType == 10)
      {
        return '<!DOCTYPE ' + node.name + ' PUBLIC "' + node.publicId + '" "' + node.systemId + '">';
      }
    }
  }

  return null;
};

Page.prototype.getBodyHtml = function()
{
  return new DomElement(this.pageDocument.find("body")[0]).getInnerHtml(false);
};

Page.prototype.getHtml = function()
{
  var nodes = this.pageDocument[0].childNodes;

  for (var i = 0; i < nodes.length; i++)
  {
    if (nodes[i].nodeType == 1)
    {
      return new DomElement(nodes[i]).getOuterHtml(false);
    }
  }

  return null;
};

Page.prototype.getSource = function()
{
  var result = "";
  var doctype = this.getDoctype();
  var html = this.getHtml();

  if (doctype != null)
  {
    result += doctype;
  }

  if (html != null)
  {
    if (jQuery.trim(result))
    {
      result += "\r\n";
    }

    result += html;
  }

  return result;
};

Page.prototype.getTitle = function()
{
  return jQuery.trim(this.pageDocument.find("title")[0].innerHTML);
};

Page.prototype.getUri = function()
{
  var source = this.pageFrame.attr("src");
  var find = "&uri=";
  var index = source.indexOf(find);

  if (index < 0)
  {
    throw "Cannot determine the base URI.";
  }

  return decodeURIComponent(source.substring(index + find.length));
};