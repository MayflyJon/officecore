﻿/*
 * The Insert Field dialog.
 */

function InsertFieldDialog()
{
  this.yesButton = jQuery("#YesButton");
  this.noButton = jQuery("#NoButton");
  this.fieldNameTextBox = jQuery("#FieldNameTextBox");
  this.errorMessage = jQuery("#ErrorMessage");
  this.fieldTypeComboBox = jQuery("#FieldTypeComboBox");
  this.okButton = jQuery("#OkButton");
  this.cancelButton = jQuery("#CancelButton");
  this.previewPane = new PreviewPane();

  var owner = this;

  this.yesButton.click(function(event) { owner.confirmContentReplacement(true); });
  this.noButton.click(function(event) { owner.confirmContentReplacement(false); });
  this.fieldNameTextBox.keyup(function(event) { owner.refresh(); });
  this.okButton.click(function(event) { owner.okButtonClick(); });
  this.cancelButton.click(function(event) { owner.cancelButtonClick(); });

  if (!this.isHeader())
  {
    this.hideWarning();
  }

  this.fieldNameTextBox[0].focus();
  this.setDefaultFieldType();
  this.refresh();

  this.replaceContent = false;
}

InsertFieldDialog.prototype.cancelButtonClick = function()
{
  window.returnValue = null;
  window.close();
};

InsertFieldDialog.prototype.confirmContentReplacement = function(contentReplacement)
{
  if (contentReplacement)
  {
    parameters.html = parameters.text;
    this.previewPane.refresh();
  }

  this.replaceContent = contentReplacement;
  this.hideWarning();
};

InsertFieldDialog.prototype.hideWarning = function()
{
  var warningPanel = jQuery("#HeaderTagsConfirmationPanel");
  var viewPanel = jQuery("#PreviewPane").find(".TabPage");
  viewPanel.height(viewPanel.height() + warningPanel.height());
  warningPanel.hide();
};

InsertFieldDialog.prototype.isFieldNameInUse = function(fieldName)
{
  for (var i = 0; i < parameters.fieldNames.length; i++)
  {
    if (parameters.fieldNames[i].toLowerCase() == fieldName.toLowerCase())
    {
      return true;
    }
  }

  return false;
};

InsertFieldDialog.prototype.isHeader = function()
{
  var tagName = parameters.tagName.toLowerCase();

  for (var i = 1; i <= 6; i++)
  {
    if (tagName == "h" + i)
    {
      return true;
    }
  }

  return false;
};

InsertFieldDialog.prototype.okButtonClick = function()
{
  window.returnValue =
  {
    fieldName: this.fieldNameTextBox.val(),
    fieldType: this.fieldTypeComboBox.val(),
    replaceContent: this.replaceContent
  };

  window.close();
};

InsertFieldDialog.prototype.refresh = function()
{
  var fieldName = this.fieldNameTextBox.val();

  if (this.isFieldNameInUse(fieldName))
  {
    this.setErrorMessage(Texts.FieldNameIsAlreadyInUse);

    return;
  }

  var owner = this;

  var onRequestComplete = function(responseText)
  {
    owner.setErrorMessage(responseText == "0" ? Texts.FieldNameIsNotValid : "");
  };

  sendRequest({ action: "isItemNameValid", name: fieldName }, onRequestComplete);
};

InsertFieldDialog.prototype.setDefaultFieldType = function()
{
  var comboBox = jQuery("#FieldTypeComboBox");
  var tagName = parameters.tagName.toLowerCase();

  comboBox.attr("value", "Rich Text");

  if (tagName == "a")
  {
    comboBox.attr("value", "General Link");
  }
  else
  {
    comboBox.find("[value='General Link']").remove();
  }

  if (tagName == "img")
  {
    comboBox.attr("value", "Image");
  }
  else
  {
    comboBox.find("[value='Image']").remove();
  }
};

InsertFieldDialog.prototype.setErrorMessage = function(errorMessage)
{
  this.errorMessage.html(errorMessage);
  this.okButton[0].disabled = errorMessage != "";
};

/*
 * The Preview Pane.
 */

function PreviewPane()
{
  this.control = jQuery("#PreviewPane");
  this.view = this.control.find(".View");
  this.viewFrame = jQuery("#PreviewFrame");

  this.mode = "Text";
  this.setMode(this.mode);

  var owner = this;
  this.control.find(".Text").click(function(event) { owner.tabButtonClick(event) });
  this.control.find(".Html").click(function(event) { owner.tabButtonClick(event) });
  this.control.find(".Preview").click(function(event) { owner.tabButtonClick(event) });
}

PreviewPane.prototype.refresh = function()
{
  switch (this.mode)
  {
    case "Text":
    {
      this.view.text(parameters.text);
      this.view.removeClass("Source");

      this.view.show();
      this.viewFrame.hide();

      break;
    }

    case "Html":
    {
      this.view.text(parameters.html);
      this.view.addClass("Source");

      this.view.show();
      this.viewFrame.hide();

      break;
    }

    case "Preview":
    {
      var preview = this.viewFrame.contents()[0];

      preview.open();
      preview.writeln(parameters.previewHtml);
      preview.close();

      this.view.hide();
      this.viewFrame.show();

      break;
    }
  }
};

PreviewPane.prototype.setMode = function(mode)
{
  var button = this.control.find("." + this.mode);
  button.attr("class", this.mode);

  button = this.control.find("." + mode);
  button.attr("class", (mode + " Active"));
  this.mode = mode;

  this.refresh();
};

PreviewPane.prototype.tabButtonClick = function(event)
{
  this.setMode(event.target.className.replace(" Active", ""));
};

jQuery(document).ready(function(event) { new InsertFieldDialog(); });