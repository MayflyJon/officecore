﻿<%@ Page Language="C#" %>
<%@ Import Namespace="Sitecore.DesignImporter.Kernel" %>
<%@ Import Namespace="Sitecore.Globalization"
%><?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

	<head>

		<title><% =Translate.Text(Texts.NoPage) %></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <style type="text/css">

      #Message
      {
        color: gray;
        font: normal 8pt Arial;
        height: 14px;
        left: 50%;
        margin-left: -100px;
        margin-top: -7px;
        position: absolute;
        text-align: center;
        top: 50%;
        width: 200px;
      }

    </style>

	</head>

	<body>

		<div id="Message">
      <% =Translate.Text(Texts.TheWebPageHasNotYetBeenLoaded) %>
    </div>

	</body>

</html>