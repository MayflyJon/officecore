﻿<%@ Page
    Language="C#"
    AutoEventWireup="true"
    Inherits="Sitecore.DesignImporter.Web.DesignImporterPage"
%><%@ Register
    Assembly="Sitecore.Kernel"
    Namespace="Sitecore.Web.UI.HtmlControls"
    TagPrefix="sc"
%><%@ Register
    Assembly="Sitecore.Kernel"
    Namespace="Sitecore.Web.UI.WebControls"
    TagPrefix="sc"
%><%@ Register
    Assembly="Sitecore.Kernel"
    Namespace="Sitecore.Web.UI.WebControls.Ribbons"
    TagPrefix="sc"
%><%@ Import
    Namespace="Sitecore.DesignImporter.Kernel"
%><%@ Import Namespace="Sitecore.DesignImporter.Configuration" %>
<%@ Import
    Namespace="Sitecore.Globalization"
%><?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

  <head runat="server">

    <title><% =Translate.Text(Texts.DesignImporter) %></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <sc:Stylesheet runat="server" Src="Ribbon.css" DeviceDependant="true"/>
    <link rel="stylesheet" type="text/css" href="CSS/Common.css"/>
    <link rel="stylesheet" type="text/css" href="CSS/DesignImporter.css"/>

    <sc:Script runat="server" Src="Prototype/prototype.js"/>
    <script type="text/javascript" src="JavaScript/Components/jquery.js"></script>
    <script type="text/javascript" src="JavaScript/Components/jquery.scrollTo.js"></script>

    <script type="text/javascript">

      jQuery.noConflict();

    </script>

    <script type="text/javascript" src="JavaScript/Components/json2.js"></script>
    <script type="text/javascript" src="JavaScript/ActionQueue.js"></script>
    <script type="text/javascript" src="JavaScript/Ajax.js"></script>
    <script type="text/javascript" src="JavaScript/DesignImporter.js"></script>
    <script type="text/javascript" src="JavaScript/Dialog.js"></script>
    <script type="text/javascript" src="JavaScript/DomElement.js"></script>
    <script type="text/javascript" src="JavaScript/DomTree.js"></script>
    <script type="text/javascript" src="JavaScript/Page.js"></script>
    <script type="text/javascript" src="JavaScript/PageExplorer.js"></script>
    <script type="text/javascript" src="JavaScript/Ribbon.js"></script>

  </head>

  <body>

    <form runat="server" id="MainForm">

      <script type="text/javascript">

        var Settings =
        {
          irreplaceableElements: "<% =Settings.IrreplaceableElements %>".toLowerCase().split(",")
        };

        var Texts =
        {
          Cancel: "<% =Translate.Text(Texts.Cancel) %>",
          Close: "<% =Translate.Text(Texts.Close) %>",
          Crop: "<% =Translate.Text(Texts.Crop) %>",
          Delete: "<% =Translate.Text(Texts.Delete) %>",
          DeleteAllTheElementsExceptThisElement: "<% =Translate.Text(Texts.DeleteAllTheElementsExceptThisElement) %>",
          DeleteTheSelection: "<% =Translate.Text(Texts.DeleteTheSelection) %>",
          Description: "<% =Translate.Text(Texts.Description) %>",
          DownloadSuccessful: "<% =Translate.Text(Texts.DownloadSuccessful) %>",
          Errors: "<% =Translate.Text(Texts.Errors) %>",
          FailedTheWebPageHasNotBeenImported: "<% =Translate.Text(Texts.FailedTheWebPageHasNotBeenImported) %>",
          Field: "<% =Translate.Text(Texts.Field) %>",
          Import: "<% =Translate.Text(Texts.Import) %>",
          LogScreen: "<% =Translate.Text(Texts.LogScreen) %>",
          Messages: "<% =Translate.Text(Texts.Messages) %>",
          Ok: "<% =Translate.Text(Texts.Ok) %>",
          Open: "<% =Translate.Text(Texts.Open) %>",
          OpenAWebPage: "<% =Translate.Text(Texts.OpenAWebPage) %>",
          OpenImportedPageInPageEditor: "<% =Translate.Text(Texts.OpenImportedPageInPageEditor) %>",
          OpenImportedPageInPreview: "<% =Translate.Text(Texts.OpenImportedPageInPreview) %>",
          OpenUrlRemapper: "<% =Translate.Text(Texts.OpenUrlRemapper) %>",
          PageEditor: "<% =Translate.Text(Texts.PageEditor) %>",
          Placeholder: "<% =Translate.Text(Texts.Placeholder) %>",
          Preview: "<% =Translate.Text(Texts.Preview) %>",
          Redo1: "<% =Translate.Text(Texts.Redo1) %>",
          Redo2: "<% =Translate.Text(Texts.Redo2) %>",
          Remap: "<% =Translate.Text(Texts.Remap) %>",
          ReplaceSelectionWithAField: "<% =Translate.Text(Texts.ReplaceSelectionWithAField) %>",
          ReplaceSelectionWithAPlaceholder: "<% =Translate.Text(Texts.ReplaceSelectionWithAPlaceholder) %>",
          SaveAnyChanges: "<% =Translate.Text(Texts.SaveAnyChanges) %>",
          SelectNone1: "<% =Translate.Text(Texts.SelectNone1) %>",
          SelectNone2: "<% =Translate.Text(Texts.SelectNone2) %>",
          TheWebPageCannotBeLoadedBecauseThePageStructureIsInconsistent: "<% =Translate.Text(Texts.TheWebPageCannotBeLoadedBecauseThePageStructureIsInconsistent) %>",
          TheWebPageHasAlreadyBeenImported: "<% =Translate.Text(Texts.TheWebPageHasAlreadyBeenImported) %>",
          Time: "<% =Translate.Text(Texts.Time) %>",
          Undo1: "<% =Translate.Text(Texts.Undo1) %>",
          Undo2: "<% =Translate.Text(Texts.Undo2) %>",
          Warnings: "<% =Translate.Text(Texts.Warnings) %>"
        };

        var idPrefix = "<% =Constants.IdPrefix %>";
        var coverId = idPrefix + "Cover";
        var fieldRendererClass = idPrefix + "FieldRenderer";
        var hoverSelectionClass = idPrefix + "HoverSelection";
        var placeholderClass = idPrefix + "Placeholder";
        var selectionClass = idPrefix + "Selection";
        var hideSaveOptions = <% =(this.IsSaveOptionsHidden() ? "true" : "false") %>;

        jQuery(document).ready(function (event) { new DesignImporter(); });

      </script>

      <sc:AjaxScriptManager runat="server" ID="AjaxScriptManager"/>
      <sc:ContinuationManager runat="server" ID="ContinuationManager"/>

      <!-- Main grid -->

      <table border="0" cellpadding="0" cellspacing="0" id="MainGrid">
        <tbody>
          <tr>

            <!-- Ribbon -->

            <td id="RibbonStrip">
              <sc:Ribbon runat="server" ID="Ribbon"/>
            </td>

            <!-- / Ribbon -->

          </tr>
          <tr>
            <td id="PanelStrip">

              <!-- Panels -->

              <table border="0" cellpadding="0" cellspacing="0" id="PanelGrid">
                <tbody>
                  <tr>

                    <!-- Left pane -->

                    <td id="LeftPane">
                      <iframe frameborder="0" id="PageFrame" src="NoPage.aspx"></iframe>
                      <iframe frameborder="0" id="WaitFrame"></iframe>
                    </td>

                    <!-- / Left pane -->

                    <!-- Right pane -->

                    <td id="RightPane">

                      <!-- Page explorer -->

                      <table border="0" cellpadding="0" cellspacing="0" id="PageExplorer">
                        <tbody>
                          <tr>
                            <td id="Tabs">
                              <div id="TabHost">

                                <div class="Tab" id="TreeTab">&nbsp;</div>
                                <div class="Tab" id="TextTab">&nbsp;</div>
                                <div class="Tab" id="HtmlTab">&nbsp;</div>

                                <!-- Preview tab -->

                                <div class="Tab" id="PreviewTab">
                                  <iframe frameborder="0" id="PreviewFrame"></iframe>
                                </div>

                                <!-- / Preview tab -->

                                <!-- Log tab -->

                                <div class="Tab" id="LogTab">
                                  <table class="Expanded" id="NoLogMessages">
                                    <tbody>
                                      <tr><td><% =Translate.Text(Texts.TheWebPageHasNotYetBeenImported) %></td></tr>
                                    </tbody>
                                  </table>
                                </div>

                                <!-- / Log tab -->

                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td id="TabButtons">

                              <div class="TabButton Active" id="TreeTabButton"><% =Translate.Text(Texts.Tree) %></div>
                              <div class="TabButton" id="TextTabButton"><% =Translate.Text(Texts.Text) %></div>
                              <div class="TabButton" id="HtmlTabButton"><% =Translate.Text(Texts.Html) %></div>
                              <div class="TabButton" id="PreviewTabButton"><% =Translate.Text(Texts.Preview) %></div>
                              <div class="TabButton" id="LogTabButton"><% =Translate.Text(Texts.Log) %></div>
                            </td>
                          </tr>
                        </tbody>
                      </table>

                      <!-- / Page explorer -->

                    </td>

                    <!-- / Right pane -->

                  </tr>
                </tbody>
              </table>

              <!-- / Panels -->

            </td>
          </tr>
        </tbody>
      </table>

      <!-- / Main grid -->

    </form>

  </body>

</html>