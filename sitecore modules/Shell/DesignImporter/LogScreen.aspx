﻿<%@ Page
    Language="C#"
    AutoEventWireup="true"
    MasterPageFile="Dialog.Master" %>
<%@ Import Namespace="Sitecore.DesignImporter.Kernel" %>
<%@ Import Namespace="Sitecore.Globalization" %>

<asp:Content runat="server" ID="TitleContent" ContentPlaceHolderID="TitleContentPlaceHolder">

<% =Translate.Text(Texts.LogScreen) %>

</asp:Content>

<asp:Content runat="server" ID="StyleContent" ContentPlaceHolderID="StyleContentPlaceHolder">

<link rel="stylesheet" type="text/css" href="CSS/LogScreen.css"/>

</asp:Content>

<asp:Content runat="server" ID="JavaScriptContent" ContentPlaceHolderID="JavaScriptContentPlaceHolder">

<script type="text/javascript" src="JavaScript/Components/json2.js"></script>
<script type="text/javascript" src="JavaScript/Ajax.js"></script>
<script type="text/javascript" src="JavaScript/Dialog.js"></script>
<script type="text/javascript" src="JavaScript/LogScreen.js"></script>

<script type="text/javascript">

	var Texts =
	{
		Errors: "<% =Translate.Text(Texts.Errors) %>",
		Messages: "<% =Translate.Text(Texts.Messages) %>",
		Warnings: "<% =Translate.Text(Texts.Warnings) %>"
	};

</script>

</asp:Content>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="BodyContentPlaceHolder">

<!-- Main panel. -->

<div id="MainPanel">

	<!-- Toolbar. -->

	<div id="Toolbar">
		<a class="TabButton Selected" id="ErrorButton" title="<% =Translate.Text(Texts.ShowHideErrors) %>">0 <% =Translate.Text(Texts.Errors) %></a>
		<a class="TabButton Selected" id="WarningButton" title="<% =Translate.Text(Texts.ShowHideWarnings) %>">0 <% =Translate.Text(Texts.Warnings) %></a>
		<a class="TabButton Selected" id="InformationButton" title="<% =Translate.Text(Texts.ShowHideMessages) %>">0 <% =Translate.Text(Texts.Messages) %></a>
		<br class="Clear"/>
	</div>

	<!-- / Toolbar. -->

	<!-- Grid. -->

	<div id="Grid">

		<!-- Grid headers. -->

		<table id="GridHeaders">
			<tbody>
				<tr>
					<td class="Time"><% =Translate.Text(Texts.Time) %></td>
					<td><% =Translate.Text(Texts.Description) %></td>
				</tr>
			</tbody>
		</table>

		<!-- / Grid headers. -->

		<!-- Scroll panel. -->

		<div id="ScrollPanel">

			<table id="GridData">
				<tbody>
					<tr>
						<td class="Time">&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</tbody>
			</table>

		</div>

		<!-- / Scroll panel. -->

	</div>

	<!-- / Grid. -->

</div>

<!-- / Main panel. -->

</asp:Content>