﻿<%@ Page
    Language="C#"
    AutoEventWireup="true"
    Inherits="Sitecore.DesignImporter.Web.UrlRemapperPage,Sitecore.DesignImporter"
%><%@ Register
    Assembly="Sitecore.Kernel"
    Namespace="Sitecore.Web.UI.HtmlControls"
    TagPrefix="sc"
%><%@ Register
    Namespace="Sitecore.Web.UI.WebControls"
    TagPrefix="sc"
%><%@ Register
    Assembly="Sitecore.Kernel"
    Namespace="Sitecore.Web.UI.WebControls.Ribbons"
    TagPrefix="sc"
%><%@ Import Namespace="Sitecore.DesignImporter.Kernel"
%><%@ Import Namespace="Sitecore.Globalization"
%><?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

  <head id="Head1" runat="server">

    <title><% =Translate.Text(Texts.UrlRemapper) %></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <sc:Stylesheet runat="server" Src="Ribbon.css" DeviceDependant="true"/>
    <link rel="stylesheet" type="text/css" href="CSS/Common.css"/>
    <link rel="stylesheet" type="text/css" href="CSS/UrlRemapper.css"/>

    <sc:Script runat="server" Src="Prototype/prototype.js"/>
    <script type="text/javascript" src="JavaScript/Components/jquery.js"></script>

    <script type="text/javascript">
      jQuery.noConflict();
    </script>

    <script type="text/javascript" src="JavaScript/Components/json2.js"></script>
    <script type="text/javascript" src="JavaScript/Ajax.js"></script>
    <script type="text/javascript" src="JavaScript/Ribbon.js"></script>
    <script type="text/javascript" src="JavaScript/UrlRemapper.js"></script>

  </head>

  <body>

    <script type="text/javascript">

      var Context =
      {
        language: "<% =Sitecore.Context.Language %>"
      };

      var Texts =
      {
        AllTheChangesYouMadeBeforeWillBeDiscarded: "<% =Translate.Text(Texts.AllTheChangesYouMadeBeforeWillBeDiscarded) %>",
        Open: "<% =Translate.Text(Texts.Open) %>",
        OpenALayoutOrSublayout: "<% =Translate.Text(Texts.OpenALayoutOrSublayout) %>",
        Save: "<% =Translate.Text(Texts.Save) %>",
        SaveAnyChanges: "<% =Translate.Text(Texts.SaveAnyChanges) %>",
        SelectALayoutOrSublayout: "<% =Translate.Text(Texts.SelectALayoutOrSublayout) %>",
        SelectTheItemThatYouWantToLink: "<% =Translate.Text(Texts.SelectTheItemThatYouWantToLink) %>",
        TheChangesWereSavedSuccessfully: "<% =Translate.Text(Texts.TheChangesWereSavedSuccessfully) %>",
        ThisWebResourceIsNotAvailable: "<% =Translate.Text(Texts.ThisWebResourceIsNotAvailable) %>",
        ThisWebResourceIsNotDownloadable: "<% =Translate.Text(Texts.ThisWebResourceIsNotDownloadable) %>"
      };

      var info = <asp:Literal runat="server" ID="LiteralInfo"/>;

    </script>

    <sc:AjaxScriptManager runat="server" ID="AjaxScriptManager"/>
    <sc:ContinuationManager runat="server" ID="ContinuationManager"/>

    <!-- Main grid -->

    <table class="Expanded">
      <tbody>
        <tr>

          <!-- Ribbon -->

          <td id="RibbonStrip">
            <sc:Ribbon runat="server" ID="Ribbon"/>
          </td>

          <!-- / Ribbon -->

        </tr>
        <tr>
          <td id="WorkspaceStrip">

            <!-- Workspace -->

            <table class="Expanded">
              <tbody>
                <tr>
                  <td id="TopPanel">

                    <!-- Link grid -->

                    <table class="Expanded">
                      <tbody>
                        <tr>

                          <!-- Links -->

                          <td id="LinkPanel" class="Expanded">
                            <div id="LinkPane">
                              <div id="LinkHost">&nbsp;</div>
                            </div>
                          </td>

                          <!-- / Links -->

                          <!-- Buttons -->

                          <td id="ButtonPanel">
                            <button id="BrowseButton"><% =Translate.Text(Texts.Browse) %></button>
                            <button id="FollowButton"><% =Translate.Text(Texts.Follow) %></button>
                            <button id="DownloadButton"><% =Translate.Text(Texts.Download) %></button>
                          </td>

                          <!-- / Buttons -->

                        </tr>
                      </tbody>
                    </table>

                    <!-- / Link grid -->

                  </td>
                </tr>
                <tr>
                  <td id="BottomPanel">

                    <!-- Page -->

                    <table class="Expanded">
                      <tbody>
                        <tr>
                          <td id="TabPanel">
                            <div class="Tab Visible" id="PreviewTab">
                              <iframe frameborder="0" id="PreviewFrame"></iframe>
                            </div>
                            <div class="Tab" id="HtmlTab">&nbsp;</div>
                          </td>
                        </tr>
                        <tr>
                          <td id="TabButtons">
                            <div class="TabButton Active" id="PreviewTabButton"><% =Translate.Text(Texts.Preview) %></div>
                            <div class="TabButton Last" id="HtmlTabButton"><% =Translate.Text(Texts.Html) %></div>
                          </td>
                        </tr>
                      </tbody>
                    </table>

                    <!-- / Page -->

                  </td>
                </tr>
              </tbody>
            </table>

            <!-- / Workspace -->

          </td>
        </tr>
      </tbody>
    </table>

    <!-- / Main grid -->

  </body>

</html>