﻿<%@ Page
    Language="C#"
    AutoEventWireup="true"
    MasterPageFile="Dialog.Master" %>
<%@ Import Namespace="Sitecore.DesignImporter.Kernel" %>
<%@ Import Namespace="Sitecore.Globalization" %>

<asp:Content runat="server" ID="TitleContent" ContentPlaceHolderID="TitleContentPlaceHolder">
<% =Translate.Text(Texts.InsertField) %>
</asp:Content>

<asp:Content runat="server" ID="StyleContent" ContentPlaceHolderID="StyleContentPlaceHolder">
  <link rel="stylesheet" type="text/css" href="CSS/InsertField.css"/>
</asp:Content>

<asp:Content runat="server" ID="JavaScriptContent" ContentPlaceHolderID="JavaScriptContentPlaceHolder">

  <script type="text/javascript" src="JavaScript/Components/jquery.js"></script>
  <script type="text/javascript" src="JavaScript/Ajax.js"></script>
  <script type="text/javascript" src="JavaScript/InsertField.js"></script>

  <script type="text/javascript">

    var Texts =
    {
      FieldNameIsAlreadyInUse: "<% =Translate.Text(Texts.FieldNameIsAlreadyInUse) %>",
      FieldNameIsNotValid: "<% =Translate.Text(Texts.FieldNameIsNotValid) %>"
    };

  </script>

</asp:Content>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="BodyContentPlaceHolder">

  <div class="Dialog" id="InsertFieldDialog">

    <!-- Confirmation of header tags removing -->

    <div id="HeaderTagsConfirmationPanel">
      <div class="MessagePanel"><% =Translate.Text(Texts.YouAreImportingAHeaderTag) %></div>
      <div class="ButtonPanel">
        <button id="YesButton"><% =Translate.Text(Texts.Yes) %></button>
        <button id="NoButton"><% =Translate.Text(Texts.No) %></button>
      </div>
    </div>

    <!-- / Confirmation of header tags removing -->

    <div class="Form">

      <table class="Controls">
        <tbody>

          <!-- Field name -->

          <tr>
            <td class="Label">
              <label id="FieldNameLabel"><% =Translate.Text(Texts.FieldName) %></label>
            </td>
            <td class="Control">
              <input id="FieldNameTextBox" type="text"/>
            </td>
          </tr>

          <!-- / Field name -->

          <!-- Error message -->

          <tr>
            <td>&nbsp;</td>
            <td id="ErrorMessage">&nbsp;</td>
          </tr>

          <!-- / Error message -->

          <!-- Field type -->

          <tr class="Indent">
            <td>
              <label id="FieldTypeLabel"><% =Translate.Text(Texts.FieldType) %></label>
            </td>
            <td>
              <select id="FieldTypeComboBox">
                <option value="Single-Line Text">Single-Line Text</option>
                <option value="Multi-Line Text">Multi-Line Text</option>
                <option value="Rich Text">Rich Text</option>
                <option value="Image">Image</option>
                <option value="General Link">General Link</option>
              </select>
            </td>
          </tr>

          <!-- / Field type -->

          <!-- Preview pane -->

          <tr class="Indent">
            <td colspan="2">

              <table id="PreviewPane">
                <tbody>
                  <tr>
                    <td class="Header">
                      <table>
                        <tbody>
                          <tr>
                            <td class="Text"><% =Translate.Text(Texts.Text) %></td>
                            <td class="Html"><% =Translate.Text(Texts.Html) %></td>
                            <td class="Preview"><% =Translate.Text(Texts.Preview) %></td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td class="ViewPanel">
                      <div class="TabPage">
                        <div class="View">&nbsp;</div>
                        <iframe frameborder="0" id="PreviewFrame"></iframe>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>

            </td>
          </tr>

          <!-- / Preview pane -->

          <!-- Buttons -->

          <tr class="Indent">
            <td colspan="2" class="ButtonStrip">
              <button id="OkButton"><% =Translate.Text(Texts.Ok) %></button>
              <button id="CancelButton"><% =Translate.Text(Texts.Cancel) %></button>
            </td>
          </tr>

          <!-- / Buttons -->

        </tbody>
      </table>

    </div>

  </div>

</asp:Content>