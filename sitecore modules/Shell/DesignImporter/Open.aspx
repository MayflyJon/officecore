﻿<%@ Page
    Language="C#"
    AutoEventWireup="true"
    MasterPageFile="Dialog.Master" %>
<%@ Import Namespace="Sitecore.DesignImporter.Kernel" %>
<%@ Import Namespace="Sitecore.Globalization" %>

<asp:Content runat="server" ID="TitleContent" ContentPlaceHolderID="TitleContentPlaceHolder">
<% =Translate.Text(Texts.Open) %>
</asp:Content>

<asp:Content runat="server" ID="StyleContent" ContentPlaceHolderID="StyleContentPlaceHolder">
<link rel="stylesheet" type="text/css" href="CSS/Open.css"/>
</asp:Content>

<asp:Content runat="server" ID="JavaScriptContent" ContentPlaceHolderID="JavaScriptContentPlaceHolder">
<script type="text/javascript" src="JavaScript/Components/jquery.js"></script>
<script type="text/javascript" src="JavaScript/Ajax.js"></script>
<script type="text/javascript" src="JavaScript/Open.js"></script>
<script type="text/javascript" src="JavaScript/Utils.js"></script>
</asp:Content>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="BodyContentPlaceHolder">

<div id="OpenDialog">

  <table>
    <tbody>

      <!-- URL -->

      <tr>
        <td>
          <label><% =Translate.Text(Texts.Url) %></label>
        </td>
        <td>
          <input class="TextBox" id="UrlTextBox" type="text" style="width: 400px"/>
        </td>
      </tr>

      <!-- / URL -->

      <!-- Buttons -->

      <tr class="Indent">
        <td colspan="2" class="ButtonStrip">
          <button id="OkButton"><% =Translate.Text(Texts.Ok) %></button>
          <button id="CancelButton"><% =Translate.Text(Texts.Cancel) %></button>
        </td>
      </tr>

      <!-- / Buttons -->

    </tbody>
  </table>

</div>

</asp:Content>