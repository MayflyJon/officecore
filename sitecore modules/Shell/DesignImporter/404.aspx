﻿<%@ Page Language="C#" %>
<%@ Import Namespace="Sitecore.DesignImporter.Kernel" %>
<%@ Import Namespace="Sitecore.Globalization"
%><?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

  <head>

    <title>HTTP 404</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <style type="text/css">

      body
      {
        font-family: Arial, sans-serif;
      }

      .Title
      {
        color: red;
        font-size: 16pt;
      }

      .Description
      {
        font-size: 10pt;
      }

    </style>

  </head>

  <body>

    <div class="Title">
      <% =Translate.Text(Texts.Http404ThePageCannotBeFound) %>
    </div>

    <hr/>

    <div class="Description">
      <% =Translate.Text(Texts.ThePageYouAreLookingForMightHaveBeenRemoved) %>
    </div>

  </body>

</html>