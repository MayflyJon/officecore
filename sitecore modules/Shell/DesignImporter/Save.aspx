﻿<%@ Page
    Language="C#"
    AutoEventWireup="true"
    MasterPageFile="Dialog.Master" %>
<%@ Import Namespace="Sitecore.DesignImporter.Kernel" %>
<%@ Import Namespace="Sitecore.Globalization" %>

<asp:Content runat="server" ID="TitleContent" ContentPlaceHolderID="TitleContentPlaceHolder">
<% =Translate.Text(Texts.Save) %>
</asp:Content>

<asp:Content runat="server" ID="StyleContent" ContentPlaceHolderID="StyleContentPlaceHolder">
<link rel="stylesheet" type="text/css" href="CSS/Save.css"/>
</asp:Content>

<asp:Content runat="server" ID="JavaScriptContent" ContentPlaceHolderID="JavaScriptContentPlaceHolder">
<script type="text/javascript" src="JavaScript/Ajax.js"></script>
<script type="text/javascript" src="JavaScript/Save.js"></script>
</asp:Content>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="BodyContentPlaceHolder">

<div id="SaveDialog">

  <table>
    <tbody>

      <!-- Name. -->

      <tr>
        <td>
          <label id="NameLabel">
            <% =Translate.Text(Texts.Name) %>
          </label>
        </td>
        <td>
          <input id="NameTextBox" type="text"/>
        </td>
      </tr>

      <!-- / Name. -->

      <!-- Save options. -->

      <tr class="Indent">
        <td>&nbsp;</td>
        <td>

          <fieldset>

            <legend><% =Translate.Text(Texts.SaveOptions) %></legend>

            <table>
              <tbody>
                <tr>
                  <td>
                    <input
                      id="Radio1"
                      name="Mode"
                      type="radio"
                      value="layout"/>
                    <label class="SaveOptionLabel">
                      <% =Translate.Text(Texts.Layout) %>
                    </label>
                  </td>
                </tr>
                <tr>
                  <td>
                    <input
                      id="Radio2"
                      name="Mode"
                      type="radio"
                      value="sublayout"/>
                    <label class="SaveOptionLabel">
                      <% =Translate.Text(Texts.Sublayout) %>
                    </label>
                  </td>
                </tr>
              </tbody>
            </table>

          </fieldset>

        </td>
      </tr>

      <!-- / Save options. -->

      <!-- Buttons. -->

      <tr class="Indent">
        <td colspan="2" class="ButtonStrip">
          <button id="OkButton"><% =Translate.Text(Texts.Ok) %></button>
          <button id="CancelButton"><% =Translate.Text(Texts.Cancel) %></button>
        </td>
      </tr>

      <!-- / Buttons. -->

    </tbody>
  </table>

</div>

</asp:Content>