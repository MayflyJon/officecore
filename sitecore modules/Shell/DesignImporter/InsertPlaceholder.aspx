﻿<%@ Page
    Language="C#"
    AutoEventWireup="true"
    MasterPageFile="Dialog.Master" %>
<%@ Import Namespace="Sitecore.DesignImporter.Kernel" %>
<%@ Import Namespace="Sitecore.Globalization" %>

<asp:Content runat="server" ID="TitleContent" ContentPlaceHolderID="TitleContentPlaceHolder">
<% =Translate.Text(Texts.InsertPlaceholder) %>
</asp:Content>

<asp:Content runat="server" ID="StyleContent" ContentPlaceHolderID="StyleContentPlaceHolder">
  <link rel="stylesheet" type="text/css" href="CSS/InsertPlaceholder.css"/>
</asp:Content>

<asp:Content runat="server" ID="JavaScriptContent" ContentPlaceHolderID="JavaScriptContentPlaceHolder">

  <script type="text/javascript" src="JavaScript/InsertPlaceholder.js"></script>

  <script type="text/javascript">

    var Texts =
    {
      KeyIsAlreadyInUse: "<% =Translate.Text(Texts.KeyIsAlreadyInUse) %>",
      KeyIsNotValid: "<% =Translate.Text(Texts.KeyIsNotValid) %>"
    };

  </script>

</asp:Content>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="BodyContentPlaceHolder">

<div id="InsertPlaceholderDialog">

  <table>
    <tbody>

      <!-- Key -->

      <tr>
        <td>
          <label><% =Translate.Text(Texts.Key) %></label>
        </td>
        <td>
          <input class="TextBox" id="KeyTextBox" type="text"/>
        </td>
      </tr>

      <!-- / Key -->

      <!-- Error message -->

      <tr>
        <td>&nbsp;</td>
        <td id="ErrorMessage">&nbsp;</td>
      </tr>

      <!-- / Error message -->

      <!-- Buttons -->

      <tr class="Indent">
        <td colspan="2" class="ButtonStrip">
          <button id="OkButton"><% =Translate.Text(Texts.Ok) %></button>
          <button id="CancelButton"><% =Translate.Text(Texts.Cancel) %></button>
        </td>
      </tr>

      <!-- / Buttons -->

    </tbody>
  </table>

</div>

</asp:Content>