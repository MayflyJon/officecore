﻿function treeController($scope, $rootScope, $window, $location, $q, imagePickerService) {

	// Note: This way of handling the open/closed state is only here for a POC. A smarter way must be found.
	var closedState = 'treeItemClosed';
	var openState = 'treeItemOpen';
	var hiddenState = 'treeItemHidden';

	// Expose the node functions.
	$scope.toggleChildNodes = toggleChildNodes;
	$scope.reloadChildNodes = reloadChildNodes;
	$scope.updateChildNodes = updateChildNodes;
	$scope.toggleOptions = toggleOptions;
	$scope.hideOptions = hideOptions;
	$scope.handleAction = handleAction;
	$scope.getNodeIconStyle = getNodeIconStyle;
	$scope.treeSetSelected = treeSetSelected;
	$scope.treeNodeOptionMenuStyle = treeNodeOptionMenuStyle;
	$scope.treeNodeLinkClasses = treeNodeLinkClasses;
	$rootScope.openTreeNodes = [];
	$rootScope.selectedNode = '';
	$scope.loadContentTree = loadContentTree;
	$scope.itemExpandClasses = itemExpandClasses;
	$scope.removeIfNameIsUndefinded = removeIfNameIsUndefinded;

    $scope.loadContentTree();

	$scope.$watch('tree', function() {
		if ($scope.tree) {
			if ($scope.tree.autoload) {
				toggleChildNodes($scope.tree);
			}
		}
	});

	$scope.getPreSelectedValues = function() {
		return $scope.preSelectedValues;
	};

	$scope.isSelectedCheckbox = function (node) {
		if (node) {
			var nodeIdentifier = node.nodeType + '_' + node.id;
			if ($scope.preSelectedValues) {
				return $scope.preSelectedValues.indexOf(nodeIdentifier) > -1;
			}
		}
		return false;
	};

	$scope.toggleCheckBoxSelected = function(node) {

		this.$emit('toggleSelectedNode', node);
		var inputField = $scope.currentNodeElement.find('input').first();
		if (inputField.attr('checked') == 'checked') {
			inputField.removeAttr('checked', '');
		} else {
			inputField.attr('checked', 'checked');
		}

		return false;
	};

	$scope.showCheckBoxForNode = function (node) {
		if (node) {
			if ($scope.hasCheckboxFor) {
				var showCheckBox = $scope.hasCheckboxFor.indexOf(node.nodeType) != -1;
				return showCheckBox;
			}
		}
		return false;
	};

	function loadContentTree() {
		if ($scope.contentPickerType) {
			imagePickerService.getRootNode($scope.contentPickerType).then(function (rootNode) {
				$scope.tree = {
					name: rootNode.name,
					nodeType: rootNode.nodeType,
					url: rootNode.url,
					icon: rootNode.icon,
					id: rootNode.id,
					hasChildren: rootNode.hasChildren,
					showOptions: false,
					loaded: false,
					selected: false,
					autoload: rootNode.autoload,
					toggleState: closedState,
					nodes: [],
					options: rootNode.options
				};
			});
		}
	}
	
	function treeNodeLinkClasses(node) {
		var classes = [];
		if (node !== undefined) {
			if (node.selected) {
				classes.push('nodeSelected');

				if ($scope.selectedNodeStyle !== 'none') {
					classes.push('nodeSelectedStyle');
				}
			}

			if (node.dimNode)
				classes.push('dimNode');
		}

		return classes;
	}

	function treeNodeOptionMenuStyle(icon) {
		if (icon)
			return {
				'background-image': 'url(' + UCommerceClientMgr.BaseUCommerceUrl + 'Images/ui/' + icon + '")'
			};
	};

	function treeSetSelected(node) {
		if (node && $scope.showCheckBoxForNode(node)) {
			$scope.toggleCheckBoxSelected(node);
		}
		if (node.url && node.url != '') {
			if ($rootScope.selectedNode != node) {

				if ($rootScope.selectedNode != undefined)
					$rootScope.selectedNode.selected = false;

				node.selected = true;
				this.$emit('changeSelectedUrl', node.url);
				this.$emit('changeSelectedId', node.id);
				this.$emit('changeSelectedName', node.name);
				$rootScope.selectedNode = node;
			}
		}
	}

	function handleAction(option, node) {
		treeSetSelected(node);
		switch (option.optionType) {
			case 'New':
				// Adding temp node before the new node is created.
				var tempNode = { parent: node };
				treeSetSelected(tempNode);

				$location.path(option.actionUrl);
				break;
			case 'Delete':
				deleteItem(node);
				break;
			case 'Refresh':
				reloadChildNodes(node);
				treeSetSelected(node);
				break;
			case 'Sort':
				$location.path(option.actionUrl);
				break;
		}
	}

	function getNodeIconStyle(icon, nodeName) {
		var baseFolder = UCommerceClientMgr.BaseUCommerceUrl;
		if (icon) {
			if (UCommerceClientMgr.Shell == 'Sitecore') {
				if ($scope.iconFolder == 'uCommerce') {
					return {
						'background-image': 'url("' + UCommerceClientMgr.BaseUCommerceUrl + 'images/ui/'+ icon + '")'
					};
				} else {
					return {
						'background-image': 'url("' + icon + '")'
					};
				}
			}
			if (UCommerceClientMgr.Shell == 'Umbraco7') {
				var lowerCaseIcon = icon.toLowerCase();
				if ($scope.iconFolder == 'uCommerce') {
					return {
						'background-image': 'url("/umbraco/uCommerce/images/ui/' + icon + '")',
						'background-repeat': 'no-repeat',
						'padding-left': '16px'
					};
				}
				if ((lowerCaseIcon.indexOf('.png') != -1) ||
				(lowerCaseIcon.indexOf('.gif') != -1) ||
				(lowerCaseIcon.indexOf('.jpg') != -1)) {
					return {
						'background-image': 'url("/umbraco/images/umbraco/' + icon + '")',
						'background-repeat': 'no-repeat',
						'padding-left': '16px'
					};
				}
			}
			if (UCommerceClientMgr.Shell == 'Umbraco') {
				if ($scope.iconFolder == 'uCommerce') {
					return {
						'background-image': 'url("/umbraco/uCommerce/images/ui/' + icon + '")',
						'background-repeat': 'no-repeat',
						'padding-left': '16px'
					};
				}
			}
		}
    }

	function toggleOptions(node) {
		for (var i = 0; i < $rootScope.openTreeNodes.length; i++) {
			$rootScope.openTreeNodes[i].showOptions = false;
		}
		$rootScope.openTreeNodes = [];
		$rootScope.openTreeNodes.push(node);
		node.showOptions = !node.showOptions;
		$scope.mousePosition = ($window.event.clientY - 10);
	}

	function removeIfNameIsUndefinded(name) {
	    if (name === undefined) {
	        return { 'display': 'none' };
	    }
	}

	function itemExpandClasses(toggleState) {
	    if (UCommerceClientMgr.Shell === 'Umbraco7') {
	        switch(toggleState) {
	            case 'treeItemClosed':
	                return ['icon-navigation-right'];
	            case 'treeItemOpen':
	                return ['icon-navigation-down'];
	        }
	    } else {
	        return [toggleState, 'treeItemExpand'];
	    }
    }

	function hideOptions(node) {
		if (node.showOptions)
			node.showOptions = false;
	}
	
	function loadChildNodes(node) {
		var i = 0;
		var children = imagePickerService.getChildren($scope.contentPickerType, node.nodeType, node.id);
		addSpinnerNode(node);
		children.then(
			function (success) { //function called after succesfully calling the function
				// Must clear child nodes to prevent double insertion of elements if network is lagging.
				clearChildNodes(node);
				for (i = 0; i < success.length; i++) {
					addChildNode(success[i], node);
				}
			},
			function (error) { //function called when request ended with an error.
				// Must clear child nodes to prevent double insertion of elements if network is lagging.
				var data = error.data;
				clearChildNodes(node);
				addErrorNodes(data, node);
			}
		);
	}

	function updateChildNodes(node) {

		var newNodes = imagePickerService.getChildren($scope.contentPickerType, node.id);
		newNodes.then(function (newNodeData) {
			if (newNodeData.length != node.nodes.length) {
				// new node created, reloading children instead
				clearChildNodes(node);

				for (var i in newNodeData) {
					addChildNode(newNodeData[i], node);
				}

				treeSetSelected(node.nodes[node.nodes.length - 1]);
				node.hasChildren = true;
			} else {
				// no new nodes, updating node names
				for (var i in newNodeData) {
					var oldNode = node.nodes[i];

					if (oldNode && oldNode.id == newNodeData[i].id)
						oldNode.name = newNodeData[i].name;
				}
			}
		});
	}

	function reloadChildNodes(node) {
		clearChildNodes(node);
		loadChildNodes(node);
	}

	function toggleChildNodes(node) {
		if (!node.loaded) {
			loadChildNodes(node);
			node.loaded = true;
			node.toggleState = openState;
		} else {
			clearChildNodes(node);
			node.loaded = false;
			node.toggleState = closedState;
		}

		if (node.autoload) {
			node.toggleState = hiddenState;
		}
	}

	function clearChildNodes(node) {
		if (node.nodes)
			node.nodes.length = 0;
		else
			node.nodes = [];
	}

	function deleteItem(node) {

	}

	// used in Tree to add a node with error message when updating childnodes causes error.
	function addErrorNodes(data, node) {
		node.nodes.push({
			name: 'Error',
			nodeType: 'Error',
			url: 'ucommerce/information/Error.html/',
			nodes: [],
			icon: 'cross.png',
			id: -1,
			description: 'Status ' + data.status + ' ' + data.data.ResponseStatus.ErrorCode + ' ' + data.data.ResponseStatus.Message + ' ' + data.data.ResponseStatus.StackTrace
		});
	}

	// Used in Tree to add placeholder with loading indicator while childnodes are loading.
	function addSpinnerNode(node) {
	    var nodeSpinner = UCommerceClientMgr.BaseUCommerceUrl + 'Images/ui/ajax-loader.gif';

	    if (UCommerceClientMgr.Shell === 'Umbraco7') {
	    	if ($scope.iconFolder == 'uCommerce') {
			    nodeSpinner = "ajax-loader.gif";

		    } else {
				nodeSpinner = 'icon-node-spinner';
			}
	    }

		if (UCommerceClientMgr.Shell === 'Umbraco') {
			nodeSpinner = "ajax-loader.gif";
		}

		if (UCommerceClientMgr.Shell === 'Sitecore') {
			if ($scope.iconFolder == 'uCommerce') {
				nodeSpinner = "ajax-loader.gif";
			}
		}

		node.nodes.push(
		{
			name: 'loading',
			icon: nodeSpinner,
			nodes: [],
			id: -1
		});
	}

	function addChildNode(childData, parentNode) {
		parentNode.nodes.push({
			name: childData.name,
			nodeType: childData.nodeType,
			url: childData.url,
			icon: childData.icon,
			id: childData.id,
			parentId: parentNode.id,
			parent: parentNode,
			hasChildren: childData.hasChildren,
			showOptions: false,
			loaded: false,
			selected: false,
			autoload: childData.autoload,
			dimNode: childData.dimNode,
			toggleState: closedState,
			nodes: [],
			options: childData.options
		});
	}
}

angular.module('ucommerce').controller("treeController", treeController);