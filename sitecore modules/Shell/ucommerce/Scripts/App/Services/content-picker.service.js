﻿angular.module('ucommerce.services')
	.factory('imagePickerService', function(contentResource) {
		return {
			getImageUrl: function(imageId) {
				return contentResource.getImageUrl(imageId);
			},
			
			getRootNode: function(contentType) {
				return contentResource.getRootNode(contentType);
			},
			
			getChildren: function(contentType, nodeType, nodeId) {
				return contentResource.getChildren(contentType, nodeType, nodeId);
			}
		};
	});