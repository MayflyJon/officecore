﻿//// Simple navigation directive, binding to a navigation view.
function ucommerceContentPickerLauncher() {
    return {
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: {
            id: "@"
        },
        template:
			'<div class="imagePicker">{{test}}' +
				'<span>' +
					'<p style="font-weight: bold; text-decoration: none; cursor: default;">{{inputValueName}}</p> ' +
					'<p data-ng-click="launchTreePicker()" class="choosePicture">{{selectButtonText}}</p>' +
					'<p data-ng-click="removeContent()" data-ng-show="selectedItemEmpty()" class="removePicture">{{removeButtonText}}</p>' +
				'</span>' +
				'<img ng-src="{{ImageUrl}}" data-ng-show="showImage()" class="{{ImageClasses}}" id="selectedImage"/>' +
			'</div>',
        controller: contentPickerLauncherController,
        link: function (scope, elm, attrs) {
            scope.dialogHeaderText = attrs["dialogHeaderText"];
            scope.dialogHeaderTinyText = attrs["dialogHeaderTinyText"];
            scope.saveText = attrs["saveText"];
            scope.saveOrText = attrs["saveOrText"];
            scope.cancelText = attrs["cancelText"];
            scope.hasPreview = attrs["hasPreview"];
            
            scope.inputValueName = attrs["inputValueName"];
            scope.selectButtonText = attrs["selectButtonText"];
            scope.removeButtonText = attrs["removeButtonText"];

            scope.pictureLoad(attrs["image"], attrs["imageClasses"], elm, attrs["contentPickerType"]);
            scope.currentNodeElement = elm;
        }
    };
}

angular.module('ucommerce.directives').directive("ucommerceContentPickerLauncher", ucommerceContentPickerLauncher);