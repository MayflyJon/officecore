﻿function ucommerceContentTree($compile) {
	return {
		restrict: 'E',
		scope: true,
		template:
			'<div class="contentTree">' +
			   '<ucommerce-tree tree="tree" has-checkbox-for="hasCheckboxFor" pre-selected-values="preSelectedValues" selected-node-style="selectedNodeStyle" content-picker-type="contentPickerType" icon-folder="iconFolder"></ucommerce-tree>' +
			   '<input type="hidden" name="{{formName}}" value="{{getPreSelectedValues()}}" />' +
		    '</div>',
		replace: true,
		controller: contentTreeController,
		compile: function (tElement, tAttr) {
			var contents = tElement.contents().remove();
			var compiledContents;
			return function (scope, iElement, iAttr) {
				scope.loadOnCompile = iAttr["loadOnCompile"];
				scope.currentNodeElement = iElement;
				if (!scope.contentPickerType) {
					scope.contentPickerType = iAttr["contentPickerType"];
				}

				if (!scope.selectedNodeStyle) {
					scope.selectedNodeStyle = iAttr["selectedNodeStyle"];
				}

				scope.hasCheckboxFor = iAttr["hasCheckboxFor"];
				scope.iconFolder = iAttr["iconFolder"];

				var preselectedVals = iAttr["preSelectedValues"];
				if (preselectedVals) {
					scope.preSelectedValues = iAttr["preSelectedValues"].split(',');

				}
				scope.formName = iAttr["formName"];
				
				if (!compiledContents) {
					compiledContents = $compile(contents);
				}
				compiledContents(scope, function (clone, scope) {
					iElement.append(clone);
				});
			};
		}
	};
}

angular.module('ucommerce.directives').directive('ucommerceContentTree', ucommerceContentTree);