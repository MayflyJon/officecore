﻿function ucommerceSplitter() {
	return {
		restrict: 'E',
		transclude: true,
		replace: true,
		scope: {
			type: '&',
			header: '=',
			close: '&',
			timeout: '@'
		},
		template: '<div class="ui-splitter"><div class="ui-splitter-top"></div><div class="ui-splitter-bottom"></div></div>',
		link: function (scope, elm, attrs) {

			var left = angular.element(attrs["left"]);
			var right = angular.element(attrs["right"]);
			var minWidth = attrs["minWidthLeft"];
			var maxWidth = attrs["maxWidthLeft"];
			
			var x = 0;

			elm.mousedown(function (event) {
				$(document).mousemove(function (e) {

					// ensure no iframe could prevent event detection
					angular.element('.ui-splitter .ui-splitter-top').css('position', 'absolute'); 

					x = e.pageX;
					var documentWidth = angular.element(document).width();
					var xPercent = (x / documentWidth) * 100;

					if (xPercent <= maxWidth && xPercent >= minWidth) {

						left.width(xPercent + '%');

						var newLeftWidth = left.width();
						var newLeftWidthPercent = (newLeftWidth / documentWidth) * 100;
						var newRightWidthPercent = 100 - newLeftWidthPercent - 0.6;

						right.width(newRightWidthPercent + '%');
					}
				});
			});
			elm.mouseup(function (e) {
				angular.element('.ui-splitter .ui-splitter-top').css('position', 'static');
				$(document).unbind("mousemove");
			});
		}
	};
}

angular.module('ucommerce.directives').directive("ucommerceSplitter", ucommerceSplitter);