﻿function ucommerceTree($compile) {
	return {
		restrict: 'E',
		scope: {
			tree: '=',
			hasCheckboxFor: '=',
			contentPickerType: '=',
			iconFolder: '=',
			loadOnCompile: '=',
			preSelectedValues: '=',
			selectedNodeStyle: '='
		},
		template:

		    '<div>' +
			    '<div class="treeItemExpandContainer">' +
			        '<a data-ng-show="tree.hasChildren" data-ng-click="toggleChildNodes(tree)" ng-class="itemExpandClasses(tree.toggleState)"><span>&nbsp;</span></a>' +
		        '</div>' +
			    '<div data-ng-click="treeSetSelected(tree)" data-right-click="toggleOptions(tree)" data-ng-class="treeNodeLinkClasses(tree)" class="treeLink">' +
			        '<span class="{{nodeIconClasses(tree.icon)}}" ng-style="getNodeIconStyle(tree.icon, tree.name)">' +
					'<input type="checkbox" ng-show="showCheckBoxForNode(tree)" ng-checked="isSelectedCheckbox(tree)" id="{{tree.nodeType}}_{{tree.id}}" name="selected_tree_values" value="{{tree.nodeType}}_{{tree.id}}"/>' +
					'<div class="nodeName"><span>{{tree.name}}</span></div></span>' +
			    '</div>' +
			    '<ul style="list-style-type:none">' +
				    '<li data-ng-repeat="child in tree.nodes">' +
					    '<ucommerce-tree tree="child" content-picker-type="contentPickerType" selected-node-style="selectedNodeStyle" pre-selected-values="preSelectedValues" has-checkbox-for="hasCheckboxFor" icon-folder="iconFolder"></ucommerce-tree>' +
				    '</li>' +
			    '</ul>' +
		    '</div>',
		replace: true,
		controller: treeController,
		compile: function (tElement, tAttr) {
			var contents = tElement.contents().remove();
			var compiledContents;

			return function (scope, iElement, iAttr) {
				scope.currentNodeElement = iElement;
				scope.nodeIconClasses = function (icon) {
					if (UCommerceClientMgr.Shell === 'Umbraco7') {
						if (icon === '.sprTreeFolder') {
							icon = 'icon-folder';
						}

						return 'treeNodeIcon icon umb-tree-icon ' + icon;
					} else {
						return 'treeNodeIcon';
					}
				};

				if (!compiledContents) {
					compiledContents = $compile(contents);
				}
				compiledContents(scope, function (clone, scope) {
					iElement.append(clone);
				});
			};
		}
	};
}

angular.module('ucommerce.directives').directive('ucommerceTree', ucommerceTree);