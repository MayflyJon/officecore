var UCommerceClientMgr = {

	BaseUCommerceUrl: "/umbraco/ucommerce/",
	Shell: "Umbraco",

	openModal: function (url, name, width, height) {
		UmbClientMgr.openModalWindow(url, name, true, width, height);
	},

	getCurrentNodeId: function () {
		return UmbClientMgr.mainTree().getActionNode().nodeId;
	},

	contentFrame: function (url) {
		window.setTimeout(function () {
			if (typeof UmbClientMgr.mainWindow().right != "undefined") {
				UmbClientMgr.mainWindow().right.location.href = url;
			}
			else {
				UmbClientMgr.mainWindow().location.href = url; //set the current windows location if the right frame doesn't exist int he current context
			}
		}, 200);

		// Not using the standard function from Umbraco
		// as it messes around with the URL if 
		// setUmbracoPath is not called on each page.
		// All uCommerce URLs include full path so
		// no handling of URLs with missing CMS
		// path is required.
		//UmbClientMgr.contentFrame(url);
	},

	refreshTree: function (nodeId) {
        // This function refreshes the Umbraco tree for a given node, by simulating a "reload nodes" right click menu action.
	    if (nodeId == null) {
	        nodeId = UmbClientMgr.mainWindow().jQuery('.sprTree.noSpr.clicked').parent().attr('id');
	    }

	    var node = UmbClientMgr.mainTree().findNode(nodeId, true);

	    if (node != false) {
	        UmbClientMgr.mainTree().onBeforeContext(node);
	        UmbClientMgr.mainTree().reloadActionNode(true, false, null);
	    }
	},

	refreshChildrenFor: function (keyVal) {
	    for (n in keyVal) {
	        callDelayedRefreshNode(n, keyVal);
	    };
	},

    buildNodeIdFromIdAndType: function(id, nodeType) {
        switch(nodeType) {
            case 'productCategory':
                return 'Category_' + id;
            case 'productCatalog':
                return 'ProductCatalog_' + id;
            default:
                return null;
        }
    },

	updateCurrentNodeText: function (nodeText) {
		UmbClientMgr.mainWindow().jQuery(".sprTree.noSpr.clicked > div").text(nodeText);
	},

	childNodeCreated: function () {
		UmbClientMgr.mainTree().childNodeCreated();
	},

	// For instance nodeId can be Category_67
	findAndSelectNode: function (nodeId) {
		var node = UmbClientMgr.mainTree().findNode(nodeId, true);
		if (node != null)
			UmbClientMgr.mainTree().selectNode(node, true, true);
	},

	closeModalWindow: function (url) {
		UmbClientMgr.closeModalWindow(url);
	},

	showSpeechBubble: function (icon, header, body) {
		UmbClientMgr.mainWindow().UmbSpeechBubble.ShowMessage(icon, header, body);
	},

	setFocusToTreeNode: function (treeNode) {
		UmbClientMgr.mainTree().selectNode(node, false, true);
	},

	showPopupWindow: function (url, width, height, saveFunction) {
		UmbClientMgr.openModalWindow(this._itemPickerUrl, this._label, this._showHeader, this._width, this._height, 60, 0, ['#cancelbutton'], function (e) { _this.SaveSelection(e); });
	}
};

function sortCategoryThis() {
    UCommerceClientMgr.openModal('ucommerce/catalog/dialogs/SortCatalog.aspx?id=' + UCommerceClientMgr.getCurrentNodeId(), 'Sort', 600, 450);
};

function sortProductsThis() {
    UCommerceClientMgr.openModal('ucommerce/catalog/dialogs/SortCatalog.aspx?id=' + UCommerceClientMgr.getCurrentNodeId() + '&products=1', 'Sort', 600, 450);
};

function callDelayedRefreshNode(n, keyVal) {
    var id = keyVal[n].id;
    var nodeType = keyVal[n].nodeType;
    var nodeId = UCommerceClientMgr.buildNodeIdFromIdAndType(id, nodeType);

    parent.setTimeout(function () {
        UCommerceClientMgr.refreshTree(nodeId);
    }, n * 500);
}


