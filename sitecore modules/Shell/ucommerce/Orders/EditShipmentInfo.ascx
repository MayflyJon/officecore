﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditShipmentInfo.ascx.cs" Inherits="UCommerce.Web.UI.UCommerce.Orders.EditShipmentInfo" %>

<asp:PlaceHolder runat="server" ID="ShowInfoPlaceHolder" Visible="True">
		<table width="100%">
			<tr>
				<th width="30%"><asp:Localize ID="Localize1" runat="server" meta:resourcekey="ShippingMethod"></asp:Localize></th>
				<td><%# Shipment.ShippingMethod.Name %></td>
			</tr>
			<tr>
				<th width="30%"><asp:Localize ID="Localize2" runat="server" meta:resourcekey="TrackAndTrace"></asp:Localize></th>
				<td><%# Shipment.TrackAndTrace %></td>
			</tr>
			<tr>
				<th width="30%"><asp:Localize ID="Localize3" runat="server" meta:resourcekey="Notes"></asp:Localize></th>
				<td><%# Shipment.DeliveryNote %></td>
			</tr>
			<tr>
				<th width="30%"></th>
				<td style="padding-left:100px;">
					<asp:LinkButton runat="server" ID="EditLink" OnClick="ToggleEditable" Text="<%$ Resources:Edit.Text %>" CausesValidation="false" />
				</td>
			</tr>
		</table>
</asp:PlaceHolder>

<asp:PlaceHolder runat="server" ID="EditInfoPlaceHolder" Visible="False">
	<table width="100%">
		<tr>
			<th width="30%"><asp:Localize ID="Localize4" runat="server" meta:resourcekey="ShippingMethod"></asp:Localize></th>
			<td><asp:DropDownList runat="server" ID="ShippingMethodDropDown" CssClass="smallWidthInput" style="width:156px;" /></td>
		</tr>
		<tr>
			<th width="30%"><asp:Localize ID="Localize5" runat="server" meta:resourcekey="TrackAndTrace"></asp:Localize></th>
			<td><asp:TextBox runat="server" ID="TrackAndTraceTextBox" CssClass="smallWidthInput" /></td>
		</tr>
		<tr>
			<th width="30%"><asp:Localize ID="Localize6" runat="server" meta:resourcekey="Notes"></asp:Localize></th>
			<td><asp:TextBox runat="server" ID="DeliveryNotesTextBox" CssClass="smallWidthInput" /></td>
		</tr>
		<tr>
			<th width="30%"></th>
			<td style="padding-left:135px;">
				<asp:LinkButton runat="server" ID="CancelLink" OnClick="ToggleEditable" Text="<%$ Resources:Cancel.Text %>" CausesValidation="false" />
			</td>
		</tr>
	</table>
</asp:PlaceHolder>