<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditOrderShipments.ascx.cs"Inherits="UCommerce.Web.UI.Orders.EditOrderShipments" %>
<%@ Register tagPrefix="uc" tagName="EditShipment" src="EditShipment.ascx" %>

<asp:Repeater runat="server" ID="ShipmentRepeater">
    <ItemTemplate>
        <uc:EditShipment EnableViewState="True" runat="server" 
            ShipmentIndex="<%# (Container.ItemIndex+1) %>"
            IsEditable="<%# IsEditable %>" 
            Shipment="<%# AsShipment(Container.DataItem) %>"
            ShipmentView="<%# View %>" />
    </ItemTemplate>
</asp:Repeater>