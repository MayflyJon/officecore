<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditOrderPayments.ascx.cs" Inherits="UCommerce.Web.UI.Orders.EditOrderPayments" %>

<div class="propertyPane leftAligned">
	<h2 class="propertyPaneTitel"><asp:Localize runat="server" meta:resourcekey="Payments" /></h2>
    <asp:Repeater runat="server" ID="PaymentRepeater" DataSource="<%# Payments %>">
        <ItemTemplate>
            <commerce:PropertyPanel runat="server" meta:resourceKey="PaymentMethod">
                <%# AsPayment(Container.DataItem).PaymentMethodName %>
            </commerce:PropertyPanel> 
			<commerce:PropertyPanel runat="server" meta:resourceKey="Amount">
                <%# AsPayment(Container.DataItem).Amount.ToString("N") %>
            </commerce:PropertyPanel>
			<commerce:PropertyPanel runat="server" meta:resourceKey="PaymentStatus">
                <%# AsPaymentStatus(Container.DataItem).Name %>
            </commerce:PropertyPanel>
            <commerce:PropertyPanel runat="server" meta:resourceKey="TransactionID">
                <%# AsPayment(Container.DataItem).TransactionId %>
            </commerce:PropertyPanel>
			<commerce:PropertyPanel runat="server" meta:resourceKey="ReferenceID">
                <%# AsPayment(Container.DataItem).ReferenceId %>
            </commerce:PropertyPanel>
        </ItemTemplate>
    </asp:Repeater>
	<div class="propertyPaneFooter">-</div>
</div>