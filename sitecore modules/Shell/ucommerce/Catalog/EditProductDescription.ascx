<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditProductDescription.ascx.cs" Inherits="UCommerce.Web.UI.Catalog.EditProductDescription" %>
<%@ Register tagPrefix="commerce" tagName="ValidationSummary" src="../Controls/ValidationSummaryDisplay.ascx" %>

<commerce:ValidationSummary runat="server" />
<div class="propertyPane">
    <div class="propertyItem">
        <div class="propertyItemHeader"><asp:Localize ID="Localize1" runat="server" meta:resourceKey="DisplayName" /></div>
        <div class="propertyItemContent">
            <asp:TextBox runat="server" CssClass="multiLingualDisplayName maxWidth" ID="DisplayNameTextBox" Text="<%# ProductDescription.DisplayName %>" />
        </div>
    </div>
    <div class="propertyItem">
        <div class="propertyItemHeader"><asp:Localize ID="Localize2" runat="server" meta:resourceKey="ShortDescription" /></div>
        <div class="propertyItemContent">
            <asp:TextBox runat="server" ID="ShortDescriptionTextBox" CssClass="maxWidth" TextMode="MultiLine" Text="<%# ProductDescription.ShortDescription %>" />
        </div>
    </div>
    <div class="propertyItem">
        <div class="propertyItemHeader"><asp:Localize ID="Localize3" runat="server" meta:resourceKey="LongDescription" /></div>
        <div class="propertyItemContent">
            <asp:PlaceHolder ID="LongDescriptionPlaceHolder" runat="server" />
        </div>
    </div>
    <div class="propertyPaneFooter"></div>
</div>

<asp:panel id="PropertyPanel" runat="server" cssclass="propertyPane"/>