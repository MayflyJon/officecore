<%@ page language="C#" autoeventwireup="true" codebehind="CreateProductRelation.aspx.cs"
	inherits="UCommerce.Web.UI.Catalog.Dialogs.CreateProductRelation" MasterPageFile="../../MasterPages/Dialog.Master" %>
<%@ import namespace="UCommerce.Web.UI.Catalog.Dialogs" %>
<%@ register tagprefix="uc" tagname="CatalogItemSelector" src="CatalogItemSelector.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderLabel" runat="server">
    <asp:Localize ID="Localize5" runat="server" meta:resourceKey="Header" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentArea" runat="server">
   	<div class="propertyPane dialog-header">
        <h3>
		    <span>
			    <asp:Localize id="Localize2" runat="server" meta:resourcekey="Header" />
		    </span>
	    </h3>
	    <div>
		    <p class="guiDialogTiny">
			    <asp:Localize id="Localize3" runat="server" meta:resourcekey="SubHeader" />
		    </p>
	    </div>
   	</div>
	<div class="propertyPane">
		<div class="propertyItem">
			<div class="propertyItemHeader" >
				<asp:Localize id="Localize4" runat="server" meta:resourcekey="RelationType" />
			</div>
			<div class="propertyItemContent" >
				<asp:DropDownList CssClass="editorDropDownBox" runat="server" id="RelationTypeSelector" datasource="<%# RelationTypes %>"
					datatextfield="Name" datavaluefield="ProductRelationTypeId" />
			</div>
		</div>
		<div class="propertyItem">
			<div class="propertyItemHeader" >
				<asp:Localize id="Localize6" runat="server" meta:resourcekey="TwoWayRelation" />
			</div>
			<div class="propertyItemContent" >
				<asp:CheckBox id="TwoWayRelationshipCheckBox" runat="server" />
			</div>
		</div>
		<uc:CatalogItemSelector runat="server" id="catalogItemSelector" SelectType="<%# CatalogItemType.ProductVariant %>"
			MultipleSelect="true" />
		<div class="propertyPaneFooter"></div>
	</div>

    <div class="propertyPane dialog-actions">
		<asp:Button id="SaveButton" runat="server" meta:resourcekey="SaveButton" onclick="SaveButton_Clicked" />
		<em>or&nbsp;</em>
		<a href="#" style="color: blue" onclick="UCommerceClientMgr.closeModalWindow()"><asp:Localize id="Localize1" runat="server" meta:resourcekey="CancelButton" /></a>
	</div>
</asp:Content>