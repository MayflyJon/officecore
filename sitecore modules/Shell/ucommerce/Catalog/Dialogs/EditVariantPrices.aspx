<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="../../masterpages/Dialog.master" CodeBehind="EditVariantPrices.aspx.cs" Inherits="UCommerce.Web.UI.Catalog.Dialogs.EditVariantPrices" %>
<%@ Register Src="../EditProductPricing.ascx" TagPrefix="commerce" TagName="EditPrices" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderLabel" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentArea" runat="server">
	<div class="ui-widget" style="height: auto; border: none; width: 100%;">
		<commerce:PropertyPanel ID="PropertyPanel1" runat="server">
			<commerce:EditPrices runat="server" ID="EditProductPrices" />
		</commerce:PropertyPanel>
        
		<div style="margin:10px;">
			<asp:Button runat="server" ID="SaveButton" Width="90px" Text="Save" OnClick="SaveButton_Clicked" />
			<em>or</em>
			<a href="#" onclick="UCommerceClientMgr.closeModalWindow();" style="color: blue; cursor: hand">Cancel</a>    
		</div>
	</div>

</asp:Content>
