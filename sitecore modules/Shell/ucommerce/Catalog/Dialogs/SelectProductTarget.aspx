<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="../../masterpages/Dialog.master" CodeBehind="SelectProductTarget.aspx.cs" Inherits="UCommerce.Web.UI.Catalog.Dialogs.SelectProductTarget" %>

<%@ import namespace="UCommerce.Web.UI.Catalog.Dialogs" %>
<%@ register tagprefix="uc" tagname="catalogItemselector" src="CatalogItemSelector.ascx" %>
<asp:content id="Content1" contentplaceholderid="head" runat="server">
</asp:content>
<asp:content id="Content2" contentplaceholderid="HeaderLabel" runat="server">
    <asp:Localize ID="Localize5" runat="server" meta:resourceKey="Header" />
</asp:content>
<asp:content id="Content3" contentplaceholderid="ContentArea" runat="server">
	<div style="margin:10px;">
		<h3>
			<span>
				<%= GetLocalResourceObject("Header.Text") %>
			</span>
		</h3>
		<div>
			<p class="guiDialogTiny">
				<%= GetLocalResourceObject("SubHeader.Text") %>
			</p>
		</div>
    </div>
	<div class="propertyPane">
		<div>
			<uc:catalogItemselector runat="server" id="catalogItemSelector" selecttype="<%# CatalogItemType.ProductVariant %>"
				multipleselect="false" />
		</div>
		<div class="propertyPaneFooter"></div>
	</div>
    <div style="margin:10px;">
		<asp:Button id="SaveButton" runat="server" onclick="SaveButton_Clicked" />
		<em><%= GetLocalResourceObject("Or.Text") %> </em>
		<a href="#" style="color: blue" onclick="UCommerceClientMgr.closeModalWindow()">
			<%= GetLocalResourceObject("CancelButton.Text") %>
		</a>
    </div>
</asp:content>