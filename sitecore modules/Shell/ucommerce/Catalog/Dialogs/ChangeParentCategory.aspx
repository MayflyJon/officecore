﻿<%@ Page Title="" Language="C#" MasterPageFile="../../MasterPages/Dialog.Master" AutoEventWireup="true" CodeBehind="ChangeParentCategory.aspx.cs" Inherits="UCommerce.Web.UI.UCommerce.Catalog.Dialogs.ChangeParentCategory" %>
<%@ Register TagPrefix="uc" Namespace="UCommerce.Web.UI.Catalog.Dialogs" Assembly="UCommerce.Admin" %>
<%@ Import Namespace="UCommerce.Web.UI.Catalog.Dialogs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderLabel" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentArea" runat="server">
	<div style="margin:10px;">
        <h3>
		    <span>
			    <asp:Localize id="Localize2" meta:resourcekey="Header" runat="server" />
		    </span>
	    </h3>
	    <div>
		    <p class="guiDialogTiny">
			    <asp:Localize id="Localize3" runat="server" meta:resourcekey="SubHeader" />
		    </p>
	    </div>
   	</div>
	<div class="propertyPane changeParentCategoryPane">
	    <div class="TreeViewWrapper large row-hover overFlowScroll" style="min-height: 350px; max-height: 350px;">
	        <asp:TreeView ID="CategorySelector" runat="server" OnSelectedNodeChanged="SelectorTreeView_OnSelectedNodeChanged"/>		        
	    </div>
	</div>
	<div style="margin:10px">
		<p>
			<asp:Button id="SaveButton" runat="server" meta:resourcekey="SaveButton" onclick="SaveButton_Clicked" />
			<em>
			    <asp:Localize id="Localize5" runat="server" meta:resourcekey="Or" />
			</em>
            <a href="#" style="color: blue; margin-left: 2px;" onclick="UCommerceClientMgr.closeModalWindow()">
				<asp:Localize id="Localize1" runat="server" meta:resourcekey="CancelButton" />
			</a>
		</p>
	</div>
</asp:Content>
