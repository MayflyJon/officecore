<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditProductCatalogs.ascx.cs" Inherits="UCommerce.Web.UI.Catalog.EditProductCatalogs" %>
<%@ Register tagPrefix="commerce" tagName="ValidationSummary" src="../Controls/ValidationSummaryDisplay.ascx" %>

<commerce:ValidationSummary runat="server" />
<div style="text-align: left;">
    <div class="propertyPane leftAligned">
        <div class="propertyItem">
            <div class="propertyItemHeader"><asp:Localize runat="server" meta:resourcekey="Productcategories"></asp:Localize></div>
            <div class="propertyItemContent">                
                <asp:PlaceHolder ID="CategoriesPlaceHolder" runat="server"></asp:PlaceHolder>
                <asp:CustomValidator runat="server" ID="ProductCategoriesValidator" ErrorMessage='<%# GetLocalResourceObject("ProductCategories") %>' Display="None" OnServerValidate="OnServerValidate"></asp:CustomValidator>
            </div>
            <div class="propertyPaneFooter"></div>
        </div>
    </div> 
</div>