function TreeController($scope, $rootScope, $window, $location, $q, treeService, localizationService) {
	localizationService.getTranslatedStrings(''); // load localized resources.
	if ($scope.tree) {
	}
	
	// Note: This way of handling the open/closed state is only here for a POC. A smarter way must be found.
	var closedState = 'treeItemClosed';
	var openState = 'treeItemOpen';
	var hiddenState = 'treeItemHidden';

	// Expose the node functions.
	$scope.toggleChildNodes = toggleChildNodes;
	$scope.reloadChildNodes = reloadChildNodes;
	$scope.updateChildNodes = updateChildNodes;
	$scope.toggleOptions = toggleOptions;
	$scope.toggleStyle = toggleStyle;
	$scope.hideOptions = hideOptions;
	$scope.handleAction = handleAction;
	$scope.getNodeIconStyle = getNodeIconStyle;
	$scope.treeSetSelected = treeSetSelected;
	$scope.treeNodeOptionMenuStyle = treeNodeOptionMenuStyle;
	$scope.treeNodeLinkClasses = treeNodeLinkClasses;
	$scope.goToLink = goToLink;
	
    $rootScope.openTreeNodes = []; 
	// Autoload.
	$scope.$watch('tree', function () {

		if ($scope.tree) {
			if ($scope.tree.autoload) {
				toggleChildNodes($scope.tree);
			}
		}
	});
	
	function goToLink(url) {
		$location.path(url);
	}

	function toggleStyle(node) {
		if (node && !node.hasChildren) {
			return { 'visibility': 'hidden' };
		}
		return '';
	}
	
	function treeNodeLinkClasses(node) {
		var classes = [];

		if (node) {
			if (node.selected)
				classes.push('nodeSelected');

			if (node.dimNode)
				classes.push('dimNode');
		}

		return classes;
	}

	function treeNodeOptionMenuStyle(icon) {
		if (icon)
			return {
				'background-image': 'url("Content/Images/tree/' + icon + '")'
			};
		
		return '';
	}

	function treeSetSelected(node) {
		if (node.url !== '') {
			if ($rootScope.selectedNode != undefined)
				$rootScope.selectedNode.selected = false;

			node.selected = true;

			$rootScope.selectedNode = node;
		} 
	}

	function handleAction(option, node) {
		treeSetSelected(node);
		switch (option.optionType) {
			case 'New':
				// Adding temp node before the new node is created.
				var tempNode = { parent: node };
				treeSetSelected(tempNode);

				$location.path(option.actionUrl);
				break;
			case 'Delete':
				deleteItem(node);
				break;
			case 'Refresh':
				reloadChildNodes(node);
				treeSetSelected(node);
				break;
			case 'Sort':
				$location.path(option.actionUrl);
				break;
		}
	}

	function getNodeIconStyle(icon) {
	    if (icon) {
		    return {
				'background-image': 'url("Content/Images/ui/' + icon + '")'
			};
	    }
		return '';
	}

	function toggleOptions(node) {
	    for (var i = 0; i < $rootScope.openTreeNodes.length; i++) {
	        $rootScope.openTreeNodes[i].showOptions = false;
	    }
	    $rootScope.openTreeNodes = [];
	    $rootScope.openTreeNodes.push(node);
		node.showOptions = !node.showOptions;
	}

	function hideOptions(node) {
		if (node.showOptions)
			node.showOptions = false;
	}

	function updateChildNodes(node) {
		var newNodes = treeService.loadChildNodes(node);

		newNodes.then(function (newNodeData) {
		    if (newNodeData.length != node.nodes.length) {
				// New nodes, adding them to the parent node
				addNewNodeChildren(newNodeData, node.nodes, node);
		    } else if (orderChanged(newNodeData, node.nodes)) {
		        addNewNodeChildren(newNodeData, node.nodes, node);
		        treeSetSelected(node);
		    } else {
		        // No new nodes, updating node name
				updateNodeNames(newNodeData, node.nodes);
			}
		});
	}

	function orderChanged(leftList, rightList) {
	    if (leftList.length != rightList.length)
	        return true;

	    for (var i = 0; i < leftList.length; i++) {
            if (leftList[i].id != rightList[i].id)
                return true;
        }

        return false;
    }

	function addNewNodeChildren(newNodes, oldNodes, parentNode) {
		// Find ids for new nodes
		var newNodeIds = getNewNodeIds(oldNodes, newNodes);

		// new node created, reloading children
		clearChildNodes(parentNode);

		for (var x in newNodes) {
			addChildNode(newNodes[x], parentNode);
		}

		// Set new node as selected
		if (newNodeIds.length > 0) {
			for (var i in oldNodes) {
				if (oldNodes[i].id == newNodeIds[0]) {
					treeSetSelected(oldNodes[i]);
					break;
				}
			}
		} else {
			treeSetSelected(oldNodes[oldNodes.length - 1]);
		}

		parentNode.hasChildren = true;
	}

	function updateNodeNames(newNodes, oldNodes) {
		for (var i in newNodes) {
			for (var ii in oldNodes) {
			    if (newNodes[i].id == oldNodes[ii].id) {
				    oldNodes[ii].name = newNodes[i].name;
				    oldNodes[ii].dimNode = newNodes[i].dimNode;
				    oldNodes[ii].icon = newNodes[i].icon;
				    oldNodes[ii].options = newNodes[i].options;
			    }
			}
		}
	}

	function getNewNodeIds(oldNodes, newNodes) {
		var oldIds = [];
		var newIds = [];
		var uniqueIds = [];

		var i;
		for (i in oldNodes) { oldIds.push(oldNodes[i].id); }
		for (i in newNodes) { newIds.push(newNodes[i].id); }

		for (i in newIds) {
			if (oldIds.indexOf(newIds[i]) == -1) {
				uniqueIds.push(newIds[i]);
			}
		}
		
		return uniqueIds;
	}

	function reloadChildNodes(node) {
		clearChildNodes(node);
		loadChildNodes(node);
	}

	function toggleChildNodes(node) {
		if (!node.loaded) {
			loadChildNodes(node);
			node.loaded = true;
			node.toggleState = openState;
		} else {
			clearChildNodes(node);
			node.loaded = false;
			node.toggleState = closedState;
		}

		if (node.autoload)
			node.toggleState = hiddenState;
	}

	function loadChildNodes(node) {
		var i;
		var children = treeService.loadChildNodes(node);
		addSpinnerNode(node); 
		children.then(
			function (success) { //function called after succesfully calling the function
				// Must clear child nodes to prevent double insertion of elements if network is lagging.
				clearChildNodes(node);
				for (i = 0; i < success.length; i++) {
					addChildNode(success[i], node);
				}
			},
			function (error) { //function called when request ended with an error.
				// Must clear child nodes to prevent double insertion of elements if network is lagging.
				clearChildNodes(node);
				addErrorNodes(error, node);
			}
		);
	}

	function clearChildNodes(node) {
		node.nodes.length = 0;
	}
	
	function deleteItem(node) {
		var confirmDelete = localizationService.getTranslatedStrings('ConfirmDelete');
		var remove = confirm(confirmDelete);
		if (remove) {
			treeService.deleteItem(node).then(function () {
				angular.element($scope.currentNodeElement).hide('fade', function() {
					node.parent.nodes.splice(node.parent.nodes.indexOf(node), 1);

					if (node.parent.nodes.length < 1)
						node.parent.hasChildren = false;
				});
			});
		}
	}
	
	// used in Tree to add a node with error message when updating childnodes causes error.
	function addErrorNodes(data, node) {
		node.nodes.push({
			name: 'Error',
			nodeType: 'Error',
			url: 'ucommerce/information/Error.html/',
			nodes: [],
			icon: 'cross.png',
			id: -1,
			description: 'Status ' + data.status + ' ' + data.data.ResponseStatus.ErrorCode + ' ' + data.data.ResponseStatus.Message + ' ' + data.data.ResponseStatus.StackTrace
		});
	}

	// Used in Tree to add placeholder with loading indicator while childnodes are loading.
	function addSpinnerNode(node) {
		node.nodes.push(
		{
			name: 'loading',
			icon: 'ajax-loader.gif',
			nodes: [],
			id: -1
		});
	}

	function addChildNode(childData, parentNode) {
		if (typeof(childData) == 'function') return; //IE treats arrayExtentions as childData. 

		var newNode = {
			name: childData.name,
			nodeType: childData.nodeType,
			url: childData.url,
			icon: childData.icon,
			id: childData.id,
			parentId: parentNode.id,
			parent: parentNode,
			hasChildren: childData.hasChildren,
			showOptions: false,
			loaded: false,
			selected: false,
			autoload: childData.autoload,
			dimNode: childData.dimNode,
			toggleState: closedState,
			nodes: [],
			options: childData.options
		};

		$rootScope.addNodeToCollection(newNode);
		parentNode.nodes.push(newNode);
	}
}

//register it
angular.module('ucommerce').controller("TreeController", TreeController);