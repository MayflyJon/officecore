﻿// The list of ucommerce directives. The _module.js script defines the 'ucommerce.directives' module.
// Add new directives to the end of the list.
yepnope({

	load: [
		'App/Directives/_module.js',
		'App/Directives/ucommerce.alert.directive.js',
		'App/Directives/ucommerce.navigation.directive.js',
		'App/Directives/ucommerce.iframe.directive.js',
        'App/Directives/ucommerce.iframe.modal.directive.js',
		'App/Directives/ucommerce.resize.directive.js',
		'App/Directives/ucommerce.rightclick.directive.js',
		'App/Directives/ucommerce.tree.directive.js',
		'App/Directives/ucommerce.splitter.directive.js'
	]
});