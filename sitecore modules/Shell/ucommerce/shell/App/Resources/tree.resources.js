﻿function treeResource($http) {
	var serviceUrl = constants.serviceBaseUrl + 'Content/Tree/';

	return {
		getRootNode: function () {
		    return $http.get(serviceUrl + 'RootNode' + top.location.search).then(function (response) {
				return response.data;
			});
		},
		
		getChildNodes: function(type, id) {
		    return $http.get(serviceUrl + 'Children/' + type + '/' + id + top.location.search).then(function (response) {
				return response.data;
			});
		}
	};
}

angular.module('ucommerce.resources').factory('treeResource', treeResource);