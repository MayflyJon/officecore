﻿function localizationResource($http) {
    var serviceUrl = constants.serviceBaseUrl + 'Localization/TranslatedStrings' + top.location.search;

	return {
		getTranslatedStrings: function () {
			return $http.get(serviceUrl).then(function (response) {
				return response.data;
			});
		}
	};
}

angular.module('ucommerce.resources').factory('localizationResource', localizationResource);