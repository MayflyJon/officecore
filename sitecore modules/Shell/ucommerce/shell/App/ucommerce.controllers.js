﻿// List of ucommerce controllers. Add new controllers to this list, to load them for use in angular. 
yepnope({

	load: [
		'App/Controllers/main.controller.js',
		'App/Controllers/tree.controller.js',
	    'App/Controllers/alert.controller.js',
		'App/Controllers/modal.controller.js'
	]
});