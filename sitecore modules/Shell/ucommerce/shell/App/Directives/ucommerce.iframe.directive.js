﻿function ucommerceIFrame() {
	function updateFrameSource(frameUrl, frameid) {

		var frame = angular.element("#" + frameid);

		// For some reason, clicking a link and updating the iFrame src each appears to cause a change in $location.path,
		// so in order to avoid double loads of the iFrame, we check if the current src is identical to the existing source.

		frame.attr('src', frameUrl);
	}

	return {
		restrict: 'A',
		replace: false,
		controller: function ($scope, $attrs, iframeUrlUpdateService) {
			// The iframeUrlService is injected.

			$scope.setUrl = function(url) {
        	    console.log('setUrl called :' + url);
				iframeUrlUpdateService.broadcastUrl(url);
			};

			$scope.$on('updateIFrameUrlEventFired', function () {
				// Whenever the "IFrame Url Updated" event is triggered, the src for the iframe is updated.
				// The new url is taken from the iframeUrlUpdateService.
				updateFrameSource(iframeUrlUpdateService.message, $attrs.id);
			});
		},
	};
}

angular.module('ucommerce.directives').directive("ucommerceIframe", ucommerceIFrame);