﻿function ucommerceAlert($timeout) {
	return {
		restrict: 'E',
		transclude: true,
		replace: true,
		scope: {
			type: '&',
			header: '=',
			close: '&',
			timeout: '@'
		},
		template: '<div ng-class="type">' +
						'<button ng-show="closeable" type="button" class="close" ng-click="close()">&times;</button>' +
						'<h4>{{header}}</h4>' +
						'<span ng-transclude=""></span>' +
					'</div>',
		link: function (scope, elm, attrs) {
			scope.closeable = 'close' in attrs;

			if ("timeout" in attrs) {
			    $timeout(function () {
			        $(elm).fadeOut(1500);
				}, attrs.timeout);
			}
		}
	};
}

angular.module('ucommerce.directives').directive("ucommerceAlert", ucommerceAlert);