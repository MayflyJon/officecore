﻿// Simple navigation directive, binding to a navigation view.
function navigationDirective() {
	return {
		restrict: "E",
		replace: true,
		templateUrl: 'App/Views/ucommerce-navigation.html'
	};
}

angular.module('ucommerce.directives').directive("ucommerceNavigation", navigationDirective);