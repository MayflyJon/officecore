﻿function rightClick($parse) {
	return function (scope, element, attrs) {
		var fn = $parse(attrs.rightClick);

		element.bind('contextmenu', function (event) {
			scope.$apply(function () {
				event.preventDefault();
				fn(scope, { $event: event });
			});
		});
	};
};

angular.module('ucommerce.directives').directive("rightClick", rightClick);