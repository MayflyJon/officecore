﻿function ucommerceModalIFrame() {
	return function(scope, element) {
		element.attr('src', scope.url);
	};
}

angular.module('ucommerce.directives').directive("ucommerceModalIframe", ucommerceModalIFrame);