﻿function ucommerceTree($compile) {
	return {
		restrict: 'E',
		scope: {
			tree: '='
		},
		template:

		    '<div class="relative">' +
			    '<div class="treeItemExpandContainer">' +
			        '<a data-ng-click="toggleChildNodes(tree)" ng-style="toggleStyle(tree)" class="{{tree.toggleState}} treeItemExpand"><span>&nbsp;</span></a>' +
		        '</div>' +
			    '<a data-ng-click="treeSetSelected(tree);goToLink(tree.url);" data-right-click="toggleOptions(tree)" data-ng-class="treeNodeLinkClasses(tree)" class="treeLink">' +
			        '<span class="treeNodeIcon" ng-style="getNodeIconStyle(tree.icon)"><div class="treeIconName"><span>{{tree.name}}</span></div></span>' +
			    '</a>' +
                '<div class="treeContextMenu absolute" data-ng-show="tree.showOptions" data-ng-mouseleave="hideOptions(tree)" data-ng-mouseup="hideOptions(tree)">' +
                    '<ul style="">' +
                        '<li data-ng-repeat="option in tree.options" ng-class="option.cssClass">' +
                            '<a href="javascript:;" data-ng-click="handleAction(option, tree)" ng-style="treeNodeOptionMenuStyle(option.icon)">{{option.displayName}}</a>' +
                        '</li>' +
                    '</ul>' +
                '</div>' +
			    '<ul style="list-style-type:none">' +
				    '<li data-ng-repeat="child in tree.nodes">' +
					    '<ucommerce-tree tree="child"></ucommerce-tree>' +
				    '</li>' +
			    '</ul>' +
		    '</div>',
		replace: true,
		controller: TreeController,
		compile: function (tElement, tAttr) {
			var contents = tElement.contents().remove();
			var compiledContents;

			return function (scope, iElement, iAttr) {
				scope.currentNodeElement = iElement;

				if (!compiledContents) {
					compiledContents = $compile(contents);
				}
				compiledContents(scope, function (clone, scope) {
					iElement.append(clone);
				});
			};
		}
	};
}

angular.module('ucommerce.directives').directive('ucommerceTree', ucommerceTree);