﻿function ucommerceSplitter() {
	return {
		restrict: 'E',
		transclude: true,
		replace: true,
		scope: {
			type: '&',
			header: '=',
			close: '&',
			timeout: '@'
		},
		template: '<div class="ui-splitter"><div class="ui-splitter-top"></div><div class="ui-splitter-bottom"></div></div>',
		link: function (scope, elm, attrs) {
			var lefts = attrs["left"].split('|');
			var rights = attrs["right"].split('|');
			var minWidthPercent = attrs["minWidthLeft"];
			var maxWidthPercent = attrs["maxWidthLeft"];
			
			var x = 0;
			var newRightWidthPercent = 0;
			var newRight = '';
			var newLeftWidthPercent = 0;
			var newLeft = '';
			var dragged = false;
			elm.mousedown(function (event) {
				angular.element('.ui-splitter .ui-splitter-top').css('position', 'fixed');
				angular.element('.ui-splitter .ui-splitter-bottom').css('visibility', 'visible');
				angular.element('.ui-splitter .ui-splitter-bottom').css('position', 'fixed');
				angular.element('.ui-splitter .ui-splitter-bottom').css('height', '100%');
				$(document).mousemove(function (e) {
					dragged = true;

					x = e.pageX;

					var documentWidth = angular.element(window).width();
					var xPercent = (x / documentWidth) * 100;
					if (xPercent <= maxWidthPercent && xPercent >= minWidthPercent) {

						angular.element('.ui-splitter .ui-splitter-bottom').css('left', e.pageX);
						newLeft = xPercent + '%';
						var newLeftWidth = x;
						newLeftWidthPercent = (newLeftWidth / documentWidth) * 100;
						newRightWidthPercent = 100 - newLeftWidthPercent - 0.4;
						newRight = newRightWidthPercent + '%';
					}
				});
			});
			elm.mouseup(function (e) {
				if (dragged) {
					dragged = false;
					
					for (var i = 0; i < lefts.length; i++) {
						var element = angular.element(lefts[i]);
						element.width(newLeft);
					}
					for (i = 0; i < rights.length; i++) {
						var element = angular.element(rights[i]);
						element.width(newRight);
					}
				}
				angular.element('.ui-splitter .ui-splitter-top').css('position', 'static');

				angular.element('.ui-splitter .ui-splitter-bottom').css('position', 'static');
				angular.element('.ui-splitter .ui-splitter-bottom').css('height', '0px');
				angular.element('.ui-splitter .ui-splitter-bottom').css('left', newLeft);
				angular.element('.ui-splitter .ui-splitter-bottom').css('visibility', 'hidden');

				$(document).unbind("mousemove");
			});
		}
	};
}

angular.module('ucommerce.directives').directive("ucommerceSplitter", ucommerceSplitter);