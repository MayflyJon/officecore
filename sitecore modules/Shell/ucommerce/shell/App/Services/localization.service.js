﻿angular.module('ucommerce.services')
	.factory('localizationService', function(localizationResource) {

		var localizationServiceObject = {};
		
		localizationServiceObject.getTranslatedStrings = function (key) {
			if (!localizationServiceObject.resourceObjects) {
				var response = localizationResource.getTranslatedStrings();
				response.then(function(data) {
					localizationServiceObject.resourceObjects = data;
				});
			}
			if (key == '')
				return '';
			return localizationServiceObject.resourceObjects[key];
		};
		
		return localizationServiceObject;
	});