<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditProductDefinitionBaseProperties.ascx.cs" Inherits="UCommerce.Web.UI.Settings.Definitions.EditProductDefinitionBaseProperties" %>

<%@ Register tagPrefix="commerce" tagName="ValidationSummary" src="../../Controls/ValidationSummaryDisplay.ascx" %>

<commerce:ValidationSummary ID="ValidationSummary1" runat="server" />
<div style="text-align: left;">
	<div class="propertyPane leftAligned">
        <div class="propertyItem">
	        <div class="propertyItemHeader"><asp:Localize runat="server" meta:resourceKey="ProductFamily" /></div>
            <div class="propertyItemContent"><asp:CheckBox runat="server" ID="ProductFamilyCheckBox" Enabled="false" Checked="<%# View.ProductDefinition.IsProductFamily() %>" /></div>
	    </div>
        <div class="propertyItem">
	        <div class="propertyItemHeader"><asp:Localize runat="server" meta:resourceKey="Description" /></div>
            <div class="propertyItemContent"><asp:TextBox runat="server" ID="DescriptionTextBox" TextMode="MultiLine" CssClass="mediumWidth smallHeight" Text="<%# View.ProductDefinition.Description %>" /></div>
	    </div>
        <div class="propertyPaneFooter"></div>
	</div>
</div>
