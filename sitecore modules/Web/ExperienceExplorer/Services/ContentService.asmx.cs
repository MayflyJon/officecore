﻿using Sitecore.Collections;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.ExperienceExplorer.Business.Constants;
using Sitecore.ExperienceExplorer.Business.Entities;
using Sitecore.ExperienceExplorer.Business.Entities.Campaigns;
using Sitecore.ExperienceExplorer.Business.Entities.GeoIps;
using Sitecore.ExperienceExplorer.Business.Entities.Goals;
using Sitecore.ExperienceExplorer.Business.Entities.PageEvents;
using Sitecore.ExperienceExplorer.Business.Entities.Profiles;
using Sitecore.ExperienceExplorer.Business.Entities.Referrals;
using Sitecore.ExperienceExplorer.Business.Entities.Rules;
using Sitecore.ExperienceExplorer.Business.Entities.Tags;
using Sitecore.ExperienceExplorer.Business.Factories;
using Sitecore.ExperienceExplorer.Business.Helpers;
using Sitecore.ExperienceExplorer.Business.Managers;
using Sitecore.ExperienceExplorer.Business.Models;
using Sitecore.ExperienceExplorer.Business.Strategies.Campaigns;
using Sitecore.ExperienceExplorer.Business.Strategies.GeoIps;
using Sitecore.ExperienceExplorer.Business.Strategies.Goals;
using Sitecore.ExperienceExplorer.Business.Strategies.PageEvents;
using Sitecore.ExperienceExplorer.Business.Strategies.Profiles;
using Sitecore.ExperienceExplorer.Business.Strategies.Referrals;
using Sitecore.ExperienceExplorer.Business.Strategies.Rules;
using Sitecore.ExperienceExplorer.Business.Strategies.Tags;
using Sitecore.ExperienceExplorer.Business.Utilities;
using Sitecore.ExperienceExplorer.Business.Utilities.Extensions;
using Sitecore.Globalization;
using Sitecore.Web.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using Sitecore.Data.Fields;

namespace Sitecore.ExperienceExplorer.Web.Services
{

    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ContentService : System.Web.Services.WebService
    {
        private const string NonImagePreset = "/sitecore modules/Web/ExperienceExplorer/Assets/images/no-image.jpg";

        /// <summary>
        /// Get Content
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public SerializableDictionary<string, object> GetContent(string contlrolId, string itemId, string siteName)
        {
            var contentResult = new SerializableDictionary<string, object>();
            itemId = Guid.Parse(itemId).ToString().ToUpper();

            Database masterDb = Sitecore.Configuration.Factory.GetDatabase(Consts.Database.Master);
            Item currentItem = masterDb.GetItem(ID.Parse(contlrolId));
            var method = currentItem.GetFieldAsString(IDs.Templates.ExperienceExplorer.Control.Fields.Method);

            switch (method)
            {
                case "GetEditorExperiencePresets":
                    contentResult = GetEditorExperiencePresets(siteName);
                    break;
                case "GetEditorExperienceMode":
                    contentResult = GetEditorExperienceMode();
                    break;
                case "GetEditorOnsiteBehaviourProfiles":
                    contentResult = GetEditorOnsiteBehaviourProfiles();
                    break;
                case "GetEditorOnsiteBehaviourGoals":
                    contentResult = GetEditorOnsiteBehaviourGoals();
                    break;
                case "GetEditorOnsiteBehaviourPageEvents":
                    contentResult = GetEditorOnsiteBehaviourPageEvents();
                    break;
                case "GetEditorVisitorInformationDevice":
                    contentResult = GetEditorVisitorInformationDevice(itemId);
                    break;
                case "GetEditorVisitorInformationGeoIp":
                    contentResult = GetEditorVisitorInformationGeoIp();
                    break;
                case "GetEditorReferralInformationCampaigns":
                    contentResult = GetEditorReferralInformationCampaigns();
                    break;
                case "GetEditorReferralInformationReferral":
                    contentResult = GetEditorReferralInformationReferral();
                    break;
                case "GetViewerExperiencePresets":
                    contentResult = GetViewerExperiencePresets();
                    break;
                case "GetViewerExperienceMode":
                    contentResult = GetViewerExperienceMode();
                    break;
                case "GetViewerOnsiteBehaviourProfiles":
                    contentResult = GetViewerOnsiteBehaviourProfiles();
                    break;
                case "GetViewerOnsiteBehaviourGoals":
                    contentResult = GetViewerOnsiteBehaviourGoals();
                    break;
                case "GetViewerOnsiteBehaviourPageEvents":
                    contentResult = GetViewerOnsiteBehaviourPageEvents();
                    break;
                case "GetViewerVisitorInformationDevice":
                    contentResult = GetViewerVisitorInformationDevice();
                    break;
                case "GetViewerVisitorInformationGeoIp":
                    contentResult = GetViewerVisitorInformationGeoIp();
                    break;
                case "GetViewerVisitorInformationTags":
                    contentResult = GetViewerVisitorInformationTags();
                    break;
                case "GetViewerReferralInformationCampaigns":
                    contentResult = GetViewerReferralInformationCampaigns();
                    break;
                case "GetViewerReferralInformationReferral":
                    contentResult = GetViewerReferralInformationReferral();
                    break;
                case "GetViewerPageConfigurationRules":
                    contentResult = GetViewerPageConfigurationRules(itemId);
                    break;
            }

            return contentResult;
        }

        #region Editor methods

        /// <summary>
        /// Get Editor Experience Presets
        /// </summary>
        /// <returns></returns>
        public SerializableDictionary<string, object> GetEditorExperiencePresets(string sitename)
        {
            var contentResult = new SerializableDictionary<string, object>();
            var lstItems = new List<object>();
            const string DefaultIcon = "/sitecore modules/Web/ExperienceExplorer/Assets/images/default.jpg";

            var model = ModuleManager.Model;
            var presetRootItem = PresetsHelper.GetPresetRootItem(sitename);
            if (presetRootItem != null)
            {
                var presets = presetRootItem.GetChildren(ChildListOptions.None).
                  Where(i => i.TemplateID == IDs.Templates.ExperienceExplorer.Preset.TemplateId || i.TemplateID == IDs.Templates.ExperienceExplorer.PresetPersona.TemplateId);

                lstItems.Add(new
                {
                    ID = "",
                    Name = Translate.Text("Default"),
                    Image = DefaultIcon,
                    Details = "",
                    Selected = false
                });

                if (presets != null)
                {
                    foreach (Item preset in presets)
                    {
                        var presetImage = !String.IsNullOrEmpty(preset.GetFieldAsMediaSource(IDs.Templates.ExperienceExplorer.Preset.Fields.Image)) ? preset.GetFieldAsMediaSource(IDs.Templates.ExperienceExplorer.Preset.Fields.Image) : NonImagePreset;

                        if (model.Preset != null && model.Preset.ID.ToString() == preset.ID.ToString())
                            lstItems.Add(new
                            {
                                ID = preset.ID.ToString(),
                                Name = preset.GetFieldAsString(IDs.Templates.ExperienceExplorer.Preset.Fields.Name),
                                Details = preset.GetFieldAsString(IDs.Templates.ExperienceExplorer.Preset.Fields.Details),
                                Image = presetImage,
                                Selected = true
                            });
                        else
                            lstItems.Add(new
                            {
                                ID = preset.ID.ToString(),
                                Name = preset.GetFieldAsString(IDs.Templates.ExperienceExplorer.Preset.Fields.Name),
                                Image = presetImage,
                                Details = preset.GetFieldAsString(IDs.Templates.ExperienceExplorer.Preset.Fields.Details),
                                Selected = false
                            });
                    }
                }
            }
            contentResult.Add("Source", lstItems);
            contentResult.Add("Title", Translate.Text("PresetEditorTitle"));
            contentResult.Add("QuestionMark", Translate.Text("PresetEditorQuestionMark"));

            return contentResult;
        }

        /// <summary>
        /// Get Editor Experience Mode
        /// </summary>
        /// <returns></returns>
        public SerializableDictionary<string, object> GetEditorExperienceMode()
        {
            var contentResult = new SerializableDictionary<string, object>();
            var model = ModuleManager.Model;
            bool Checked = model == null || model.JourneyMode == Journey.Mode.Journey;
            var lstModes = new List<Object>
                  {
                    new
                      {
                        Mode = Journey.Mode.Journey.ToString(),
                        Checked = Checked
                      },
                      new
                      {
                        Mode = Journey.Mode.Fixed.ToString(),
                        Checked = !Checked
                      }
                  };
            contentResult.Add("Source", lstModes);
            contentResult.Add("Title", Translate.Text("ModeEditorTitle"));
            contentResult.Add("Description", Translate.Text("ModeEditorDescription"));
            contentResult.Add("QuestionMark", Translate.Text("ModeEditorQuestionMark"));
            return contentResult;
        }

        /// <summary>
        /// Get Editor OnsiteBehaviour Profiles
        /// </summary>
        /// <returns></returns>
        public SerializableDictionary<string, object> GetEditorOnsiteBehaviourProfiles()
        {
            ModuleManager.Model.DoNotRestoreProfile = true;
            var contentResult = new SerializableDictionary<string, object>();
            var profiles = AbstractFactory.Create<ProfileDtos, ProfileStrategy>(new ProfileTrackerStrategy());
            contentResult.Add("Source", profiles);
            contentResult.Add("Title", Translate.Text("ProfileEditorTitle"));
            contentResult.Add("Description", Translate.Text("ProfileEditorDescription"));
            contentResult.Add("QuestionMark", Translate.Text("ProfileEditorQuestionMark"));
            return contentResult;
        }


        /// <summary>
        /// Get Editor OnsiteBehaviour Goals
        /// </summary>
        /// <returns></returns>
        public SerializableDictionary<string, object> GetEditorOnsiteBehaviourGoals()
        {
            ModuleManager.Model.DoNotRestoreGoals = true;
            var contentResult = new SerializableDictionary<string, object>();
            var goals = AbstractFactory.Create<GoalDtos, GoalStrategy>(new GoalTrackerStrategy());
            contentResult.Add("Source", goals);
            contentResult.Add("Title", Translate.Text("GoalEditorTitle"));
            contentResult.Add("Description", Translate.Text("GoalEditorDescription"));
            contentResult.Add("QuestionMark", Translate.Text("GoalEditorQuestionMark"));
            return contentResult;
        }

        /// <summary>
        /// Get Editor OnsiteBehaviour PageEvents
        /// </summary>
        /// <returns></returns>
        public SerializableDictionary<string, object> GetEditorOnsiteBehaviourPageEvents()
        {
            ModuleManager.Model.DoNotRestorePageEvents = true;
            var contentResult = new SerializableDictionary<string, object>();
            var pageEvents = AbstractFactory.Create<PageEventDtos, PageEventStrategy>(new PageEventTrackerStrategy());
            contentResult.Add("Source", pageEvents);
            contentResult.Add("Title", Translate.Text("PageEventEditorTitle"));
            contentResult.Add("Description", Translate.Text("PageEventEditorDescription"));
            contentResult.Add("QuestionMark", Translate.Text("PageEventEditorQuestionMark"));
            return contentResult;
        }

        /// <summary>
        /// Get Editor Visitor Information Device
        /// </summary>
        /// <returns></returns>
        public SerializableDictionary<string, object> GetEditorVisitorInformationDevice(string itemId)
        {
            ModuleManager.Model.DoNotRestoreDevice = true;
            Database masterDb = Sitecore.Configuration.Factory.GetDatabase(Consts.Database.Master);
            Item currentItem = masterDb.GetItem(ID.Parse(itemId));
            return GetEditorVisitorInformationDevice(currentItem);
        }

        private SerializableDictionary<string, object> GetEditorVisitorInformationDevice(Item item)
        {
            ModuleManager.Model.DoNotRestoreDevice = true;
            var contentResult = new SerializableDictionary<string, object>();
            var lstItems = new List<object>();
            var deviceItemId = QueryStringHelper.GetContextDeviceId;
            var model = ModuleManager.Model;
            if (model != null && model.DeviceDtos != null)
            {
                var selectedDevice = model.DeviceDtos.Find(
                     d => d.Selected
                     );
                if (selectedDevice != null && !string.IsNullOrEmpty(selectedDevice.ItemId))
                {
                    deviceItemId = selectedDevice.ItemId;
                }
            }
            DeviceItem deviceItem = !string.IsNullOrEmpty(deviceItemId) && deviceItemId.IsGuid()
              ? Sitecore.Data.Database.GetDatabase(Consts.Database.Master).Resources.Devices[deviceItemId]
              : Sitecore.Context.Device;
            foreach (var device in Sitecore.Context.Database.Resources.Devices.GetAll())
            {
                var icon = IconHelpers.GetIconPath(device.InnerItem, ImageDimension.id16x16);

                if ((item.Template.StandardValues["__Renderings"].IndexOf(device.ID.ToString()) != -1) || (item.Fields["__Renderings"].Value.IndexOf(device.ID.ToString()) != -1))
                {
                    if (deviceItem.InnerItem.ID.ToString() == device.InnerItem.ID.ToString())
                    {
                        lstItems.Add(new
                        {
                            ID = device.InnerItem.ID.ToString(),
                            Name = device.InnerItem.Name,
                            Selected = true,
                            Icon = icon
                        });
                    }
                    else
                    {
                        lstItems.Add(new
                        {
                            ID = device.InnerItem.ID.ToString(),
                            Name = device.InnerItem.Name,
                            Selected = false,
                            Icon = icon
                        });
                    }
                }
            }
            contentResult.Add("Source", lstItems);
            contentResult.Add("Title", Translate.Text("DeviceEditorTitle"));
            contentResult.Add("QuestionMark", Translate.Text("DeviceEditorQuestionMark"));
            return contentResult;
        }

        /// <summary>
        /// Get Editor Visitor Information Geo
        /// </summary>
        /// <returns></returns>
        public SerializableDictionary<string, object> GetEditorVisitorInformationGeoIp()
        {
            ModuleManager.Model.DoNotRestoreMap = true;
            var contentResult = new SerializableDictionary<string, object>();
            var geoIpDto = AbstractFactory.Create<GeoIpDto, GeoIpStrategy>(new GeoIpTrackerStrategy());
            contentResult.Add("Source", geoIpDto);
            Item[] items = Sitecore.Context.Database.SelectItems(Paths.Local.Countries.AbsolutePath);
            var lstCountries = new List<object>();
            foreach (var item in items.OrderBy(t => t.Name))
            {
                var countryCode = item.Fields[IDs.Templates.Systems.Analytics.Countries.Fields.CountryCode];
                lstCountries.Add(
                  geoIpDto.Country.Equals(countryCode.Value)
                    ? new { item.Name, countryCode.Value, Selected = true }
                    : new { item.Name, countryCode.Value, Selected = false });
            }

            var mapProvider = GeoIpHelpers.GetGeoIpProvider();

            contentResult.Add("MapProvider", mapProvider);
            contentResult.Add("Countries", lstCountries);
            contentResult.Add("Title", Translate.Text("GeoEditorTitle"));
            contentResult.Add("QuestionMark", Translate.Text("GeoEditorQuestionMark"));
            contentResult.Add("GeoTestMaxmind", Translate.Text("GeoTestMaxmind"));
            contentResult.Add("Ip", Translate.Text("Ip"));
            contentResult.Add("Country", Translate.Text("Country"));
            contentResult.Add("Map", Translate.Text("Map"));
            contentResult.Add("Country Code", Translate.Text("Country Code"));
            contentResult.Add("Area Code", Translate.Text("Area Code"));
            contentResult.Add("City", Translate.Text("City"));
            contentResult.Add("Postal Code", Translate.Text("Postal Code"));
            contentResult.Add("Business Name", Translate.Text("Business Name"));
            contentResult.Add("Metro Code", Translate.Text("Metro Code"));
            contentResult.Add("ISP", Translate.Text("ISP"));
            contentResult.Add("Latitude", Translate.Text("Latitude"));
            contentResult.Add("Longtitude", Translate.Text("Longtitude"));
            return contentResult;
        }

        /// <summary>
        /// Get Editor Referral Information Campaigns
        /// </summary>
        /// <returns></returns>
        public SerializableDictionary<string, object> GetEditorReferralInformationCampaigns()
        {
            ModuleManager.Model.DoNotRestoreCampaigns = true;
            var contentResult = new SerializableDictionary<string, object>();
            var campaigns = AbstractFactory.Create<CampaignDtos, CampaignStrategy>(new CampaignTrackerStrategy());
            contentResult.Add("Source", campaigns);
            contentResult.Add("Title", Translate.Text("CampaignsEditorTitle"));
            contentResult.Add("Description", Translate.Text("CampaignsEditorDescription"));
            contentResult.Add("QuestionMark", Translate.Text("CampaignsEditorQuestionMark"));
            return contentResult;
        }

        /// <summary>
        /// Get Editor Referral Information Referral
        /// </summary>
        /// <returns></returns>
        public SerializableDictionary<string, object> GetEditorReferralInformationReferral()
        {
            ModuleManager.Model.DoNotRestoreReferer = true;
            var contentResult = new SerializableDictionary<string, object>();
            var referral = AbstractFactory.Create<ReferralDto, ReferralTrackerStrategy>(new ReferralTrackerStrategy());
            contentResult.Add("Source", referral.Referrer);
            contentResult.Add("Title", Translate.Text("ReferralEditorTitle"));
            contentResult.Add("Referral", Translate.Text("ReferralEditorReferrer"));
            contentResult.Add("QuestionMark", Translate.Text("ReferralEditorQuestionMark"));
            return contentResult;
        }

        #endregion

        #region Viewer methods

        /// <summary>
        /// Get Viewer Experience Presets
        /// </summary>
        /// <returns></returns>
        public SerializableDictionary<string, object> GetViewerExperiencePresets()
        {
            var contentResult = new SerializableDictionary<string, object>();
            var presetDetails = new object();
            var model = ModuleManager.Model;
            if (!String.IsNullOrEmpty(model.PresetId) && model.PresetId.IsGuid())
            {
                var presetId = new ID(model.PresetId);
                var preset = presetId.GetPreset();

                if (preset != null)
                {
                    var presetImage = !String.IsNullOrEmpty(preset.GetFieldAsMediaSource(IDs.Templates.ExperienceExplorer.Preset.Fields.Image)) ? preset.GetFieldAsMediaSource(IDs.Templates.ExperienceExplorer.Preset.Fields.Image) : NonImagePreset;

                    presetDetails = new
                                      {
                                          ImageUrl = presetImage,
                                          Name = preset.GetFieldAsString(IDs.Templates.ExperienceExplorer.Preset.Fields.Name),
                                          Details = preset.GetFieldAsString(IDs.Templates.ExperienceExplorer.Preset.Fields.Details),
                                          Description = preset.GetFieldAsString(IDs.Templates.ExperienceExplorer.Preset.Fields.Description),
                                          Age = preset.GetFieldAsString(IDs.Templates.ExperienceExplorer.PresetPersona.Fields.Age),
                                          DayOfMyLife = preset.GetFieldAsString(IDs.Templates.ExperienceExplorer.PresetPersona.Fields.DayOfMyLife),
                                          Education = preset.GetFieldAsString(IDs.Templates.ExperienceExplorer.PresetPersona.Fields.Education),
                                          Environment = preset.GetFieldAsString(IDs.Templates.ExperienceExplorer.PresetPersona.Fields.Environment),
                                          Family = preset.GetFieldAsString(IDs.Templates.ExperienceExplorer.PresetPersona.Fields.Family),
                                          Interests = preset.GetFieldAsString(IDs.Templates.ExperienceExplorer.PresetPersona.Fields.Interests),
                                          Organization = preset.GetFieldAsString(IDs.Templates.ExperienceExplorer.PresetPersona.Fields.Organization),
                                          Psychographics = preset.GetFieldAsString(IDs.Templates.ExperienceExplorer.PresetPersona.Fields.Psychographics),
                                          Responsible = preset.GetFieldAsString(IDs.Templates.ExperienceExplorer.PresetPersona.Fields.Responsible)
                                      };
                }
            }
            contentResult.Add("Source", presetDetails);
            contentResult.Add("Title", Translate.Text("PresetViewerTitle"));
            contentResult.Add("QuestionMark", Translate.Text("PresetViewerQuestionMark"));
            contentResult.Add("DescriptionLabel", Translate.Text("PresetViewerDescriptionLabel"));
            contentResult.Add("PresetViewerAge", Translate.Text("PresetViewerAge"));
            contentResult.Add("PresetViewerDayOfMyLife", Translate.Text("PresetViewerDayOfMyLife"));
            contentResult.Add("PresetViewerEducation", Translate.Text("PresetViewerEducation"));
            contentResult.Add("PresetViewerEnvironment", Translate.Text("PresetViewerEnvironment"));
            contentResult.Add("PresetViewerFamily", Translate.Text("PresetViewerFamily"));
            contentResult.Add("PresetViewerGoal", Translate.Text("PresetViewerGoal"));
            contentResult.Add("PresetViewerInterests", Translate.Text("PresetViewerInterests"));
            contentResult.Add("PresetViewerOrganization", Translate.Text("PresetViewerOrganization"));
            contentResult.Add("PresetViewerPsychographics", Translate.Text("PresetViewerPsychographics"));
            contentResult.Add("PresetViewerResponsible", Translate.Text("PresetViewerResponsible"));
            contentResult.Add("PresetViewerNoResults", Translate.Text("PresetViewerNoResults"));
            return contentResult;
        }

        /// <summary>
        /// Get Viewer Experience Mode
        /// </summary>
        /// <returns></returns>
        public SerializableDictionary<string, object> GetViewerExperienceMode()
        {
            var contentResult = new SerializableDictionary<string, object>();
            var model = ModuleManager.Model;
            var mode = string.Empty;
            if (model != null)
                mode = model.JourneyMode.ToString();
            contentResult.Add("Source", mode);
            contentResult.Add("Title", Translate.Text("ModeViewerTitle"));
            contentResult.Add("Description", Translate.Text("ModeViewerDescription"));
            contentResult.Add("ModeViewerMode", Translate.Text("ModeViewerMode"));
            return contentResult;
        }

        /// <summary>
        /// Get Viewer OnsiteBehaviour Profiles
        /// </summary>
        /// <returns></returns>
        public SerializableDictionary<string, object> GetViewerOnsiteBehaviourProfiles()
        {
            var contentResult = new SerializableDictionary<string, object>();
            var profiles = AbstractFactory.Create<ProfileDtos, ProfileStrategy>(new ProfileTrackerStrategy());
            var profileDtos = profiles.Where(profileDto => profileDto.ProfileKeyDtos.Any(profileKeyDto => profileKeyDto.Value != 0)).ToList();
            contentResult.Add("Source", profileDtos);
            contentResult.Add("Title", Translate.Text("ProfileViewerTitle"));
            contentResult.Add("Description", Translate.Text("ProfileViewerDescription"));
            contentResult.Add("QuestionMark", Translate.Text("ProfileViewerQuestionMark"));
            contentResult.Add("NoResults", Translate.Text("ProfileViewerNoResults"));
            contentResult.Add("PatternMatchTitle", Translate.Text("ProfileViewerPatternMatchTitle"));
            contentResult.Add("NoPatternCards", Translate.Text("No pattern cards in this category to match"));
            return contentResult;
        }

        /// <summary>
        /// Get Viewer OnsiteBehaviour Profiles
        /// </summary>
        /// <returns></returns>
        public SerializableDictionary<string, object> GetViewerOnsiteBehaviourGoals()
        {
            var contentResult = new SerializableDictionary<string, object>();
            var goals = AbstractFactory.Create<GoalDtos, GoalStrategy>(new GoalTrackerStrategy());
            IEnumerable<GoalDto> selectedGoals = null;
            if (goals != null)
            {
                selectedGoals = goals.Where(g => g.Selected);
            }
            contentResult.Add("Source", selectedGoals);
            contentResult.Add("Title", Translate.Text("GoalViewerTitle"));
            contentResult.Add("GoalNoGoalsTriggered", Translate.Text("GoalNoGoalsTriggered"));
            contentResult.Add("QuestionMark", Translate.Text("GoalViewerQuestionMark"));
            return contentResult;
        }

        /// <summary>
        /// Get Viewer OnsiteBehaviour PageEvents
        /// </summary>
        /// <returns></returns>
        public SerializableDictionary<string, object> GetViewerOnsiteBehaviourPageEvents()
        {
            var contentResult = new SerializableDictionary<string, object>();
            var pageEvents = AbstractFactory.Create<PageEventDtos, PageEventStrategy>(new PageEventTrackerStrategy());
            IEnumerable<PageEventDto> selectedPageEvents = null;
            if (pageEvents != null)
            {
                selectedPageEvents = pageEvents.Where(g => g.Selected);
            }
            contentResult.Add("Source", selectedPageEvents);
            contentResult.Add("Title", Translate.Text("PageEventViewerTitle"));
            contentResult.Add("NoPageEventsTriggered", Translate.Text("NoPageEventsTriggered"));
            contentResult.Add("QuestionMark", Translate.Text("PageEventViewerQuestionMark"));
            return contentResult;
        }
        /// <summary>
        /// Get Viewer Visitor Information Device
        /// </summary>
        /// <returns></returns>
        public SerializableDictionary<string, object> GetViewerVisitorInformationDevice()
        {
            var contentResult = new SerializableDictionary<string, object>();
            var deviceDetails = new object();
            var model = ModuleManager.Model;
            var devices = model.DeviceDtos;
            var deviceItemId = string.Empty;
            if (devices != null)
            {
                var selectedDevice = devices.Find(d => d.Selected);
                var selectedDeviceItem =
                  Sitecore.Context.Database.Resources.Devices.GetAll().FirstOrDefault(t => t.DisplayName == "Default");
                if (selectedDeviceItem != null)
                    deviceItemId = selectedDeviceItem.ID.ToString();

                if (selectedDevice != null && selectedDevice.ItemId != null)
                    deviceItemId = selectedDevice.ItemId;
            }
            else
            {
                var selectedDevice =
                  Sitecore.Context.Database.Resources.Devices.GetAll().FirstOrDefault(t => t.DisplayName == "Default");
                if (selectedDevice != null)
                    deviceItemId = selectedDevice.ID.ToString();
            }

            if (!string.IsNullOrEmpty(deviceItemId))
            {
                var device = new DeviceItem(Sitecore.Context.Database.GetItem(deviceItemId));
                deviceDetails = new
                                  {
                                      ImageUrl = IconHelpers.GetIconPath(device.InnerItem, ImageDimension.id48x48),
                                      DeviceName = device.Name
                                  };
            }

            contentResult.Add("Source", deviceDetails);
            contentResult.Add("Title", Translate.Text("DeviceViewerTitle"));
            contentResult.Add("QuestionMark", Translate.Text("DeviceViewerQuestionMark"));
            contentResult.Add("NoResults", Translate.Text("DeviceNoResults"));
            return contentResult;
        }

        /// <summary>
        /// Get Viewer Visitor Information Geo
        /// </summary>
        /// <returns></returns>
        public SerializableDictionary<string, object> GetViewerVisitorInformationGeoIp()
        {
            var contentResult = new SerializableDictionary<string, object>();
            var geoIpDto = AbstractFactory.Create<GeoIpDto, GeoIpStrategy>(new GeoIpTrackerStrategy());
            var mapProvider = GeoIpHelpers.GetGeoIpProvider();

            contentResult.Add("MapProvider", mapProvider);
            contentResult.Add("Source", geoIpDto);
            contentResult.Add("Title", Translate.Text("GeoViewerTitle"));
            contentResult.Add("QuestionMark", Translate.Text("GeoViewerQuestionMark"));
            contentResult.Add("Ip", Translate.Text("Ip"));
            contentResult.Add("CountryCode", Translate.Text("Country Code"));
            contentResult.Add("AreaCode", Translate.Text("Area Code"));
            contentResult.Add("City", Translate.Text("City"));
            contentResult.Add("PostalCode", Translate.Text("Postal Code"));
            contentResult.Add("BusinessName", Translate.Text("Business Name"));
            contentResult.Add("MetroCode", Translate.Text("Metro Code"));
            contentResult.Add("ISP", Translate.Text("ISP"));
            contentResult.Add("Latitude", Translate.Text("Latitude"));
            contentResult.Add("Longtitude", Translate.Text("Longtitude"));
            contentResult.Add("isMapTabNull", ExperienceExplorerModel.IsMapTabNull);
            contentResult.Add("isCountryTabNull", ExperienceExplorerModel.IsCountryTabNull);
            contentResult.Add("isIpTabNull", ExperienceExplorerModel.IsIpTabNull);
            return contentResult;
        }

        /// <summary>
        /// Get Viewer Visitor Information Tags
        /// </summary>
        /// <returns></returns>
        public SerializableDictionary<string, object> GetViewerVisitorInformationTags()
        {
            var contentResult = new SerializableDictionary<string, object>();
            var tags = AbstractFactory.Create<TagDtos, TagStrategy>(new TagTrackerStrategy());
            contentResult.Add("Source", tags);
            contentResult.Add("Title", Translate.Text("TagsViewerTitle"));
            contentResult.Add("QuestionMark", Translate.Text("TagsViewerQuestionMark"));
            contentResult.Add("NoResults", Translate.Text("TagsViewerNoResults"));
            return contentResult;
        }

        /// <summary>
        /// Get Viewer Referral Information Campaigns
        /// </summary>
        /// <returns></returns>
        public SerializableDictionary<string, object> GetViewerReferralInformationCampaigns()
        {
            var contentResult = new SerializableDictionary<string, object>();
            var campaigns = AbstractFactory.Create<CampaignDtos, CampaignStrategy>(new CampaignTrackerStrategy());
            List<CampaignDto> selectedCampaigns = null;
            if (campaigns != null)
            {
                selectedCampaigns = campaigns.Where(g => g.Selected).ToList();
            }
            contentResult.Add("Source", selectedCampaigns);
            contentResult.Add("Title", Translate.Text("CampaignsViewerTitle"));
            contentResult.Add("QuestionMark", Translate.Text("CampaignsViewerQuestionMark"));
            contentResult.Add("NoResults", Translate.Text("CampaignsViewerNoResults"));
            return contentResult;
        }

        /// <summary>
        /// Get v Referral Information Referral
        /// </summary>
        /// <returns></returns>
        public SerializableDictionary<string, object> GetViewerReferralInformationReferral()
        {
            var contentResult = new SerializableDictionary<string, object>();
            var referral = AbstractFactory.Create<ReferralDto, ReferralTrackerStrategy>(new ReferralTrackerStrategy());
            contentResult.Add("Source", referral);
            contentResult.Add("Title", Translate.Text("ReferralViewerTitle"));
            contentResult.Add("QuestionMark", Translate.Text("ReferralViewerQuestionMark"));
            contentResult.Add("ReferralViewerReferrer", Translate.Text("ReferralViewerReferrer"));
            contentResult.Add("ReferralViewerSiteHost", Translate.Text("ReferralViewerSiteHost"));
            contentResult.Add("ReferralViewerKeywords", Translate.Text("ReferralViewerKeywords"));
            contentResult.Add("NoResults", Translate.Text("ReferralViewerNoResults"));
            return contentResult;
        }

        /// <summary>
        /// Get v Referral Information Referral
        /// </summary>
        /// <returns></returns>

        public SerializableDictionary<string, object> GetViewerPageConfigurationRules(string itemId)
        {
            Database masterDb = Sitecore.Configuration.Factory.GetDatabase(Consts.Database.Master);
            Item currentItem = masterDb.GetItem(ID.Parse(itemId));
            return GetViewerPageConfigurationRules(currentItem);
        }
        public SerializableDictionary<string, object> GetViewerPageConfigurationRules(Item item)
        {
            var contentResult = new SerializableDictionary<string, object>();
            var rules = AbstractFactory.Create<RenderingsDtos, RuleStrategy>(new RuleStrategy(item));
            contentResult.Add("Source", rules.SetRuleType());
            contentResult.Add("Title", Translate.Text("RulesViewerTitle"));
            contentResult.Add("QuestionMark", Translate.Text("RulesViewerQuestionMark"));
            return contentResult;
        }

        #endregion
    }
}
