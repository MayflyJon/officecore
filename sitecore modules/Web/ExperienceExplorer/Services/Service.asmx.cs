﻿using System;
using System.Net;
using System.Web.Http;
using System.Web.Services;
using Sitecore.Configuration;
using Sitecore.Diagnostics;

using Sitecore.ExperienceExplorer.Business.Helpers;
using Sitecore.ExperienceExplorer.Business.Managers;
using Sitecore.ExperienceExplorer.Business.Models;

namespace Sitecore.ExperienceExplorer.Web.Services
{
    using Sitecore.Sites;

    /// <summary>
    /// Summary description for Update
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Service : System.Web.Services.WebService
    {
        #region Private Helpers
        /// <summary>
        /// If the user has changed the preset, lets use all values from the 
        /// preset and discard all other selected settings.
        /// </summary>
        private bool PresetHasChanged(ExperienceExplorerJsonModel modelDto)
        {
            return
                (ModuleManager.Model.Preset != null && ModuleManager.Model.Preset.ID.ToString() != modelDto.PresetId) ||
                (ModuleManager.Model.Preset == null && !string.IsNullOrEmpty(modelDto.PresetId)) ||
                (modelDto.ResetPreset && !string.IsNullOrEmpty(modelDto.PresetId));
        }
        #endregion

        [WebMethod(EnableSession = true)]
        public GeoIpTest GeoIpTest()
        {
            string ip = SettingsHelper.GeoTestingIp;
            return GeoIpHelpers.TestGeoIp(ip);
        }

        [WebMethod(EnableSession = true)]
        public bool Test(TestObj test)
        {
            return true;
        }

        [WebMethod(EnableSession = true)]
        public bool Update(ExperienceExplorerJsonModel modelDto)
        {
            bool result = true;

            try
            {
                if (modelDto == null)
                {
                    Log.Warn("ExperienceExplorer Update: Model is invalid or null", this);
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }

                //If the user has changed the preset, lets use all values from the preset and discard all other selected settings.
                if (PresetHasChanged(modelDto))
                {
                    modelDto = new ExperienceExplorerJsonModel { PresetId = modelDto.PresetId };
                }

                ModuleManager.Model = ExperienceExplorerModel.GetExperienceExplorerModel(modelDto);

                //Invalidate current visitor
                PageModeHelper.ResetAnalyticsCookie();
            }
            catch (Exception exp)
            {
                Log.Error("Service Update ExperienceExplorerJsonModel Failed", exp);
                result = false;
            }

            return result;
        }
    }

    public class TestObj
    {
        public string Name { get; set; }
    }
}
