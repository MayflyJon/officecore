﻿<%@ Control Language="C#" Inherits="System.Web.UI.UserControl" %>
<%@ Import Namespace="Sitecore.ExperienceExplorer.Business.Constants" %>
<%@ Import Namespace="Sitecore.ExperienceExplorer.Business.Helpers" %>

<link rel="stylesheet" type="text/css" href="/sitecore modules/Web/ExperienceExplorer/Assets/css/experience-explorer.css" />

<div class="experience-explorer">
    <div class="panel editor">
        <button type="button" class="page-editor-link">Page Editor</button>
        <a class="trigger" href="#"></a>
        <iframe scrolling="no" src="<%= Paths.Module.Controls.RelativePath %>ExpEditor.aspx?<%=SettingsHelper.AddOnQueryStringKey%>=1&<%=SettingsHelper.ContextItemIdQueryStringKey %>=<%=Sitecore.Context.Item.ID.ToString() %>&<%=SettingsHelper.ContextDeviceIdQueryStringKey %>=<%=Sitecore.Context.Device.ID.ToString() %>&<%=SettingsHelper.CurrentWebsiteStringKey%>=<%=Sitecore.Context.Site.Name %>" id="IframeExperienceExplorerEditor" class="ee-iframe"></iframe>
    </div>
    <div class="panel viewer">
        <a class="trigger" href="#"></a>
        <iframe scrolling="no" src="<%= Paths.Module.Controls.RelativePath %>ExpViewer.aspx?<%=SettingsHelper.AddOnQueryStringKey%>=1&<%=SettingsHelper.ContextItemIdQueryStringKey %>=<%=Sitecore.Context.Item.ID.ToString() %>&<%=SettingsHelper.ContextDeviceIdQueryStringKey %>=<%=Sitecore.Context.Device.ID.ToString() %>&<%=SettingsHelper.CurrentWebsiteStringKey%>=<%=Sitecore.Context.Site.Name %>" id="IframeExperienceExplorerViewer" class="ee-iframe"></iframe>
    </div>
</div>

<script src="/sitecore modules/Web/ExperienceExplorer/Assets/experience-explorer.min.js"></script>
