﻿using System;
using System.Globalization;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore.Analytics;
using Sitecore.Data.Items;
using Sitecore.ExperienceExplorer.Business.Constants;
using Sitecore.ExperienceExplorer.Business.Entities;
using Sitecore.ExperienceExplorer.Business.Managers;
using Sitecore.Globalization;

namespace Sitecore.ExperienceExplorer.Web.Controls
{
  public partial class ExpViewer1 : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        ExperienceViewerViews.Text = (Tracker.CurrentVisit.VisitPageCount - 1).ToString(CultureInfo.InvariantCulture);
        ExperienceViewerValue.Text = Tracker.CurrentVisit.Value.ToString(CultureInfo.InvariantCulture);

        litViewer.SetText("Viewer");
        litViews.SetText("Views");
        litValue.SetText("Value");

        ModeInit();

        var viewerRootItem = Sitecore.Context.Database.GetItem(IDs.Controls.ViewerRootId);
        if (viewerRootItem != null)
        {
          rpAccordion.DataSource = viewerRootItem.GetChildren();
          rpAccordion.DataBind();
        }
      }
    }

    protected override void OnInit(System.EventArgs e)
    {
      Tracker.CurrentPage.Cancel();
      Sitecore.Context.Database = Sitecore.Data.Database.GetDatabase("master");
      base.OnInit(e);
    }

    protected void rpAccordion_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {

      var item = e.Item.DataItem as Item;
      if (item == null) return;

      if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
      {
        HtmlControl divAccordionBody = (HtmlControl)e.Item.FindControl("accordions_body");
        HtmlControl accordionLink = (HtmlControl)e.Item.FindControl("accordionLink");
        accordionLink.Attributes.Add("href", "#" + divAccordionBody.ClientID);

        Literal litAccordionName = (Literal)e.Item.FindControl("litAccordionName");
        litAccordionName.Text = item["Accordion Name"];

        if (item.HasChildren)
        {
          Repeater rpTabs = (Repeater)e.Item.FindControl("rpTabs");
          Repeater rpControls = (Repeater)e.Item.FindControl("rpControls");

          rpControls.DataSource = item.GetChildren();
          rpTabs.DataSource = item.GetChildren();

          rpControls.DataBind();
          rpTabs.DataBind();

        }
      }


    }

    protected void tabs_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      var item = e.Item.DataItem as Item;
      if (item == null) return;

      if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
      {
        if (e.Item.ItemIndex > 0)
        {
          HtmlControl liItem = (HtmlControl)e.Item.FindControl("liItem");
          liItem.Attributes.Remove("class");
        }

        Literal litTabName = (Literal)e.Item.FindControl("litTabName");
        litTabName.Text = item["Tab Name"];

        HtmlControl tabLink = (HtmlControl)e.Item.FindControl("tabLink");
        tabLink.Attributes.Add("href", "#" + item.ID.ToShortID());
      }
    }


    protected void rpControls_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      var item = e.Item.DataItem as Item;
      if (item == null) return;
    }

    protected void ModeInit()
    {
      var model = ModuleManager.Model;

      if (model == null || model.JourneyMode == Journey.Mode.Journey)
      {
        JournayLbl.Attributes["class"] = "icon-journey active";
        lblModeSelected.Text = Translate.Text("Journey Mode");
      }
      else
      {
        FixedLbl.Attributes["class"] = "icon-fixed active";
        lblModeSelected.Text = Translate.Text("Fixed Mode");
      }
    }
  }
}
