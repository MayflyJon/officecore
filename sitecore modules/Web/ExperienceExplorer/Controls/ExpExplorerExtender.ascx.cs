﻿using System;
using Sitecore.ExperienceExplorer.Business.Helpers;
using Sitecore.Web.UI;

namespace Sitecore.ExperienceExplorer.Web.Controls
{
    public partial class ExpExplorerExtender : System.Web.UI.UserControl
    {
        public string ItemId
        {
            get {return Sitecore.Context.Item.ID.ToString() ;}
        }

        public string DeviceItemId
        {
            get {return Sitecore.Context.Device.ID.ToString() ;}
        }

        
        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}