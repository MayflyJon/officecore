﻿using ComponentArt.Web.UI;
using Sitecore.Controls;
using Sitecore.Extensions;
using Sitecore.Security.Accounts;
using Sitecore.Web.UI.Grids;
using Sitecore.Web.UI.Sheer;
using System;

namespace Sitecore.ExperienceExplorer.Web.Dialogs
{
  public class SelectUserPage : DialogPage
  {
    /// <summary>
    /// Sitecore users
    /// </summary>
    protected Grid Users;


    protected override void OK_Click()
    {
      var selectedValue = GridUtil.GetSelectedValue("Users");
      if (selectedValue != null)
      {
        SheerResponse.SetDialogValue(selectedValue);
        base.OK_Click();
      }
    }

    protected override void OnLoad(EventArgs e)
    {
      base.OnLoad(e);
      if (!AjaxScriptManager.IsEvent)
      {
        Users.GroupingNotificationText = "";
        Users.SearchText = "";
      }
      var managedUsers = Sitecore.Context.User.Delegation.GetManagedUsers();
      ComponentArtGridHandler<User>.Manage(Users, new GridSource<User>(managedUsers), !AjaxScriptManager.IsEvent);
      Users.LocalizeGrid();
      Users.ClientEvents.ItemDoubleClick = new ClientEvent("onUsersDoubleClick");
    }
  }
}