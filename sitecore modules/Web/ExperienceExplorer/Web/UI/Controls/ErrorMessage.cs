﻿using System.Web.UI;

namespace Sitecore.ExperienceExplorer.Web.UI.Controls
{
    public class ErrorMessage : Control
    {
        public string Message { get; set; }

        public ErrorMessage()
        {
        }

        public ErrorMessage(string errorMessage)
        {
            this.Message = errorMessage;
        }

        protected override void Render(HtmlTextWriter writer)
        {
            writer.AddStyleAttribute(HtmlTextWriterStyle.BorderColor, "#F00");
            writer.AddStyleAttribute(HtmlTextWriterStyle.BorderStyle, "solid");
            writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "3px");
            writer.AddStyleAttribute(HtmlTextWriterStyle.BackgroundColor, "#FCC");
            writer.AddStyleAttribute(HtmlTextWriterStyle.Padding, "10px");
            writer.AddStyleAttribute(HtmlTextWriterStyle.Color, "#F00");

            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            base.Render(writer);
            writer.Write(this.Message);
            
            writer.RenderEndTag();
        }
    }
}