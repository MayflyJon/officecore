﻿using System.Web.UI;
using Sitecore.ExperienceExplorer.Web.UI.Controls;

namespace Sitecore.ExperienceExplorer.Web.UI
{
    public abstract class ExperienceUserControl : UserControl
    {
        private ErrorMessage ErrorMessage { get; set; }

        protected void DisplayErrorMessage(string message)
        {
            this.ErrorMessage = new ErrorMessage(message);
        }

        protected void ClearErrorMessage()
        {
            this.ErrorMessage = null;
        }

        protected override void Render(HtmlTextWriter htmlTextWriter)
        {
            #if DEBUG
            htmlTextWriter.WriteLine("<!-- " + this.ToString() + " START -->");
            htmlTextWriter.WriteLine("<!-- Class: '" + this.GetType().ToString() + "', Namespace: '" + this.GetType().Namespace + " -->");
            #endif

            if (this.ErrorMessage == null)
                base.Render(htmlTextWriter);
            else
                this.ErrorMessage.RenderControl(htmlTextWriter);

            #if DEBUG
            htmlTextWriter.WriteLine("<!-- " + this.ToString() + " END -->");
            #endif
        }
    }
}