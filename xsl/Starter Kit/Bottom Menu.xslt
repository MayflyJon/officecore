﻿<?xml version="1.0" encoding="UTF-8"?>

<!--=============================================================
    File: Bottom Menu.xslt                                                   
    Created by: sitecore\admin                                       
    Created: 11/22/2007 11:30:18 AM                                               
    Copyright notice at bottom of file
==============================================================-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sc="http://www.sitecore.net/sc"
  xmlns:dot="http://www.sitecore.net/dot"
  exclude-result-prefixes="dot sc">

  <!-- import required renderings and library functions -->
  <xsl:import href="Show Help Link.xslt"/>

  <!-- output directives -->
  <xsl:output method="html" indent="no" encoding="UTF-8" />

  <!-- parameters -->
  <xsl:param name="lang" select="'en'"/>
  <xsl:param name="id" select="''"/>
  <xsl:param name="sc_item"/>
  <xsl:param name="sc_currentitem"/>

  <!-- variables -->
  <xsl:variable name="home" select="$sc_item/ancestor-or-self::item[@template='site root']" />

  <xsl:variable name="this_section" select="$home/child::item[@template='site section']" />
  <!--<xsl:variable name="this_section" select="sc:GetItemsOfType('site section',$sc_item/ancestor-or-self::item)" />-->
  <xsl:variable name="common" select="/*/item[@key='content']/item[@key='settings']/item[@key='common text']" />
  <xsl:variable name="about" select="$common/item[@key='about link']" />

  <!-- entry point -->
  <xsl:template match="*">
    <xsl:apply-templates select="$sc_item" mode="main"/>
  </xsl:template>

  <!--==============================================================-->
  <!-- main                                                         -->
  <!--==============================================================-->
  <xsl:template match="*" mode="main">
    <div id="bottom-menu">
      <div class="grid">
        <div class="col-25">
          <h6>Sections</h6>
          <ul>
            <li>
              <sc:link select="$home">
                <sc:text field="menu title" select="$home"/>
              </sc:link>
            </li>
            <xsl:for-each select="sc:GetItemsOfType('site section',$home/item)">
              <li>
                <sc:link select="." title="{sc:fld('menu help',$home)}">
                  <sc:text field="menu title"/>
                </sc:link>
              </li>
            </xsl:for-each>
          </ul>
        </div>
        <div class="col-25">
          <h6>Follow Us</h6>
          <ul id="social-list">
            <li id="social-tw-link" class="twitter">
              <a target="_blank" href="http://twitter.com/"></a>
            </li>
            <li id="social-fb-link" class="facebook">
              <a target="_blank" href="http://facebook.com/"></a>
            </li>
            <li id="social-yt-link" class="youtube">
              <a target="_blank" href="http://youtube.com/"></a>
            </li>
            <li id="social-fl-link" class="flickr">
              <a target="_blank" href="http://flickr.com/"></a>
            </li>
            <li id="social-rs-link" class="rss">
              <a target="_blank" href="http://facebook.com/"></a>
            </li>
          </ul>
        </div>
        <div class="col-25">
          
        </div>
        <div class="col-25">z</div>
      </div>

    </div>
    <div class="clear"></div>
  </xsl:template>

</xsl:stylesheet>