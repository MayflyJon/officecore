﻿jQuery('.carousel-indicators li').each(function (index) {
    jQuery(this).attr('data-slide-to', index);
    if (index == 0) {
        jQuery(this).addClass('active');
    }
});

jQuery('#news-carousel .carousel-inner div').first().addClass('active');

jQuery('.scfForm').addClass('form-group');
jQuery('.scfForm input:text').each(function () {
    jQuery(this).addClass('form-control');
});
jQuery('.scfForm :button').each(function () {
    jQuery(this).addClass('btn btn-default');
});

// equalHeights plugin
(function ($) {

    $.fn.equalheights = function () {

        var currentTallest = 0, currentRowStart = 0, rowDivs = new Array(), $el, topPosition = 0;

        return this.each(function () {
            // Do something to each element here.
            $el = $(this);
            topPosition = $el.offset().top;

            if (currentRowStart != topPosition) {

                // we just came to a new row.  Set all the heights on the completed row
                for (var currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                    rowDivs[currentDiv].height(currentTallest);
                }

                // set the variables for the new row
                rowDivs.length = 0; // empty the array
                currentRowStart = topPosition;
                currentTallest = $el.height();
                rowDivs.push($el);

            } else {

                // another div on the current row.  Add it to the list and check if it's taller
                rowDivs.push($el);
                currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);

            }

            // do the last row
            for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
        });
    };
}(jQuery));

jQuery(document).ready(function () {
    var pageeditmode = jQuery('#hiddenPageEdit').val() === 'true';

    if (pageeditmode) {
        // In page edit mode carousel needs to be paused by default so we can edit the carousel images
        jQuery('#news-carousel').carousel({
            interval: false,
        });

        // Functions for next and prev have to be explicitly defined in page edit mode
        jQuery("#news-carousel .right").click(function () {
            jQuery('#news-carousel').carousel('next');
        });
        jQuery("#news-carousel .left").click(function () {
            jQuery('#news-carousel').carousel('prev');
        });
    }
    else {
        var i = jQuery('#hiddenInterval').val();
        var p = jQuery('#hiddenPause').val();
        var w = jQuery('#hiddenWrap').val() === 'true';

        jQuery('#news-carousel').carousel({
            interval: i,
            pause: p,
            wrap: w
        });
    }
});

imagesLoaded(document.querySelector('.desktop-search-results'), function (instance) {
    jQuery('.desktop-search-results .product-overview').equalheights();
});