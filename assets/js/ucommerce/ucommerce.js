﻿jQuery('.controls.rating input:radio').hide();
jQuery('.controls.rating')
    .on('mouseenter', 'label', function () {
        DisplayRating(jQuery(this));
    }
    )
    .on('mouseleave', function () {
        var $this = jQuery(this),
            $selectedRating = $this.find('input:checked');

        if ($selectedRating.length == 1) {
            DisplayRating($selectedRating.parent()); 
        } else {
            $this.children().removeClass('selected').removeClass('glyphicon-star').addClass('glyphicon-star-empty');
        };
    }        
);

var DisplayRating = function ($el) {
    $el.addClass('selected glyphicon glyphicon-star').removeClass('glyphicon-star-empty')
       .prevAll().addClass('selected glyphicon glyphicon-star').removeClass('glyphicon-star-empty');
    $el.nextAll().removeClass('selected glyphicon-star').addClass('glyphicon-star-empty');
};