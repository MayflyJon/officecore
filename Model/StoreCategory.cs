﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UCommerce.EntitiesV2;

namespace Officecore.Website.Model
{
    public class StoreCategory
    {
        public UCommerce.DemoStore.Controls.Image mediaImage { get; set; }
        public Category category { get; set; }
        public Product product { get; set; }
        public String url { get; set; }
        
    }
}