﻿<%@ control language="c#" autoeventwireup="true" %>

<sc:sublayout runat="server" path="/layouts/Starter Kit/Sublayouts/Breadcrumb.ascx" id="uxBreadcrumb" placeholder="content"></sc:sublayout>
<div class="row-fluid">
    <div class="col-md-3">
        <sc:placeholder runat="server" key="column-left" id="columnleft"></sc:placeholder>
    </div>
    <div class="col-md-6">
        <sc:placeholder runat="server" key="column-content" id="columncontent"></sc:placeholder>
    </div>
    <div class="col-md-3">
        <sc:placeholder runat="server" key="column-right" id="columnright"></sc:placeholder>
    </div>
</div>
<div class="clearfix"></div>
