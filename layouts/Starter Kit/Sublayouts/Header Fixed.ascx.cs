﻿using Officecore.Website.code.Service;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Officecore.Website.layouts.Starter_Kit.Sublayouts
{
    public partial class Header_Fixed : System.Web.UI.UserControl
    {
        ProductService p = new ProductService();
        public bool toggleState = true;

        protected void Page_Load(object sender, EventArgs e)
        {
            toggleState = p.ActivateCommerce();
        }
    }
}