﻿<div class="row-fluid">
    <div class="col-md-9">
        <sc:Placeholder runat="server" Key="content" ID="content"></sc:Placeholder>
    </div>
    <div class="col-md-3">
        <sc:Placeholder runat="server" Key="column-right-bottom" ID="columnrightbottom"></sc:Placeholder>
    </div>
</div>
<div class="clear"></div>
