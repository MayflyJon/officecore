﻿<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Control Language="c#" AutoEventWireup="true" %>
<sc:sublayout runat="server" renderingid="{2BF352A5-6413-45DD-ABD3-2787356F555C}"
    path="/layouts/Starter Kit/Sublayouts/Breadcrumb.ascx" id="uxBreadcrumb" placeholder="content"
    parameters="lang&amp;id&amp;sc_item&amp;sc_currentitem"></sc:sublayout>

<sc:Placeholder runat="server" Key="column-content" ID="columncontent" />
