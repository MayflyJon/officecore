using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Globalization;
using Sitecore.Publishing;
using System;
using Sitecore.Diagnostics.PerformanceCounters;
using Sitecore.Pipelines;
using Sitecore.SecurityModel;
using Sitecore.Workflows.Simple;
using UCommerce.Infrastructure;
using UCommerce.Search.Indexers;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Maintenance;

namespace Officecore.Website.layouts
{
    public partial class Publish : System.Web.UI.Page
    {
        private readonly Database webDb = Sitecore.Configuration.Factory.GetDatabase("web");
        private readonly Database masterDb = Sitecore.Configuration.Factory.GetDatabase("master");

        protected void Page_Load(object sender, EventArgs e)
        {
            Sitecore.Diagnostics.Log.Info("Starting Officecore publish", this);

            Sitecore.Security.Accounts.User user = Sitecore.Security.Accounts.User.FromName(@"sitecore\admin", false);
            using (new Sitecore.Security.Accounts.UserSwitcher(user))
            {
                DeployGoals();
                DeployCampaigns();
                Publisher("/sitecore");
                IndexRebuild("sitecore_master_index");
                IndexRebuild("sitecore_web_index");
                RebuildUcommerceIndexes();
            }

            Sitecore.Diagnostics.Log.Info("Officecore publish page over and out", this);
        }

        private void RebuildUcommerceIndexes()
        {
            Sitecore.Diagnostics.Log.Info("Officecore calling a Ucommerce index rebuild", this);
            ObjectFactory.Instance.Resolve<ScratchIndexer>().Index();
        }

        private void DeployCampaigns()
        {
            Item[] campaigns = masterDb.SelectItems("fast:/sitecore/system/Marketing Center/Campaigns//*[@@templateid='{56682FE6-9679-4B69-9589-60C99AA08BEC}' or @@templateid='{94FD1606-139E-46EE-86FF-BC5BF3C79804}']");
            foreach (Item campaign in campaigns)
            {
                Sitecore.Diagnostics.Log.Info("Deploying Campaign: " + campaign.Name, this);
                this.MoveToStateAndExecuteActions(campaign, new ID("{39156DC0-21C6-4F64-B641-31E85C8F5DFE}"));
                this.MoveToStateAndExecuteActions(campaign, new ID("{EDCBB550-BED3-490F-82B8-7B2F14CCD26E}"));
            }
        }

        private void DeployGoals()
        {
            Item[] goals = masterDb.SelectItems("fast:/sitecore/system/Marketing Center/Goals//*[@@templateid='{475E9026-333F-432D-A4DC-52E03B75CB6B}' or @@templateid='{059CFBDF-49FC-4F14-A4E5-B63E1E1AFB1E}']");
            foreach (Item goal in goals)
            {
                Sitecore.Diagnostics.Log.Info("Deploying Goal: " + goal.Name, this);
                this.MoveToStateAndExecuteActions(goal, new ID("{39156DC0-21C6-4F64-B641-31E85C8F5DFE}"));
                this.MoveToStateAndExecuteActions(goal, new ID("{EDCBB550-BED3-490F-82B8-7B2F14CCD26E}"));
            }
        }

        public void MoveToStateAndExecuteActions(Item item, ID workflowStateId)
        {
            Sitecore.Workflows.IWorkflowProvider workflowProvider = item.Database.WorkflowProvider;
            Sitecore.Workflows.IWorkflow workflow = workflowProvider.GetWorkflow(item);

            // if item is in any workflow
            if (workflow != null)
            {
                using (new EditContext(item))
                {
                    // update item's state to the new one
                    item[Sitecore.FieldIDs.WorkflowState] = workflowStateId.ToString();
                }

                Item stateItem = ItemManager.GetItem(workflowStateId, Language.Current, Sitecore.Data.Version.Latest, item.Database, SecurityCheck.Disable);

                // if there are any actions for the new state
                if (!stateItem.HasChildren)
                    return;

                WorkflowPipelineArgs workflowPipelineArgs = new WorkflowPipelineArgs(item, null, null);

                // start executing the actions
                Pipeline pipeline = Pipeline.Start(stateItem, workflowPipelineArgs);
                if (pipeline == null)
                    return;

                WorkflowCounters.ActionsExecuted.IncrementBy(pipeline.Processors.Count);
            }
        }

        private void Publisher(String root)
        {
            Sitecore.Diagnostics.Log.Info("Officecore calling a publish", this);

            try
            {
                foreach (Language language in masterDb.Languages)
                {
                    //loops on the languages and do a full republish on the whole sitecore content tree
                    var options = new PublishOptions(masterDb, webDb, PublishMode.Smart, language, DateTime.Now) { RootItem = masterDb.Items[root], RepublishAll = false, Deep = true };
                    var myPublisher = new Publisher(options);
                    myPublisher.Publish();
                }
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("Could not publish root: " + root + " in the master database to the web", ex);
                Sitecore.Diagnostics.Log.Error(ex.StackTrace.ToString(), ex);
                throw;
            }

            Sitecore.Diagnostics.Log.Info("Officecore finished a publish", this);
        }

        private void IndexRebuild(String indexName)
        {
            Sitecore.Diagnostics.Log.Info("Officecore calling a " + indexName + " rebuild", this);
            Sitecore.ContentSearch.ISearchIndex index = Sitecore.ContentSearch.ContentSearchManager.GetIndex(indexName);
            index.Rebuild();
            Sitecore.Diagnostics.Log.Info("Officecore finished a " + indexName + " rebuild", this);
        }

        private void IndexCustodianRebuild(String indexName)
        {
            Sitecore.Diagnostics.Log.Info("Officecore calling a " + indexName + " rebuild", this);
            // To rebuild “new” search indexes, use this piece of code for every “new” index 
            IndexCustodian.FullRebuild(ContentSearchManager.GetIndex(indexName), true);
            // Or to rebuild all indexes, use the following piece of code: 
            IndexCustodian.RebuildAll();
            Sitecore.Diagnostics.Log.Info("Officecore finished a " + indexName + " rebuild", this);
        }
    }
}