﻿namespace Officecore.Website.layouts.Officecore.Layouts
{
    using System;

    public partial class LogOut : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Sitecore.Security.Authentication.AuthenticationManager.Logout();
            Sitecore.Web.WebUtil.Redirect("/");
        }
    }
}