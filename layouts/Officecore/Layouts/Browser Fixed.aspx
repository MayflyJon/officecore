﻿<%@ page language="C#" autoeventwireup="True" codebehind="Browser Fixed.aspx.cs" inherits="Officecore.Website.layouts.Starter_Kit.Layouts.Browser_Fixed" %>

<%@ register tagprefix="sk" namespace="Officecore.Website" assembly="Officecore.Website" %>
<%@ register tagprefix="sc" namespace="Sitecore.Web.UI.WebControls" assembly="Sitecore.Kernel" %>
<%@ register tagprefix="mt" namespace="Sitecore.WebControls" assembly="Sitecore.MetaTags" %>
<!DOCTYPE html>
<html xmln="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <asp:Literal runat="server" ID="Title" />
    </title>
    <link rel="stylesheet" href="/assets/css/ucommerce/uCommerce.base.css" />
    <link rel="stylesheet" href="/assets/css/bootstrap/bootstrap.min.css" />
    <link rel="stylesheet" href="/assets/css/bootstrap/bootstrap-switch.min.css" />

    <link rel="stylesheet" href="/assets/css/nivo/default/default.css" />
    <link rel="stylesheet" href="/assets/css/nivo/light/light.css" />
    <link rel="stylesheet" href="/assets/css/nivo/dark/dark.css" />
    <link rel="stylesheet" href="/assets/css/nivo/bar/bar.css" />
    <link rel="stylesheet" href="/assets/css/nivo/nivo-slider.css" />
    <link rel="stylesheet" href="/assets/fancybox/source/jquery.fancybox.css?v=2.0.6" media="screen" />
    <link rel="stylesheet" href="/assets/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.2" />
    <link rel="stylesheet" href="/assets/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.2" />
    
    
    <link rel="stylesheet" href="/assets/CSS/officecore-base.css" />
    <link rel="stylesheet" href="/assets/CSS/officecore-forms.css" />
    
    <script type="text/javascript" src="/assets/fancybox/lib/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="/assets/js/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="/assets/js/bootstrap/bootstrap-switch.min.js"></script>
</head>
<body>
    <form id="mainform" method="post" runat="server">
        <div id="wrapper">
            <div id="toolbox-wrapper">
                <div id="toolbox">
                    <sc:sublayout runat="server" placeholder="content" path="/layouts/Officecore/Controls/Desktop-Toolbox.ascx" id="Toolbox" />
                </div>
            </div>
            <div id="header-wrapper">
                <div id="header">
                    <sc:sublayout runat="server" placeholder="content" path="/layouts/Officecore/Controls/Desktop-Header.ascx" id="HeaderFixed" />
                </div>
            </div>
            <nav>
                <div id="menu">
                    <sc:sublayout id="MenuContainer" path="/layouts/Officecore/Controls/Desktop-Menu.ascx" runat="server" />
                </div>
            </nav>
            <div id="hero-wrapper">
                <sc:placeholder runat="server" key="banner" id="Banner"></sc:placeholder>
            </div>
            <div id="content-wrapper">
                <div id="content">
                    <div class="container-fluid">
                        <sc:placeholder runat="server" key="page-columns" id="pagecolumns"></sc:placeholder>
                    </div>
                </div>
            </div>
            <div id="footer-wrapper">
                <div id="footer">
                    <sc:sublayout id="FooterContainer" path="/layouts/Officecore/Controls/Desktop-Footer.ascx" runat="server" />
                </div>
            </div>
        </div>
    </form>
    <script type="text/javascript" src="/assets/js/jquery.nivo.slider.pack.js"></script>
    <script type="text/javascript" src="/assets/fancybox/source/jquery.fancybox.js?v=2.0.6"></script>
    <script type="text/javascript" src="/assets/js/browser-fixed-inline.js?v=1.0.0"></script>
    <script type="text/javascript" src="/assets/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.2"></script>
    <script type="text/javascript" src="/assets/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.2"></script>
    <script type="text/javascript" src="/assets/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.0"></script>
    <script type="text/javascript" src="/assets/starterkit.js"></script>

    <script type="text/javascript" src="/assets/js/ucommerce/ucommerce.js"></script>
    <script type="text/javascript" src="/assets/js/jquery.elevateZoom-3.0.8.min.js"></script>
    <script>
        jQuery(document).ready(function ($) {
            $('#imgProduct').elevateZoom({
                easing: true,
                zoomWindowWidth: 375,
                zoomWindowHeight: 500,
                zoomWindowFadeIn: 500,
                zoomWindowFadeOut: 500,
                lensFadeIn: 500,
                lensFadeOut: 500
            });
        });
    </script>
    
    <script src="/assets/js/pageslide/jquery.pageslide.min.js"></script>
    <script type="text/javascript">
        jQuery('#cart-dropdown').click(function (e) {
            e.stopPropagation();
        });
    </script>
    
    <script type="text/javascript" src="/assets/js/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" src="/assets/js/officecore.js"></script>
</body>
</html>
