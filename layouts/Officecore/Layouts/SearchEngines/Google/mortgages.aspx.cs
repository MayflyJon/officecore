﻿using Officecore.Website.code.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Officecore.Website.layouts.Officecore.Layouts.Google
{
    public partial class Mortgages : System.Web.UI.Page
    {
        public String SITE_URL;
        private SiteService ss = new SiteService();

        protected void Page_Load(object sender, EventArgs e)
        {
            SITE_URL = ss.SetHostAndToggle("false");        
        }
    }
}