﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Officecore.Website.code.Constants;
using Sitecore.Data.Items;
using UCommerce.Runtime;
using Sitecore.Analytics;
using Sitecore.Analytics.Data.DataAccess.DataSets;
using Officecore.Website.code.Service;
using System.Web.UI.WebControls;

namespace Officecore.Website.layouts.Starter_Kit.Layouts
{
    public partial class Browser_Fixed : System.Web.UI.Page
    {
        //public string pageCss { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {            
            // Show banner if correct section, not sub content type
            if (!isBannerSection(Sitecore.Context.Item))
            {
                Banner.Enabled = false;
                Banner.Visible = false;
            }

            //This gets the CSS file link for the Theme CSS item.
            Sitecore.Data.Items.Item root = Sitecore.Context.Database.SelectSingleItem(SitecoreSiteRoot);
            string ThemeGuid = root.Fields["Theme"].Value;
            Sitecore.Data.Items.Item ThemeItem = Sitecore.Context.Database.GetItem(ThemeGuid);

            var pageCss = ThemeItem.Fields["CSS File"].ToString();
            var javascript = "<script>jQuery('head').append('<link rel=\"stylesheet\" type=\"text/css\" href=\"/assets/CSS/" + pageCss + "\">');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "bla", javascript, false);

            if (SiteContext.Current.CatalogContext.CurrentCategory != null || SiteContext.Current.CatalogContext.CurrentProduct != null)
            {
                Title.Text = Sitecore.Context.Item["Display name"];
            }
            else
            {
                Title.Text = Sitecore.Context.Item["Title"];
            }
        }

        public bool isBannerSection(Item currentItem)
        {
            if (currentItem.TemplateName.Contains(TemplateConstants.SiteRootKey) || currentItem.TemplateName.Contains(TemplateConstants.SiteSectionKey))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string SitecoreSiteRoot
        {
            get
            {
                Sitecore.Data.Items.Item current = Sitecore.Context.Item; ;
                Sitecore.Data.Items.Item root = current.Axes.SelectSingleItem("ancestor-or-self::*[@@templateid='" + TemplateConstants.SiteRootId + "']");
                if (root != null)
                {
                    return root.Paths.Path;
                }
                return Sitecore.Context.Site.StartPath;
            }
        }
    }
}