﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Desktop-Sublayout-5050.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Sublayouts.Sublayout_5050" %>
<sc:sublayout runat="server" renderingid="{2BF352A5-6413-45DD-ABD3-2787356F555C}"
    path="/layouts/Starter Kit/Sublayouts/Breadcrumb.ascx" id="uxBreadcrumb" placeholder="content"
    parameters="lang&amp;id&amp;sc_item&amp;sc_currentitem"></sc:sublayout>

  <div class="col-md-6">
     <sc:placeholder key="column-left" ID="colleft" runat="server" />
  </div>
  <div class="col-md-6">
     <sc:placeholder key="column-right" ID="colright" runat="server" />
  </div>