﻿namespace Officecore.Website.layouts.Officecore.Sublayouts.Virtual
{
    using System;
    using code.Service;
    using code.Helpers;
    using Sitecore.Text;
    using Sitecore.Web;

    public partial class LoggedIn : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.Equals(this.Request.QueryString["oc_loginError"], "2"))
            {
                this.litAccessErrorTitle.Text = TranslationHelper.Translate("SocialLogin/PermissionDeniedTitle");
                this.litAccessErrorBody.Text = TranslationHelper.Translate("SocialLogin/PermissionDeniedBody");
                pnlAccessError.Visible = true;
            }

            if (Request.QueryString["authResult"] == "success")
            {
                Sitecore.Data.ID socialMediaLoginGoal;

                var properties = WebUtil.ParseUrlParameters(Attributes["sc_parameters"]);
                if (Sitecore.Data.ID.TryParse(properties["SocialMediaLoginGoal"], out socialMediaLoginGoal))
                {
                    AnalyticsHelper.TriggerGoal(socialMediaLoginGoal);
                }

                new CustomerService().FixAutomationStates();

                var urlString = new UrlString(Request.RawUrl);
                urlString.Remove("authResult");
                Response.Redirect(urlString.ToString());
            }
        }
    }
}