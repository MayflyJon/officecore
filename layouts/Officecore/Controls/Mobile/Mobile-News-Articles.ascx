﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Mobile_News_Articles.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Mobile_News_Articles" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>



    <asp:repeater id="newsItems" runat="server" onitemdatabound="newsItems__ItemDataBound">
        <itemtemplate>
            <div class="subitem-overview">

                <div class="grid">
                    <div class="col-25">
                        <div class="photo-float-left">
                            <asp:hyperlink id="itemimageLink" runat="server"><sc:Image id="itemImage" runat="server" Field="Image"  CssClass="photo-border photo-float-left" MaxWidth="100" MaxHeight="100" as="1" /></asp:hyperlink> 
                        </div>
                    </div>
                    <div class="col-75">

                        <h2><sc:text runat="server" id="itemTitle" Field="Title"  /></h2>
                        <span class="news-date">
                            <sc:Date id="itemDate" Field="date"  Format="MMMM dd, yyyy" runat="server" />
                        </span>
                        <p><sc:Text id="itemAbstract" Field="Overview Abstract" runat="server" />
                        <br />
                            <asp:hyperlink id="itemLink" runat="server">
                            <sc:Text id="Text2" Field="Text" Item="<%# ReadMore %>" runat="server" />
                        </asp:hyperlink>
                            </p>
                    </div>
                </div>
                <div class="clear"></div>

            </div>
        </itemtemplate>
    </asp:repeater>
