﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce;
using UCommerce.Api;
using UCommerce.EntitiesV2;
using UCommerce.Runtime;

namespace Officecore.Website.layouts.Officecore.Controls.Mobile
{
    public partial class Mobile_Cart_Small : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PurchaseOrder basket = null;
            var currency = SiteContext.Current.CatalogContext.CurrentCatalog.PriceGroup.Currency;
            var orderTotal = new Money(0, currency);

            if (TransactionLibrary.HasBasket())
            {
                basket = TransactionLibrary.GetBasket(false).PurchaseOrder;
                if (basket.OrderTotal.HasValue)
                {
                    orderTotal = new Money(basket.OrderTotal.Value, currency);
                }
            }

            var minicartString = "";
            if (basket != null && basket.OrderLines.Any())
            {
                minicartString += "<span class=\"badge badge-basket\">";
                minicartString += basket.OrderLines.Sum(x => x.Quantity).ToString("#,##");
                minicartString += "</span>";
            }
            
            litMinicart.Text = minicartString;

        }
    }
}