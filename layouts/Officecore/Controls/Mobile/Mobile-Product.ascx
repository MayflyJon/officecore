﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Mobile-Product.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Mobile.Mobile_Product" %>
<%@ Register TagPrefix="store" Namespace="Officecore.Website.code.Model" Assembly="Officecore.Website" %>
<div class="panel panel-default" style="margin-bottom: 5px;">
    <div class="panel-heading" style="background-color: white;">
        <strong>
            <asp:HyperLink ID="hlProduct" runat="server" />
        </strong>
    </div>
    <div class="panel-body">
        <div class="media">
            <a class="pull-left" href="<%= CurrentProduct.Url %>">
                <img src="<%# CurrentProduct.ThumbnailImage %>" class="img-thumbnail media-object" height="124" width="104"/>
            </a>
            <div class="media-body">
                <div class="list-group" style="margin-bottom: 0px;">
                    <div class="list-group-item">
                        <asp:Literal ID="litShortDescription" Text="<%# CurrentProduct.ShortDescription %>" runat="server" />
                    </div>
                    <% if(ActivateCommerce) {%><div class="list-group-item">
                        <strong>
                            <asp:Literal ID="litPrice" Text="<%# CurrentProduct.Price %>" runat="server" /></strong>
                    </div>
                    <div class="list-group-item">
                        <asp:Literal ID="rating" runat="server" />
                    </div> 
                    <% } %>
                </div>
            </div>
        </div>
    </div>
    
    <div class="panel-footer" style="background-color: white;">
        <a href="<%= CurrentProduct.Url %>" class="btn btn-info"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;Product Details</a>        
        <% if(ActivateCommerce) {%>
        <button id="Button1" type="button" class="btn btn-success pull-right" runat="server" onserverclick="btnAddToBasket_Click"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;Add to cart</button>
        <% } %>
    </div>
</div>
