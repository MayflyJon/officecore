﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Officecore.Website.layouts.Officecore.Controls.Mobile
{
    public partial class Mobile_Product_Carosel : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            IEnumerable<UCommerce.EntitiesV2.Product> products = UCommerce.Runtime.SiteContext.Current.CatalogContext.CurrentCatalogGroup.ProductCatalogs.SelectMany(category => category.Categories.SelectMany(product => product.Products).Where
                    (
                    p =>
                    p.VariantSku == null
                    && p.DisplayOnSite
                    )).Distinct().ToList();
            lvProducts2.DataSource = products;
            lvProducts2.DataBind();
        }
    }
}