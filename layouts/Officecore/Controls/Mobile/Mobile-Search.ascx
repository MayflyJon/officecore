﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Mobile-Search.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Mobile.Mobile_Search" %>

<div class="input-group" style="margin-bottom:15px;">
    <input class="form-control" type="text" id="SearchTerm" name="SearchTerm" value="<%= SearchTerm %>">
    <span class="input-group-btn" >
        <button type="submit" class="btn btn-success"><span style="line-height: 1.45em;" class="glyphicon glyphicon-search"></span></button>
    </span>
</div>
