﻿using System;
using System.Linq;
using System.Web;
using Officecore.Website.code.Helpers;
using UCommerce.EntitiesV2;
using Officecore.Website.code.DataRepositories;
using System.Collections.Generic;
using Officecore.Website.code.Service;

namespace Officecore.Website.layouts.Officecore.Controls.Mobile
{
    public partial class Mobile_Search_Results : System.Web.UI.UserControl
    {
        private ProductService ps = new ProductService();

        protected void Page_Load(object sender, EventArgs e)
        {
            var searchterm = Request.Form.Get("SearchTerm");
            if (searchterm == null)
            {
                searchterm = string.Empty;
            }

            lvProducts.DataSource = ps.GetProducts(searchterm, HttpContext.Current.Request.QueryString);
            lvProducts.DataBind();
        }
    }
}