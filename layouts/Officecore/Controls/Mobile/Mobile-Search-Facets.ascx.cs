﻿using Sitecore.Sites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.Api;
using UCommerce.Runtime;
using UCommerce.Search.Facets;
using Officecore.Website.code.Helpers;
using UCommerce.Search;
using Officecore.Website.code.DataRepositories;
using Sitecore.Text;
using Officecore.Website.code.Constants;

namespace Officecore.Website.layouts.Officecore.Controls.Mobile
{
    public partial class Mobile_Search_Facets : System.Web.UI.UserControl
    {
        private bool _anyFacetHits = false;
        public List<Button> _controls = new List<Button>();
        public string _currentQueryStringKey = "";
        private ProductRepository repo = new ProductRepository();
        protected int facetSize = 0;
        public Boolean ActivateCommerce = true;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ActivateCommerce = repo.ActivateCommerce();
                IList<Facet> facetsForQuerying = UcommerceHelper.GetFacets(HttpContext.Current.Request.QueryString);
                var searchterm = Request.Form.Get("SearchTerm");
                if (searchterm == null)
                {
                    searchterm = string.Empty;
                }

                IEnumerable<Facet> facets;

                if (Sitecore.Context.Item.TemplateID == TemplateConstants.uCommerce.CategoryWithFacetsId)
                {
                    List<code.Model.Product> products;
                    facets = repo.SearchFacets(UCommerce.Runtime.SiteContext.Current.CatalogContext.CurrentCategory, facetsForQuerying, out products);
                }
                else
                {
                    List<code.Model.Product> products;
                    facets = repo.SearchFacets(searchterm, facetsForQuerying, out products);
                }

                rptFacets.DataSource = facets;
                rptFacets.DataBind();
                facetSize = facets.Count();
                FindChildControlsRecursive(rptFacets);
                EnsureCheckboxesAreChecked();

                if (!_anyFacetHits)
                {
                    facetsDiv.Visible = false;
                }
            }
        }


        public void FindChildControlsRecursive(Control control)
        {
            foreach (Control childControl in control.Controls)
            {
                if (childControl.GetType() == typeof(Button))
                {
                    Button currentButton = (Button)childControl;
                    currentButton.Attributes.Add("class", "checkbox");
                    _controls.Add((Button)childControl);
                }
                else
                {
                    FindChildControlsRecursive(childControl);
                }
            }
        }

        public void EnsureCheckboxesAreChecked()
        {
            List<KeyValuePair<string, string>> queryStrings = new List<KeyValuePair<string, string>>();

            foreach (string key in Request.QueryString.AllKeys)
            {
                queryStrings.Add(new KeyValuePair<string, string>(key, Request.QueryString[key]));
            }

            foreach (var pair in queryStrings)
            {
                string[] values = pair.Value.Split('|');

                if (pair.Key == "category")
                {
                    continue;
                }

                foreach (var value in values)
                {
                    if (string.IsNullOrEmpty(value))
                    {
                        continue;
                    }

                    //Now i just have to find the corresponding buttons and give them the css class.
                    foreach (Button control in _controls)
                    {
                        if (control.Attributes["queryStringKey"] == pair.Key && control.Attributes["queryStringValue"] == value)
                        {
                            control.Attributes.Add("class", "checkbox checked");
                        }
                    }
                }
            }
        }

        public void rptFacets_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            Facet currentItem = (Facet)e.Item.DataItem;
            var litHeadline = (Literal)e.Item.FindControl("litHeadline");
            var rptCheckBoxes = (Repeater)e.Item.FindControl("rptCheckBoxes");

            litHeadline.Text = currentItem.DisplayName;
            _currentQueryStringKey = currentItem.Name;

            rptCheckBoxes.DataSource = currentItem.FacetValues.Where(x => x.Hits > 0);
            rptCheckBoxes.DataBind();
        }

        public void rptCheckBoxes_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            FacetValue currentItem = (FacetValue)e.Item.DataItem;

            Button btnCheckBox = (Button)e.Item.FindControl("btnCheckBox");
            btnCheckBox.Attributes.Add("queryStringKey", _currentQueryStringKey);
            btnCheckBox.Attributes.Add("queryStringValue", currentItem.Value);
            btnCheckBox.Text = currentItem.Value + "(" + currentItem.Hits + ")";

            var redirectUrl = new UrlString(Request.RawUrl.Replace("%20", " "));

            // RedirectUrl contains current button querystring
            if (redirectUrl[btnCheckBox.Attributes["queryStringKey"]] != null)
            {
                if (redirectUrl[btnCheckBox.Attributes["queryStringKey"]].Replace("+", " ") == btnCheckBox.Attributes["queryStringValue"])
                {
                    redirectUrl.Remove(btnCheckBox.Attributes["queryStringKey"]);
                }
                else
                {
                    redirectUrl.Add(btnCheckBox.Attributes["queryStringKey"], btnCheckBox.Attributes["queryStringValue"]);
                }
            }
            else
            {
                redirectUrl.Add(btnCheckBox.Attributes["queryStringKey"], btnCheckBox.Attributes["queryStringValue"]);
            }

            btnCheckBox.PostBackUrl = redirectUrl.ToString();

            _anyFacetHits = true;
        }
    }


}