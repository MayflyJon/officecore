﻿using System;
using System.Linq;
using Officecore.Website.code.Constants;
using Officecore.Website.code.DataRepositories;

namespace Officecore.Website.layouts.Officecore.Controls.Mobile
{
    public partial class Mobile_Menu : System.Web.UI.UserControl
    {
        private readonly ItemRepositoryBase menus = new ItemRepositoryBase();

        protected void Page_Load(object sender, EventArgs e)
        {
            MenuRepeater.DataSource = this.menus.GetMainMenu(DataRepositoryConstants.MENUMOBILE).ToList();
            MenuRepeater.DataBind();
        }
    }
}