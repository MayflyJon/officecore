﻿using System;
using System.Linq;
using System.Collections.Generic;
using Officecore.Website.code.Constants;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.Data;
using Sitecore.Data.Items;
using System.Web.UI.WebControls;
using Sitecore.Links;

namespace Officecore.Website.layouts.Officecore.Controls
{
    using global::Officecore.Website.code.DataRepositories;

    public partial class Mobile_News_Articles : System.Web.UI.UserControl
    {
        public Item ReadMore {get; set;}

        private void Page_Load(object sender, EventArgs e)
        {
            ID id = new ID(DataRepositoryConstants.READ_MORE_TEXT);
            ReadMore = Sitecore.Context.Database.GetItem(id);

            newsItems.DataSource = RetrieveNewsItems();
            newsItems.DataBind();
        }

        public void newsItems__ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var result = e.Item.DataItem as NewsItem;
                var item = result.GetItem();

                HyperLink itemimageLink = e.Item.FindControl("itemimageLink") as HyperLink;
                itemimageLink.NavigateUrl = LinkManager.GetItemUrl(item);

                HyperLink itemLink = e.Item.FindControl("itemLink") as HyperLink;
                itemLink.NavigateUrl = LinkManager.GetItemUrl(item);

                Sitecore.Web.UI.WebControls.Image itemImage = e.Item.FindControl("itemImage") as Sitecore.Web.UI.WebControls.Image;
                itemImage.Item = item;

                Sitecore.Web.UI.WebControls.Date itemDate = e.Item.FindControl("itemDate") as Sitecore.Web.UI.WebControls.Date;
                itemDate.Item = item;

                Sitecore.Web.UI.WebControls.Text itemTitle = e.Item.FindControl("itemTitle") as Sitecore.Web.UI.WebControls.Text;
                itemTitle.Item = item;

                Sitecore.Web.UI.WebControls.Text itemAbstract = e.Item.FindControl("itemAbstract") as Sitecore.Web.UI.WebControls.Text;
                itemAbstract.Item = item;

            }
        }

        private IEnumerable<NewsItem> RetrieveNewsItems()
        {
            string searchindex = SearchHelper.GetSearchIndex();

            using (var context = ContentSearchManager.GetIndex(searchindex).CreateSearchContext())
            {
                return context.GetQueryable<NewsItem>().Where(
                        item => item.TemplateId == new ID("{34A5EDED-6BD2-4DF5-ADDB-163F4655BEC7}")
                                || item.TemplateId == new ID("{D89BDC0E-FF2B-4AA9-BA39-8F863A6056E9}") 
                                || item.TemplateId == new ID("{74A07E7C-B826-4B23-B87C-11398E9D6FEF}"))
                        .Where(item => item.Name != "__Standard Values")
                        .Where(item => item.Language == Sitecore.Context.Language.Name)
                        .Where(item => item.IsLatestVersion)
                        .OrderBy(f => f.CreatedDate).Take(10).ToList();
            } 
        }
    }


    public class NewsItem : SearchResultItem
    {
        [IndexField("_latestversion")]
        public bool IsLatestVersion { get; set; }
    }
}