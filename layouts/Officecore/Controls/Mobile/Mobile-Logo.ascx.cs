﻿using Officecore.Website.code.Controls;
using Officecore.Website.code.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Officecore.Website.layouts.Officecore.Controls.Mobile
{
    public partial class Mobile_Logo : DatasourceControl
    {
        public Boolean ActivateCommerce = true;
        private ProductService ps = new ProductService();

        protected void Page_Load(object sender, EventArgs e)
        {
            ActivateCommerce = ps.ActivateCommerce();
            Logo.Item = Sitecore.Context.Database.GetItem(Sitecore.Context.Site.ContentStartPath+"/Home");
        }
    }
}