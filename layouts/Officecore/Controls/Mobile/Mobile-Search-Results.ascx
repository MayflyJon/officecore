﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Mobile-Search-Results.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Mobile.Mobile_Search_Results" %>
<%@ Import Namespace="UCommerce.EntitiesV2" %>
<%@ Register Src="~/layouts/Officecore/Controls/Mobile/Mobile-Product.ascx" TagPrefix="store" TagName="Product" %>
<div class="row-fluid">
    <div class="span12 product-list">
        <asp:ListView runat="server" ID="lvProducts" ItemType="Officecore.Website.code.Model.Product">
            <ItemTemplate>
                <store:product runat="server" id="Product" currentproduct="<%# Item %>" />
            </ItemTemplate>
        </asp:ListView>
    </div>
</div>
