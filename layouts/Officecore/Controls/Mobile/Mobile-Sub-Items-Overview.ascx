﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Mobile-Sub-Items-Overview.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Mobile.Mobile_Sub_Items_Overview" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<asp:Repeater ID="subItems" runat="server" OnItemDataBound="subItems__ItemDataBound">
    <ItemTemplate>
        <div class="panel panel-default">
            <div class="panel-heading" style="background-color:white;">
                <h4><sc:text runat="server" id="itemTitle" field="Title" /></h4>
            </div>
            <div class="panel-body">
                <asp:HyperLink ID="itemimageLink" runat="server">
                    <sc:image id="itemImage" runat="server" field="Image" cssclass="photo-border photo-float-left" maxwidth="100" maxheight="100" as="1" />
                </asp:HyperLink>
                <sc:text id="itemAbstract" field="Overview Abstract" runat="server" />
                 <asp:HyperLink ID="itemLink" runat="server">
                    <sc:text id="Text2" field="Text" item="<%# ReadMore %>" runat="server" />
                </asp:HyperLink>
            </div>
        </div>
    </ItemTemplate>
</asp:Repeater>

