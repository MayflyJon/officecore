﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Mobile-Product-Details.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Mobile.Mobile_Product_Details" %>
<%@ Import Namespace="UCommerce.Api" %>
<%@ Register TagPrefix="store" Namespace="Officecore.Website.code.Model" Assembly="Officecore.Website" %>
<%@ Register Src="~/layouts/Officecore/Controls/Desktop/Products/Desktop-ProductReview.ascx" TagPrefix="officecore" TagName="DesktopProductReview" %>

<div class="panel panel-default" style="margin-bottom: 5px;">
    <div class="panel-heading">
        <strong><%= CurrentProduct.Name  %>
            <%= CurrentProduct.Price  %></strong>
    </div>
    <div class="panel-body"style="padding-bottom:5px;">
        <div>
            <img id="imgProduct" src="<%= CurrentProduct.PrimaryImageUrl %>"  height="200" class="img-responsive img-rounded text-center" data-zoom-image="<%= CurrentProduct.PrimaryImageUrl %>" 
        </div>
        <div><%=  CurrentProduct.LongDescription  %></div>
        <% if(ActivateCommerce) {%>
        <table class="table borderless">
            <tbody>
                <tr>
                    <td>
                        <div>Price: <%=  CurrentProduct.Price  %></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div>VAT:  <%=  CurrentProduct.Tax  %></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div>
                            <asp:Button runat="server" ID="btnAddToBasket" ClientIDMode="static" class="btn btn-block btn-success" Text="Add to basket!" OnClick="btnAddToBasket_Click" />
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
        <% } %>
    </div>
</div>
<% if(ActivateCommerce) {%>
<div class="panel-group" id="accordion">
    <div class="panel panel-default">
        <div id="one" class="panel-heading clearfix" style="background-color: white;" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" onclick="myFunction(id)">
            <h4 class="panel-title pull-left">
                <strong>
                    <asp:Literal runat="server" ID="shippingHeading" /></strong>
            </h4>
            <div class="pull-right">
                <img src="/assets/images/plus-icon-sml.gif" /></span>
            </div>
        </div>
        <div id="collapseOne" class="panel-collapse collapse">
            <div class="panel-body">
                <ul>
                    <li>
                        <span class="glyphicon glyphicon-gift">&nbsp;</span>Click &amp; collect from 2pm tomorrow
						<p>
                            <span>Order before <strong>9 PM</strong><br />
                                Free from Officecore shops</span>
                        </p>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-road">&nbsp;</span>UK delivery
						<p>
                            <span class="price">
                                <strong>£3.00</strong>
                            </span>
                            Standard UK delivery within 5 working days
                        </p>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-plane">&nbsp;</span>
                        International delivery not available.
						<p>
                            See our <a href="/About-Us.aspx">terms and conditions</a> for how we might still be able to assist.
                        </p>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-share-alt">&nbsp;</span>Returns are free
                        See our <a href="/About-Us.aspx">terms and conditions</a> about returning this product.
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div id="two" class="panel-heading clearfix" style="background-color: white;" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" onclick="myFunction(id)">
            <h4 class="panel-title pull-left">
                <strong>
                    <asp:Literal runat="server" ID="ratingsHeading" /></strong>
            </h4>
            <div class="pull-right">
                <img src="/assets/images/plus-icon-sml.gif" />
            </div>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse">
            <div class="panel-body">
                <asp:Repeater runat="server" ID="ratingsRepeater" ItemType="UCommerce.EntitiesV2.ProductReview">
                    <ItemTemplate>
                        <officecore:desktopproductreview runat="server" id="DesktopProductReview" review="<%# Item %>" />
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
</div>
<% } %>
<script type="text/javascript" >

    //get all the child names for the containing div of the input tickboxs, use a loop
    //within the loop get all the names of them tickboxs
    //check to see if that item has been checked


    function myFunction(id1) {
        var headDiv = document.getElementById(id1);
        //alert(headDiv.id);

        if (headDiv.getAttribute("class") == "panel-heading clearfix") {
            headDiv.children[1].children[0].setAttribute("src", "/assets/images/plus-icon-sml.gif");
        }
        else if (headDiv.getAttribute("class") == "panel-heading clearfix collapsed") {
            headDiv.children[1].children[0].setAttribute("src", "/assets/images/minus-icon-sml.gif");
        }
    }

</script>
