﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Mobile_News_Item.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Mobile_News_Item" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>


<div class="subitem-overview">
    <div class="grid">
        <div class="col-25">
            <div class="photo-float-left">
                <sc:Image id="Image1" runat="server" Field="Image"  CssClass="photo-border photo-float-left" MaxWidth="100" MaxHeight="100" as="1" />
            </div>
        </div>
        <div class="col-75">
            <h2><sc:Text id="Text1" Field="Title"  runat="server" /></h2>
            <p><sc:Text id="Text2" Field="Text"  runat="server" /></p>
        </div>

    </div>
</div>