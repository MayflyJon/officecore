﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Mobile-Shipping.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Mobile.Mobile_Shipping" %>
<br/><br/>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <strong>Shipping method</strong>
        </h4>
    </div>
    <div class="panel-body">
        <div id="pPaymentAlert" class="alert alert-danger" runat="server">
            <asp:Literal ID="litAlert" runat="server" />
        </div>
        <div id="buttonHolder" class="btn-group-vertical btn-block" data-toggle="buttons">
            <asp:Repeater runat="server" ID="ListView1">
                <ItemTemplate>
                    <label id="button" class="btn btn-default" onclick="checkshippingclicked(this)">
                        <input type="radio" name="shippingMethod" value="<%# ((System.Web.UI.WebControls.ListItem)(Container.DataItem)).Value %>">
                        <%# ((System.Web.UI.WebControls.ListItem)(Container.DataItem)).Text %>
                    </label>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</div>

<div class="panel panel-default" style="margin-top: 15px;">
    <div class="panel-body" style="padding:10px!important">
        <a href="/cart/address.aspx" class="btn btn-large btn-danger pull-left">Back</a>
        <asp:Button UseSubmitBehavior="true" ID="btnUpdateShipment" ClientIDMode="Static" CssClass="btn btn-large btn-success pull-right " runat="server" Text="Continue to payment" OnClick="btnUpdateShipment_Click" disabled />
    </div>
</div>

<script type="text/javascript" >
    function checkshippingclicked(label) {
        //Set the class attribute when its clicked
        document.getElementById(label.getAttribute("id")).setAttribute("class", "btn", "btn-success");

        //Remove the disabled attribute
        document.getElementById("btnUpdateShipment").removeAttribute("disabled");
    }
</script>