﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Mobile-Confirmation.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Mobile.Mobile_Confirmation" %>
<br/><br/>
<div class="panel panel-success">
  <div class="panel-heading">Thank you for your order</div>
  <div class="panel-body" style="padding: 15px !important">
    An order confirmation email has been sent to your email address
  </div>
</div>