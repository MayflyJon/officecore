﻿using Officecore.Website.code.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Officecore.Website.layouts.Officecore.Controls.Mobile
{
    public partial class Mobile_Home : DatasourceControl
    {
        public string BannerImgUrl { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            BannerHeader.Item = this.Item;
            BannerSlogan.Item = this.Item;
            BannerLink.Item = this.Item;
            BannerImgUrl = GetImageURL(this.Item, "Banner Image");
        } 
    }
}