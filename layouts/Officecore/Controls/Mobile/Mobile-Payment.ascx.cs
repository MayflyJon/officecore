﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce;
using UCommerce.Api;
using UCommerce.EntitiesV2;

namespace Officecore.Website.layouts.Officecore.Controls.Mobile
{
    public partial class Mobile_Payment : System.Web.UI.UserControl
    {
        //private bool _selectFirst = true;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                return;
            }

            var basket = TransactionLibrary.GetBasket().PurchaseOrder;
            var billingCountry = TransactionLibrary.GetShippingInformation().Country;

            if (billingCountry == null)
            {
                string warning =
                    "WARNING: No billing country has been selected. <a href=\"/cart/address\">Click here</a> to select a billing country.";
                litAlert.Text = warning;
                btnContinue.Visible = false;

                return;
            }

            var availableBillingMethods = TransactionLibrary.GetPaymentMethods(billingCountry);
            var payment = basket.Payments.FirstOrDefault();

            if (availableBillingMethods.Count != 0)
            {
                pPaymentAlert.Visible = false;
            }
            else
            {
                string warning =
                    "WARNING: No payment methods have been configured for " + billingCountry.Name + " within <a href=\"http://ucommerce.net\">UCommerce</a> administration area.";
                litAlert.Text = warning;
                btnContinue.Visible = false;
            }
            List<ListItem> asm = new List<ListItem>();
            foreach (PaymentMethod paymentMethod in availableBillingMethods)
            {
                decimal feePercent = paymentMethod.FeePercent;

                var fee = paymentMethod.GetFeeForCurrency(basket.BillingCurrency);
                var formattedFee = new Money((fee == null ? 0 : fee.Fee), basket.BillingCurrency);

                string paymentMethodText = paymentMethod.Name + "<text>(</text>" + formattedFee + "<text>+</text>"
                                + feePercent.ToString("0.00") + "<text>%)</text>";

                ListItem currentListItem = new ListItem(paymentMethodText, paymentMethod.Id.ToString());
                asm.Add(currentListItem);
            }
            ListView1.DataSource = asm;
            ListView1.DataBind();

        }

        public void btnContinue_Click(object sender, EventArgs e)
        {
            int paymentMethodId = 0;
            Int32.TryParse(Request.Form.Get("paymentMethod"), out paymentMethodId);

            TransactionLibrary.CreatePayment(paymentMethodId, requestPayment: false);
            TransactionLibrary.ExecuteBasketPipeline();
            HttpContext.Current.Response.Redirect("/cart/Preview");
        }

        protected string GetItemActive(int itemIndex)
        {
            if (itemIndex == 0)
            {
                return "active";
            }
            return "";
        }
    }
}