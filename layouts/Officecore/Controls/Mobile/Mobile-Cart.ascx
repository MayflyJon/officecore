﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Mobile-Cart.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Mobile.Mobile_Cart" %>
<%@ Register TagPrefix="store" Namespace="Officecore.Website.code.Model" Assembly="Officecore.Website" %>
<asp:Repeater ID="rptCart" runat="server" OnItemDataBound="CartRepeaterItemDataBound">
    <ItemTemplate>
        <div class="panel panel-default col-lg-6 col-md-6 col-sm-6 cart-panel">
            <div class="panel-body" style="padding:10px!important;  ">
                <asp:HyperLink runat="server" ID="lnkItemLink1">
                    <store:image runat="server" id="imgProduct" class="cart-images thumbnail" style="margin-bottom:0px" /> 
                </asp:HyperLink>
                <div class="cart-description">
                    <div class="list-group" style="margin-bottom: 5px;">
                        <div class="list-group-item">
                            <strong>
                                <asp:HyperLink ID="lnkItemLink" runat="server" /></strong>
                        </div>
                        <div class="list-group-item">
                            <asp:Literal ID="litDesc" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="input-group">
                    <span class="input-group-addon">
                        <strong>
                            <asp:Literal ID="litPrice" runat="server" /></strong></span>
                    <asp:TextBox ClientIDMode="static" ID="txtQuantity" runat="server" value="13" class="form-control"></asp:TextBox>
                    <span class="input-group-btn">
                        <button id="btnUpdateQuantities" style="height: 34px;" type="button" class="btn btn-success" clientidmode="static" onserverclick="btnUpdateQuantities_Click" runat="server"><span class="glyphicon glyphicon-repeat"></span></button>
                        <button id="btnRemoveLine" style="height: 34px;" name="basket-remove-item" type="button" class="btn btn-danger" clientidmode="static" onserverclick="btnRemoveLine_Click" runat="server"><span class="glyphicon glyphicon-remove-circle"></span></button>
                    </span>
                </div>

            </div>
        </div>
    </ItemTemplate>
</asp:Repeater>
<div class="panel panel-default">
    <table class="table" style="text-align: center" runat="server" id="overviewtable">
        <tr>
            <td>Subtotal:</td>
            <td>
                <asp:Literal runat="server" ID="litSubtotal" />
            </td>
        </tr>
        <tr>
            <td>VAT:</td>
            <td>
                <asp:Literal ID="litTax" runat="server" />
            </td>
        </tr>
        <tr>
            <td><strong>Total:</strong></td>
            <td>
                <strong>
                    <asp:Literal ID="litTotal" runat="server" /></strong>
            </td>
        </tr>
    </table>
    <table class="table" style="text-align: center" visible="false" runat="server" id="emptyBasket">
        <tr>
            <td>
                <asp:Literal runat="server" Text="Your basket is empty" />
            </td>
        </tr>
    </table>
    <div class="panel-footer">
        <button type="button" runat="server" id="cartButton" class="btn btn-block btn-success" onserverclick="CartButton_Click">Continue Securely</button>
        <a href="/products/" class="btn btn-block btn-default">Continue Shopping</a>
    </div>
</div>
