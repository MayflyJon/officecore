﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Mobile-Preview.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Mobile.Mobile_Preview" %>
<%@ Register TagPrefix="store" Namespace="Officecore.Website.code.Model" Assembly="Officecore.Website" %>
<div class="row-fluid">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <strong>Billing address</strong>
            </div>
            <div class="panel-body" style="padding:15px !important;">
                <address>
                    <strong><asp:Literal ID="litBillingName" runat="server" /></strong><br />
                    <asp:Literal ID="litBillingStreet" runat="server" /><br />
                    <asp:Literal ID="litBillingCity" runat="server" /><br />
                    <asp:Literal ID="litBillingPostalCode" runat="server" /><br />
                    <asp:Literal ID="litBillingCountry" runat="server" /><br/>
                    <abbr title="Phone">P:</abbr><asp:Literal ID="litBillingPhone" runat="server" /><br />
                    <abbr title="Mobile">M:</abbr><asp:Literal ID="litBillingMobilePhone" runat="server" /><br />
                    <abbr title="Email">E:</abbr><asp:HyperLink ID="lnkBillingMail" runat="server" />
                </address>
            </div>
        </div>        
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <strong>Shipping address</strong>
            </div>
            <div class="panel-body" style="padding:15px !important;">
                <address>
                    <strong><asp:Literal ID="litShippingName" runat="server" /></strong>
                    <br />
                    <asp:Literal ID="litShippingStreet" runat="server" /><br />
                    <asp:Literal ID="litShippingCity" runat="server" /><br />
                    <asp:Literal ID="litShippingPostalCode" runat="server" /><br />
                    <asp:Literal ID="litShippingCountry" runat="server" /><br />
                    <abbr title="Phone">P:</abbr><asp:Literal ID="litShippingPhone" runat="server" /><br />
                    <abbr title="Mobile">M:</abbr><asp:Literal ID="litShippingMobilePhone" runat="server" /><br />
                    <abbr title="Email">E:</abbr><asp:HyperLink ID="lnkShippingMail" runat="server" />
                </address>
            </div>
        </div>
    </div>
</div>

<div class="row-fluid">
<asp:Repeater ID="rptPreviewItems" runat="server" OnItemDataBound="rptPreviewItems_DataBound">
    <ItemTemplate>
          <div class="col-md-6">
        <div class="panel panel-default" style="">
            <div class="panel-heading">
                <strong>
                    <asp:Literal ID="litItemSku" runat="server" />
                    <asp:Literal ID="litItemName" runat="server" /></strong>
            </div>
            <div class="panel-body">
                <div style="float: left; width: 40%; padding: 15px;">
                    <store:image runat="server" id="imgProduct" class="img-thumbnail media-object" height="100" width="100" />
                </div>
                <div class="preview-product">
                    <div class="list-group" style="margin-bottom: 0px;">
                        <div class="list-group-item">
                            <span><strong>Price:</strong></span>
                            <span class="pull-right">
                                <asp:Literal ID="litPrice" runat="server" /></span>
                        </div>
                        <div class="list-group-item">
                            <span><strong>VAT:</strong></span>
                            <span class="pull-right">
                                <asp:Literal ID="litVat" runat="server" /></span>
                        </div>
                        <div class="list-group-item">
                            <span><strong>Quantity:</strong></span>
                            <span class="pull-right">
                                <asp:Literal ID="litQuantity" runat="server" /></span>
                        </div>
                        <div class="list-group-item">
                            <span><strong>Total:</strong></span>
                            <span class="pull-right">
                                <asp:Literal ID="litTotal" runat="server" /></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
              </div>
    </ItemTemplate>
</asp:Repeater>
        </div>

<div class="row-fluid">

<div class="col-md-12">
    <div class="list-group" style="border-radius:4px;border: 1px solid #ddd;padding:5px;">
        <div class="list-group-item">
            <span><strong>Sub total:</strong></span>
            <span class="pull-right">
                <asp:Literal ID="litSubTotal" runat="server" /></span>
        </div>
        <div class="list-group-item">
            <span><strong>VAT:</strong></span><span class="pull-right">
                <asp:Literal ID="litVat" runat="server" /></span>
        </div>
        <div class="list-group-item" id="divDiscounts" runat="server">
            <span><strong>Order discounts:</strong></span><span class="pull-right">
                <asp:Literal ID="litDiscount" runat="server" /></span>
        </div>
        <div class="list-group-item" id="divShipping" runat="server">
            <span><strong>Shipping:
                <asp:Literal ID="litShipments" runat="server" /></strong></span>
            <span class="pull-right">
                <asp:Literal ID="litShippingTotal" runat="server" /></span>
        </div>
        <div class="list-group-item" id="divPaymentTotal" runat="server">
            <span><strong>Payment:</strong></span><span class="pull-right">
                <asp:Literal ID="litPaymentMethods" runat="server" />
                <asp:Literal ID="litPaymentTotal" runat="server" /></span>
        </div>
        <div class="list-group-item">
            <span><strong>Order total:</strong></span>
                <span class="pull-right">
                    <asp:Literal ID="litOrderTotal" runat="server" /></span>
        </div>
    </div>
</div>
    
</div>

<div class="panel panel-default" style="margin-top: 15px;">
    <div class="panel-body" style="padding:10px!important">
    <a href="/cart/payment.aspx" class="pull-left btn btn-large btn-danger">Back</a>
    <asp:Button ID="btnContinue" runat="server" OnClick="btnContinue_Click" class="pull-right btn btn-large btn-success" Text="Complete Order" />        
        </div></div>
