﻿using System;
using System.Collections.Generic;
using Officecore.Website.code.Constants;
using Sitecore.ContentSearch;
using Sitecore.Data;
using Sitecore.Data.Items;
using System.Web.UI.WebControls;
using Sitecore.Links;
using Officecore.Website.code.Model;

namespace Officecore.Website.layouts.Officecore.Controls.Mobile
{
    using System.Linq;

    using global::Officecore.Website.code.DataRepositories;

    public partial class Mobile_Sub_Items_Overview : System.Web.UI.UserControl
    {
        public Item ReadMore { get; set; }
        private string curSection = string.Empty;
        private code.Service.SiteService siteService = new code.Service.SiteService();

        private void Page_Load(object sender, EventArgs e)
        {
            ID id = new ID(DataRepositoryConstants.READ_MORE_TEXT);
            ReadMore = Sitecore.Context.Database.GetItem(id);

            curSection = siteService.GetCurrentSection();

            subItems.DataSource = RetrieveSubItems();
            subItems.DataBind();
        }

        public void subItems__ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var result = e.Item.DataItem as SubItem;
                var item = result.GetItem();


                Sitecore.Web.UI.WebControls.Image itemImage = e.Item.FindControl("itemImage") as Sitecore.Web.UI.WebControls.Image;
                itemImage.Item = item;

                HyperLink itemimageLink = e.Item.FindControl("itemimageLink") as HyperLink;
                itemimageLink.NavigateUrl = LinkManager.GetItemUrl(item);


                HyperLink itemLink = e.Item.FindControl("itemLink") as HyperLink;
                itemLink.NavigateUrl = LinkManager.GetItemUrl(item);


                Sitecore.Web.UI.WebControls.Text itemTitle = e.Item.FindControl("itemTitle") as Sitecore.Web.UI.WebControls.Text;
                itemTitle.Item = item;

                Sitecore.Web.UI.WebControls.Text itemAbstract = e.Item.FindControl("itemAbstract") as Sitecore.Web.UI.WebControls.Text;
                itemAbstract.Item = item;

            }
        }

        private IEnumerable<SubItem> RetrieveSubItems()
        {
            var searchindex = SearchHelper.GetSearchIndex();          
            
            using (var context = ContentSearchManager.GetIndex(searchindex).CreateSearchContext())
            {               
                ID templateID = siteService.GetSection(curSection);
                
                return context.GetQueryable<SubItem>().Where(
                        item => item.TemplateId == templateID)
                        .Where(item => item.Name != "__Standard Values")
                        .Where(item => item.Language == Sitecore.Context.Language.Name)
                        .Where(item => item.IsLatestVersion)
                        .Take(10)
                        .ToList();
            }
        }      
    }
}
