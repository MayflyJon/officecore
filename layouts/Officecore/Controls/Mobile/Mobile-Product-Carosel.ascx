﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Mobile-Product-Carosel.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Mobile.Mobile_Product_Carosel" %>
<%@ Register TagPrefix="store" Namespace="Officecore.Website.code.Model" Assembly="Officecore.Website" %>
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <asp:Repeater ID="lvProducts2" runat="server">
                <HeaderTemplate>
                   <div class="item active" >
                    <img src="/~/media/Images/Banners/london-pano.jpg" />
                    <div class="carousel-caption">
                       <%#Container.ItemIndex  %>
                    </div>
                </div>
                </HeaderTemplate>        
            <ItemTemplate>
                <div class="item" >
                    <img src="/~/media/Images/Banners/london-pano.jpg" />
                    <div class="carousel-caption">
                       <%#Container.ItemIndex  %>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
</div>
