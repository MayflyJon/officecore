﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Mobile-Address.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Mobile.Mobile_Address" %>
<div class="panel-group" id="accordion">
    <div class="panel panel-default">
        <div class="panel-heading clearfix" style="background-color: white;" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
            <h4 class="panel-title pull-left">
                <strong>Billing address</strong>
            </h4>
            <div class="pull-right"><span id="icon-collapse-one" style="font-size: x-large; line-height: 0.5;" class="glyphicon glyphicon-collapse-up"></span></div>
        </div>
        <div id="collapseOne" class="panel-collapse collapse in">
            <div class="panel-body">
                <div class="form-group">
                    <label>Title</label>
                    <input type="text" clientidmode="static" id="billingAttention" class="form-control" runat="server" placeholder="Title" />
                </div>
                <div class="form-group">
                    <label>First name</label>
                    <input type="text" clientidmode="static" id="billingFirstName" class="form-control" runat="server" placeholder="First name" required />
                </div>
                <div class="form-group">
                    <label>Last name</label>
                    <input type="text" clientidmode="static" id="billingLastName" class="form-control" runat="server" placeholder="Last name" required />
                </div>
                <div class="form-group">
                    <label>E-mail</label>
                    <input type="email" clientidmode="static" id="billingEmail" class="form-control" runat="server" placeholder="Email" required />
                </div>
                <div class="form-group">
                    <label>Company</label>
                    <input type="text" clientidmode="static" id="billingCompany" class="form-control" runat="server" placeholder="Company" />
                </div>
                <div class="form-group">
                    <label>House</label>
                    <input type="text" clientidmode="static" id="billingStreet" class="form-control" runat="server" placeholder="House" required />
                </div>
                <div class="form-group">
                    <label>Street</label>
                    <input type="text" clientidmode="static" id="billingStreetTwo" class="form-control" runat="server" placeholder="Street" />
                </div>
                <div class="form-group">
                    <label>City</label>
                    <input type="text" clientidmode="static" id="billingCity" class="form-control" runat="server" placeholder="City" />
                </div>
                <div class="form-group">
                    <label>Postal code</label>
                    <input type="text" clientidmode="static" id="billingPostalCode" class="form-control" runat="server" placeholder="Postal Code" required />
                </div>
                <div class="form-group">
                    <label>Country</label>
                    <asp:DropDownList ClientIDMode="static" type="text" ID="billingCountry" CssClass="form-control" DataTextField="Name" DataValueField="CountryId" runat="server"></asp:DropDownList>
                </div>
                <div class="form-group">
                    <label>Phone</label>
                    <input type="text" clientidmode="static" id="billingPhone" class="form-control" runat="server" placeholder="Phone" />
                </div>
                <div class="form-group">
                    <label>Mobile phone</label>
                    <input type="text" clientidmode="static" id="billingMobile" class="form-control" runat="server" placeholder="Mobile phone" />
                </div>
                <div class="form-group">
                    <input type="checkbox" id="checkHideShipping" />Shipping address same as billing
                </div>
                <a class="btn btn-large btn-danger btn-arrow-right" href="/cart/">Back</a>
                <asp:Button runat="server" ID="btnBillingUpdate" class="pull-right btn btn-large btn-success btn-arrow-right" Text="Continue to Billing" ClientIDMode="static" OnClick="btnBillingUpdate_Click" />
            </div>
        </div>
    </div>
    <div class="panel panel-default" id="shippingHolder" style="visibility: visible;">
        <div class="panel-heading clearfix" style="background-color: white;" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
            <h4 class="panel-title pull-left">
                <strong>Shipping address</strong>
            </h4>
            <div class="pull-right"><span id="icon-collapse-two" style="font-size: x-large; line-height: 0.5;" class="glyphicon glyphicon-collapse-down"></span></div>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse">
            <div class="panel-body">
                <div class="form-group">
                    <label>Title</label>
                    <input type="text" clientidmode="static" id="shippingAttention" class="form-control" runat="server" placeholder="Title" />
                </div>
                <div class="form-group">
                    <label>First name</label>
                    <input type="text" clientidmode="static" id="shippingFirstName" class="form-control" runat="server" placeholder="First name" required />
                </div>
                <div class="form-group">
                    <label>Last name</label>
                    <input type="text" clientidmode="static" id="shippingLastName" class="form-control" runat="server" placeholder="Last name" required />
                </div>
                <div class="form-group">
                    <label>E-mail</label>
                    <input type="email" clientidmode="static" id="shippingEmail" class="form-control" runat="server" placeholder="Email" required />
                </div>
                <div class="form-group">
                    <label>Company</label>
                    <input type="text" clientidmode="static" id="shippingCompany" class="form-control" runat="server" placeholder="Company" />
                </div>
                <div class="form-group">
                    <label>House</label>
                    <input type="text" clientidmode="static" id="shippingStreet" class="form-control" runat="server" placeholder="House" required />
                </div>
                <div class="form-group">
                    <label>Street</label>
                    <input type="text" clientidmode="static" id="shippingStreetTwo" class="form-control" runat="server" placeholder="Street" />
                </div>
                <div class="form-group">
                    <label>City</label>
                    <input type="text" clientidmode="static" id="shippingCity" class="form-control" runat="server" placeholder="City" />
                </div>
                <div class="form-group">
                    <label>Postal code</label>
                    <input type="text" clientidmode="static" id="shippingPostalCode" class="form-control" runat="server" placeholder="Postal code" required />
                </div>
                <div class="form-group">
                    <label>Country</label>
                    <asp:DropDownList ID="shippingCountry" CssClass="form-control" runat="server" DataTextField="Name" DataValueField="CountryId"></asp:DropDownList>
                </div>
                <div class="form-group">
                    <label>Phone</label>
                    <input type="text" clientidmode="static" id="shippingPhone" class="form-control" runat="server" placeholder="Phone" />
                </div>
                <div class="form-group">
                    <label>Mobile phone</label>
                    <input type="text" clientidmode="static" id="shippingMobile" class="form-control" runat="server" placeholder="Mobile phone" />
                </div>
                <a class="btn btn-large btn-danger btn-arrow-right" href="/cart/">Back</a>
                <asp:Button runat="server" ID="btnBillingAndShippingUpdate" class="pull-right btn btn-large btn-success btn-arrow-right" ClientIDMode="static" Text="Continue to Billing" OnClick="btnBillingAndShippingUpdate_Click" />
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" >
    var hide = document.getElementById("checkHideShipping").checked;

    document.getElementById('checkHideShipping').onclick = function () {
        if (document.getElementById("checkHideShipping").checked) {
            document.getElementById("shippingHolder").style.visibility = "hidden";

            document.getElementById("shippingFirstName").removeAttribute('required');
            document.getElementById("shippingLastName").removeAttribute('required');
            document.getElementById("shippingStreet").removeAttribute('required');
            document.getElementById("shippingEmail").removeAttribute('required');
            document.getElementById("shippingPostalCode").removeAttribute('required');
        }
        else {
            document.getElementById("shippingHolder").style.visibility = "visible";

            document.getElementById("shippingFirstName").attributes['required'] = 'required';
            document.getElementById("shippingFirstName").attributes['required'] = 'required';
            document.getElementById("shippingLastName").attributes['required'] = 'required';
            document.getElementById("shippingStreet").attributes['required'] = 'required';
            document.getElementById("shippingEmail").attributes['required'] = 'required';
            document.getElementById("shippingPostalCode").attributes['required'] = 'required';
        }
    }
</script>