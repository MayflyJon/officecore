﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.Api;

namespace Officecore.Website.layouts.Officecore.Controls.Mobile
{
    public partial class Mobile_Address : System.Web.UI.UserControl
    {
        private code.Service.SiteService siteService = new code.Service.SiteService();

        private void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                var billingAddress = TransactionLibrary.GetBillingInformation();
                var shipmentAddress = TransactionLibrary.GetShippingInformation();
                var countries = TransactionLibrary.GetCountries().OrderBy(x => x.Name);

                billingFirstName.Value = billingAddress.FirstName;
                billingLastName.Value = billingAddress.LastName;
                billingEmail.Value = billingAddress.EmailAddress;
                billingCompany.Value = billingAddress.CompanyName;
                billingAttention.Value = billingAddress.Attention;
                billingStreet.Value = billingAddress.Line1;
                billingStreetTwo.Value = billingAddress.Line2;
                billingCity.Value = billingAddress.City;
                billingPostalCode.Value = billingAddress.PostalCode;
                billingPhone.Value = billingAddress.PhoneNumber;
                billingMobile.Value = billingAddress.MobilePhoneNumber;

                billingCountry.DataSource = countries;
                billingCountry.DataBind();
                siteService.SetSelectedCountry(billingCountry);

                shippingFirstName.Value = shipmentAddress.FirstName;
                shippingLastName.Value = shipmentAddress.LastName;
                shippingEmail.Value = shipmentAddress.EmailAddress;
                shippingCompany.Value = shipmentAddress.CompanyName;
                shippingAttention.Value = shipmentAddress.Attention;
                shippingStreet.Value = shipmentAddress.Line1;
                shippingStreetTwo.Value = shipmentAddress.Line2;
                shippingCity.Value = shipmentAddress.City;
                shippingPostalCode.Value = shipmentAddress.PostalCode;
                shippingPhone.Value = shipmentAddress.PhoneNumber;
                shippingMobile.Value = shipmentAddress.MobilePhoneNumber;

                shippingCountry.DataSource = countries;
                shippingCountry.DataBind();
                siteService.SetSelectedCountry(shippingCountry);

            }
        }
        
        public void EditBillingInformation()
        {
            TransactionLibrary.EditBillingInformation(
                billingFirstName.Value,
                billingLastName.Value,
                billingEmail.Value,
                billingPhone.Value,
                billingMobile.Value,
                billingCompany.Value,
                billingStreet.Value,
                billingStreetTwo.Value,
                billingPostalCode.Value,
                billingCity.Value,
                "",//this is the state, which cant be chosen in the frontend
                billingAttention.Value,
                Int32.Parse(billingCountry.SelectedValue)
            );
        }

        public void btnBillingAndShippingUpdate_Click(object sender, EventArgs e)
        {
            UpdateAddress();
        }

        public void btnBillingUpdate_Click(object sender, EventArgs e)
        {
            UpdateAddress();
        }

        public void btnBillingUpdateBottom_Click(object sender, EventArgs e)
        {
            UpdateAddress();
        }

        private void UpdateAddress()
        {
            EditBillingInformation();

            //if the shipping is the same as billing.
            TransactionLibrary.EditShippingInformation(
                billingFirstName.Value,
                billingLastName.Value,
                billingEmail.Value,
                billingPhone.Value,
                billingMobile.Value,
                billingCompany.Value,
                billingStreet.Value,
                billingStreetTwo.Value,
                billingPostalCode.Value,
                billingCity.Value,
                "",//this is the state, which cant be chosen in the frontend
                billingAttention.Value,
                Int32.Parse(billingCountry.SelectedValue)
            );
            Response.Redirect("/cart/Shipping");
        }
    }
}