﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Mobile-Payment.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Mobile.Mobile_Payment" %>
<br/><br/>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <strong>Payment method</strong>
        </h4>
    </div>
    <div class="panel-body">
        <div id="pPaymentAlert" class="alert alert-danger" runat="server">
            <asp:Literal ID="litAlert" runat="server" />
        </div>
        <div class="btn-group-vertical btn-block" data-toggle="buttons">
            <asp:Repeater runat="server" ID="ListView1">
                <ItemTemplate>
                    <label id="button" class="btn btn-default" onclick="checkshippingclicked(this)">
                        <input type="radio" name="paymentMethod" value="<%# ((System.Web.UI.WebControls.ListItem)(Container.DataItem)).Value %>">
                        <%# ((System.Web.UI.WebControls.ListItem)(Container.DataItem)).Text %>
                    </label>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-body" style="padding:10px!important">
        <a href="/cart/shipping.aspx" class="btn btn-large btn-danger pull-left">Back</a>
        <asp:Button CssClass="btn btn-large btn-success pull-right" ClientIDMode="Static" OnClick="btnContinue_Click" ID="btnContinue" runat="server" Text="Continue to Preview" disabled />
    </div>
</div>

<script type="text/javascript" >
    function checkshippingclicked(label) {
        //Set the class attribute when its clicked
        document.getElementById(label.getAttribute("id")).setAttribute("class", "btn", "btn-success");

        //Remove the disabled attribute
        document.getElementById("btnContinue").removeAttribute("disabled");
    }
</script>