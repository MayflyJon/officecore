﻿using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.Api;
using UCommerce.EntitiesV2;
using UCommerce.Extensions;
using UCommerce.Runtime;
using Officecore.Website.code.Constants;
using Officecore.Website.code.Helpers;
using Officecore.Website.code.Service;

namespace Officecore.Website.layouts.Officecore.Controls.Mobile
{
    public partial class Mobile_Product : System.Web.UI.UserControl
    {
        public Category CurrentCategory { get; set; }
        public code.Model.Product CurrentProduct { get; set; }
        public Boolean ActivateCommerce = true;
        public ProductService ps = new ProductService();

        protected void Page_Load(object sender, EventArgs e)
        {
            ActivateCommerce = ps.ActivateCommerce();
            if (CurrentProduct == null)
            {
                Visible = false;
                return;
            }
            
            //TODO: fix the below
            rating.Text = "";

        }

        public void btnAddToBasket_Click(object sender, EventArgs e)
        {
            TransactionLibrary.AddToBasket(1, CurrentProduct.Sku, CurrentProduct.VariantSku);
            Response.Redirect(Request.RawUrl);
        }
    }
}