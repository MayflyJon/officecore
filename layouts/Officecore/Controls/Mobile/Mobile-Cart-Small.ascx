﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Mobile-Cart-Small.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Mobile.Mobile_Cart_Small" %>
<div class="basket">
    <a href="/cart/" style="border-bottom: none;">
        <img class="cart-image" src="/assets/images/icons/shopping_cart2.png" />
        <asp:Literal ID="litMinicart" runat="server" />
    </a>
</div>



