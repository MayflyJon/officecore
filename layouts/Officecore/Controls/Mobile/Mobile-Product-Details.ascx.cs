﻿using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Resources.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.Api;
using UCommerce.EntitiesV2;
using UCommerce.Runtime;
using Officecore.Website.code.Helpers;
using Officecore.Website.code.DataRepositories;
using Officecore.Website.code.Service;

namespace Officecore.Website.layouts.Officecore.Controls.Mobile
{
    public partial class Mobile_Product_Details : System.Web.UI.UserControl
    {
        public code.Model.Product CurrentProduct { get; set; }
        protected ProductService ps = new ProductService();
        public Boolean ActivateCommerce = true;

        private void Page_Load(object sender, EventArgs e)
        {
            ActivateCommerce = ps.ActivateCommerce();
            if (ActivateCommerce)
            {
                CurrentProduct = new code.Model.Product(SiteContext.Current.CatalogContext.CurrentProduct);
            }
            else
            {
                CurrentProduct = new code.Model.Product(Sitecore.Context.Item);
            }

            shippingHeading.Text = DictionaryHelper.GetText("Shipping tab heading");
            ratingsHeading.Text = DictionaryHelper.GetText("Ratings tab heading");
            ratingsRepeater.DataSource = CurrentProduct.ProductReviews;
            ratingsRepeater.DataBind();
        }

        public void btnAddToBasket_Click(object sender, EventArgs e)
        {
            var variant = ps.GetVariantFromPostData(SiteContext.Current.CatalogContext.CurrentProduct, "variation-");

            if (variant == null)
            {
                return;
            }
            TransactionLibrary.AddToBasket(1, variant.Sku, variant.VariantSku);
            Response.Redirect(Request.RawUrl);
        }        
    }
}