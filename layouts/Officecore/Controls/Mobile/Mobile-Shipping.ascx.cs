﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce;
using UCommerce.Api;
using UCommerce.EntitiesV2;

namespace Officecore.Website.layouts.Officecore.Controls.Mobile
{
    public partial class Mobile_Shipping : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                return;
            }

            var currentShippingMethod = new ShippingMethod();
            var currentBasket = new PurchaseOrder();
            var shippingCountry = new Country();
            var availableShippingMethods = new List<ShippingMethod>();

            try
            {
                currentShippingMethod = TransactionLibrary.GetShippingMethod();
                currentBasket = TransactionLibrary.GetBasket().PurchaseOrder;
                shippingCountry = TransactionLibrary.GetShippingInformation().Country;
                availableShippingMethods = TransactionLibrary.GetShippingMethods(shippingCountry).ToList();
            }
            catch (InvalidOperationException)
            {
                Response.Redirect("/cart/address");
            }

            if (availableShippingMethods.Count != 0)
            {
                pPaymentAlert.Visible = false;
            }
            else
            {
                string warning =
                    "WARNING: No payment methods have been configured for " + shippingCountry.Name + " please contact your system administrator.";
                litAlert.Text = warning;
                btnUpdateShipment.Visible = false;
            }

            List<ListItem> asm = new List<ListItem>();
            foreach (ShippingMethod shippingMethod in availableShippingMethods)
            {
                
                var price = shippingMethod.GetPriceForCurrency(currentBasket.BillingCurrency);
                var formattedPrice = new Money((price == null ? 0 : price.Price), currentBasket.BillingCurrency);

                ListItem currentListItem = new ListItem(shippingMethod.Name + "<text>(</text>" + formattedPrice + "<text>)</text> ", shippingMethod.Id.ToString());

                if (currentShippingMethod.Id == shippingMethod.Id)
                {
                    currentListItem.Selected = true;
                }
                asm.Add(currentListItem);
                
            }
            ListView1.DataSource = asm;
            ListView1.DataBind();
        }

        public void btnUpdateShipment_Click(object sender, EventArgs e)
        {

            int shippingMethodId = 0;
            Int32.TryParse(Request.Form.Get("shippingMethod"), out shippingMethodId);
            
            TransactionLibrary.CreateShipment(shippingMethodId, overwriteExisting: true);
            TransactionLibrary.ExecuteBasketPipeline();
            HttpContext.Current.Response.Redirect("/cart/Payment");
        }

        protected string GetItemActive(int itemIndex)
        {
            if (itemIndex == 0)
            {
                return "";
            }
            return "";
        }
    }
}