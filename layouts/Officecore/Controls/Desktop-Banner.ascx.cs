﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Officecore.Website.code.Controls;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using Sitecore.Web.UI.WebControls;

namespace Officecore.Website.layouts.Officecore.Controls
{
    public partial class Desktop_Banner : DatasourceControl
    {
        public string BannerImgUrl { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            BannerHeader.Item = this.Item;
            BannerSlogan.Item = this.Item;
            BannerLink.Item = this.Item;
            BannerHeaderLive.Item = this.Item;
            BannerSloganLive.Item = this.Item;
            BannerLinkLive.Item = this.Item;

            if (Sitecore.Context.PageMode.IsPageEditorEditing)
            {
                BannerImage.Item = this.Item;
            }
            else
            { 
                BannerImgUrl = GetImageURL(this.Item, "Banner Image");  
            }
        }

        
        
    }
}