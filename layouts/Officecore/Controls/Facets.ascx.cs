﻿namespace Officecore.Website.layouts.Officecore.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI.WebControls;
    using Sitecore.ContentSearch.Linq;
    using Sitecore.Data;
    using Sitecore.Text;

    public partial class Facets : System.Web.UI.UserControl
    {
        private UrlString urlString;

        private ID selectedID;

        private void Page_PreRender(object sender, EventArgs e)
        {
            var facetCategories = Sitecore.Context.Items["oc_FacetCategories"] as List<FacetCategory>;
            if (facetCategories == null)
            {
                this.Visible = false;
                return;
            }

            this.urlString = new UrlString(this.Request.RawUrl);
            Sitecore.Data.ID.TryParse(this.Request.QueryString["templateID"], out this.selectedID);

            this.rpt.DataSource = facetCategories[0].Values;
            this.rpt.DataBind();
        }

        protected void rpt_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var facetValue = e.Item.DataItem as FacetValue;
            if (facetValue == null) return;

            ShortID shortID;
            if (!ShortID.TryParse(facetValue.Name, out shortID)) return;

            var id = shortID.ToID();
            var item = Sitecore.Context.Database.GetItem(id);

            if (item == null) return;

            var lnk = (HyperLink)e.Item.FindControl("lnk");

            var displayName = item.DisplayName;
            {
                var arr = displayName.Split(new[] { " - " }, StringSplitOptions.None);
                if (arr.Length > 1)
                {
                    displayName = arr[0];
                }
            }

            lnk.Text = string.Format("{0} ({1})", displayName, facetValue.AggregateCount);

            if (Equals(this.selectedID, id))
            {
                lnk.CssClass += " selected";
                lnk.NavigateUrl = this.urlString.Remove("templateID");
            }
            else
            {
                lnk.NavigateUrl = this.urlString.Add("templateID", id.ToString());
            }
        }
    }
}