﻿<%@ control language="C#" autoeventwireup="true" codebehind="BlogCommentList.ascx.cs" inherits="Officecore.Website.layouts.Officecore.Controls.BlogCommentList" enableviewstate="false" %>
<div class="row">
    <div class="col-md-12" style="margin-top: 15px;">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Comments</h3>
            </div>
            <div class="panel-body" style="padding: 15px !important">
                <asp:Panel runat="server" ID="pnlNoComments" Visible="False">
                    No comments yet. Be the first to post!
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlComments" Visible="False">
                    <asp:Repeater runat="server" ID="rptComments" ItemType="Sitecore.Data.Items.Item">
                        <ItemTemplate>
                            <div style="margin-bottom: 15px;">
                                <div><strong><%#Item["Name"]%></strong> - <%#Item["Email"]%></div>
                                <div style="color: #aaaaaa"><%#Item.Statistics.Created.ToString("f")%></div>
                                <div style="margin-top: 5px;">
                                    <%#nl2br(Item["Comment"])%>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </asp:Panel>
            </div>
        </div>
    </div>
</div>