﻿<%@ control language="C#" autoeventwireup="true" codebehind="MyInterests.ascx.cs" inherits="Officecore.Website.layouts.Officecore.Controls.MyInterests" %>
<%@ import namespace="Officecore.Website.code.Helpers" %>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><%=TranslationHelper.Translate("AssignRoles/Heading")%></h3>
    </div>
    <div class="panel-body" style="padding: 15px !important">
        <asp:panel runat="server" cssclass="form-horizontal" role="form" defaultbutton="btnSubmit">
            <asp:Repeater runat="server" ID="rpt" OnItemDataBound="DataBound" ItemType="Sitecore.Data.Items.Item">
                <ItemTemplate>
                    <div class="form-group">
                        <label for="<%#Container.FindControl("chk").ClientID%>" class="col-sm-2 control-label"><%#Item.DisplayName%></label>
                        <div class="col-sm-10">
                            <asp:HiddenField runat="server" ID="hid" Visible="False" />
                            <asp:CheckBox runat="server" ID="chk" />
                        </div>
                    </div>                               
                </ItemTemplate>
            </asp:Repeater>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <asp:Button runat="server" ID="btnSubmit" CssClass="btn btn-default" Text="Save changes" OnClick="btnSubmit_Click" />
                </div>
            </div>
        </asp:panel>
    </div>
</div>