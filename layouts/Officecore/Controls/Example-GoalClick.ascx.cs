﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Analytics.Data;

namespace Officecore.Website.layouts.Officecore.Controls
{
    public partial class Example_GoalClick : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void registerevent_Click(object sender, EventArgs e)
        {
            // This checks if we are on the live site and not using page editor. 
            // Also checks to ensure we are only recording on pages and not non-webpaged items
            if (Sitecore.Analytics.Tracker.IsActive && Sitecore.Analytics.Tracker.CurrentPage != null)
            {
                var goalName = "Login"; 
                var pageEventData = new PageEventData(goalName)
                {
                    // You can specify the current page ID here. Sitecore.Context.Item.ID.ToGuid()
                    ItemId = Sitecore.Context.Item.ID.ToGuid(),

                    // This is the Goal name, identical to above.
                    DataKey = goalName,

                    Data = Sitecore.Context.Item.Paths.ContentPath,
                    Text = Sitecore.Context.Item.Name

                };
                Sitecore.Analytics.Tracker.CurrentPage.Register(pageEventData);  
            }
        }
    }
}