﻿namespace Officecore.Website.layouts.Officecore.Controls
{
    using System;
    using System.Linq;

    using global::Officecore.Website.code.Search;

    using Sitecore.ContentSearch;
    using Sitecore.Data;

    public partial class BlogCommentList : System.Web.UI.UserControl
    {
        protected string nl2br(string str)
        {
            return str.Replace("\r\n", "\n").Replace("\r", "\n").Replace("\n", "<br/>\r\n");
        }

        private void Page_PreRender(object sender, EventArgs e)
        {
            using (var context = ContentSearchManager.GetIndex("sitecore_master_index").CreateSearchContext())
            {
                this.rptComments.DataSource = context.GetQueryable<CommentResultItem>()
                    .Where(item => item.TemplateId == new ID("{753A6E1D-940E-4C37-AEBB-C095C33E38A0}"))
                    .Where(item => item.BlogPost == Sitecore.Context.Item.ID)
                    .Where(item => item.Name != "__Standard Values")
                    .Where(item => item.Language == Sitecore.Context.Language.Name)
                    .Where(item => item.IsLatestVersion)
                    .OrderBy(f => f.CreatedDate)
                    .Select(item => item.GetItem())
                    .ToList();

                this.rptComments.DataBind();
            }

            if (this.rptComments.Items.Count == 0)
            {
                this.pnlNoComments.Visible = true;
            }
            else
            {
                this.pnlComments.Visible = true;
            }
        }
    }
}