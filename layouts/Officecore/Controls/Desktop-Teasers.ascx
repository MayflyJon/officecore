﻿<%@ control language="C#" autoeventwireup="true" codebehind="Desktop-Teasers.ascx.cs" inherits="Officecore.Website.layouts.Officecore.Controls.Desktop_Teasers" %>
<asp:repeater runat="server" id="Repeater1" itemtype="Sitecore.Data.Items.Item">
    <HeaderTemplate>
        <div class="teaser-group">
    </HeaderTemplate>
    <ItemTemplate>
        <div class="teaser">
            <sc:Link field="Teaser Link" runat="server" Item="<%# Item %>">
                <sc:Image runat="server" bc="white" as="1" field="teaser image" Item="<%# Item %>" CssClass="teaser-image" Width="260" Height="173"  />
            </sc:Link>
            <div>   
                <h4><sc:text field="Teaser Title" id="TeaserTitle" Item="<%# Item %>" runat="server" /></h4>
                <p><sc:text field="teaser abstract" Item="<%# Item %>" runat="server" /></p>
            </div>
        </div>
    </ItemTemplate>
    <FooterTemplate>
        <div class="clearfix"></div>
        </div>
    </FooterTemplate>
</asp:repeater>
<div class="clearfix"></div>