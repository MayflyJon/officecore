﻿using System;
using System.Collections.Generic;
using Officecore.Website.code.Controls;
using Officecore.Website.code.Constants;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.Linq.Utilities;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Web;

namespace Officecore.Website.layouts.Officecore.Controls
{
    using System.Web.UI.WebControls;

    using global::Officecore.Website.code.DataRepositories;
    using global::Officecore.Website.code.Search;

    using Sitecore.Resources.Media;

    public partial class Desktop_KB_Articles : DatasourceControl
    {
        public Item ReadMore = null;
        public HashSet<ID> SearchTemplates = new HashSet<ID>();
        public string SearchTerm = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            SearchTemplates.Add(new ID("{23CFC180-AF56-4F12-B1DB-F26534BCD218}"));
            SearchTemplates.Add(new ID("{0418A650-AE57-4DD3-BDB4-CF8E6C7F89BA}"));

            ReadMore = Sitecore.Context.Database.GetItem(new ID(DataRepositoryConstants.READ_MORE_TEXT));
            SearchTerm = this.Item.ID.ToShortID().ToString();

            var helper = new SearchHelper();

            Dictionary<string, string> facets = new Dictionary<string, string>();
            var querystring = new Sitecore.Text.UrlString(WebUtil.GetRawUrl());
            foreach (var parameter in querystring.Parameters.AllKeys)
            {
                if (!string.Equals("searchStr", parameter, StringComparison.OrdinalIgnoreCase))
                {
                    if (!parameter.StartsWith("sc_"))
                    {
                        facets.Add(parameter, querystring.Parameters[parameter]);
                    }
                }
            }

            List<FacetCategory> _facets;
            int numResults;

            // Do the base predicate
            var predicate = PredicateBuilder.True<TagsResultItem>();
            predicate = predicate.And(item => item.IsLatestVersion);
            
            // Check if we need to search by SearchTerm
            if (Sitecore.Context.Item.ID != this.Item.ID)
            {
                predicate = predicate.And(item => item.Terms.Contains(SearchTerm));
            }

            // Now check the roles
            predicate = predicate.And(SearchHelper.GetInterestsPredicate());

            var results = helper.SearchWithFacets(facets, SearchTemplates, out _facets, out numResults, predicate, item => item.DateRange);

            var list = new List<Item>();

            foreach (var item in results)
            {
                list.Add(item.GetItem());
            }

            News.DataSource = list;
            News.DataBind();
        }

        protected void News_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var item = e.Item.DataItem as Item;
            if (item == null) return;

            var img = (Image)e.Item.FindControl("img");
            var imageField = (Sitecore.Data.Fields.ImageField)item.Fields["Image"];
            if (imageField == null || imageField.MediaItem == null)
            {
                img.Visible = false;
                return;
            }

            img.ImageUrl = MediaManager.GetMediaUrl(imageField.MediaItem, new MediaUrlOptions(100, 100, false)) + "&crop=1";
        }
    }
}