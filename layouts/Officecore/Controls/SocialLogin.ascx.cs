﻿namespace Officecore.Website.layouts.Officecore.Controls
{
    using System;
    using System.Web.Security;

    using code.Helpers;
    using code.Service;

    using Sitecore.Data;
    using Sitecore.Web;

    public partial class SocialLogin : System.Web.UI.UserControl
    {
        ID loginGoal, registerGoal;

        private void Page_Load(object sender, EventArgs e)
        {
            var properties = WebUtil.ParseUrlParameters(Attributes["sc_parameters"]);
            Sitecore.Data.ID.TryParse(properties["LoginGoal"], out loginGoal);
            Sitecore.Data.ID.TryParse(properties["RegisterGoal"], out registerGoal);

            this.DoLoginMessage();

            if (Sitecore.Context.IsLoggedIn)
            {
                pnlForms.Visible = !Sitecore.Context.PageMode.IsNormal;
            }
            else
            {
                pnlForms.Visible = true;
            }
        }

        private void DoLoginMessage()
        {
            pnlAccessError.Visible = false;

            if (!this.IsPostBack)
            {
                if (string.Equals(this.Request.QueryString["oc_loginError"], "1"))
                {
                    this.litAccessErrorTitle.Text = TranslationHelper.Translate("SocialLogin/AccessDeniedTitle");
                    this.litAccessErrorBody.Text = TranslationHelper.Translate("SocialLogin/AccessDeniedBody");
                    pnlAccessError.Visible = true;
                }
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (this.txtUsername.Text == string.Empty || this.txtPassword.Text == string.Empty)
            {
                this.pnlLoginFailed.Visible = true;
                this.litLoginFailed.Text = TranslationHelper.Translate("SocialLogin/LoginErrorEnterAllDetails");
                return;
            }

            if (!DoLogin(this.txtUsername.Text.Trim(), this.txtPassword.Text.Trim()))
            {
                this.litLoginFailed.Text = TranslationHelper.Translate("SocialLogin/LoginFailed");
                this.pnlLoginFailed.Visible = true;
                return;
            }

            AnalyticsHelper.TriggerGoal(loginGoal);
            new CustomerService().FixAutomationStates();

            WebUtil.ReloadPage();
        }

        private static bool DoLogin(string username, string password)
        {
            username = string.Format(@"{0}\{1}", Sitecore.Context.Domain.Name, username);
            return Sitecore.Security.Authentication.AuthenticationManager.Login(username, password);
        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.txtRegisterUsername.Text)
                || string.IsNullOrWhiteSpace(this.txtRegisterPassword.Text)
                || string.IsNullOrWhiteSpace(this.txtRegisterEmail.Text))
            {
                this.pnlRegisterFailed.Visible = true;
                this.litRegisterFailed.Text = TranslationHelper.Translate("SocialLogin/RegisterErrorEnterAllDetails");
                return;
            }

            var username = string.Format(@"{0}\{1}", Sitecore.Context.Domain.Name, this.txtRegisterUsername.Text.Trim());
            var password = this.txtRegisterPassword.Text.Trim();
            var email = this.txtRegisterEmail.Text.Trim();

            if (Sitecore.Security.Accounts.User.Exists(username))
            {
                this.pnlRegisterFailed.Visible = true;
                this.litRegisterFailed.Text = TranslationHelper.Translate("SocialLogin/RegisterErrorUsernameExists");
                return;
            }

            if (Membership.FindUsersByEmail(email).Count > 0)
            {
                this.pnlRegisterFailed.Visible = true;
                this.litRegisterFailed.Text = TranslationHelper.Translate("SocialLogin/RegisterErrorEmailExists");
                return;
            }

            var user = Sitecore.Security.Accounts.User.Create(username, password);
            user.Profile.Email = email;
            user.Profile.Save();

            AnalyticsHelper.TriggerGoal(registerGoal);
            new CustomerService().FixAutomationStates();

            Sitecore.Security.Authentication.AuthenticationManager.Login(user);
            WebUtil.ReloadPage();
        }
    }
}