﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Desktop-Toolbox.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Desktop_Toolbox" %>

<!-- Toolbox  -->
<div id="container-languages">
    <asp:Repeater ID="Toolbox" runat="server" ItemType="Sitecore.Data.Items.Item">
        <ItemTemplate>
            <div class="language">
                <div style="float: left;">
                    <a href="<%# Item["Alternative Link"] %>" class="<%# Item["CSS Class"] %>">
                        <sc:image runat="server" item="<%# Item %>" field="Toolbox Image" disablewebediting="true" />
                    </a>
                </div>
                <div style="float: left;">
                    <a href="<%# Item["Alternative Link"] %>" class="<%# Item["CSS Class"] %>">
                        <sc:text field="Toolbox Title" runat="server" item='<%# Item %>' />
                    </a>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
</div>
