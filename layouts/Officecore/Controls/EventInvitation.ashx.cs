﻿namespace Officecore.Website.layouts.Officecore.Controls
{
    using System;
    using System.Text;
    using System.Web;
    using DDay.iCal;
    using DDay.iCal.Serialization;
    using Sitecore.Data;
    using Sitecore.Data.Fields;
    using Sitecore.Links;

    public class EventInvitation : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            ID id;
            if (!ID.TryParse(context.Request.QueryString["id"], out id))
            {
                context.Response.StatusCode = 500;
                context.Response.StatusDescription = "ID missing";
                context.Response.End();
                return;
            }

            var item = Sitecore.Context.Database.GetItem(id);
            if (item == null)
            {
                context.Response.StatusCode = 500;
                context.Response.StatusDescription = "Item not found";
                context.Response.End();
                return;
            }

            var iCal = new iCalendar();

            // Create the event, and add it to the iCalendar
            var evt = iCal.Create<Event>();

            // Set information about the event
            evt.IsAllDay = true;
            evt.Start = new iCalDateTime(((DateField)item.Fields["Date"]).DateTime);
            evt.Summary = item["Title"];
            evt.Description = item["Description"];

            var urlOptions = LinkManager.GetDefaultUrlOptions();
            urlOptions.AlwaysIncludeServerUrl = true;

            evt.Url = new Uri(LinkManager.GetItemUrl(item, urlOptions));

            var ctx = new SerializationContext();
            var factory = new DDay.iCal.Serialization.iCalendar.SerializerFactory();
            
            // Get a serializer for our object
            var serializer = factory.Build(iCal.GetType(), ctx) as IStringSerializer;
            if (serializer == null)
            {
                context.Response.StatusCode = 500;
                context.Response.StatusDescription = "Serializer is null";
                context.Response.End();
                return;
            }

            context.Response.ContentType = "text/calendar";
            context.Response.AddHeader("Content-Disposition", "attachment; filename=event.ics");
            serializer.Serialize(iCal, context.Response.OutputStream, Encoding.UTF8);
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}