﻿using System;
using System.Linq;
using System.Collections.Generic;
using Officecore.Website.code.Constants;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Utilities;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.Data;
using Sitecore.Data.Items;
using System.Web.UI.WebControls;
using Sitecore.Links;
using Sitecore.Web;
using Sitecore.ContentSearch.Linq.Utilities;
using System.Collections.Specialized;

namespace Officecore.Website.layouts.Officecore.Controls
{
    public partial class Search_Results : System.Web.UI.UserControl
    {
        private string _searchTerm;
        public string SearchTerm
        {
            get
            {
                if (_searchTerm == null)
                {
                    return string.Empty;
                }
                return _searchTerm;
            }
            set { _searchTerm = value; }
        }

        private string _searchMsg;
        public string SearchMsg
        {
            get
            {
                if (_searchMsg == null)
                {
                    return string.Empty;
                }
                return _searchMsg;
            }
            set { _searchMsg = value; }
        }

        private static readonly string templateProduct = "{F62FEE2A-4D52-4360-828B-FFAD730E7F67}";
        private static readonly string templateService = "{C0A77BC0-4B1B-497B-919B-263CCCA0F788}";
        private static readonly string templateNews1 = "{34A5EDED-6BD2-4DF5-ADDB-163F4655BEC7}";
        private static readonly string templateNews2 = "{D89BDC0E-FF2B-4AA9-BA39-8F863A6056E9}";
        private static readonly string templateNews3 = "{74A07E7C-B826-4B23-B87C-11398E9D6FEF}";
        private static readonly string templatePeople = "{F1FA70C4-301B-451D-BCF2-ABDBEFD0CED9}";
        private static readonly string templateContact = "{F1FA70C4-301B-451D-BCF2-ABDBEFD0CED9}";
        private static readonly string templateAbout = "{39E0C4D8-BCF0-4DC6-9141-3AEB255B0A55}";
        private static readonly string templateSection = "{B2477E15-F54E-4DA1-B09D-825FF4D13F1D}";
        private static readonly string templateProductCategory = "{8A471A2C-F885-416F-826E-5CD3272CDBC4}";
        private static readonly string templateServicesCategory = "{373CAA7B-5698-4E20-AA90-7698C4CE81EA}";

        public HashSet<ID> SearchTemplates = new HashSet<ID> { new ID(templateProduct), new ID(templateService), new ID(templateNews1), new ID(templateNews2), new ID(templateNews3), new ID(templatePeople), new ID(templateContact), new ID(templateAbout), new ID(templateSection), new ID(templateProductCategory), new ID(templateServicesCategory) };

        private void Page_Load(object sender, EventArgs e)
        {
            SearchTerm = Server.UrlDecode(WebUtil.GetQueryString("searchStr"));

            Dictionary<string, string> facets = new Dictionary<string, string>();
            var querystring = new Sitecore.Text.UrlString(WebUtil.GetRawUrl());
            foreach (var parameter in querystring.Parameters.AllKeys)
            {
                if (!string.Equals("searchStr", parameter, StringComparison.OrdinalIgnoreCase))
                {
                    facets.Add(parameter, querystring.Parameters[parameter]);
                }
            }

            List<FacetCategory> _facets;
            string _facetList = string.Empty;
            int numResults = 0;

            var helper = new code.DataRepositories.SearchHelper();

            srchResults.DataSource = helper.SearchWithFacets<code.Model.ResultItem>(facets, SearchTemplates, out _facets, out numResults,
                                                                                item => item.Title.Contains(SearchTerm)
                                                                                            ||
                                                                                            item.Text.Contains(SearchTerm)
                                                                                            ||
                                                                                            item.Abstract.Contains(SearchTerm)
                                                                                            ||
                                                                                            item.Description.Contains(SearchTerm)
                                                                                            ||
                                                                                            item.Keywords.Contains(SearchTerm)
                                                                                            ||
                                                                                            item.DefinedKeywords.Contains(SearchTerm),
                                                                                item => item.DateRange);

            srchResults.DataBind();

            SearchMsg = numResults.ToString() + " results returned";

            if (_facets != null)
            {
                foreach (var itemfacet in _facets)
                {
                    _facetList += "<li><strong>" + itemfacet.Name + "</strong></li>";
                    foreach (var f in itemfacet.Values)
                    {
                        if (f.AggregateCount > 0)
                        {
                            querystring.Remove(itemfacet.Name);
                            querystring.Add(itemfacet.Name, f.Name);

                            _facetList += "<li><a href='" + querystring + "'>" + f.Name + " (" + f.AggregateCount + ")</a></li>";
                        }
                    }
                }
            }

            _facetList = "<ul id='search-facets'>" + _facetList + "</ul>";
            Facets.Text = _facetList;
        }
    }

    
}