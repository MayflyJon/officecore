﻿using Officecore.Website.code.Controls;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Officecore.Website.layouts.Officecore.Controls
{
    public partial class Desktop_Render_News_Article : ContentDetails
    {
        protected Item Author { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            Author = ((Sitecore.Data.Fields.LookupField)this.Item.Fields["Author"]).TargetItem;
            DataBind();
        }
    }
}