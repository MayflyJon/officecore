﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Desktop-Menu.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Desktop_Menu" %>

<ul>
    <asp:Repeater ID="MenuRepeater" runat="server" ItemType="Sitecore.Data.Items.Item">
        <ItemTemplate>
            <li>
                <sc:Link Field="Link" runat="server" Item="<%# Item %>" Parameters="disable-web-editing=true" >
                    <sc:Text Field="Link Text" runat="server" Item="<%# Item %>" />
                </sc:Link>
                <asp:Repeater ID="InnerMenuRepeater" runat="server" ItemType="Sitecore.Data.Items.Item" DataSource="<%# GetSubItems(Item) %>" OnPreRender="Repeater_PreRender" >
                    <HeaderTemplate>
                        <ul>
                    </HeaderTemplate>
                    
                    <ItemTemplate>
                        
                        <li>
                            <sc:Link Field="Link" runat="server" Item="<%# Item %>">
                                <sc:Text Field="Link Text" runat="server" Item="<%# Item %>" />
                            </sc:Link>
                        </li>
                        
                    </ItemTemplate>
                        
                    <FooterTemplate>
                        </ul>
                    </FooterTemplate>
                </asp:Repeater>
            </li>
        </ItemTemplate>
    </asp:Repeater>
</ul>