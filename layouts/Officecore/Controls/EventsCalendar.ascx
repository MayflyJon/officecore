﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EventsCalendar.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.EventsCalendar" %>

<link rel="stylesheet" href="/assets/eventCalendar/css/eventCalendar.css">
<link rel="stylesheet" href="/assets/eventCalendar/css/eventCalendar_theme_responsive.css">
<script src="/assets/eventCalendar/js/jquery.eventCalendar.js" type="text/javascript"></script>
<style type="text/css">
    time > small {
        display: none;
    }
</style>

<div id="eventsCalendar"></div>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('#eventsCalendar').eventCalendar({
            jsonData: <%=EventsJson%>,
            jsonDateFormat: 'human',
            eventsLimit: 3
        });
    });
</script>