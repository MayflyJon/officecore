﻿using Officecore.Website.code.Constants;
using Officecore.Website.code.Controls;
using Officecore.Website.code.DataRepositories;
using Officecore.Website.code.Helpers;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Officecore.Website.layouts.Officecore.Controls
{
    public partial class Desktop_Menu : DatasourceControl
    {
        private readonly ItemRepositoryBase _menus = new ItemRepositoryBase();

        protected void Page_Load(object sender, EventArgs e)
        {
            MenuRepeater.DataSource = _menus.GetMainMenu(DataRepositoryConstants.MENU).ToList();
            MenuRepeater.DataBind();
        }

        protected IEnumerable<Item> GetSubItems(Item item)
        {
            return _menus.GetMainMenu(item).ToList();
        }

        protected void Repeater_PreRender(object sender, EventArgs e)
        {
            if (((Repeater)sender).Items.Count == 0)
            {
                ((Repeater)sender).Visible = false;
            }
        }
    }
}