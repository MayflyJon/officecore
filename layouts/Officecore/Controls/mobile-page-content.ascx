﻿<%@ control language="C#" autoeventwireup="true" codebehind="mobile_page_content.ascx.cs" inherits="Officecore.Website.layouts.Officecore.Controls.mobile_page_content" %>
<%@ register tagprefix="sc" namespace="Sitecore.Web.UI.WebControls" assembly="Sitecore.Kernel" %>
<div style="margin: 0px 5px;">
    <sc:image field="Image" runat="server" maxheight="90" cssstyle="float:right;padding:10px;" />
    <h2>
        <sc:text field="Title" runat="server" />
    </h2>
    <p>
        <sc:text field="Text" runat="server" />
    </p>
</div>
