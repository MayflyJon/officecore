﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Officecore.Website.code.Controls;
using Officecore.Website.code.Service;
using Officecore.Website.code.Constants;
using Sitecore.Data;

namespace Officecore.Website.layouts.Officecore.Controls
{
    public partial class Desktop_Header : DatasourceControl
    {
        ProductService p = new ProductService();
        public bool toggleState = true;


        protected void Page_Load(object sender, EventArgs e)
        {
            toggleState = p.ActivateCommerce();
            
            Sitecore.Data.Database context = Sitecore.Context.Database;
            Sitecore.Data.Items.Item item = context.GetItem(DataRepositoryConstants.SITE_ROOT);

            Logo.Item = item;
        }

    }
}