﻿namespace Officecore.Website.layouts.Officecore.Controls
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;

    using Facebook;

    using global::Officecore.Website.code.DataRepositories;
    using global::Officecore.Website.code.Extensions;
    using global::Officecore.Website.code.Helpers;
    using global::Officecore.Website.code.Search;

    using Sitecore.ContentSearch;
    using Sitecore.ContentSearch.Linq;
    using Sitecore.ContentSearch.Linq.Utilities;
    using Sitecore.Data;
    using Sitecore.Data.Fields;
    using Sitecore.Data.Items;
    using Sitecore.Diagnostics;
    using Sitecore.Links;
    using Sitecore.Resources.Media;
    using Sitecore.SecurityModel;

    using TweetSharp;

    public partial class SocialMatrix : System.Web.UI.UserControl
    {
        protected class SocialMatrixItem
        {
            public string Title { get; set; }
            public string Link { get; set; }
            public string Image { get; set; }
            public string Date { get; set; }
            public string Author { get; set; }
        }

        protected List<SocialMatrixItem> Tweets = new List<SocialMatrixItem>();
        protected List<SocialMatrixItem> MatrixItems = new List<SocialMatrixItem>();

        protected string SingleCssClass;

        protected string DoubleCssClass;

        protected string SingleImageParams;

        protected string DoubleImageParams;

        protected string RowClass;

        private void Page_Load(object sender, EventArgs e)
        {
            if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                this.Visible = false;
                return;
            }

            var dataSourceString = this.Attributes["sc_datasource"];
            if (string.IsNullOrEmpty(dataSourceString))
            {
                this.Visible = false;
                return;
            }

            var dataSource = Sitecore.Context.Database.GetItem(dataSourceString);
            if (dataSource == null)
            {
                this.Visible = false;
                return;
            }

            if (Sitecore.Context.Device != null && Sitecore.Context.Device.InnerItem != null && Sitecore.Context.Device.InnerItem.Name == "iPhone")
            {
                this.SingleCssClass = "col-md-4";
                this.DoubleCssClass = "col-md-4";
                this.SingleImageParams = "?w=340&amp;h=240&amp;crop=1";
                this.DoubleImageParams = "?w=340&amp;h=240&amp;crop=1";
                this.RowClass = "row";
            }
            else
            {
                this.SingleCssClass = "social-matrix-single";
                this.DoubleCssClass = "social-matrix-double";
                this.SingleImageParams = "?w=292&amp;h=292&amp;crop=1";
                this.DoubleImageParams = "?w=584&amp;h=292&amp;crop=1";
                this.RowClass = string.Empty;
            }

            var searchTweets = dataSource["Tweets"];
            var searchEvents = dataSource["Events"];
            var searchKB = dataSource["KnowledgeBase"];
            var searchBlogs = dataSource["BlogPosts"];
            var searchNews = dataSource["News"];

            if (!this.DoTweets(searchTweets))
            {
                this.Visible = false;
                return;
            }

            using (new SecurityDisabler())
            {
                var searchindex = SearchHelper.GetSearchIndex();

                this.DoFacebook();
                this.DoBlogs(searchindex, searchBlogs);
                this.DoNews(searchindex, searchNews);
                this.DoEvents(searchindex, searchEvents);
                this.DoKB(searchindex, searchKB);

                if (this.MatrixItems.Count < 4)
                {
                    this.Visible = false;
                    return;
                }
            }

            this.MatrixItems = this.MatrixItems.TakeRandom(4);
        }

        private bool DoTweets(string searchTweets)
        {
            var service = TwitterHelper.GetAuthenticatedService();

            var tweets = service.Search(new SearchOptions
            {
                Q = searchTweets,
                IncludeEntities = true,
                Lang = "en",
                Count = 20
            });

            if (tweets != null)
            {
                try
                {
                    foreach (var tweet in tweets.Statuses)
                    {
                        this.Tweets.Add(new SocialMatrixItem
                        {
                            Link = string.Format("https://www.twitter.com/{0}/status/{1}", tweet.Author.ScreenName, tweet.Id),
                            Title = tweet.Text,
                            Date = tweet.CreatedDate.ToString("MMMM dd, yyyy")
                        });
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("Tweet polling failed", ex, this);
                }
            }

            if (this.Tweets.Count < 6)
            {
                return false;
            }

            this.Tweets = this.Tweets.TakeRandom(6);
            return true;
        }

        private void DoFacebook()
        {
            var masterDb = Sitecore.Configuration.Factory.GetDatabase("master");

            var client = new FacebookClient
            {
                AccessToken = "CAADqu3XjxJ8BAGJ9RTSOUXNye1QpSmzHqSHIPFcO4N9mYLBSTcedZAZAZBWnRPRMvWmeNwg5RTqSGzpiBfgDaUWzJ9HA7SdaDwVSaHZAZBvOkTObeHJZBsgGShTi8cfIAt9NZAgwxeNQ7GNfmWkRobawKRT6yfHPrlGZCZACm5NkiGqKoTMDX19eoJ99pq6LUUcchp0VaP452Tea7MyB1H4yv",
                AppId = "258090857645215",
                AppSecret = "a119b2add8cdb9ab9315c5704e66feef"
            };

            var feedObject = (JsonObject)client.Get("/168247546646019/feed");

            var dataArray = (JsonArray)feedObject["data"];

            var fbenum = dataArray
                .OfType<JsonObject>()
                .Where(x => x["type"].ToString() == "photo");

            foreach (var fb in fbenum)
            {
                var object_id = fb["object_id"].ToString();

                var imagePath = "/sitecore/media library/Images/Facebook/" + object_id;
                MediaItem imageItem = masterDb.GetItem(imagePath);
                
                if (imageItem == null)
                {
                    var photo = (JsonObject)client.Get(object_id);
                    var images = (JsonArray)photo["images"];

                    if (images.Any())
                    {
                        var largestImage =
                        images.Cast<JsonObject>()
                            .Select(x => new
                            {
                                width = int.Parse(x["width"].ToString()),
                                height = int.Parse(x["height"].ToString()),
                                pixels = int.Parse(x["width"].ToString()) * int.Parse(x["height"].ToString()),
                                source = x["source"]
                            })
                            .OrderByDescending(x => x.pixels)
                            .First();

                        var imageUrl = largestImage.source.ToString();

                        var options = new MediaCreatorOptions
                                          {
                                              FileBased = false,
                                              IncludeExtensionInItemName = false,
                                              KeepExisting = false,
                                              Versioned = false,
                                              Destination = imagePath,
                                              Database = masterDb
                                          };

                        var creator = new MediaCreator();
                        using (var webClient = new WebClient())
                        {
                            using (var stream = new MemoryStream(webClient.DownloadData(imageUrl)))
                            {
                                var imageFileName = Path.GetFileName(imageUrl.Replace("/", "\\"));

                                imageItem = creator.CreateFromStream(stream, imageFileName, options);
                            }
                        }
                    }
                }

                dynamic fbd = fb;

                this.MatrixItems.Add(new SocialMatrixItem
                {
                    Link = fbd.actions[0].link.ToString(),
                    Title = fbd.message == null ? string.Empty : fbd.message.ToString(),
                    Image = MediaManager.GetMediaUrl(imageItem),
                    Date = DateTime.Parse(fbd.created_time.ToString()).ToString("MMMM dd, yyyy")
                });
            }
        }

        private void DoBlogs(string searchindex, string searchBlogs)
        {
            using (var context = ContentSearchManager.GetIndex(searchindex).CreateSearchContext())
            {
                var interestsPredicate = SearchHelper.GetInterestsPredicate();
                var predicate = PredicateBuilder.True<TagsResultItem>();
                foreach (
                    var templateIDs in
                        new[]
                        {
                            "{1D578EDE-4723-4E21-B38E-57DAA0F017B1}"
                        })
                {
                    var templateID = new ID(templateIDs);
                    predicate = predicate.Or(x => x.TemplateId == templateID);
                }

                if (!string.IsNullOrEmpty(searchBlogs))
                {
                    predicate = predicate.And(item => item.Content.Like(searchBlogs));
                }

                var items = context.GetQueryable<TagsResultItem>()
                    .Where(predicate)
                    .Where(interestsPredicate)
                    .Where(item => item.Name != "__Standard Values")
                    .Where(item => item.Language == Sitecore.Context.Language.Name)
                    .Where(item => item.IsLatestVersion)
                    .OrderByDescending(f => f.CreatedDate)
                    .Take(5)
                    .ToList();

                foreach (var sri in items)
                {
                    var item = sri.GetItem();

                    var imageField = (ImageField)item.Fields["Image"];
                    if (imageField == null || imageField.MediaItem == null) continue;

                    var authorField = (ReferenceField)item.Fields["Author"];
                    if (authorField == null || authorField.TargetItem == null ||
                        string.IsNullOrEmpty(authorField.TargetItem["Name"])) continue;

                    var dateField = (DateField)item.Fields["Date"];

                    this.MatrixItems.Add(new SocialMatrixItem
                    {
                        Link = LinkManager.GetItemUrl(item),
                        Title = item["Title"],
                        Image = MediaManager.GetMediaUrl(imageField.MediaItem),
                        Date = dateField.DateTime.ToString("MMMM dd, yyyy"),
                        Author = authorField.TargetItem["Name"]
                    });
                }
            }
        }

        private void DoNews(string searchindex, string searchNews)
        {
            using (var context = ContentSearchManager.GetIndex(searchindex).CreateSearchContext())
            {
                var interestsPredicate = SearchHelper.GetInterestsPredicate();
                var predicate = PredicateBuilder.True<TagsResultItem>();
                foreach (var templateIDs in new[] { "{34A5EDED-6BD2-4DF5-ADDB-163F4655BEC7}", "{D89BDC0E-FF2B-4AA9-BA39-8F863A6056E9}", "{74A07E7C-B826-4B23-B87C-11398E9D6FEF}" })
                {
                    var templateID = new ID(templateIDs);
                    predicate = predicate.Or(x => x.TemplateId == templateID);
                }

                if (!string.IsNullOrEmpty(searchNews))
                {
                    predicate = predicate.And(item => item.Content.Like(searchNews));
                }

                var items = context.GetQueryable<TagsResultItem>()
                    .Where(predicate)
                    .Where(interestsPredicate)
                    .Where(item => item.Name != "__Standard Values")
                    .Where(item => item.Language == Sitecore.Context.Language.Name)
                    .Where(item => item.IsLatestVersion)
                    .OrderByDescending(f => f.CreatedDate)
                    .Select(item => item.GetItem())
                    .Take(5)
                    .ToList();

                foreach (var item in items)
                {
                    var imageField = (ImageField)item.Fields["Image"];
                    if (imageField == null || imageField.MediaItem == null) continue;

                    var dateField = (DateField)item.Fields["Date"];

                    this.MatrixItems.Add(new SocialMatrixItem
                    {
                        Link = LinkManager.GetItemUrl(item),
                        Title = item["Title"],
                        Image = MediaManager.GetMediaUrl(imageField.MediaItem),
                        Date = dateField.DateTime.ToString("MMMM dd, yyyy")
                    });
                }
            }
        }

        private void DoEvents(string searchindex, string searchEvents)
        {
            using (var context = ContentSearchManager.GetIndex(searchindex).CreateSearchContext())
            {
                var interestsPredicate = SearchHelper.GetInterestsPredicate();

                var predicate = PredicateBuilder.True<TagsResultItem>();
                foreach (var templateIDs in new[] { "{8104A35C-485D-4241-B80D-83E0C1926D9F}", "{C0A59B39-8C63-4FF1-B7CB-75457DB35FA6}" })
                {
                    var templateID = new ID(templateIDs);
                    predicate = predicate.Or(x => x.TemplateId == templateID);
                }

                if (!string.IsNullOrEmpty(searchEvents))
                {
                    predicate = predicate.And(item => item.Content.Like(searchEvents));
                }

                var items = context.GetQueryable<TagsResultItem>()
                    .Where(predicate)
                    .Where(interestsPredicate)
                    .Where(item => item.Name != "__Standard Values")
                    .Where(item => item.Language == Sitecore.Context.Language.Name)
                    .Where(item => item.IsLatestVersion)
                    .OrderByDescending(f => f.CreatedDate)
                    .Select(item => item.GetItem())
                    .Take(5)
                    .ToList();

                foreach (var item in items)
                {
                    var imageField = (ImageField)item.Fields["Image"];
                    if (imageField == null || imageField.MediaItem == null) continue;

                    var dateField = (DateField)item.Fields["Date"];

                    this.MatrixItems.Add(new SocialMatrixItem
                    {
                        Link = LinkManager.GetItemUrl(item),
                        Title = item["Title"],
                        Image = MediaManager.GetMediaUrl(imageField.MediaItem),
                        Date = dateField.DateTime.ToString("MMMM dd, yyyy")
                    });
                }
            }
        }

        private void DoKB(string searchindex, string searchKb)
        {
            using (var context = ContentSearchManager.GetIndex(searchindex).CreateSearchContext())
            {
                var interestsPredicate = SearchHelper.GetInterestsPredicate();

                var predicate = PredicateBuilder.True<TagsResultItem>();
                foreach (var templateIDs in new[] { "{23CFC180-AF56-4F12-B1DB-F26534BCD218}", "{0418A650-AE57-4DD3-BDB4-CF8E6C7F89BA}" })
                {
                    var templateID = new ID(templateIDs);
                    predicate = predicate.Or(x => x.TemplateId == templateID);
                }

                if (!string.IsNullOrEmpty(searchKb))
                {
                    predicate = predicate.And(item => item.Content.Like(searchKb));
                }

                var items = context.GetQueryable<TagsResultItem>()
                    .Where(interestsPredicate)
                    .Where(predicate)
                    .Where(item => item.Name != "__Standard Values")
                    .Where(item => item.Language == Sitecore.Context.Language.Name)
                    .Where(item => item.IsLatestVersion)
                    .OrderByDescending(f => f.CreatedDate)
                    .Select(item => item.GetItem())
                    .Take(5)
                    .ToList();

                foreach (var item in items)
                {
                    var imageField = (ImageField)item.Fields["Image"];
                    if (imageField == null || imageField.MediaItem == null) continue;

                    var dateField = (DateField)item.Fields["Date"];

                    this.MatrixItems.Add(new SocialMatrixItem
                    {
                        Link = LinkManager.GetItemUrl(item),
                        Title = item["Title"],
                        Image = MediaManager.GetMediaUrl(imageField.MediaItem),
                        Date = dateField.DateTime.ToString("MMMM dd, yyyy")
                    });
                }
            }
        }
    }
}