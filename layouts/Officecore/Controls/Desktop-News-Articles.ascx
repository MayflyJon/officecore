﻿<%@ control language="C#" autoeventwireup="true" codebehind="Desktop-News-Articles.ascx.cs" inherits="Officecore.Website.layouts.Officecore.Controls.Desktop_News_Articles" %>
<asp:repeater id="News" runat="server" itemtype="Sitecore.Data.Items.Item">
    <ItemTemplate>

        <div class="row-fluid">
            <div class="col-md-2">
                <sc:image runat="server" field="Image" item='<%# Item %>' cssclass="photo-border" maxwidth="100" maxheight="100" as="1" />
            </div>
            <div class="col-md-10">
                <h3>
                    <sc:text field="Title" item="<%# Item %>" runat="server" />
                </h3>
                <p><i>
                    <sc:date field="date" item='<%# Item %>' format="MMMM dd, yyyy" runat="server" />
                </i></p>
                <p>
                    <sc:text field="Overview Abstract" item="<%# Item %>" runat="server" />
                </p>
                <p class="readMore">
                    <a href="<%# Sitecore.Links.LinkManager.GetItemUrl(Item) %>">
                        <sc:text field="Text" item="<%# ReadMore %>" runat="server" />
                    </a>
                </p>
            </div>
        </div>

    </ItemTemplate>
</asp:repeater>
