﻿using System;
using System.Collections.Generic;
using System.Linq;

using Officecore.Website.code.Controls;
using Officecore.Website.code.Constants;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.Linq.Utilities;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Web;
using Officecore.Website.code.Model;

namespace Officecore.Website.layouts.Officecore.Controls
{
    public partial class Desktop_News_Articles : DatasourceControl
    {
        public Item ReadMore = null;
        public HashSet<ID> SearchTemplates = new HashSet<ID>();
        public string SearchTerm = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            SearchTemplates.Add(new ID(TemplateConstants.NewsArticleOneColumnId));
            SearchTemplates.Add(new ID(TemplateConstants.NewsArticleTwoColumnId));
            SearchTemplates.Add(new ID(TemplateConstants.NewsArticleThreeColumnId));

            ReadMore = Sitecore.Context.Database.GetItem(new ID(DataRepositoryConstants.READ_MORE_TEXT));
            SearchTerm = this.Item.ID.ToShortID().ToString();

            var helper = new code.DataRepositories.SearchHelper();

            Dictionary<string, string> facets = new Dictionary<string, string>();
            var querystring = new Sitecore.Text.UrlString(WebUtil.GetRawUrl());
            foreach (var parameter in querystring.Parameters.AllKeys)
            {
                if (!string.Equals("searchStr", parameter, StringComparison.OrdinalIgnoreCase))
                {
                    if (!parameter.StartsWith("sc_"))
                    {
                        facets.Add(parameter, querystring.Parameters[parameter]);
                    }
                }
            }

            List<FacetCategory> _facets;
            string _facetList = string.Empty;
            int numResults = 0;

            IEnumerable<ResultItem> results;
            if (Sitecore.Context.Item.ID == this.Item.ID)
            {
                results = helper.SearchWithFacets<ResultItem>(facets, SearchTemplates, out _facets, out numResults,
                                                                  item => item.IsLatestVersion,
                                                                  item => item.DateRange);
            }
            else
            {
                results = helper.SearchWithFacets<ResultItem>(facets, SearchTemplates, out _facets, out numResults,
                                                                  item => item.IsLatestVersion && item.Terms.Contains(SearchTerm),
                                                                  item => item.DateRange);
            }

            // TODO: Create NewsResultItem and bind that instead of looping through all ResultItems and getting item.
            var list = new List<Item>();

            foreach (var item in results)
            {
                if (item.GetItem() != null)
                {
                    list.Add(item.GetItem());
                }
            }


            News.DataSource = list;
            News.DataBind();
        }
    }
}