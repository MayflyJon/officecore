﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Desktop-Render-Event.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Desktop_Render_Event" %>
<sc:editframe buttons="Officecore-news" runat="server" id="authorframe">
    <div class="row-fluid">
        <div class="col-md-12">
            <h1>
                <asp:Literal ID="newsTitle" runat="server" Text="<%# RenderTitleField() %>" />
            </h1>
        </div>
    </div>
    <div class="row-fluid">
        <div class="col-md-4">
            <sc:Placeholder Key="content-image" runat="server"></sc:Placeholder>
        </div>
        <div class="col-md-8">
            <p class="news-date">
            <sc:Date Format="MMMM dd, yyyy" runat="server" Field="date" />
            </p>
            
            <oc:TextEnforceParagraph runat="server" Field="text"/>

            <p>
                <br/>
                <button class="btn btn-success btn-large" onclick="location.href='/layouts/Officecore/Controls/EventInvitation.ashx?id=<%=Sitecore.Context.Item.ID%>'; return false;">
                    <span class="glyphicon glyphicon-calendar"></span> Add to calendar
                </button>
            </p>
        </div>
    </div>
</sc:editframe>