﻿<%@ control language="C#" autoeventwireup="true" codebehind="SiteMap.ascx.cs" inherits="Officecore.Website.layouts.Officecore.Controls.SiteMap" %>
<link rel="stylesheet" href="/assets/css/sitemap.css" />
<link rel="stylesheet" href="/assets/css/qtip2/jquery.qtip.min.css" />
<div class="row">
    <div class="col-md-12">
        <div class="tree">
            <%=Tree %>
        </div>
    </div>
</div>
<script type="text/javascript" src="/assets/js/sitemap.js"></script>
<script type="text/javascript" src="/assets/js/qtip2/jquery.qtip.min.js"></script>
<script type="text/javascript" src="/assets/js/jquery.zoomer.js"></script>
<script type="text/javascript" src="/assets/js/qtip2/sitemappreview.js"></script>