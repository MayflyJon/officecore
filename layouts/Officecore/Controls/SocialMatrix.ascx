﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SocialMatrix.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.SocialMatrix" %>
<style type="text/css">
    .social-matrix {
        color: #fff;
        padding: 0;
        overflow: hidden;
        font-weight: 700;
        display: block;
    }

        .social-matrix a:visited {
            color: #fff;
        }

        .social-matrix:hover {
            text-decoration: none;
            color: #ffffff;
        }

    .social-matrix.social-matrix-single {
        width: 240px;
        height: 240px;
        float: left;
    }

    .social-matrix-padding-top { height: 150px; }

    .social-matrix.social-matrix-tweet {
        padding: 15px;
    }

    .social-matrix.social-matrix-double {
        width: 480px;
        float: left;
    }

    .social-matrix .social-matrix-date {
        font-weight: 400;font-size: 12px;
    }

    .social-matrix .social-matrix-author {
        font-weight: 400;font-size: 11px;
    }

    .social-matrix-blue { background-color: #406390; }
    .social-matrix-orange { background-color: #EA5F25; }
    .social-matrix-green { background-color: #3F9418; }
    .social-matrix-red { background-color: #CF2A24; }

    .social-matrix-wrapper { margin: 0 auto 30px auto;overflow:auto; }

    @media (min-width: 1024px) {
        .social-matrix-wrapper { width: 960px; }
    }

    @media (min-width: 1280px) {
        .social-matrix.social-matrix-single {
            width: 292px;
            height: 292px;
        }

        .social-matrix.social-matrix-double { width: 584px; }
        .social-matrix-padding-top { height: 202px; }
        .social-matrix-wrapper { width: 1168px; }
    }
</style>

<div class="social-matrix-wrapper <%=RowClass%>">
    <a class="social-matrix <%=DoubleCssClass%>" href="<%=MatrixItems[0].Link%>">
        <div style="background-image:url('<%=MatrixItems[0].Image%><%=DoubleImageParams %>')">
            <div class="social-matrix-padding-top"></div>
            <div style="height: 90px; padding: 15px; background-color: #000000; background-color: rgba(0,0,0,0.6)">
                <div class="social-matrix-date"><%=MatrixItems[0].Date%></div>
                <div><%=MatrixItems[0].Title%></div>
                <div class="social-matrix-author"><%=MatrixItems[0].Author%></div>
            </div>    
        </div>
    </a>
    <a class="social-matrix social-matrix-tweet social-matrix-blue <%=SingleCssClass%>" href="<%=Tweets[0].Link%>">
        <div class="social-matrix-date" style="margin-bottom: 5px"><img src="/assets/images/icons/Twitter_logo_white.png" width="19" height="16"/> Posted on <%=Tweets[0].Date%></div>
        <%=Tweets[0].Title%>
    </a>
    <a class="social-matrix social-matrix-tweet social-matrix-orange <%=SingleCssClass%>" href="<%=Tweets[1].Link%>">
        <div class="social-matrix-date" style="margin-bottom: 5px"><img src="/assets/images/icons/Twitter_logo_white.png" width="19" height="16"/> Posted on <%=Tweets[1].Date%></div>        
        <%=Tweets[1].Title%>
    </a>
    <a class="social-matrix social-matrix-tweet social-matrix-blue <%=SingleCssClass%>" href="<%=Tweets[2].Link%>">
        <div class="social-matrix-date" style="margin-bottom: 5px"><img src="/assets/images/icons/Twitter_logo_white.png" width="19" height="16"/> Posted on <%=Tweets[2].Date%></div>        
        <%=Tweets[2].Title%>
    </a>
    <a class="social-matrix social-matrix-tweet social-matrix-red <%=SingleCssClass%>" href="<%=Tweets[3].Link%>">
        <div class="social-matrix-date" style="margin-bottom: 5px"><img src="/assets/images/icons/Twitter_logo_white.png" width="19" height="16"/> Posted on <%=Tweets[3].Date%></div>        
        <%=Tweets[3].Title%>
    </a>
    <a class="social-matrix <%=SingleCssClass%>" href="<%=MatrixItems[1].Link%>">
        <div style="background-image:url('<%=MatrixItems[1].Image%><%=SingleImageParams %>')">
            <div class="social-matrix-padding-top"></div>
            <div style="height: 90px; padding: 15px; background-color: #000000; background-color: rgba(0,0,0,0.6)">
                <div class="social-matrix-date"><%=MatrixItems[1].Date%></div>
                <div><%=MatrixItems[1].Title%></div>
                <div class="social-matrix-author"><%=MatrixItems[1].Author%></div>
            </div>    
        </div>
    </a>
    <a class="social-matrix social-matrix-tweet social-matrix-green <%=SingleCssClass%>" href="<%=Tweets[4].Link%>">
        <div class="social-matrix-date" style="margin-bottom: 5px"><img src="/assets/images/icons/Twitter_logo_white.png" width="19" height="16"/> Posted on <%=Tweets[4].Date%></div>        
        <%=Tweets[4].Title%>
    </a>
    <a class="social-matrix <%=SingleCssClass%>" href="<%=MatrixItems[2].Link%>">
        <div style="background-image:url('<%=MatrixItems[2].Image%><%=SingleImageParams %>')">
            <div class="social-matrix-padding-top"></div>
            <div style="height: 90px; padding: 15px; background-color: #000000; background-color: rgba(0,0,0,0.6)">
                <div class="social-matrix-date"><%=MatrixItems[2].Date%></div>
                <div><%=MatrixItems[2].Title%></div>
                <div class="social-matrix-author"><%=MatrixItems[2].Author%></div>
            </div>    
        </div>
    </a>
    <a class="social-matrix social-matrix-tweet social-matrix-green <%=SingleCssClass%>" href="<%=Tweets[5].Link%>">
        <div class="social-matrix-date" style="margin-bottom: 5px"><img src="/assets/images/icons/Twitter_logo_white.png" width="19" height="16"/> Posted on <%=Tweets[5].Date%></div>        
        <%=Tweets[5].Title%>
    </a>
    <a class="social-matrix <%=DoubleCssClass%>" href="<%=MatrixItems[3].Link%>">
        <div style="background-image:url('<%=MatrixItems[3].Image%><%=DoubleImageParams %>')">
            <div class="social-matrix-padding-top"></div>
            <div style="height: 90px; padding: 15px; background-color: #000000; background-color: rgba(0,0,0,0.6)">
                <div class="social-matrix-date"><%=MatrixItems[3].Date%></div>
                <div><%=MatrixItems[3].Title%></div>
                <div class="social-matrix-author"><%=MatrixItems[3].Author%></div>
            </div>    
        </div>
    </a>
</div>