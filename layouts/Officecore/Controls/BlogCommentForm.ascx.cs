﻿namespace Officecore.Website.layouts.Officecore.Controls
{
    using System;
    using System.Threading;
    using Sitecore.Data;
    using Sitecore.SecurityModel;
    using Sitecore.Text;

    public partial class BlogCommentForm : System.Web.UI.UserControl
    {
        private void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack && this.Request.QueryString["sent"] != null)
            {
                this.pnlForm.Visible = false;
                this.pnlSent.Visible = true;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.txtName.Text) || string.IsNullOrWhiteSpace(this.txtEmail.Text) || string.IsNullOrWhiteSpace(this.txtComment.Text))
            {
                this.pnlError.Visible = true;
                this.litError.Text = "Please fill in all fields";
                return;
            }

            var masterDb = Sitecore.Configuration.Factory.GetDatabase("master");

            var commentsFolder = masterDb.GetItem("{9A4ECF4C-DA2F-4C14-A81B-F652B83FE192}");
            var commentTemplateID = new TemplateID(new ID("{753A6E1D-940E-4C37-AEBB-C095C33E38A0}"));

            using (new SecurityDisabler())
            {
                var name = Sitecore.Data.Items.ItemUtil.ProposeValidItemName(string.Format("{0}_{1}_{2:yyyyMMdd}", this.txtName.Text.Trim(), this.txtEmail.Text.Trim(), DateTime.Now));
                var comment = commentsFolder.Add(name, commentTemplateID);

                comment.Editing.BeginEdit();
                comment["Name"] = this.txtName.Text.Trim();
                comment["Email"] = this.txtEmail.Text.Trim();
                comment["Comment"] = this.txtComment.Text.Trim();
                comment["BlogPost"] = Sitecore.Context.Item.ID.ToString();
                comment.Editing.EndEdit();
            }

            // Let the index catch up :) Any other way?
            Thread.Sleep(2000);

            var newUrl = new UrlString(this.Request.RawUrl);
            newUrl.Add("sent", "1");

            this.Response.Redirect(newUrl.ToString());
            this.Response.End();
        }
    }
}