﻿using Officecore.Website.code.Controls;

using System;
using System.Linq;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;

namespace Officecore.Website.layouts.Officecore.Controls
{
    using Sitecore.Data.Fields;

    public partial class Desktop_Teasers : DatasourceControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Item == null)
            {
                if (Sitecore.Context.PageMode.IsNormal)
                {
                    Visible = false;
                    return;
                }
            }
            else
            {
                // Bind the item ID's from a Multilist field
                Repeater1.DataSource = ((MultilistField)this.Item.Fields["Teasers"]).TargetIDs.Select(id => Sitecore.Context.Database.GetItem(id));
                Repeater1.DataBind();                
            }
        }
    }
}