﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoanSlider.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.LoanSlider" %>
<link rel="stylesheet" href="/assets/CSS/jquery-ui.css">
<link rel="stylesheet" href="/assets/CSS/bootstrap/FormSlider/styles.css">
<div class="row price-box">
    <div class="col-xs-12 col-md-6">
        <div class="price-slider">
            <h4 class="great">Amount</h4>
            <span>Minimum £10 is required</span>
            <div class="col-sm-12">
                <div id="slider"></div>
            </div>
        </div>
        <div class="price-slider">
            <h4 class="great">Duration</h4>
            <span>Minimum 1 week is required</span>
            <div class="col-sm-12">
                <div id="slider2"></div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-1">
    </div>
    <div class="col-xs-12 col-md-5">
        <div class="form-group">
            <label for="total" class="col-sm-6 control-label">Total to repay</label>
            <span class="help-text"></span>            
                <input type="hidden" id="total" class="form-control">
                <p class="price lead" id="total-label"></p>                      
        </div>
        <div class="form-group">
            <label for="Apr" class="col-sm-6 control-label">APR</label>
            <span class="help-text"></span>
                <input type="hidden" id="Apr" class="form-control" value="<%=AprValue%>">
                <p class="price lead" id="Apr-label"><%=AprValue%></p>
                <span class="price">%</span>            
        </div>
        <div class="form-group">
            <label for="weekly" class="col-sm-6"><strong>Weekly repayment amount</strong></label>    
                <span class="help-text"></span>               
                <input type="hidden" id="weekly" class="form-control">
                <p class="price lead" id="weekly-label"></p>        
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-12">
            <button type="submit" class="btn btn-success btn-lg btn-block">Apply Now <span class="glyphicon glyphicon-chevron-right pull-right" style="padding-right: 10px;"></span></button>
        </div>
    </div>
</div>
<input type="hidden" id="amount" class="form-control">
<input type="hidden" id="duration" class="form-control">

<script type="text/javascript" src="/assets/js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="/assets/js/jquery-ui.min.js"></script>

<script>
    jQuery(document).ready(function () {
        jQuery("#slider").slider({
            range: "min",
            animate: true,
            value: 1,
            min: 10,
            max: '<%=MaxLoan%>',
            step: 10,
            slide: function (event, ui) {
                update(1, ui.value); //changed
            }
        });

        jQuery("#slider2").slider({
            range: "min",
            animate: true,
            value: 1,
            min: 1,
            max: '<%=MaxDuration%>',
            step: 1,
            slide: function (event, ui) {
                update(2, ui.value); //changed
            }
        });
        jQuery("#amount").val(10);
        jQuery("#duration").val(1);
        update();
    });    

    //changed. now with parameter
    function update(slider, val) {
        //changed. Now, directly take value from ui.value. if not set (initial, will use current value.)
        var $amount = slider == 1 ? val : $("#amount").val();
        var $duration = slider == 2 ? val : $("#duration").val();
        var $apr = 136;
        var $time = $duration / 52;
        var $aprreal = $apr / 100;
        var $poundSign = "£";

        $total = $amount * (1 + ($aprreal * $time))
        $weeklyPayment = $total / $duration;
        jQuery("#amount").val($amount);
        jQuery("#duration").val($duration);
        jQuery("#total").val($total.toFixed(2));
        jQuery("#total-label").text($poundSign + $total.toFixed(2));
        jQuery("#weekly").val($weeklyPayment.toFixed(2));
        jQuery("#weekly-label").text($poundSign + $weeklyPayment.toFixed(2));
        jQuery('#slider a').html('<label><span class="glyphicon glyphicon-chevron-left"></span> £' + $amount + ' <span class="glyphicon glyphicon-chevron-right"></span></label>');
        jQuery('#slider2 a').html('<label><span class="glyphicon glyphicon-chevron-left"></span> ' + $duration + ' weeks <span class="glyphicon glyphicon-chevron-right"></span></label>');
    }
    </script>
