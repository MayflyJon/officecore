﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogCommentForm.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.BlogCommentForm" %>
<div class="row">
    <div class="col-md-12" style="margin-top: 15px;">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Post a comment</h3>
            </div>
            <div class="panel-body" style="padding: 15px !important">
                <asp:Panel runat="server" ID="pnlError" CssClass="alert alert-danger" role="alert" Visible="False" EnableViewState="False">
                    <asp:Literal runat="server" ID="litError"/>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlSent" CssClass="alert alert-success" role="alert" Visible="False" EnableViewState="False">
                    <p>Your comment has been saved, thank you.</p>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlForm" CssClass="form-horizontal" role="form" DefaultButton="btnSubmit">
                    <div class="form-group">
                        <label for="<%=txtName.ClientID%>" class="col-sm-3 control-label">Name</label>
                        <div class="col-sm-9">
                            <asp:Textbox runat="server" ID="txtName" CssClass="form-control" placeholder="Name" AutoCompleteType="Disabled" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="<%=txtEmail.ClientID%>" class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-9">
                            <asp:Textbox runat="server" ID="txtEmail" CssClass="form-control" placeholder="Email" AutoCompleteType="Email" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="<%=txtComment.ClientID%>" class="col-sm-3 control-label">Your comment</label>
                        <div class="col-sm-9">
                            <asp:Textbox runat="server" ID="txtComment" CssClass="form-control" placeholder="Your comment..." AutoCompleteType="Disabled" TextMode="MultiLine" Rows="5" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <asp:Button runat="server" ID="btnSubmit" CssClass="btn btn-default" Text="Post comment" OnClick="btnSubmit_Click" />
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
</div>