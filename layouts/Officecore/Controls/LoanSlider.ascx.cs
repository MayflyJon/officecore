﻿using Officecore.Website.code.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Officecore.Website.layouts.Officecore.Controls
{
    public partial class LoanSlider : DatasourceControl
    {
        public String AprValue { get; set; }
        public String MaxLoan { get; set; }
        public String MaxDuration { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            AprValue = this.Item["APR"]; 
            MaxLoan = this.Item["Max Loan"]; 
            MaxDuration = this.Item["Max Duration"]; 
        }
    }
}