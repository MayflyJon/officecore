﻿using System;
using Officecore.Website.code.Constants;
using Officecore.Website.code.DataRepositories;

namespace Officecore.Website.layouts.Officecore.Controls
{
    using System.Linq;

    public partial class Desktop_Footer : System.Web.UI.UserControl
    {
        private readonly ItemRepositoryBase menus = new ItemRepositoryBase();

        protected void Page_Load(object sender, EventArgs e)
        {
            FooterRepeater.DataSource = this.menus.GetMainMenu(DataRepositoryConstants.MENUFOOTER).ToList();
            FooterRepeater.DataBind();
        }
    }
}