﻿using System;
using System.Linq;
using Officecore.Website.code.Service;
using Sitecore.Data.Fields;
using Sitecore.Links;

namespace Officecore.Website.layouts.Officecore.Controls
{
    public partial class EventsCalendar : System.Web.UI.UserControl
    {
        protected class EventRow
        {
            public string date { get; set; }
            public string type { get; set; }
            public string title { get; set; }
            public string description { get; set; }
            public string url { get; set; }
        }
        
        protected string EventsJson { get; set; }

        private void Page_Load(object sender, EventArgs e)
        {
            var events = EventService.GetEvents(null)
                .Select(x => new EventRow
                {
                    date = ((DateField)x.Fields["Date"]).DateTime.ToString("yyyy-MM-dd 00:00:00"),
                    type = "event",
                    title = x["Title"],
                    description = x["Description"],
                    url = LinkManager.GetItemUrl(x)
                })
                .ToArray();

            if (!events.Any())
            {
                Visible = false;
                return;
            }

            EventsJson = Newtonsoft.Json.JsonConvert.SerializeObject(events);
        }
    }
}