﻿<%@ control language="C#" autoeventwireup="true" codebehind="Desktop-Footer.ascx.cs" inherits="Officecore.Website.layouts.Officecore.Controls.Desktop_Footer" %>
<%@ register src="~/layouts/Starter Kit/Controls/Search Box.ascx" tagprefix="uc1" tagname="SearchBox" %>
<div id="bottom-menu">
    <div class="row-fluid">
        <div class="col-md-3">
            <h6>Follow Us</h6>
            <ul id="social-list">
                <li id="social-fb-link" class="facebook">
                    <a target="_blank" href="http://facebook.com/"></a>
                </li>
                <li id="social-tw-link" class="twitter">
                    <a target="_blank" href="http://twitter.com/"></a>
                </li>
                <li id="social-yt-link" class="youtube">
                    <a target="_blank" href="http://youtube.com/"></a>
                </li>
                <li id="social-fl-link" class="flickr">
                    <a target="_blank" href="http://flickr.com/"></a>
                </li>
                <li id="social-rs-link" class="rss">
                    <a target="_blank" href="<%# Sitecore.Context.Site.Name %>/sitecore/shell/~/feed/workflow.aspx?sc_rssu=7ede3ExkZFEHE5xHwKUzqq-jlEZunKz-GarJnAgW56QrxTNXyFfZ_E0tvn3Mk7aeuiHuHm6uxiu3gAQsdJsuHYq797BdPc4QjmjiMl_2iA41&sc_rssh=CAC3CB018E4354FD7C26&wf=%7BA5BC37E7-ED96-4C1E-8590-A26E64DB55EA%7D"></a>
                </li>
            </ul>
            <div class="clear"></div>
            <div id="boring-stuff">
                <sc:sublayout id="CommerceToggle" path="/layouts/Officecore/Controls/Commerce-Toggle.ascx" runat="server" />
                <p>Copyright &copy; <%=DateTime.Now.Year%></p>
                <p>Sitecore Corporation</p>
                <p><a href="http://www.sitecore.net">www.sitecore.net</a></p>
            </div>
        </div>
        <div class="col-md-3">
            <h6>Sections</h6>
            <ul>
                <asp:Repeater ID="FooterRepeater" runat="server" ItemType="Sitecore.Data.Items.Item">
                    <ItemTemplate>
                        <li>
                            <sc:Link Field="Link" runat="server" Item="<%# Item %>" CssClass='<%#Item.ID.Equals(Sitecore.Context.Item.ID) ? "current" : string.Empty%>'>
                                <sc:Text Field="Link Text" runat="server" Item="<%# Item %>" />
                            </sc:Link>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
        <div class="col-md-6">
            <sc:placeholder runat="server" key="Footer-Column" id="FooterColumn" />
        </div>
    </div>
    <div class="clearfix"></div>
</div>