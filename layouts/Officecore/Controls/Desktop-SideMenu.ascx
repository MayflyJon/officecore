﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Desktop-SideMenu.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Desktop_SideMenu" %>
<div id="tree-menu" class="panel">
    <asp:Repeater ID="News" runat="server">
        <HeaderTemplate>
            <ul>
        </HeaderTemplate>
        <ItemTemplate>
            <li style="clear: both; margin-top:3px">
                <a class="tree-menu style="padding-bottom:4px;" href="<%# Sitecore.Links.LinkManager.GetItemUrl((Sitecore.Data.Items.Item)(Container.DataItem)) %>"><%# GetText((Sitecore.Data.Items.Item)(Container.DataItem)) %></a>
            </li>
            <asp:Repeater ID="InnerProductCategories" runat="server" DataSource="<%# GetChildren((Sitecore.Data.Items.Item)(Container.DataItem)) %>">
                    <HeaderTemplate>
                        <ul class="nav nav-pills nav-stacked">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li>
                            <a href="<%# Sitecore.Links.LinkManager.GetItemUrl((Sitecore.Data.Items.Item)(Container.DataItem)) %>" class="tree-menu treelist" style="padding:2px;margin-left:15px;">
                                <%# ((Sitecore.Data.Items.Item)(Container.DataItem)).Name %>
                            </a>
                        </li>
                    </ItemTemplate>
                    <FooterTemplate>
                        </ul>
                    </FooterTemplate>
                </asp:Repeater>
        </ItemTemplate>
        <FooterTemplate>
            </ul>
        </FooterTemplate>
    </asp:Repeater>
</div>

