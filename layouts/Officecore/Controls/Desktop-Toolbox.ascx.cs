﻿using System;
using Officecore.Website.code.DataRepositories;
using Officecore.Website.code.Constants;

namespace Officecore.Website.layouts.Officecore.Controls
{
    public partial class Desktop_Toolbox : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Toolbox.DataSource = new ItemRepositoryBase().GetItems(DataRepositoryConstants.TOOLBOX_QUERY);
            Toolbox.DataBind();
        }
    }
}