﻿<%@ control language="C#" autoeventwireup="true" codebehind="Desktop-Banner.ascx.cs" inherits="Officecore.Website.layouts.Officecore.Controls.Desktop_Banner" %>

<%if (Sitecore.Context.PageMode.IsPageEditorEditing)
  { %>
    <div style="margin: 0 auto; position: relative; width: 1170px; height: 329px;">
        <sc:image field="Banner Image" id="BannerImage" runat="server" maxwidth="1170" CssStyle="clip: rect(0px,0px,220px,0px);" />
        <div style="position: absolute; top: 130px; padding-left: 20px;">
            <div class="row-fluid">
                <div class="col-md-12">
                    <div id="hero-strapline">
                        <p>
                            <sc:text field="Banner Header" id="BannerHeader" runat="server" />
                        </p>
                        <span>
                            <sc:text field="Banner Slogan" id="BannerSlogan" runat="server" /> <sc:link field="Banner Link" id="BannerLink" runat="server" />
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

<% }
  else
  { %>

<div id="hero-image-live" style="background-image: url('<%= BannerImgUrl %>?w=1600'); background-size: cover;">
    <div id="hero-image-overlay">
        <div id="hero-strapline-wrapper">
            <div class="row-fluid">
                <div class="col-md-12"> 
                    <div id="hero-strapline">
                        <p>
                            <sc:text field="Banner Header" id="BannerHeaderLive" runat="server" />
                        </p>
                        <span>
                            <sc:text field="Banner Slogan" id="BannerSloganLive" runat="server" /> <sc:link field="Banner Link" id="BannerLinkLive" runat="server" />
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<% } %>