﻿using System;
using Officecore.Website.code.Controls;
using Officecore.Website.code.Constants;
using Officecore.Website.code.Service;
using Sitecore.Data;
using Sitecore.Data.Items;

namespace Officecore.Website.layouts.Officecore.Controls
{
    using System.Web.UI.WebControls;

    using Sitecore.Resources.Media;

    public partial class Desktop_Events : DatasourceControl
    {
        public Item ReadMore = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            ReadMore = Sitecore.Context.Database.GetItem(new ID(DataRepositoryConstants.READ_MORE_TEXT));
            
            News.DataSource = EventService.GetEvents(null);
            News.DataBind();
        }

        protected void News_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var item = e.Item.DataItem as Item;
            if (item == null) return;

            var img = (Image)e.Item.FindControl("img");
            var imageField = (Sitecore.Data.Fields.ImageField)item.Fields["Image"];
            if (imageField == null || imageField.MediaItem == null)
            {
                img.Visible = false;
                return;
            }

            img.ImageUrl = MediaManager.GetMediaUrl(imageField.MediaItem, new MediaUrlOptions(100, 100, false)) + "&crop=1";
        }
    }
}