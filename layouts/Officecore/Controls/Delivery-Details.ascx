﻿<%@ control language="C#" autoeventwireup="true" codebehind="Delivery_Details.ascx.cs" inherits="Officecore.Website.layouts.Officecore.Controls.DeliveryDetails" %>

<%--
This file wants to take in a parameter from the item template called listamount,
this will then be used to limit the amount of delivery address records based on this template that are shown,
as this will be used for the cart process where we will only want to see the chosen delivery address,
and it will be used on the form when changing their personal details/delivery address records (may have more than 1)  
--%>

<div class="panel col-md-12 col-sm-6" style="padding: 10px;">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <strong>Confirm delivery address</strong>
            </h4>
        </div>
        <div class="panel-body" style="padding:10px!important">
            <p>To :</p>
            <p>House name :</p>
            <p>Street :</p>
            <p>Postcode :</p>
        </div>
    </div>
</div>