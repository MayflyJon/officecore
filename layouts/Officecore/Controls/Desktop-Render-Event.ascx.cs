﻿using Officecore.Website.code.Controls;
using System;

namespace Officecore.Website.layouts.Officecore.Controls
{
    public partial class Desktop_Render_Event : ContentDetails
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataBind();
        }
    }
}