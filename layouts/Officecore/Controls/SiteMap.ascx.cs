﻿using Officecore.Website.code.Constants;
using Officecore.Website.code.DataRepositories;
using Sitecore.Data.Items;
using Sitecore.Links;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Officecore.Website.layouts.Officecore.Controls
{
    public partial class SiteMap : System.Web.UI.UserControl
    {
        public String Tree;
        private readonly ItemRepositoryBase _menus = new ItemRepositoryBase();
        ProductRepository repo = new ProductRepository();

        public int ThumbWidth { get; set; }
        public int ThumbHeight { get; set; }
        public int BrowserWidth { get; set; }
        public int BrowserHeight { get; set; }

        public string Url { get; set; }
        public Bitmap ThumbImage { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            List<Item> items = _menus.GetItems(DataRepositoryConstants.SITE_SECTION).ToList();
            if (repo.ActivateCommerce())
            {
                int index = items.FindIndex(c => c.ID.ToString() == "{07D9A696-A2FE-4A59-88FB-A57FE386B8AD}");
                items[index] = Sitecore.Data.Database.GetDatabase("web").GetItem("{B978AAEF-C01C-4AE4-A198-5400713F04D8}");
            }
            Tree = GenerateUL(items);
        }

        private string GenerateUL(IEnumerable<Item> items)
        {
            var sb = new StringBuilder();
            sb.AppendLine("<ul>");
            foreach (var item in items)
            {
                String name = item.DisplayName;
                if (name.Length > 25)
                {
                    name = name.Substring(0, 25);
                }
                if (item.Children.Count > 0)
                {
                    sb.AppendLine("<li><div><span style=\"font-size: 1.2em;\" class=\"glyphicon glyphicon-minus\"></span><div style=\"border:none\">" + name + "</div><a rel=\"#tip-content\" href=\"" + LinkManager.GetItemUrl(item) + "\"><span style=\"font-size: 1.2em;\" class=\"glyphicon glyphicon-play-circle\"><span></a></div>");
                    sb.Append(GenerateUL(item.Children));
                    sb.AppendLine("</li>");
                }
                else
                {
                    sb.AppendLine("<li><div><span style=\"font-size: 1.2em;\" class=\"glyphicon glyphicon-record\"></span><div style=\"border:none\">" + name + "</div><a rel=\"#tip-content\" href=\"" + LinkManager.GetItemUrl(item) + "\"><span style=\"font-size: 1.2em;\" class=\"glyphicon glyphicon-play-circle\"><span></a></div></li>");
                }
            }
            sb.AppendLine("</ul>");
            return sb.ToString();
        }

    }


}