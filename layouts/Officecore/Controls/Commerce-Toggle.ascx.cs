﻿using Officecore.Website.code.Constants;
using Officecore.Website.code.Service;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Officecore.Website.layouts.Officecore.Controls
{
    public partial class Commerce_Toggle : System.Web.UI.UserControl
    {
        protected bool toggleState = true;
        public ProductService ps = new ProductService();

        protected void Page_Load(object sender, EventArgs e)
        {
            String commerceValue = Request.Form.Get("commercevalue");
            //If its posted back and there is a value
            if (IsPostBack && !String.IsNullOrWhiteSpace(commerceValue))
            {
                //Get the toggleState from the value of the form toggle switch
                ps.UpdateCommerceToggle(commerceValue);
                //have to do this as other controls render before we set this, would be nice to use a click binding, 
                //but cant get that to work with the bootrap switch.
                Item siteRoot = Sitecore.Context.Database.GetItem(DataRepositoryConstants.SITE_ROOT);
                Response.Redirect(Sitecore.Links.LinkManager.GetItemUrl(siteRoot));
            }
            else
            {
                toggleState = ps.ActivateCommerce();
            }
        }
    }
}