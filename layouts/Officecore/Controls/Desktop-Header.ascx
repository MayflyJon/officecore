﻿<%@ control language="C#" autoeventwireup="true" codebehind="Desktop-Header.ascx.cs" inherits="Officecore.Website.layouts.Officecore.Controls.Desktop_Header" %>
<div id="header-container">
    <div class="row-fluid">
        <div class="col-md-4 col-xs-4">
            <a href="/"><sc:Image Field="logo" runat="server" Id="Logo" /></a>
        </div>
        <% if(toggleState) {%>
        <div class="col-md-5 col-xs-5" style="padding: 15px 4px 0px 0px;">
            <sc:sublayout runat="server" placeholder="search" path="/layouts/Officecore/Controls/Desktop/Products/Desktop-Search-Header.ascx" id="search" />
        </div>
        <div class="col-md-3 col-xs-3" style="padding: 15px 4px 0px 0px;">
            <sc:sublayout runat="server" placeholder="cart" path="/layouts/Officecore/Controls/Desktop/Products/Desktop-Cart.ascx" id="cart" />
        </div>
        <% } else {%>
        <div class="col-md-8" style="padding: 15px 4px 0px 0px;">
            <sc:sublayout runat="server" placeholder="search" path="/layouts/Officecore/Controls/Desktop/Products/Desktop-Search-Header.ascx" id="search2" />
        </div>
        <% }%>
    </div>
</div>
