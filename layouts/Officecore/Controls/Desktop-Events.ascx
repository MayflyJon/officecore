﻿<%@ control language="C#" autoeventwireup="true" codebehind="Desktop-Events.ascx.cs" inherits="Officecore.Website.layouts.Officecore.Controls.Desktop_Events" %>
<div class="desktop-news-articles">
    <asp:repeater id="News" runat="server" itemtype="Sitecore.Data.Items.Item" onitemdatabound="News_OnItemDataBound">
    <ItemTemplate>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <strong><a href="<%# Sitecore.Links.LinkManager.GetItemUrl(Item) %>">
                                <sc:text field="Title" item="<%# Item %>" runat="server" />
                            </a></strong>
                            </h3>
                    </div>
                    <div class="panel-body" style="padding: 15px !important">
                        <div class="row">
                            <div class="col-md-2">
                                <a href="<%# Sitecore.Links.LinkManager.GetItemUrl(Item) %>">
                                    <% if (Sitecore.Context.PageMode.IsPageEditor) { %>
                                        <sc:Image runat="server" Item="<%#Item%>" Field="Image" CssClass="photo-border" MaxWidth="100" MaxHeight="100" AllowStretch="True" />
                                    <% } else { %>
                                        <asp:Image runat="server" ID="img" CssClass="photo-border" Width="100" Height="100" />
                                    <% } %>
                                </a>
                            </div>
                            <div class="col-md-10">
                                <span class="news-date">
                                    <sc:date field="date" item='<%# Item %>' format="MMMM dd, yyyy" runat="server" />
                                </span>
                                <p>
                                    <sc:text field="Overview Abstract" item="<%# Item %>" runat="server" />
                                </p>
                                <span class="readMore">
                                    <a href="<%# Sitecore.Links.LinkManager.GetItemUrl(Item) %>">
                                        <sc:text field="Text" item="<%# ReadMore %>" runat="server" />
                                    </a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
    </ItemTemplate>
</asp:repeater>
</div>