﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Desktop-Render-News-Article.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Desktop_Render_News_Article" %>
<sc:editframe buttons="Officecore-news" runat="server" id="authorframe">
    <div class="row-fluid">
        <div class="col-md-12">
            <h1>
                <asp:Literal ID="newsTitle" runat="server" Text="<%# RenderTitleField() %>" />
            </h1>
        </div>
    </div>
    <div class="row-fluid">
        <div class="col-md-4">
            <sc:Placeholder Key="content-image" runat="server"></sc:Placeholder>
        </div>
        <div class="col-md-8">
            <p class="news-date">
            <sc:Date Format="MMMM dd, yyyy" runat="server" Field="date" />
            </p>
            <% if (Officecore.Website.code.Helpers.ItemHelper.PrefixWithParagraphTag(this.Item, "text"))
               {  %>
            <p>
            <% } %>
                <sc:Text Field="Text" runat="server" />
            <% if (Officecore.Website.code.Helpers.ItemHelper.PrefixWithParagraphTag(this.Item, "text")) {  %>
            </p>
            <% } %>
            <div class="news-author">
                <sc:Text runat="server" Field="Name" Item="<%# Author  %>" id="author"/><br />
                <sc:Text runat="server" Field="Email address" Item="<%# Author  %>" id="email"/><br />
                <sc:Text runat="server" Field="Phone number" Item="<%# Author  %>" id="phone"/>
            </div>
        </div>
    </div>
</sc:editframe>