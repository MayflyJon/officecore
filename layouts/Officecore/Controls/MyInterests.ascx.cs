﻿namespace Officecore.Website.layouts.Officecore.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;

    using global::Officecore.Website.code.Helpers;

    using Sitecore.Data;
    using Sitecore.Data.Items;
    using Sitecore.Diagnostics;
    using Sitecore.Links;

    public partial class MyInterests : System.Web.UI.UserControl
    {
        private List<ID> currentTags;

        private bool hadTagsSet;

        private void Page_Load(object sender, EventArgs e)
        {
            Assert.IsTrue(Sitecore.Context.IsLoggedIn, "Must be logged in");
            if (Sitecore.Context.User.Profile == null)
            {
                Visible = false;
                return;
            }

            currentTags = ProfileHelper.GetInterests().ToList();
            hadTagsSet = currentTags.Any();

            if (!IsPostBack)
            {
                var possibleInterests = Sitecore.Context.Database.GetItem("/sitecore/content/Global/Tags/Interests").Children;
                rpt.DataSource = possibleInterests;
                rpt.DataBind();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            var newIDs = new List<ID>();

            foreach (RepeaterItem item in rpt.Items)
            {
                var chk = (CheckBox)item.FindControl("chk");
                if (chk.Checked)
                {
                    var hid = (HiddenField)item.FindControl("hid");
                    newIDs.Add(Sitecore.Data.ID.Parse(hid.Value));
                }
            }

            Sitecore.Context.User.Profile.SetCustomProperty("Interests", string.Join("|", newIDs));
            Sitecore.Context.User.Profile.Save();

            var myAccountsPage = Sitecore.Context.Database.GetItem("{FF4722A8-ACFC-4EFF-B49D-239899B7E545}");
            Sitecore.Web.WebUtil.Redirect(LinkManager.GetItemUrl(myAccountsPage));
        }

        protected void DataBound(object sender, RepeaterItemEventArgs e)
        {
            var item = e.Item.DataItem as Item;
            if (item == null) return;

            var chk = (CheckBox)e.Item.FindControl("chk");
            chk.Checked = currentTags.Contains(item.ID);

            var hid = (HiddenField)e.Item.FindControl("hid");
            hid.Value = item.ID.ToString();
        }
    }
}