﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Facets.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Facets" %>
<style type="text/css">
    #tree-menu ul li a.selected {
        font-weight: 700;
    }
</style>

<div id="tree-menu" class="panel panel-default">
    <asp:Repeater ID="rpt" runat="server" ItemType="Sitecore.ContentSearch.Linq.FacetValue" OnItemDataBound="rpt_OnItemDataBound">
        <HeaderTemplate>
            <ul>
        </HeaderTemplate>
        <ItemTemplate>
            <li style="clear: both; margin-top:3px">
                <asp:HyperLink runat="server" ID="lnk" />
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul>
        </FooterTemplate>
    </asp:Repeater>
</div>