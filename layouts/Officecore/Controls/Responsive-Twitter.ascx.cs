﻿using Officecore.Website.code.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Officecore.Website.layouts.Officecore.Controls
{
    public partial class Responsive_Twitter : DatasourceControl
    {
        protected string DataId;

        protected void Page_Load(object sender, EventArgs e)
        {
            Header.Item = this.Item;

            // Take the Data ID of the widget and pass it here. To get this ID you need to goto https://twitter.com/settings/widgets
            DataId = this.Item["data-widget-id"];
            
            DataBind();
        }
    }
}