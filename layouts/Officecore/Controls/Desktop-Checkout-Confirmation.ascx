﻿<%@ control language="C#" autoeventwireup="true" codebehind="Desktop-Checkout-Confirmation.ascx.cs" inherits="Officecore.Website.layouts.Officecore.Controls.Desktop_Checkout_Confirmation" %>
<div>
    <div class="alert alert-info">
        <h1>Thank you for your order</h1>
        <br />
        <p>An order confirmation email has been sent to your email address</p>
    </div>
</div>