﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Commerce-Toggle.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Commerce_Toggle" %>

    <input id="commercetogglebox" name="commercetogglebox" type="checkbox" <% if(toggleState) { %>checked<% } %> data-label-text="<span class='glyphicon glyphicon-shopping-cart'></span>" data-on-color="success" data-off-color="danger"/>
    <input type="hidden" id="commercevalue" name="commercevalue" />
<script>
    $ct = jQuery.noConflict();
    $ct("#commercetogglebox").bootstrapSwitch();    
    $ct('#commercetogglebox').on('switchChange', function (e, data) {        
        $ct("#commercevalue").val(data.value);
        $ct('#mainform').submit();
    });
    
</script>