﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Desktop-Analytics-Plan.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Desktop.Analytics.Desktop_Analytics_Plan" %>
<ul class="list-group">
    <asp:Repeater ID="PlanRepeater" runat="server">
        <ItemTemplate>
            <li class="<%# GetClass(Eval("Name").ToString()) %>">
                <span class="badge" style="background-color: white; color: black;"><%#Container.ItemIndex+1  %></span> <%# Eval("Name") %>
            </li>
        </ItemTemplate>
    </asp:Repeater>
</ul>
