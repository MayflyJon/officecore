﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Officecore.Website.code.Controls;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using Sitecore.Analytics;
using System.Text;
using Sitecore.Analytics.Data.DataAccess.DataSets;
using Sitecore.Data;
using Sitecore.Analytics.Data.Items;
using Sitecore.Analytics.Web;

namespace Officecore.Website.layouts.Officecore.Controls
{
    public partial class Desktop_Analytics_Overview : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Tracker.CurrentVisit != null)
            {                
                Dictionary<String, String> overview = new Dictionary<string, string>();
                //do this before loadall just want the current visit.
                // get the traffic types folder by id so we can iterate through its decendants
                Item TrafficTypes = Sitecore.Context.Database.GetItem("7E265978-8C1B-419D-BC06-0B5D101F04DF");
                Item items = TrafficTypes.Axes.GetDescendants().Where(p => p.Fields["Value"].Value == Tracker.Visitor.CurrentVisit.TrafficType.ToString()).FirstOrDefault();
                overview.Add("Traffic Type", items.Name);
                if (!Tracker.Visitor.CurrentVisit.IsCampaignIdNull())
                {
                    String campaignID = Tracker.CurrentVisit.CampaignId.ToString();
                    Item campaign = Sitecore.Context.Database.GetItem(campaignID);
                    overview.Add("Campaign", campaign.Name);
                }   
                // Dont just consider the currentvisit load data from all visits
                Tracker.Visitor.LoadAll();
                // Detect if returning visitor
                var visitCount = Tracker.Visitor.VisitCount;
                string visitType = (visitCount > 1 ? "Returning" : visitType = "New");
                overview.Add("New vs. Returning", visitType);                
                overview.Add("Engagement Value", Tracker.CurrentVisit.Value.ToString());                         
                OverviewRepeater.DataSource = overview;
                OverviewRepeater.DataBind();
            }

        }

    }
}