﻿using Officecore.Website.code.Constants;
using Sitecore.Analytics;
using Sitecore.Analytics.Data.DataAccess;
using Sitecore.Analytics.Data.DataAccess.DataSets;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Officecore.Website.layouts.Officecore.Controls.Desktop.Analytics
{
    public partial class Desktop_Analytics_Plan : System.Web.UI.UserControl
    {
        public String CurrentState = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            Visitor visitor = Tracker.Visitor;
            VisitorLoadOptions visitorLoadOption = new VisitorLoadOptions();
            visitorLoadOption.Options = VisitorOptions.AutomationStates;
            visitor.Load(visitorLoadOption);
            Sitecore.Analytics.Data.DataAccess.DataSets.VisitorDataSet.AutomationStatesRow row = visitor.DataContext.AutomationStates.Where(x => x.AutomationId == new Guid(AnalyticsConstants.ENGAGEMENT_PLAN)).FirstOrDefault();
            if (row != null)
            {
                CurrentState = row.StateName;
            }
            Item plan = Sitecore.Context.Database.GetItem(AnalyticsConstants.ENGAGEMENT_PLAN);
            PlanRepeater.DataSource = plan.GetChildren();
            PlanRepeater.DataBind();
        }

        public String GetClass(String value)
        {
            if (value == CurrentState)
            {
                return "list-group-item list-group-item-success";
            }
            return "list-group-item";
        }
    }
}