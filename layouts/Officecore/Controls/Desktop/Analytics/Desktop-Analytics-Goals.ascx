﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Desktop-Analytics-Goals.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Desktop_Analytics_Goals" %>
<div id="sessiondetailswrapper">
    <asp:Panel ID="GoalPanel" runat="server" Visible="true">
        <div class="grayborder">
            <asp:Label ID="NoGoals" Text="You have not triggered any goals yet." runat="server" CssClass="profile-table"></asp:Label>
            <asp:Repeater ID="GoalRepeater" runat="server">
                <HeaderTemplate>
                    <table cellpadding="2" cellspacing="0" class="profile-table">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><%# ((Sitecore.Analytics.Data.DataAccess.DataSets.VisitorDataSet.PageEventsRow)Eval("Event")).DateTime.ToString() %></td>
                        <td><%# ((Sitecore.Data.Items.Item)Eval("ScItem")).Name %> (<%# ((Sitecore.Data.Items.Item)Eval("ScItem")).Fields["Points"] %>)</td>
                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr>
                        <td class="alternate"><%# ((Sitecore.Analytics.Data.DataAccess.DataSets.VisitorDataSet.PageEventsRow)Eval("Event")).DateTime.ToString() %></td>
                        <td class="alternate"><%# ((Sitecore.Data.Items.Item)Eval("ScItem")).Name %> (<%# ((Sitecore.Data.Items.Item)Eval("ScItem")).Fields["Points"] %>)</td>
                    </tr>
                </AlternatingItemTemplate>
                <FooterTemplate></table></FooterTemplate>
            </asp:Repeater>
        </div>
    </asp:Panel> 
</div>
