﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Desktop-Analytics-PageViews.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Desktop_Analytics_PageViews" %>
<div id="sessiondetailswrapper">
    <asp:Panel ID="PageViewValues" runat="server" Visible="true">
        <div class="grayborder">
            <asp:Repeater ID="PageViewer" runat="server">
                <HeaderTemplate>
                    <table cellpadding="2" cellspacing="0" class="profile-table">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><%# Eval("Date") %></td>
                        <td><%# Eval("Url") %></td>
                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr>
                        <td class="alternate"><%# Eval("Date") %></td>
                        <td class="alternate"><%# Eval("Url") %></td>
                    </tr>
                </AlternatingItemTemplate>
                <FooterTemplate></table></FooterTemplate>
            </asp:Repeater>
        </div>
    </asp:Panel>
</div>
