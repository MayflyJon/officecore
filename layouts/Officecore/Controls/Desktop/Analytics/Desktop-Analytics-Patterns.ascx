﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Desktop-Analytics-Patterns.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Desktop_Analytics_Patterns" %>
  
        <asp:Panel ID="PatternValues" runat="server" Visible="false">
            <div style="padding:5px;"><sc:FieldRenderer ID="Name" runat="server" /></div>      
            <div style="padding:5px;"><sc:FieldRenderer ID="Image" runat="server" Parameters="MaxHeight=120&MaxWidth=140" /></div>  
            <div style="padding:5px;"><sc:FieldRenderer ID="Description" runat="server" /></div>          
        </asp:Panel>

