﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Desktop-Analytics-Overview.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Desktop_Analytics_Overview" %>
<div id="sessiondetailswrapper">
    <asp:Panel ID="GoalPanel" runat="server" Visible="true">
        <div class="grayborder">
            <asp:Repeater ID="OverviewRepeater" runat="server">
                <HeaderTemplate>
                    <table cellpadding="2" cellspacing="0" class="profile-table">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><%# Eval("Key") %>:</td>
                        <td><%# Eval("Value") %></td>
                    </tr>
                </ItemTemplate>
                <%--<AlternatingItemTemplate>
                    <tr>
                        <td class="alternate"><%# Eval("Key") %>:</td>
                        <td class="alternate"><%# Eval("Value") %></td>
                    </tr>
                </AlternatingItemTemplate>--%>
                <FooterTemplate></table></FooterTemplate>
            </asp:Repeater>
        </div>
    </asp:Panel>
</div>
