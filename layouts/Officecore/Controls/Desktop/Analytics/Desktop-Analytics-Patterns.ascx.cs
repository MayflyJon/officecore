﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Officecore.Website.code.Controls;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using Sitecore.Analytics;
using System.Text;
using Sitecore.Analytics.Data.DataAccess.DataSets;
using Sitecore.Data;
using Sitecore.Analytics.Data.Items;
using Sitecore.Analytics.Web;
using Officecore.Website.code.Service;

namespace Officecore.Website.layouts.Officecore.Controls
{
    public partial class Desktop_Analytics_Patterns : System.Web.UI.UserControl
    {
        ProductService ps = new ProductService();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Tracker.CurrentVisit != null)
            {
                Item evaluatorTypeProfile = null;
                if (ps.ActivateCommerce())
                {
                    evaluatorTypeProfile = Sitecore.Context.Database.GetItem("95056025-5410-43DF-BB8C-67F8FEC8F45E");
                }
                else
                {
                    evaluatorTypeProfile = Sitecore.Context.Database.GetItem("A23A2EC2-5DF0-4FD0-BD79-0FF310788C61");
                }
                // show the pattern match if there is one.
                GetPatternData(evaluatorTypeProfile);                         
            }
        }

        private void GetPatternData(Item evaluatorTypeProfile)
        {
            var personaProfile = Tracker.CurrentVisit.Profiles.Where(profile => profile.ProfileName == evaluatorTypeProfile.Name).FirstOrDefault();
            if (personaProfile != null)
            {
                // load the details about the matching pattern
                Item i = Sitecore.Context.Database.GetItem(new ID(personaProfile.PatternId));
                if (i != null)
                {
                    PatternValues.Visible = true;
                    Name.Item = i;
                    Name.FieldName = "Name";
                    Description.Item = i;
                    Description.FieldName = "Description";
                    Image.Item = i;
                    Image.FieldName = "Image";
                }
            }
        }
    }
}