﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Desktop-Profile.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Desktop_Profile" %>

<%@ Import Namespace="Sitecore.Marketing.Profiles" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<script type="text/javascript" src="/sitecore/shell/Applications/Analytics/Personalization/Chart/canvastext.js"></script>
<script type="text/javascript" src="/sitecore/shell/controls/lib/prototype/prototype.js"></script>
<script type="text/javascript" src="/sitecore/shell/Applications/Analytics/Personalization/Chart/flotr.js"></script>
<style type="text/css">
    .flotr-legend-label {
        font-size: inherit;
    }

    .flotr-legend {
        left: 420px !important;
        top: 140px !important;
        font-size: 13px;
        text-wrap:none;
    }
    .flotr-legend table { width:160px;}

    .flotr-legend-color-box div:first-of-type { border:none!important; }   

    td, body {
        font-family: 'Open Sans', Verdana;
    }
</style>

<script type="text/javascript">

    var chartData = "";
    var chartDiv = "";
    var chartTicks = "";
    var chartfontsize = "12";

    function CreateChartDiv(profileName, width, height, fontSize) {
        var div = document.createElement("div");       
        document.getElementById('Charts-content-graph').appendChild(div);
        var radarChart = parent.document.getElementById('radarChart' + profileName);
        if (radarChart != null) {
            width = radarChart.offsetWidth - 100;
            height = width / 2;
            radarChart.height = height + 100;
        }
        div.setAttribute('style', 'width:' + width + 'px;height:' + height + 'px;font-size:' + (fontSize + 4) + 'pt;float:left;');
        return div;
    }

    //var div = ;
    document.observe('dom:loaded', function initialize() {
            <%
                var helper = new RadarChartHelper();                
                Response.Write(helper.DisplayRadarCharts(400, 300, profiles));
             %>
    });


</script>

