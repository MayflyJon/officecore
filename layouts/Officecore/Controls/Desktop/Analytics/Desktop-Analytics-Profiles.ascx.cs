﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Officecore.Website.code.Controls;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using Sitecore.Analytics;
using System.Text;
using Sitecore.Analytics.Data.DataAccess.DataSets;
using Sitecore.Data;
using Sitecore.Analytics.Data.Items;
using Sitecore.Analytics.Web;
using Officecore.Website.code.Service;

namespace Officecore.Website.layouts.Officecore.Controls
{
    public partial class Desktop_Analytics_Profiles : System.Web.UI.UserControl
    {
        public String Header { get; set; }
        ProductService ps = new ProductService();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Tracker.CurrentVisit != null)
            {
                Item evaluatorTypeProfile = null;
                if (ps.ActivateCommerce())
                {
                    evaluatorTypeProfile = Sitecore.Context.Database.GetItem("95056025-5410-43DF-BB8C-67F8FEC8F45E");
                }
                else
                {
                    evaluatorTypeProfile = Sitecore.Context.Database.GetItem("A23A2EC2-5DF0-4FD0-BD79-0FF310788C61");
                }
                
                GetProfileData(evaluatorTypeProfile);
                
            }

        }       

        private void GetProfileData(Item evaluatorTypeProfile)
        {
            if (evaluatorTypeProfile != null)
            {
                List<KeyValuePair<string, float>> results = new List<KeyValuePair<string, float>>();

                if (Tracker.CurrentVisit.Profiles != null)
                {
                    

                    var selected = from profile in Tracker.Visitor.DataSet.Profiles
                                   where profile.ProfileName.Equals(evaluatorTypeProfile.Name, StringComparison.OrdinalIgnoreCase)
                                   select profile;

                    var groupedproffiles = selected.GroupBy(x => x.ProfileName);
                    foreach (var profile in groupedproffiles)
                    {
                        var profileItem = new ProfileItem(Sitecore.Context.Database.GetItem("/sitecore/system/Marketing Center/Profiles/" + profile.Key));
                        foreach (var key in profileItem.Keys)
                        {
                            results.Add(new KeyValuePair<string, float>(key.KeyName, Tracker.CurrentVisit.Profiles.Where(x => x.ProfileName.Equals(profile.Key)).FirstOrDefault().Values[key.KeyName]));
                        }

                        Header = profile.Key;
                        ProfileValues.DataSource = results;
                        ProfileValues.DataBind();

                        ProfileKeyValues.Visible = true;
                        NoProfile.Visible = false;
                    }
                }
            }
        }
    }
}