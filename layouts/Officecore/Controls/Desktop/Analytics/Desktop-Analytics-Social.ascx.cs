﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Officecore.Website.code.Controls;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using Sitecore.Analytics;
using System.Text;
using Sitecore.Analytics.Data.DataAccess.DataSets;
using Sitecore.Data;
using Sitecore.Analytics.Data.Items;
using Sitecore.Analytics.Web;
using Sitecore.Social.Client;
using Sitecore.Social.BusinessLogic.Rules.Managers;
using Sitecore.Social.BusinessLogic.Users;
using Sitecore.Security.Accounts;

namespace Officecore.Website.layouts.Officecore.Controls
{
    public partial class Desktop_Social_Overview : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SocialName.Text =   Sitecore.Context.User.DisplayName;
            SocialEmail.Text =  Sitecore.Context.User.Profile.Email;
            SocialGender.Text = GetSinglePropertyNameValue("fb_gender");
            //SocialLikes.Text = GetSinglePropertyNameValue("fb_likes");        
        }

        string GetSinglePropertyNameValue(string propertyName)
        {
            // Set the context of the user as current logged in user
            User user = Sitecore.Context.User;
            // Check to see if they are authenticated
            if (user.IsAuthenticated)
            {
                // Grab the property you are looking for from the Profile
                string item = user.Profile[propertyName];
                // Check to see if it is empty or not
                if (!string.IsNullOrEmpty(item))
                {
                    return item;
                }
            }
            // Return a message if not collected
            return "This user did not want their value shown.";
        }

    }
}