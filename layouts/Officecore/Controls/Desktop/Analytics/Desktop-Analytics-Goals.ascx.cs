﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Officecore.Website.code.Controls;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using Sitecore.Analytics;
using System.Text;
using Sitecore.Analytics.Data.DataAccess.DataSets;
using Sitecore.Data;
using Sitecore.Analytics.Data.Items;
using Sitecore.Analytics.Web;

namespace Officecore.Website.layouts.Officecore.Controls
{
    public partial class Desktop_Analytics_Goals : System.Web.UI.UserControl
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Tracker.CurrentVisit != null)
            {
                //KERN: Query the Sitecore Context rather than doing a join on the tables
                var pageEvents = Tracker.Visitor.DataContext.PageEvents.Select(pe => new EventAndItem { Event = pe, ScItem = Sitecore.Context.Database.GetItem(new ID(pe.PageEventDefinitionId)) }).Where(i => i.ScItem["IsGoal"] == "1").OrderByDescending(i => i.Event.DateTime).Take(5);
                if (pageEvents.Count() > 0)
                {
                    GoalRepeater.Visible = true;
                    NoGoals.Visible = false;                    
                    GoalRepeater.DataSource = pageEvents;
                    GoalRepeater.DataBind();
                }
            }
        }

        public class EventAndItem
        {
            public VisitorDataSet.PageEventsRow Event { get; set; }
            public Item ScItem { get; set; }
        }
    }
}