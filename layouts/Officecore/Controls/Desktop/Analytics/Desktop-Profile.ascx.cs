﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Officecore.Website.code.Controls;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using Sitecore.Analytics;
using Sitecore.Links;
using Sitecore.Analytics.Data.DataAccess.DataSets;
using System.Text;
using Sitecore.Analytics.Data;
using System.Configuration;
using Officecore.Website.code.Service;
using Officecore.Website.code.Constants;

namespace Officecore.Website.layouts.Officecore.Controls
{
    public partial class Desktop_Profile : DatasourceControl
    {
        ProductService ps = new ProductService();
        public String[] profiles = new String[2];

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ps.ActivateCommerce())
            {
                profiles[0] = AnalyticsConstants.OFFICE_PROFILE;
                profiles[1] = AnalyticsConstants.BUDGET_PROFILE;                
            }
            else
            {
                profiles[0] = AnalyticsConstants.FINANCE_PROFILE;
                profiles[1] = AnalyticsConstants.BUDGET_PROFILE;                
            }
        }
    }   
}