﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Desktop-Analytics-Social.ascx.cs"
    Inherits="Officecore.Website.layouts.Officecore.Controls.Desktop_Social_Overview" %>
<div id="sessiondetailswrapper">
    <div class="grayborder">
        Logged in using
        <img src="/sitecore/shell/Themes/Standard/Custom/24x24/facebook3.png" />
        <br /><br />
        <asp:Label ID="SocialName" runat="server"></asp:Label><br />
        <asp:Label ID="SocialEmail" runat="server"></asp:Label><br />
        <asp:Label ID="SocialGender" runat="server"></asp:Label><br />
        <asp:Label ID="SocialLikes" runat="server"></asp:Label>
    </div>
</div>
