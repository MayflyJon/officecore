﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Desktop-Analytics-Profiles.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Desktop_Analytics_Profiles" %>


<h3>Behavioural profile</h3>
<div style="padding: 0em 0em;">
    <div id="sessiondetailswrapper">
        <div class="grayborder">
            <asp:Label ID="NoProfile" Text="You have not built a behavioural profile yet." runat="server" CssClass="profile-table"></asp:Label>
            <asp:Panel ID="ProfileKeyValues" runat="server" Visible="false">

                <h4>
                    <script type="text/javascript" >
                        document.observe('dom:loaded', function initialize() {
                            document.getElementById('ProfileHeader').innerHTML = '<%= Header %>';
                        });
                    </script>
                </h4>
                <table class="profile-table">
                    <tr>
                        <td>
                            <sc:Sublayout runat="server" placeholder="content" Path="/layouts/Officecore/Controls/Desktop/Analytics/Desktop-Analytics-Patterns.ascx" ID="Sublayout7" />
                        </td>
                        <td>
                            <asp:Repeater ID="ProfileValues" runat="server">
                                <HeaderTemplate>
                                    <table cellpadding="2" cellspacing="0" style="border: 2px solid #D8D8D8;" class="profile-table">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("key")%></td>
                                        <td><%# Eval("value")%></td>
                                    </tr>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <tr>
                                        <td class="alternate"><%# Eval("key")%></td>
                                        <td class="alternate"><%# Eval("value")%></td>
                                    </tr>
                                </AlternatingItemTemplate>
                                <FooterTemplate></table></FooterTemplate>
                            </asp:Repeater>
                        </td>

                    </tr>
                </table>

            </asp:Panel>
        </div>
    </div>
</div>
