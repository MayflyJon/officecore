﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Officecore.Website.code.Controls;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using Sitecore.Analytics;
using System.Text;
using Sitecore.Analytics.Data.DataAccess.DataSets;
using Sitecore.Data;
using Sitecore.Analytics.Data.Items;
using Sitecore.Analytics.Web;

namespace Officecore.Website.layouts.Officecore.Controls
{
    public partial class Desktop_Analytics_PageViews : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Tracker.CurrentVisit != null)
            {
                // Dont just consider the currentvisit load data from all visits
                Tracker.Visitor.LoadAll();
                var pages = Tracker.CurrentVisit.GetPages().OrderByDescending(p => p.DateTime).Skip(1).Take(5);

                //DataContext.Pages
                List<PageHolder> lastFivePages = new List<PageHolder>();
                foreach (var row in pages)
                {
                    lastFivePages.Add(new PageHolder(row.PageId.ToString(), row.DateTime.ToString(), row.Url));
                }
                PageViewer.DataSource = lastFivePages;
                PageViewer.DataBind();
            }
        }

        public class PageHolder
        {
            public String Id { get; set; }
            public String Date { get; set; }
            public String Url { get; set; }

            public PageHolder(String id, String date, String url)
            {
                Id = id;
                Date = date;
                Url = url;
            }
        }
    }
}