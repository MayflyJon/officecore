﻿namespace Officecore.Website.layouts.Officecore.Controls.Desktop.News
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using code.Constants;
    using code.Controls;
    using Sitecore.ContentSearch;
    using Sitecore.ContentSearch.Linq;
    using Sitecore.ContentSearch.Linq.Utilities;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using Sitecore.Web;

    public partial class Desktop_Recent_News : DatasourceControl
    {
        private Item ReadMore { get; set; }
        protected Item NewsRoot { get; set; }

        public HashSet<ID> SearchTemplates
        {
            get
            {
                if (SearchTemplates == null)
                {
                    new HashSet<ID>();
                }
                return SearchTemplates;
            }
            set
            {
                SearchTemplates = value;
            }
        }

        public string SearchTerm
        {
            get
            {
                if (SearchTerm == null)
                {
                    return string.Empty;
                }
                return SearchTerm;
            }
            set
            {
                SearchTerm = value;
            }
        }

        private void Page_Load(object sender, EventArgs e)
        {
            SearchTemplates.Add(new ID(TemplateConstants.NewsArticleOneColumnId));
            SearchTemplates.Add(new ID(TemplateConstants.NewsArticleTwoColumnId));
            SearchTemplates.Add(new ID(TemplateConstants.NewsArticleThreeColumnId));

            ReadMore = Sitecore.Context.Database.GetItem(new ID(DataRepositoryConstants.READ_MORE_TEXT));
            NewsRoot = Sitecore.Context.Database.GetItem(new ID("{E58768BF-9A8C-4E04-B031-80F2DA354081}"));

            SearchTerm = this.Item.ID.ToShortID().ToString();

            var helper = new code.DataRepositories.SearchHelper();

            Dictionary<string, string> facets = new Dictionary<string, string>();
            var querystring = new Sitecore.Text.UrlString(WebUtil.GetRawUrl());
            foreach (var parameter in querystring.Parameters.AllKeys)
            {
                if (!string.Equals("searchStr", parameter, StringComparison.OrdinalIgnoreCase))
                {
                    if (!parameter.StartsWith("sc_"))
                    {
                        facets.Add(parameter, querystring.Parameters[parameter]);
                    }
                }
            }

            List<FacetCategory> _facets;
            string _facetList = string.Empty;
            int numResults = 0;

            IEnumerable<code.Model.ResultItem> results;
            if (Sitecore.Context.Item.ID == this.Item.ID)
            {
                results = helper.SearchWithFacets<code.Model.ResultItem>(facets, SearchTemplates, out _facets, out numResults,
                                                                  item => item.IsLatestVersion,
                                                                  item => item.DateRange);
            }
            else
            {
                results = helper.SearchWithFacets<code.Model.ResultItem>(facets, SearchTemplates, out _facets, out numResults,
                                                                  item => item.IsLatestVersion && item.Terms.Contains(SearchTerm),
                                                                  item => item.DateRange);
            }

            // TODO: Create NewsResultItem and bind that instead of looping through all ResultItems and getting item.
            var list = new List<Item>();

            foreach (var item in results)
            {
                list.Add(item.GetItem());
            }

            RecentNews.DataSource = list;
            DataBind();
        }
    }
}