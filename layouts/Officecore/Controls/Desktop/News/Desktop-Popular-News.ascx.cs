﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Officecore.Website.code.Constants;
using Sitecore.Analytics;
using Sitecore.Analytics.Data.DataAccess.DataAdapters;
using Sitecore.Analytics.Data.DataAccess.DataSets;
using Sitecore.Analytics.Data.Items;
using Sitecore.Data.DataProviders.Sql;
using Sitecore.Data.Items;
using Sitecore.Links;

namespace Officecore.Website.layouts.Officecore.Controls.Desktop.News
{
    public partial class Desktop_Popular_News : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<PopularItem> items = GetPageViews();
            if (items.Count == 0)
            {
                NoNews.Visible = true;
            } else {
                newsrepeater.DataSource = items;
                DataBind();
            }            
        }

        public List<PopularItem> GetPageViews()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["analytics"].ConnectionString;
            var builder = new SqlConnectionStringBuilder(connectionString);

            var query = "SELECT itemid, count(Url) as NumberOfVisits FROM [" + builder.InitialCatalog + "].[dbo].[Pages] where url like '%/news/%' group by itemid order by NumberOfVisits desc";
            var list = new List<PopularItem>();

            list.AddRange(DataAdapterManager.Provider.Sql.ReadMany<PopularItem>(query, GetPopularItem).Where(x => x.Item != null && x.Item.Visualization.Layout != null));            
            return list.Take(5).ToList();
        }

        private PopularItem GetPopularItem(DataProviderReader reader)
        {
            var guid = DataAdapterManager.Provider.Sql.GetGuid(0, reader);
            var count = DataAdapterManager.Provider.Sql.GetInt(1, reader);

            var item = new PopularItem(guid.ToString(), count);

            return item;
        }
    }

    public class PopularItem
    {
        public Item Item { get; set; }
        public int Count { get; set; }

        public PopularItem(string id, int count)
        {
            Item = Sitecore.Context.Database.GetItem(id);
            Count = count;
        }
    }
}