﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Desktop_Recent_News.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Desktop.News.Desktop_Recent_News" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="news-teaser">
    <div class="news-teaser-header">
        <p style="width: 25px;">
            <sc:Text runat="server" Field="title" Item="<%# NewsRoot %>" />
        </p>
    </div>
    <div class="news-teaser-content">
        <ul class="news-links">
            <asp:Repeater runat="server" ID="RecentNews" ItemType="Sitecore.Data.Items.Item">
                <ItemTemplate>
                    <li>
                        <div>
                            <span class="news-date">
                                <sc:Date runat="server" Field="date" Format="MMMM dd, yyyy" Item="<%# Item %>" />
                            </span>
                        </div>
                        <div>
                            <a href="<%# Sitecore.Links.LinkManager.GetItemUrl(NewsRoot) %>">
                            <sc:Text runat="server" Field="Display name" Item="<%# Item %>"/>
                          </a>
                        </div>

                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
        <div class="read-more-link">
            <a href="<%# Sitecore.Links.LinkManager.GetItemUrl(NewsRoot) %>">
                More news
            </a>
        </div>
    </div>
</div>
