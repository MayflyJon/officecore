﻿<%@ Control Language="C#" AutoEventWireup="true" EnableViewState="false" CodeBehind="Desktop-Popular-News.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Desktop.News.Desktop_Popular_News" %>
<div class="sidebar-section">
    <h3>Most popular stories</h3>
    <asp:Repeater ID="newsrepeater" runat="server" ItemType="Officecore.Website.layouts.Officecore.Controls.Desktop.News.PopularItem">
        <ItemTemplate>
            <div>
                <span class="news-date">
                    <sc:date format="MMMM dd, yyyy" runat="server" field="date" item="<%# Item.Item %>" />
                </span>
            </div>
            <div>
                <p>
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="<%# Sitecore.Links.LinkManager.GetItemUrl(Item.Item) %>">
                        <sc:text runat="server" id="itemTitle" field="Title" item="<%# Item.Item %>" />
                        (<asp:Literal ID="Literal1" runat="server" Text="<%# Item.Count %>" />)
                    </asp:HyperLink>
                </p>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <asp:label runat="server" ID="NoNews" Visible="false" Text="No recent activity, when more news articles have been viewed the most popular ones will be shown here." />
</div>