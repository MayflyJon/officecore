﻿<%@ control language="C#" autoeventwireup="true" codebehind="Desktop_Account_Orders.ascx.cs" inherits="Officecore.Website.layouts.Officecore.Controls.Desktop.MyAccount.Desktop_Account_Orders" %>

    <div class="panel-group" id="accordionOrders">
        <div class="panel panel-default">
            <div class="panel-heading accountheadingpanel">
                <h4 class="panel-title accountheading">My Recent Order History</h4>
            </div>
            <div id="orders" class="panel-collapse collapse in">
                <div class="panel-body">
                    
                    <asp:Panel runat="server" id="pnlNoOrders" Visible="false">
                        <div style="padding: 15px">
                            No orders found.
                        </div>
                    </asp:Panel>
                    
                    <%-- Do a check to see if we can actually get this information, if we can hyperlink things like the john lewis site to redirect them to an update form to update their details --%>
                    <asp:Repeater ID="myOrderHistory" runat="server" ItemType="UCommerce.EntitiesV2.PurchaseOrder" OnItemDataBound="InnerItemDataBound">
                        <ItemTemplate>
                        <div class="panel panel-default" style="margin: 5px;">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#<%#Container.FindControl("panelorder").ClientID %>" style="padding:0px;">
                                <table class="table" style="margin-bottom:0px;">
                                    <tr>
                                        <td style="font-weight: 800; width:20%; border:none;">Order ID:<%# Item.OrderId %></td>
                                        <td style="font-weight: 800; width:40%; border:none;">Date:<%# Item.CompletedDate %></td>
                                        <td style="font-weight: 800; width:16%; border:none;">Total:£<%# ((decimal)Item.OrderTotal).ToString("N2")%></td>
                                    </tr>
                                </table>
                            </div>
                            <asp:panel runat="server" ID="panelorder" CssClass="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="panel panel-default" style="margin:5px;">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Item Name</th>
                                                    <th>Short Description</th>
                                                    <th>Quantity</th>
                                                    <th>Sub total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="myOrderLineHistory" runat="server" ItemType="UCommerce.EntitiesV2.OrderLine">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td><%# Item.Id %></td>
                                                            <td><%# Item.ProductName %></td>
                                                            <td><%# Item.Quantity %></td>
                                                            <td>£<%# ((decimal)Item.Total).ToString("N2")%></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </asp:panel>
                        </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </div>

