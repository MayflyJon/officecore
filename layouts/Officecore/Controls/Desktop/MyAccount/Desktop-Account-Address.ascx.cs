﻿using System;

using Sitecore.Security.Accounts;

namespace Officecore.Website.layouts.Officecore.Controls.Desktop.MyAccount
{
    public partial class Desktop_Account_Address : System.Web.UI.UserControl
    {
        private void Page_Load(object sender, EventArgs e)
        {
            User user = Sitecore.Context.User;
            house.Text = user.Profile.GetCustomProperty("BilHouse");
            street.Text = user.Profile.GetCustomProperty("BilStreet");
            city.Text = user.Profile.GetCustomProperty("BilCity");
            county.Text = user.Profile.GetCustomProperty("BilCounty");
            country.Text = user.Profile.GetCustomProperty("BilCountry");
            postcode.Text = user.Profile.GetCustomProperty("BilPostcode");
        }
    }
}