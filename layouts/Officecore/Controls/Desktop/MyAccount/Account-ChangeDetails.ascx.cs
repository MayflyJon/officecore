﻿using System;
using System.Linq;

using Sitecore.Buckets.Extensions;

using Officecore.Website.code.Service;
using UCommerce.EntitiesV2;
using User = Sitecore.Security.Accounts.User;

namespace Officecore.Website.layouts.Officecore.Controls.Desktop.MyAccount
{
    using Sitecore.Diagnostics;

    public partial class Address_ChangeDetails : System.Web.UI.UserControl
    {
        User user = Sitecore.Context.User;
        private bool wasProfileEmpty;
        private string email = User.Current.Profile.Email;


        private void Page_Load(object sender, EventArgs e)
        {
            Assert.IsTrue(Sitecore.Context.IsLoggedIn, "Must be logged in");

            this.wasProfileEmpty = string.IsNullOrEmpty(user.Profile.GetCustomProperty("BilFirstName"));

            if (!IsPostBack)
            {
                ddlTitle.SelectedValue = user.Profile.GetCustomProperty("BilTitle");
                txtFName.Text = user.Profile.GetCustomProperty("BilFirstName");
                txtLName.Text = user.Profile.GetCustomProperty("BilLastName");
                txtPhone.Text = user.Profile.GetCustomProperty("BilPhone");
                txtMPhone.Text = user.Profile.GetCustomProperty("BilMobile");
                txtEmail.Text = user.Profile.Email;

                txtHouseName.Text = user.Profile.GetCustomProperty("BilHouse");
                txtStreetName.Text = user.Profile.GetCustomProperty("BilStreet");
                txtCity.Text = user.Profile.GetCustomProperty("BilCity");
                txtCounty.Text = user.Profile.GetCustomProperty("BilCounty");
                txtPostcode.Text = user.Profile.GetCustomProperty("BilPostcode");

                ddlCountry.DataSource = Country.All();
                ddlCountry.DataBind();

                var country = user.Profile.GetCustomProperty("BilCountry");
                if (country.IsNotEmpty())
                {
                    var countryObject = Country.All().FirstOrDefault(i => i.Name == country);
                    if (countryObject != null) ddlCountry.SelectedValue = countryObject.Id.ToString();
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            CustomerService _cs = new CustomerService();
            if (_cs.UserNameExists(Sitecore.Context.User.Name))
            {
                if (txtEmail.Text != Sitecore.Context.User.Profile.Email)
                {
                    //then its changed, so next we check if it doesnt exists in the database
                    email = !_cs.UserEmailExists(txtEmail.Text) ? txtEmail.Text : string.Empty;
                }

                //therefore its changed from the original and not being currently used in the database, so set it.
                _cs.UpdateCustomer("Bil", 
                    title: ddlTitle.SelectedValue, 
                    firstname: txtFName.Text, 
                    lastname: txtLName.Text, 
                    phone: txtPhone.Text, 
                    mobile: txtMPhone.Text, 
                    email: email, 
                    house: txtHouseName.Text, 
                    street: txtStreetName.Text, 
                    city: txtCity.Text, 
                    county: txtCounty.Text, 
                    country: ddlCountry.SelectedItem.Text, 
                    postcode: txtPostcode.Text);

                var redirectID = wasProfileEmpty
                    ? "{BE45BB90-8BD7-47A1-8C89-4796A17CAB75}" // My interests
                    : "{9B0EFB29-61C2-4771-9E01-1A8471D695A6}"; // My account

                Response.Redirect(Sitecore.Links.LinkManager.GetItemUrl(Sitecore.Context.Database.GetItem(redirectID)));
            }
        }
    }
}