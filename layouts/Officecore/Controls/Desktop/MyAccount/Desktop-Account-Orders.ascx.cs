﻿using Officecore.Website.code.Service;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;

namespace Officecore.Website.layouts.Officecore.Controls.Desktop.MyAccount
{
    public partial class Desktop_Account_Orders : System.Web.UI.UserControl
    {
        private void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var customerService = new CustomerService();

                IEnumerable<PurchaseOrder> orderHistory = customerService.GetCustomerOrders();
                myOrderHistory.DataSource = orderHistory;
                myOrderHistory.DataBind();

                if (myOrderHistory.Items.Count == 0)
                {
                    pnlNoOrders.Visible = true;
                }
            }
        }

        protected void InnerItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var order = e.Item.DataItem as PurchaseOrder;
            if (order == null) return;

            Repeater childRepeater = (Repeater)e.Item.FindControl("myOrderLineHistory");

            childRepeater.DataSource = order.OrderLines;
            childRepeater.DataBind();
        }
    }
}