﻿using Officecore.Website.code.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Officecore.Website.layouts.Officecore.Controls.Desktop.Global
{
    public partial class Desktop_SidebarText : DatasourceControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            title.Item = this.Item;
        }
    }
}