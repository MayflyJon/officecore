﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Desktop-ContactUs.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Desktop.Desktop_ContactUs" %>


 <% if (!Sitecore.Context.PageMode.IsPageEditorEditing)
       { %>
<style type="text/css">
    #map-canvas {
        height: 450px;
        width: 600px;
    }
</style>
<script type="text/javascript">
    var points = "";
</script>
<script type="text/javascript"
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCuknx7eNttt8b2qD9clfSUC8oJvD1s7Ag&sensor=false">
</script>
<div id="map-canvas" />
<script type="text/javascript">
    var latlons = [<%= LatLonList %>];
    function initialize() {
        var mapOptions = {
            zoom: 1,
            center: new google.maps.LatLng(14.083301, -92.460937)
        };

        var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

        for (var i = 0; i < latlons.length; i++) {
            var myinfowindow = new google.maps.InfoWindow({
                content: latlons[i][3],
                maxWidth: 300
            });
            var marker = new google.maps.Marker({
                map: map,
                position: new google.maps.LatLng(latlons[i][0], latlons[i][1]),
                title: latlons[i][2],
                infowindow: myinfowindow
            });
            google.maps.event.addListener(marker, 'click', function () {
                this.infowindow.open(map, this);
            });

        }
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>

    <% } else { %>
<div style="width:100%; height:230px; text-align:center; vertical-align:middle; border:1px dashed lightGrey; padding:60px;"><p>Your Google Map will render here in Preview mode.</p></div>
<% } %>