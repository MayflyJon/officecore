﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Desktop_Cart.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Desktop.Products.Desktop_Cart" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="store" Namespace="Officecore.Website.code.Model" Assembly="Officecore.Website" %>

<div class="dropdown col-lg-5">
    <a data-toggle="dropdown" id="drop" href="#">
        <img class="cart-image" src="/assets/images/icons/shopping_cart2.png" />
        <span class="badge badge-basket">
            <asp:Literal runat="server" ID="basketBadge" />
        </span>
    </a>
    <asp:Repeater runat="server" ID="dropdownRepeater" ItemType="UCommerce.EntitiesV2.Product">
        <HeaderTemplate>
            <ul id="cart-dropdown" class="dropdown-menu" role="menu" aria-labelledby="drop">
        </HeaderTemplate>
        <ItemTemplate>
            <li role="presentation" class="presentation">
                <div class="panel product-overview panel-default row" style="margin-left: 0px; margin-right: 0px; padding: 5px 0px 5px 0px;">
                    <div class="col-md-3" style="margin-top: 8px;">
                        <asp:HyperLink runat="server" ID="hlProductImage">
                            <store:image runat="server" id="imgProduct" width="50" maxwidth="50" />
                        </asp:HyperLink>
                    </div>
                    <div class="col-md-5" style="margin-top: 8px;">
                        <asp:HyperLink runat="server" ID="hlProduct" />
                    </div>
                    <div class="col-md-4" style="padding-left: 0px; padding-right: 10px;">
                        <div class="input-group" style="margin-bottom:5px;">
                            <span class="input-group-addon">
                                <strong>Qty</strong>
                            </span>
                            <asp:TextBox ID="amount" ClientIDMode="Static" value="13" runat="server" class="form-control"></asp:TextBox>
                        </div>
                        <span>
                            <button id="btnUpdateQuantities" style="height: 34px;" type="button" class="btn btn-success pull-left" clientidmode="static" onserverclick="btnUpdateQuantities_Click" runat="server"><span class="glyphicon glyphicon-repeat"></span></button>
                            <button id="btnRemoveLine" style="height: 34px;" name="basket-remove-item" type="button" class="btn btn-danger pull-right" clientidmode="static" onserverclick="btnRemoveLine_Click" runat="server"><span class="glyphicon glyphicon-remove-circle"></span></button>
                        </span>
                    </div>
                </div>
            </li>
        </ItemTemplate>
        <FooterTemplate>
            <li role="presentation" class="presentation">
                <div class="panel panel-default">
                    <table class="table" style="text-align: center" runat="server" id="overviewtable">
                        <tr>
                            <td>Subtotal:</td>
                            <td>
                                <asp:Literal runat="server" ID="litSubtotal" />
                            </td>
                        </tr>
                        <tr>
                            <td>VAT:</td>
                            <td>
                                <asp:Literal ID="litTax" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Total:</strong></td>
                            <td>
                                <strong>
                                    <asp:Literal ID="litTotal" runat="server" /></strong>
                            </td>
                        </tr>
                    </table>
                    <table class="table" style="text-align: center" visible="false" runat="server" id="emptyBasket">
                        <tr>
                            <td>
                                <asp:Literal ID="Literal1" runat="server" Text="Your basket is empty" />
                            </td>
                        </tr>
                    </table>
                </div>
            </li>
            <div class="panel-footer">
                <div class="pull-right">
                    <a href="/cart/" class="btn btn-large btn-default" id="innercartdetails" runat="server">Cart Details</a>
                    <a class="btn btn-large btn-default btn-success" href="/cart/address" id="innercheckout" runat="server">Checkout</a>
                </div>
                <div class="clearfix"></div>
            </div>
            </ul>
        </FooterTemplate>
    </asp:Repeater>
</div>

<div class="col-lg-4 col-lg-offset-1">
    <a class="btn btn-large btn-success" href="/cart/address" id="checkout" runat="server">Checkout</a>
</div>
