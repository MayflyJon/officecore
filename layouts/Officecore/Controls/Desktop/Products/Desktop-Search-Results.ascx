﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Desktop-Search-Results.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Mobile.Desktop_Search_Results" %>
<%@ Register Src="~/layouts/Officecore/Controls/Desktop/Products/Desktop-Product-Teaser.ascx" TagPrefix="store" TagName="Product" %>
<div class="col-md-12 desktop-search-results">
    <asp:ListView runat="server" ID="lvProducts" ItemType="Officecore.Website.code.Model.Product" GroupItemCount="3">
        <GroupTemplate>
            <div class="row">
                <div id="itemPlaceholder" runat="server"></div>
            </div>
        </GroupTemplate>
        <ItemTemplate>
            <div class="col-md-4">
                <store:product runat="server" id="Product" currentproduct="<%# Item %>" />
            </div>
        </ItemTemplate>
    </asp:ListView>
</div>