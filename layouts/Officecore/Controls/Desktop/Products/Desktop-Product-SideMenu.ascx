﻿<%@ Control Language="C#" EnableViewState="false" AutoEventWireup="true" CodeBehind="Desktop_Product_SideMenu.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Desktop.Desktop_Product_SideMenu" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div id="tree-menu" class="panel panel-default">
    <asp:Repeater ID="ProductCategories" runat="server" ItemType="Officecore.Website.code.Model.Category" DataSource="<%# GetCategories() %>">
        <HeaderTemplate>
            <ul>
        </HeaderTemplate>
        <ItemTemplate>
            <li style="clear: both; margin-top:3px">
                <a href="<%# Item.Url  %>">
                    <%# Item.Name %>
                </a>
                <asp:Repeater ID="InnerProductCategories" runat="server" ItemType="Officecore.Website.code.Model.Category" DataSource="<%# GetCategories(Item) %>">
                    <HeaderTemplate>
                        <ul class="nav nav-pills nav-stacked">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li class="<%# GetClass(Item) %>">
                            <a href="<%# Item.Url %>" class="<%# GetClass(Item) %>">
                                <%# Item.Name %>
                            </a>
                        </li>
                    </ItemTemplate>
                    <FooterTemplate>
                        </ul>
                    </FooterTemplate>
                </asp:Repeater>
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul>
        </FooterTemplate>
    </asp:Repeater>
</div>

