﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Desktop-ProductReview.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Desktop.Desktop_ProductReview" %>


<div class="panel-body" style="margin:10px 0px">
    <div class="col-md-3">
        <asp:Literal ID="litDisplayStars" runat="server" />
    </div>
    <div class="col-md-9">
        <div class="col-md-12">
            <b><asp:Literal runat="server" ID="ReviewTitle" /></b>
        </div>
        <div class="col-md-12">
            Reviewed on <asp:Literal ID="ReviewDate" runat="server" />
        </div>
        <div class="col-md-12">
            <b>By <asp:Literal runat="server" ID="Reviewer" /></b>
        </div>
        <div class="col-md-12">
            <asp:Literal runat="server" ID="ReviewText" />
        </div>
    </div>
</div>
