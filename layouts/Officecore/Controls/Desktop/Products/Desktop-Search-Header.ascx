﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Desktop-Search-Header.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Desktop.Desktop_Search_Header" %>
<div class="input-group" style="margin-bottom:15px;">
    <input class="form-control" type="text" id="SearchTermHeader" name="SearchTermHeader" value="<%= SearchTermHeader %>"/>
    <span class="input-group-btn"  style="line-height: 1.45em">
        <button type="submit" runat="server" ID="btnHeaderSearch" name="btnHeaderSearch" class="btn btn-success"  OnServerClick="btnHeaderSearch_Click"><span class="glyphicon glyphicon-search"></span></button>
    </span>
</div>
