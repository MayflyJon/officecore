﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Officecore.Website.code.Helpers;
using UCommerce.Api;
using UCommerce.EntitiesV2;
using UCommerce.Runtime;

namespace Officecore.Website.layouts.Officecore.Controls.Desktop
{
    public partial class Desktop_ProductReview : System.Web.UI.UserControl
    {
        public ProductReview Review { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Review == null)
            {
                if (SiteContext.Current.CatalogContext.CurrentProduct.ProductReviews.Count > 0)
                {
                    Review = SiteContext.Current.CatalogContext.CurrentProduct.ProductReviews.First();
                }
            }
            else
            {
                ReviewTitle.Text = Review.ReviewHeadline;
                ReviewDate.Text = string.Format(CultureInfo.CurrentCulture, "{0:D}", Review.CreatedOn);
                Reviewer.Text = Review.Customer.FirstName; 
                ReviewText.Text = Review.ReviewText;
                litDisplayStars.Text = UcommerceHelper.GetReviewRating(Review);
            }
        }
    }
}