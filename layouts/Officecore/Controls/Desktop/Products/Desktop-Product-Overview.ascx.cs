﻿namespace Officecore.Website.layouts.Officecore.Controls.Desktop
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using code.Helpers;
    using code.DataRepositories;
    using UCommerce.Api;
    using UCommerce.EntitiesV2;
    using UCommerce.Runtime;
    using UCommerce.Search.Facets;
    using code.Service;
    using Sitecore.Data.Items;

    /// <summary>
    /// TODO: This control might be obsolete, we could use the Desktop-Search-Result control in the same fashion we use Desktop-Search-Facets
    /// </summary>
    public partial class Desktop_Product_Overview : System.Web.UI.UserControl
    {
        ProductRepository repo = new ProductRepository();
        ProductService ps = new ProductService();
        Category currentCategory;

        private void Page_Load(object sender, EventArgs e)
        {
            if (repo.ActivateCommerce())
            {
                if (SiteContext.Current.CatalogContext.CurrentCategory == null)
                {
                    currentCategory = ps.GetUcommerceCategory(Sitecore.Context.Item);
                }
                else
                {
                    currentCategory = SiteContext.Current.CatalogContext.CurrentCategory;
                }
                lvProducts.DataSource = repo.GetProductsWithFacets(currentCategory);
            }
            else
            {
                lvProducts.DataSource = ps.GetProductsByCategory(Sitecore.Context.Item);
            }
            lvProducts.DataBind();
        }
    }
}