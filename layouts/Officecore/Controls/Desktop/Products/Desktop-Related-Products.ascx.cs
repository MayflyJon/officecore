﻿namespace Officecore.Website.layouts.Officecore.Controls.Desktop
{
    using System;
    using System.Collections.Generic;
    using code.DataRepositories;
    using code.Controls;
    using UCommerce.EntitiesV2;
    using UCommerce.Runtime;
    using code.Helpers;
    using code.Constants;
    using code.Service;

    public partial class Desktop_Related_Products : DatasourceControl
    {
        private ProductRepository repo = new ProductRepository();
        private ProductService ps = new ProductService();
        public code.Model.Product CurrentProduct { get; set; }

        private void Page_Load(object sender, EventArgs e)
        {
            heading.Text = Item[FieldConstants.Ucommerce.ProductRelationTypeHeading];
            DataBind();
        }

        /// <summary>
        /// Get the related products for the current product
        /// </summary>
        /// <returns>The list of related products</returns>
        public List<code.Model.Product> GetRelatedProducts()
        {
            if (SiteContext.Current.CatalogContext.CurrentProduct == null)
            {
                return repo.GetRelatedProducts(ps.GetUcommerceProduct(Sitecore.Context.Item), UcommerceHelper.GetProductRelationType(this.Item));
            }
            else
            {
                return repo.GetRelatedProducts(SiteContext.Current.CatalogContext.CurrentProduct, UcommerceHelper.GetProductRelationType(this.Item));
            }
            
        }
    }
}