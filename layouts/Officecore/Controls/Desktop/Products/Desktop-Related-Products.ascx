﻿<%@ control language="C#" autoeventwireup="true" codebehind="Desktop_Related_Products.ascx.cs" inherits="Officecore.Website.layouts.Officecore.Controls.Desktop.Desktop_Related_Products" %>
<%@ register tagprefix="sc" namespace="Sitecore.Web.UI.WebControls" assembly="Sitecore.Kernel" %>
<%@ register src="~/layouts/Officecore/Controls/Desktop/Products/Desktop-Product-Teaser.ascx" tagprefix="store" tagname="Product" %>

        <h4>
            <asp:literal runat="server" id="heading" />
        </h4>
        <asp:repeater id="relatedProducts" runat="server" itemtype="Officecore.Website.code.Model.Product" selectmethod="GetRelatedProducts">
        <ItemTemplate>
            <store:Product runat="server" id="Teaser"  currentproduct="<%# Item %>"/>
        </ItemTemplate>
    </asp:repeater>
