﻿using System;
using Officecore.Website.code.Controls;
using Officecore.Website.code.Helpers;
using UCommerce.Api;
using UCommerce.EntitiesV2;
using UCommerce.Extensions;
using Officecore.Website.code.Service;

namespace Officecore.Website.layouts.Officecore.Controls.Desktop
{
    public partial class Desktop_Product_Teaser : System.Web.UI.UserControl
    {
        public Category CurrentCategory { get; set; }
        public code.Model.Product CurrentProduct { get; set; }
        public ProductService ps = new ProductService();
        public Boolean ActivateCommerce = true;

        protected void Page_Load(object sender, EventArgs e)
        {
            ActivateCommerce = ps.ActivateCommerce();
            if (CurrentProduct == null)
            {
                Visible = false; 
                return;
            }
            rating.Text = UcommerceHelper.GetProductRating(CurrentProduct, "");
        }

        public void btnAddToBasket_Click(object sender, EventArgs e)
        {
            //Display a confirmation message on the screen
       
            TransactionLibrary.AddToBasket(1, CurrentProduct.Sku, CurrentProduct.VariantSku);
            Response.Redirect(Request.RawUrl);
        }
    }
}