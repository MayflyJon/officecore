﻿<%@ control language="C#" autoeventwireup="true" codebehind="Desktop-Product-Teaser.ascx.cs" inherits="Officecore.Website.layouts.Officecore.Controls.Desktop.Desktop_Product_Teaser" %>
<div class="product-overview panel panel-default panel-body" style="padding: 15px !important">    
    <div>
        <a href="<%= CurrentProduct.Url %>">
            <img src="<%# CurrentProduct.ThumbnailImage %>" width="100%" class="thumbnail" />
        </a>
    </div>
    <h4 style="margin: 0;">
        <asp:hyperlink runat="server" id="hlProduct" Text="<%# CurrentProduct.DisplayName %>" NavigateUrl="<%# CurrentProduct.Url %>"/>
    </h4>
    <% if(ActivateCommerce) {%>
        <h4 class="product-price"><a href="<%# CurrentProduct.Url %>"><asp:literal runat="server" id="litPrice" Text="<%# CurrentProduct.Price %>"/></a></h4>
        <asp:literal id="rating" runat="server" />
     <% } %>
    <div class="teaser-description" style="font-size: 14px;">
        <asp:literal id="description" runat="server" Text="<%# CurrentProduct.ShortDescription %>" />
    </div>
    <% if(ActivateCommerce) {%>
        <div class="pull-right" style="padding-bottom:12px;">
            <button id="Button1" type="button" class="btn btn-success pull-right" runat="server" onserverclick="btnAddToBasket_Click">
                <span class="glyphicon glyphicon-shopping-cart"></span>
                <asp:literal runat="server" id="Submit" Text="<%# CurrentProduct.Submit %>"/>
            </button>
        </div>
    <% } %>
</div>
