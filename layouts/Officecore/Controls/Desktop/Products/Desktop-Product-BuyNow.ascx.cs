﻿namespace Officecore.Website.layouts.Officecore.Controls.Desktop.Products
{
    using System;
    using code.Helpers;
    using UCommerce.Api;
    using UCommerce.EntitiesV2;
    using UCommerce.Runtime;
    using code.Service;

    public partial class Desktop_Product_BuyNow : System.Web.UI.UserControl
    {
        code.Model.Product CurrentProduct;
        ProductService ps = new ProductService();

        private void Page_Load(object sender, EventArgs e)
        {
            if (SiteContext.Current.CatalogContext.CurrentProduct == null)
            {
                CurrentProduct = new code.Model.Product(ps.GetUcommerceProduct(Sitecore.Context.Item));
            }
            else
            {
                CurrentProduct = new code.Model.Product(SiteContext.Current.CatalogContext.CurrentProduct);
            }
            productCode.Text = CurrentProduct.Sku;
            rating.Text = UcommerceHelper.GetProductRating(CurrentProduct, String.Empty);
            productPrice.Text = CurrentProduct.Price;
        }

        public void btnAddToBasket_Click(object sender, EventArgs e)
        {
            int q = 0;
            if (int.TryParse(quantity.Value, out q) && q > 0)
            {
                TransactionLibrary.AddToBasket(q, CurrentProduct.Sku, CurrentProduct.VariantSku);
            }
            Response.Redirect(Request.RawUrl);
        }
    }
}