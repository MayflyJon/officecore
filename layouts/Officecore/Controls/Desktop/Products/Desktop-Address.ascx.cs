﻿using System;
using UCommerce.Api;
using Officecore.Website.code.Service;

namespace Officecore.Website.layouts.Officecore.Controls.Desktop.Products
{
    using System.Linq;

    public partial class Desktop_Address : System.Web.UI.UserControl
    {
        readonly CustomerService cs = new CustomerService();

        private void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var countries = UCommerce.EntitiesV2.Country.All().ToList();

                billingCountry.DataSource = countries;
                billingCountry.DataBind();
                billingCountry.SelectedValue = "10";

                shippingCountry.DataSource = countries;
                shippingCountry.DataBind();
                shippingCountry.SelectedValue = "10";

                var basket = TransactionLibrary.GetBasket(true).PurchaseOrder;
                if (basket.OrderLines.Count == 0)
                {
                    btnSubmit.Attributes.Add("disabled", "disabled");
                }

                var user = Sitecore.Context.User;
                if (user != null && user.Profile != null)
                {
                    billingTitle.Value = user.Profile.GetCustomProperty("BilTitle");
                    billingFirstName.Value = user.Profile.GetCustomProperty("BilFirstName");
                    billingLastName.Value = user.Profile.GetCustomProperty("BilLastName");
                    billingEmail.Value = user.Profile.Email;
                    billingPhone.Value = user.Profile.GetCustomProperty("BilPhone");
                    billingMobile.Value = user.Profile.GetCustomProperty("BilMobile");

                    billingHouse.Value = user.Profile.GetCustomProperty("BilHouse");
                    billingStreet.Value = user.Profile.GetCustomProperty("BilStreet");
                    billingCity.Value = user.Profile.GetCustomProperty("BilCity");
                    billingCounty.Value = user.Profile.GetCustomProperty("BilCounty");
                    billingPostalCode.Value = user.Profile.GetCustomProperty("BilPostcode");

                    var countryName = user.Profile.GetCustomProperty("BilCountry");
                    if (countryName != null)
                    {
                        var country = countries.FirstOrDefault(x => x.Name == countryName);

                        if (country != null)
                        {
                            billingCountry.SelectedValue = country.Id.ToString();
                        }
                    }

                    checkHideShipping.Checked = user.Profile.GetCustomProperty("DelUseBillingAddress") == "1";

                    shippingTitle.Value = user.Profile.GetCustomProperty("DelTitle");
                    shippingFirstName.Value = user.Profile.GetCustomProperty("DelFirstName");
                    shippingLastName.Value = user.Profile.GetCustomProperty("DelLastName");
                    shippingEmail.Value = user.Profile.Email;
                    shippingPhone.Value = user.Profile.GetCustomProperty("DelPhone");
                    shippingMobile.Value = user.Profile.GetCustomProperty("DelMobile");
                    shippingHouse.Value = user.Profile.GetCustomProperty("DelHouse");
                    shippingStreet.Value = user.Profile.GetCustomProperty("DelStreet");
                    shippingCity.Value = user.Profile.GetCustomProperty("DelCity");
                    shippingCounty.Value = user.Profile.GetCustomProperty("DelCounty");
                    shippingPostalCode.Value = user.Profile.GetCustomProperty("DelPostcode");

                    countryName = user.Profile.GetCustomProperty("DelCountry");
                    if (countryName != null)
                    {
                        var country = countries.FirstOrDefault(x => x.Name == countryName);

                        if (country != null)
                        {
                            shippingCountry.SelectedValue = country.Id.ToString();
                        }
                    }
                }
            }
        }

        private void DoUcommerce()
        {
            TransactionLibrary.EditBillingInformation(
                billingFirstName.Value,
                billingLastName.Value,
                billingEmail.Value,
                billingPhone.Value,
                billingMobile.Value,
                string.Empty, // won't supply company
                billingHouse.Value,
                billingStreet.Value,
                billingPostalCode.Value,
                billingCity.Value,
                billingCounty.Value,
                billingTitle.Value,
                Convert.ToInt32(billingCountry.SelectedValue)
            );

            if (checkHideShipping.Checked)
            {
                TransactionLibrary.EditShippingInformation(
                    billingFirstName.Value,
                    billingLastName.Value,
                    billingEmail.Value,
                    billingPhone.Value,
                    billingMobile.Value,
                    string.Empty, // company not provided
                    billingHouse.Value,
                    billingStreet.Value,
                    billingPostalCode.Value,
                    billingCity.Value,
                    billingCounty.Value,
                    billingTitle.Value,
                    Convert.ToInt32(billingCountry.SelectedValue)
                );
            }
            else
            {
                TransactionLibrary.EditShippingInformation(
                    shippingFirstName.Value,
                    shippingLastName.Value,
                    shippingEmail.Value,
                    shippingPhone.Value,
                    shippingMobile.Value,
                    string.Empty, // company not provided
                    shippingHouse.Value,
                    shippingStreet.Value,
                    shippingPostalCode.Value,
                    shippingCity.Value,
                    shippingCounty.Value,
                    shippingTitle.Value,
                    Convert.ToInt32(shippingCountry.SelectedValue)
                );
            }
        }

        public void btnBillingUpdate_Click(object sender, EventArgs e)
        {
            NeedNewUser();
            this.cs.UpdateCustomer("Bil",
                title: billingTitle.Value,
                firstname: billingFirstName.Value,
                lastname: billingLastName.Value,
                phone: billingPhone.Value,
                mobile: billingMobile.Value,
                house: billingHouse.Value,
                street: billingStreet.Value,
                city: billingCity.Value,
                county: billingCounty.Value,
                country: billingCountry.SelectedItem.Text,
                postcode: billingPostalCode.Value);

            Sitecore.Context.User.Profile.SetCustomProperty("DelUseBillingAddress", checkHideShipping.Checked ? "1" : "0");
            if (checkHideShipping.Checked)
            {
                this.cs.UpdateCustomer("Del",
                    title: billingTitle.Value,
                    firstname: billingFirstName.Value,
                    lastname: billingLastName.Value,
                    phone: billingPhone.Value,
                    mobile: billingMobile.Value,
                    house: billingHouse.Value,
                    street: billingStreet.Value,
                    city: billingCity.Value,
                    county: billingCounty.Value,
                    country: billingCountry.SelectedItem.Text,
                    postcode: billingPostalCode.Value);
            }
            else
            {
                this.cs.UpdateCustomer("Del",
                    title: shippingTitle.Value,
                    firstname: shippingFirstName.Value,
                    lastname: shippingLastName.Value,
                    phone: shippingPhone.Value,
                    mobile: shippingMobile.Value,
                    house: shippingHouse.Value,
                    street: shippingStreet.Value,
                    city: shippingCity.Value,
                    county: shippingCounty.Value,
                    country: shippingCountry.SelectedItem.Text,
                    postcode: shippingPostalCode.Value);
            }

            this.DoUcommerce();

            // TODO: un-hardcode the URL
            Response.Redirect("/cart/Shipping");
        }

        private void NeedNewUser()
        {
            if (Sitecore.Context.IsLoggedIn) return;

            var user = this.cs.GetUserByEmailAddress(billingEmail.Value);
            if (user != null)
            {
                Sitecore.Security.Authentication.AuthenticationManager.Login(user);
                return;
            }

            var newUsername = this.cs.EscapeEmailToUsername(billingEmail.Value);

            var newUser = this.cs.CreateUser(newUsername, "a", billingTitle.Value, billingFirstName.Value, billingLastName.Value, billingPhone.Value, billingMobile.Value, billingEmail.Value);
            Sitecore.Security.Authentication.AuthenticationManager.Login(newUser);
        }
    }
}