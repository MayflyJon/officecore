﻿namespace Officecore.Website.layouts.Officecore.Controls.Desktop.Products
{
    using System;
    using System.Collections.Generic;
    using code.DataRepositories;
    using code.Helpers;
    using UCommerce.EntitiesV2;

    public partial class Desktop_Product_Recently_Viewed : System.Web.UI.UserControl
    {
        private ProductRepository repo = new ProductRepository();
        private int maxWidth = 226;

        private void Page_Load(object sender, EventArgs e)
        {
            title.Text = DictionaryHelper.GetText("Recently viewed");
            var list = repo.GetRecentlyViewedProduct();
            lvProducts.DataSource = list;

            if (list.Count > 2)
            {
                recentlyViewedGroup.Style.Add("width", maxWidth * list.Count + "px");
            }
            else
            {
                recentlyViewedGroup.Style.Add("width", maxWidth * 3 + "px");  
            }
            DataBind();

            if (list.Count <= 0)
            {
                recentlyViewed.Visible = false;
            }
        }
    }
}