﻿using Sitecore.Web;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Officecore.Website.layouts.Officecore.Controls.Desktop
{
    public partial class Desktop_Search : System.Web.UI.UserControl
    {
       public String SearchTerm { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Form.Get("SearchTerm") != null)
            {
                SearchTerm = Request.Form.Get("SearchTerm").ToString();
            }
            else 
            {
                SearchTerm = Server.UrlDecode(WebUtil.GetQueryString("SearchTermHeader"));
            }
        }
    }
}