﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Desktop_Product_BuyNow.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Desktop.Products.Desktop_Product_BuyNow" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

        <ul class="list-group">
            <li class="list-group-item">
                <h3><asp:Literal runat="server" ID="productPrice" /></h3>
                <asp:Literal runat="server" ID="rating"/>
            </li>
            <li class="list-group-item">
                Product code: <asp:Literal ID="productCode" runat="server" />
            </li>
            <li class="list-group-item">
                <b>Additional services</b><br />
                2 year guarantee included.
            </li>
            <li class="list-group-item">
                <b>More than 10 in stock.</b>
            </li>
            <li class="list-group-item">
                <div id="form" class="form-inline">
                    <div class="form-group">
                        <input type="text" id="quantity" runat="server" name="quantity" placeholder="Quantity" class="required span12 form-control buynow" />
                        <button id="Button1" type="button" class="btn btn-success pull-right" runat="server" onserverclick="btnAddToBasket_Click" validationgroup="BuyNow">
                            <span class="glyphicon glyphicon-shopping-cart"></span>
                            <asp:Literal runat="server" ID="Submit" />
                        </button>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                            ControlToValidate="quantity"
                            ErrorMessage="Quantity required"
                            Display="Dynamic"
                            ForeColor="Red"
                            ValidationGroup="BuyNow">
                        </asp:RequiredFieldValidator>
                    </div>
                </div>
                
            </li>
        </ul>
    