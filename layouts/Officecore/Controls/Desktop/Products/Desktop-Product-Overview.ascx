﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Desktop_Product_Overview.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Desktop.Desktop_Product_Overview" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register Src="~/layouts/Officecore/Controls/Desktop/Products/Desktop-Product-Teaser.ascx" TagPrefix="model" TagName="Product" %>
<div class="col-md-12">
    <asp:ListView runat="server" ID="lvProducts" ItemType="Officecore.Website.code.Model.Product" GroupItemCount="3">
        <GroupTemplate>
            <div class="row">
                <div id="itemPlaceholder" runat="server"></div>
            </div>
        </GroupTemplate>
        <ItemTemplate>
            <div class="col-md-4">
                <model:product runat="server" id="Product" currentproduct="<%# Item %>" />
            </div>
        </ItemTemplate>
    </asp:ListView>
</div>
