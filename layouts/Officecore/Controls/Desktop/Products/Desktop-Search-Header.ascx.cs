﻿using Sitecore.Web;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Officecore.Website.layouts.Officecore.Controls.Desktop
{
    public partial class Desktop_Search_Header : System.Web.UI.UserControl
    {
        public String SearchTermHeader { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Form.Get("SearchTerm") != null)
            {
                SearchTermHeader = Request.Form.Get("SearchTerm").ToString();
            }
            else
            {
                SearchTermHeader = Server.UrlDecode(WebUtil.GetQueryString("SearchTermHeader"));
            }
        }

        public void btnHeaderSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Products.aspx?SearchTermHeader=" + Request.Form.Get("SearchTermHeader"));
        }
    }
}