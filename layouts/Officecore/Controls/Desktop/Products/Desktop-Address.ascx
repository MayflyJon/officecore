﻿<%@ control language="C#" autoeventwireup="true" codebehind="Desktop-Address.ascx.cs" inherits="Officecore.Website.layouts.Officecore.Controls.Desktop.Products.Desktop_Address" %>
<div class="panel-group" id="accordion">
    <div class="panel panel-default">
        <div id="two" class="panel-heading clearfix" style="background-color: white;">
            <h4 class="panel-title pull-left">
                <strong>Billing address</strong>
            </h4>
        </div>
        <div class="twocolumnform">
            <div class="panel-body" style="padding: 15px 0 !important;">
                <div class="col-lg-12" style="padding-left: 0px; padding-right: 0px;">
                    <div class="col-lg-6" style="padding-right: 0px;">
                        <div class="input-group field">
                            <span class="input-group-addon formfield">Title</span>
                            <input type="text" clientidmode="static" id="billingTitle" class="form-control inputfield" runat="server" placeholder="Title" required />
                        </div>

                        <div class="input-group field">
                            <span class="input-group-addon formfield">First name</span>
                            <input type="text" clientidmode="static" id="billingFirstName" class="form-control inputfield" runat="server" placeholder="First name" required />
                        </div>

                        <div class="input-group field">
                            <span class="input-group-addon formfield">Last name</span>
                            <input type="text" clientidmode="static" id="billingLastName" class="form-control inputfield" runat="server" placeholder="Last name" required />
                        </div>

                        <div class="input-group field">
                            <span class="input-group-addon formfield">Phone</span>
                            <input type="text" clientidmode="static" id="billingPhone" class="form-control inputfield" runat="server" placeholder="Phone" />
                        </div>

                        <div class="input-group field">
                            <span class="input-group-addon formfield">Mobile phone</span>
                            <input type="text" clientidmode="static" id="billingMobile" class="form-control inputfield" runat="server" placeholder="Mobile phone" />
                        </div>

                        <div class="input-group field">
                            <span class="input-group-addon formfield">E-mail</span>
                            <input type="email" clientidmode="static" id="billingEmail" class="form-control inputfield" runat="server" placeholder="Email" required />
                        </div>
                    </div>

                    <div class="col-lg-6" style="padding-left: 35px;">
                        <div class="input-group field">
                            <span class="input-group-addon formfield">House</span>
                            <input type="text" clientidmode="static" id="billingHouse" class="form-control inputfield" runat="server" placeholder="House" required />
                        </div>

                        <div class="input-group field">
                            <span class="input-group-addon formfield">Street</span>
                            <input type="text" clientidmode="static" id="billingStreet" class="form-control inputfield" runat="server" placeholder="Street" required />
                        </div>

                        <div class="input-group field">
                            <span class="input-group-addon formfield">City</span>
                            <input type="text" clientidmode="static" id="billingCity" class="form-control inputfield" runat="server" placeholder="City" required />
                        </div>

                        <div class="input-group field">
                            <span class="input-group-addon formfield">Postal code</span>
                            <input type="text" clientidmode="static" id="billingPostalCode" class="form-control inputfield" runat="server" placeholder="Postal code" required />
                        </div>
                        <div class="input-group field">
                            <span class="input-group-addon formfield">County</span>
                            <input type="text" clientidmode="static" id="billingCounty" class="form-control inputfield" runat="server" placeholder="County" />
                        </div>
                        <div class="input-group field">
                            <span class="input-group-addon formfield">Country</span>
                            <asp:dropdownlist runat="server" datavaluefield="Id" datatextfield="Name" id="billingCountry" cssclass="form-control dropboxfield" />
                        </div>
                    </div>

                </div>

                <div class="col-lg-12">
                    <div class="form-group" style="margin:15px 0 0 0">
                        <label>
                            <asp:checkbox runat="server" id="checkHideShipping" onchange="validateFields();" clientidmode="static" />
                            Shipping address same as billing
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default" id="shippingHolder" style="visibility: visible;">
        <div id="one" class="panel-heading clearfix" style="background-color: white;">
            <h4 class="panel-title pull-left">
                <strong>Shipping address</strong>
            </h4>
        </div>
        <div class="twocolumnform">
            <div class="panel-body" style="padding: 15px 0px!important;">
                <div class="col-lg-12" style="padding-left: 0px; padding-right: 0px;">
                    <div class="col-lg-6" style="padding-right: 0px;">

                        <div class="input-group field">
                            <span class="input-group-addon formfield">Title</span>
                            <input type="text" clientidmode="static" id="shippingTitle" class="form-control inputfield required2" runat="server" placeholder="Title" />
                        </div>
                        <div class="input-group field">
                            <span class="input-group-addon formfield">First name</span>
                            <input type="text" clientidmode="static" id="shippingFirstName" class="form-control inputfield required2" runat="server" placeholder="First name" />
                        </div>
                        <div class="input-group field">
                            <span class="input-group-addon formfield">Last name</span>
                            <input type="text" clientidmode="static" id="shippingLastName" class="form-control inputfield required2" runat="server" placeholder="Last name" />
                        </div>
                        <div class="input-group field">
                            <span class="input-group-addon formfield">Phone</span>
                            <input type="text" clientidmode="static" id="shippingPhone" class="form-control inputfield" runat="server" placeholder="Phone" />
                        </div>
                        <div class="input-group field">
                            <span class="input-group-addon formfield">Mobile phone</span>
                            <input type="text" clientidmode="static" id="shippingMobile" class="form-control inputfield" runat="server" placeholder="Mobile phone" />
                        </div>
                        <div class="input-group field">
                            <span class="input-group-addon formfield">E-mail</span>
                            <input type="email" clientidmode="static" id="shippingEmail" class="form-control inputfield required2" runat="server" placeholder="Email" />
                        </div>
                    </div>

                    <div class="col-lg-6" style="padding-left: 35px;">
                        <div class="input-group field">
                            <span class="input-group-addon formfield">House</span>
                            <input type="text" clientidmode="static" id="shippingHouse" class="form-control inputfield required2" runat="server" placeholder="House" />
                        </div>
                        <div class="input-group field">
                            <span class="input-group-addon formfield">Street</span>
                            <input type="text" clientidmode="static" id="shippingStreet" class="form-control inputfield required2" runat="server" placeholder="Street" />
                        </div>
                        <div class="input-group field">
                            <span class="input-group-addon formfield">City</span>
                            <input type="text" clientidmode="static" id="shippingCity" class="form-control inputfield required2" runat="server" placeholder="City" />
                        </div>
                        <div class="input-group field">
                            <span class="input-group-addon formfield">Postal code</span>
                            <input type="text" clientidmode="static" id="shippingPostalCode" class="form-control inputfield required2" runat="server" placeholder="Postal Code" required />
                        </div>
                        <div class="input-group field">
                            <span class="input-group-addon formfield">County</span>
                            <input type="text" clientidmode="static" id="shippingCounty" class="form-control inputfield" runat="server" placeholder="County" />
                        </div>
                        <div class="input-group field">
                            <span class="input-group-addon formfield">Country</span>

                            <asp:dropdownlist runat="server" id="shippingCountry" datavaluefield="Id" datatextfield="Name" cssclass="form-control dropboxfield" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default" style="margin-top: 5px">
    <div class="panel-body" style="padding:10px!important">
        <a class="btn btn-large btn-danger btn-arrow-right" href="/cart/" style="margin-right: 15px;">Back</a>        
        <asp:button runat="server" id="btnSubmit" class="pull-right btn btn-large btn-success btn-arrow-right" clientidmode="static" text="Continue to Shipping Method" onclick="btnBillingUpdate_Click" />
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {
        validateFields();
    });

    function validateFields() {
        if (document.getElementById("checkHideShipping").checked) {
            jQuery('#shippingHolder .required2').removeAttr('required');
            jQuery('#shippingHolder').hide();
        }
        else {
            jQuery('#shippingHolder .required2').attr('required', 'required');
            jQuery('#shippingHolder').show();
        }
    }
</script>