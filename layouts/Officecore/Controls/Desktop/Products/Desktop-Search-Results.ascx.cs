﻿using System;
using System.Web;

using Sitecore.Web;
using Officecore.Website.code.Service;

using System.Collections.Generic;

namespace Officecore.Website.layouts.Officecore.Controls.Mobile
{
    public partial class Desktop_Search_Results : System.Web.UI.UserControl
    {
        private ProductService ps = new ProductService();

        protected void Page_Load(object sender, EventArgs e)
        {
            var searchterm = Request.Form.Get("SearchTerm");            
            if (string.IsNullOrEmpty(searchterm))
            {
                var qs = WebUtil.GetQueryString("SearchTermHeader");

                if (qs != null)
                {
                    searchterm = Server.UrlDecode(qs);
                }
            }

            if (searchterm == null)
            {
                searchterm = String.Empty;
            }

            List<code.Model.Product> productResults = ps.GetProducts(searchterm, HttpContext.Current.Request.QueryString);
            lvProducts.DataSource = productResults;
            lvProducts.DataBind();
        }
    }
}