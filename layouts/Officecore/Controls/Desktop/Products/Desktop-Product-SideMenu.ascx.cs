﻿namespace Officecore.Website.layouts.Officecore.Controls.Desktop
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using code.Constants;
    using code.DataRepositories;
    using UCommerce.Api;
    using UCommerce.Runtime;
    using code.Model;

    /// <summary>
    /// Defines the side menu for the product pages
    /// </summary>
    public partial class Desktop_Product_SideMenu : System.Web.UI.UserControl
    {
        /// <summary>
        /// The product repository
        /// </summary>
        private ProductRepository repo = new ProductRepository();

        /// <summary>
        /// The current category
        /// </summary>
        private Category _currentCategory { get; set; }

        /// <summary>
        /// The page load method
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The eventargs</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (repo.ActivateCommerce())
            {
                UCommerce.EntitiesV2.Category c = SiteContext.Current.CatalogContext.CurrentCategory;
                if (c != null)
                {
                    _currentCategory = new Category(c);
                }
            }
            else
            {
                _currentCategory = new Category(Sitecore.Context.Item);
            }
            DataBind();
        }

        /// <summary>
        /// Get all categories
        /// </summary>
        /// <returns>List of all categories</returns>
        public List<code.Model.Category> GetCategories()
        {
            return GetCategories(null);
        }

        /// <summary>
        /// Get the categories in a specific category
        /// </summary>
        /// <param name="item">The category</param>
        /// <returns>The list of inner categories</returns>
        public List<Category> GetCategories(Category item)
        {
            return repo.GetCategories(item);
        }

        /// <summary>
        /// Get the CSS class to use
        /// </summary>
        /// <param name="item">The category</param>
        /// <returns>The CSS class as string</returns>
        public string GetClass(Category item)
        {
            string cssClass = string.Empty;

            if (_currentCategory != null)
            {
                if (repo.ActivateCommerce())
                {
                    if (_currentCategory.CommCategory.Id == item.CommCategory.ParentCategory.Id)
                    {
                        cssClass = CssConstants.PillActive;
                    }

                    if (item.CommCategory.ParentCategory.Categories.Any(x => x.Id == _currentCategory.CommCategory.Id))
                    {
                        cssClass = CssConstants.PillActive;
                    }
                }
                else
                {
                    cssClass = CssConstants.PillActive;
                }
            }

            return cssClass;
        }


    }
}