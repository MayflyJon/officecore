﻿using System;
using System.Linq;
using System.Web;
using Officecore.Website.code.Helpers;
using UCommerce.EntitiesV2;
using Officecore.Website.code.DataRepositories;
using Sitecore.Web;
using Officecore.Website.code.Service;
using Officecore.Website.code.Model;
using System.Collections.Generic;
using Sitecore.Data;
using UCommerce.Api;
using UCommerce.Runtime;

namespace Officecore.Website.layouts.Officecore.Controls.Mobile
{
    public partial class Desktop_Related_Cart : System.Web.UI.UserControl
    {
        private ProductService ps = new ProductService();
        private ProductRepository pr = new ProductRepository();

        protected void Page_Load(object sender, EventArgs e)
        {           
            List<UCommerce.EntitiesV2.Product> products = new List<UCommerce.EntitiesV2.Product>();
            PurchaseOrder basket = TransactionLibrary.GetBasket(true).PurchaseOrder;
            List<UCommerce.EntitiesV2.Product> productResults = new List<UCommerce.EntitiesV2.Product>();
            productResults = pr.GetProductsFromBasket(basket);
            foreach (UCommerce.EntitiesV2.Product p in productResults)
            {
                products.AddRange(p.GetRelatedProducts().First().Value);
            }
            // We could get duplicates so now lets make sure we only have one of each in the list
            products = products.Select(x => x).Distinct().ToList();
            // we could have products that are already in the cart so we need to remove those
            products = products.Except(productResults).ToList();
            if (products.Count >= 3)
            {
                lvProducts.DataSource = products.Select(x => new code.Model.Product(x)).Take(3);
                lvProducts.DataBind();
            } else {
                ProductCatalogGroup pcg = pr.GetProductCatalogGroup();
                // add on some products that arent in the cart
                List<UCommerce.EntitiesV2.Product> bulkup = UCommerce.EntitiesV2.Product.Find(p => (p.CategoryProductRelations.Any(y => y.Category.ProductCatalog.ProductCatalogGroup == pcg)) && p.DisplayOnSite && !productResults.Contains(p) && !products.Contains(p)).ToList();
                products.AddRange(bulkup);
                lvProducts.DataSource = products.Select(x => new code.Model.Product(x)).Take(3);
                lvProducts.DataBind();
            }            
        }

        
    }
}