﻿namespace Officecore.Website.layouts.Officecore.Controls.Desktop.Products
{
    using System;
    using System.Linq;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using code.DataRepositories;
    using code.Helpers;
    using UCommerce;
    using UCommerce.Api;
    using UCommerce.EntitiesV2;
    using UCommerce.Extensions;

    public partial class Desktop_Cart : System.Web.UI.UserControl
    {
        private ProductRepository repo = new ProductRepository();
        private int _currentIteration = 0;
        private PurchaseOrder basket = TransactionLibrary.GetBasket(true).PurchaseOrder;

        private void Page_Load(object sender, EventArgs e)
        {
            basketBadge.Text = basket.OrderLines.Sum(x => x.Quantity).ToString();

            if (basket.OrderLines.Count == 0)
            {
                checkout.Attributes.Add("disabled", "disabled");
            }

            dropdownRepeater.ItemDataBound += dropdownRepeater_ItemDataBound;
            dropdownRepeater.DataSource = basket.OrderLines;
            dropdownRepeater.DataBind();
        }

        void dropdownRepeater_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Footer)
            {
                Currency currency = basket.BillingCurrency;

                var subTotal = new Money(0, currency);
                var tax = new Money(0, currency);
                var discount = new Money(0, currency);
                var orderTotal = new Money(0, currency);

                if (basket.SubTotal.HasValue)
                {
                    subTotal = new Money(basket.SubTotal.Value, currency);
                }
                if (basket.VAT.HasValue)
                {
                    tax = new Money(basket.VAT.Value, currency);
                }
                if (basket.DiscountTotal.HasValue)
                {
                    discount = new Money(basket.DiscountTotal.Value * -1, currency);
                }
                if (basket.OrderTotal.HasValue)
                {
                    orderTotal = new Money(basket.OrderTotal.Value, currency);
                }
                if (basket.OrderLines.Count == 0)
                {
                    HtmlTable overviewtable = (HtmlTable)e.Item.FindControl("overviewtable");
                    overviewtable.Visible = false;
                    HtmlTable emptyBasket = (HtmlTable)e.Item.FindControl("emptyBasket");
                    emptyBasket.Visible = true;

                    HtmlAnchor innercartdetails = (HtmlAnchor)e.Item.FindControl("innercartdetails");
                    innercartdetails.Attributes.Add("disabled", "disabled");
                    
                    HtmlAnchor innercheckout = (HtmlAnchor)e.Item.FindControl("innercheckout");
                    innercheckout.Attributes.Add("disabled", "disabled");
                }
                Sitecore.Data.Database db = Sitecore.Context.Database;

                Literal litSubtotal = (Literal)e.Item.FindControl("litSubtotal");
                litSubtotal.Text = subTotal.ToString();

                Literal litTax = (Literal)e.Item.FindControl("litTax");
                litTax.Text = tax.ToString();

                Literal litTotal = (Literal)e.Item.FindControl("litTotal");
                litTotal.Text = orderTotal.ToString();
            }

            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            OrderLine currentItem = (OrderLine)e.Item.DataItem;
            var product = CatalogLibrary.GetProduct(currentItem.Sku);
            if (!string.IsNullOrWhiteSpace(product.ThumbnailImageMediaId))
            {
                Sitecore.Data.ID mediaId = Sitecore.Data.ID.Parse(product.ThumbnailImageMediaId);
                var mediaItem = Sitecore.Context.Database.GetItem(mediaId);

                code.Model.Image imgProduct = (code.Model.Image)e.Item.FindControl("imgProduct");

                imgProduct.MediaItem = mediaItem;
            }

            var hlProduct = (HyperLink)e.Item.FindControl("hlProduct");
            var hlProductImg = (HyperLink)e.Item.FindControl("hlProductImage");
            hlProduct.Text = product.DisplayName();
            hlProduct.NavigateUrl = CatalogLibrary.GetNiceUrlForProduct(product, product.GetCategories().FirstOrDefault());
            hlProductImg.NavigateUrl = CatalogLibrary.GetNiceUrlForProduct(product, product.GetCategories().FirstOrDefault());

            var txtQuantity = (TextBox)e.Item.FindControl("amount");
            txtQuantity.Text = currentItem.Quantity.ToString();
            txtQuantity.Attributes.Add("orderlinenumber", _currentIteration.ToString());

            var btnUpdateQuantities = (HtmlButton)e.Item.FindControl("btnUpdateQuantities");
            var btnRemoveLine = (HtmlButton)e.Item.FindControl("btnRemoveLine");
            btnUpdateQuantities.Attributes.Add("orderlinenumber", _currentIteration.ToString());
            btnRemoveLine.Attributes.Add("orderlinenumber", _currentIteration.ToString());
            btnUpdateQuantities.ID = "btnUpdateQuantities" + _currentIteration;
            txtQuantity.ID = "txtQuantity" + _currentIteration;
            txtQuantity.Attributes.Add("data-orderlineid", currentItem.OrderLineId.ToString());
            _currentIteration++;
        }

        public void btnUpdateQuantities_Click(object sender, EventArgs e)
        {
            var senderButton = (HtmlButton)sender;
            var orderLineNumber = senderButton.Attributes["orderlinenumber"];
            var repeaterItem = (RepeaterItem)senderButton.NamingContainer;
            TextBox txtQuantity = (TextBox)repeaterItem.FindControl("txtQuantity" + orderLineNumber);

            var didItSucceed = UpdateCartLine(orderLineNumber, txtQuantity.Text);

            Response.Redirect(Request.RawUrl);
        }

        public void btnRemoveLine_Click(object sender, EventArgs e)
        {
            var senderButton = (HtmlButton)sender;
            var orderLineNumber = senderButton.Attributes["orderlinenumber"];

            var didItSucceed = UpdateCartLine(orderLineNumber, "0");

            Response.Redirect(Request.RawUrl);
        }

        public static bool? UpdateCartLine(string lineNumberString, string quantityString)
        {
            var basket = TransactionLibrary.GetBasket().PurchaseOrder;
            int lineNumber = 0;
            int quantity = 0;

            if (!Int32.TryParse(lineNumberString, out lineNumber) || !Int32.TryParse(quantityString, out quantity))
            {
                //if we cant parse the input to ints, we cant go on
                return false;
            }

            var listOfOrderLineIds = basket.OrderLines.Select(x => x.OrderLineId).ToList();
            var currentOrderLineId = listOfOrderLineIds[lineNumber];

            TransactionLibrary.UpdateLineItem(currentOrderLineId, quantity);

            TransactionLibrary.ExecuteBasketPipeline();
            return true;
        }
    }
}