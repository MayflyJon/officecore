﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Desktop-Promotion-Spot.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Desktop.Products.Desktop_Promotion_Spot" %>
<div class="sidebar-section">
    <h2>Popular Products</h2>

    <div class="clear"></div>
    <sc:image field="Thumbnail Image" id="ThumbnailImage" runat="server" maxwidth="180" border="0" class="img-responsive" />
    <div class="pop-products">
        <sc:link>
          <h3>
            <sc:Text Field="Display Name" ID="DisplayName" runat="server" />
          </h3>
        </sc:link>
        <sc:text field="Short Description" id="ShortDescription" runat="server" />
        <% if(ActivateCommerce) {%>
        <h4>
            <sc:text field="Price" id="Price" runat="server" />
        </h4>
        <%} %>
    </div>
</div>
