﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;
using UCommerce.Runtime;
using UCommerce.Api;
using Sitecore.Data.Items;
using Officecore.Website.code.Model;

namespace Officecore.Website.layouts.Officecore.Controls
{
    public partial class Desktop_Product_Categories : System.Web.UI.UserControl {

        private void Page_Load(object sender, EventArgs e)
        {

            List<StoreCategory> categories = new List<StoreCategory>();
            foreach (UCommerce.EntitiesV2.Category cat in CatalogLibrary.GetCategories(SiteContext.Current.CatalogContext.CurrentCategory))
            {
                StoreCategory s = new StoreCategory();
                s.category = cat;
                if (!string.IsNullOrWhiteSpace(cat.ImageMediaId))
                {
                    Sitecore.Data.ID mediaId = Sitecore.Data.ID.Parse(cat.ImageMediaId);
                    Item mediaItem = Sitecore.Context.Database.GetItem(mediaId);
                    s.mediaImage = new code.Model.Image();
                    s.mediaImage.MediaItem = mediaItem;
                }
                s.url = CatalogLibrary.GetNiceUrlForCategory(cat, SiteContext.Current.CatalogContext.CurrentCatalog);
                categories.Add(s);
            }
            this.lvCat.DataSource = categories;
            this.lvCat.DataBind();
            List<StoreCategory> products = new List<StoreCategory>();
            foreach (UCommerce.EntitiesV2.Product prd in CatalogLibrary.GetProducts(SiteContext.Current.CatalogContext.CurrentCategory))
            {
                StoreCategory s = new StoreCategory();
                s.product = prd;
                if (!string.IsNullOrWhiteSpace(prd.ThumbnailImageMediaId))
                {
                    Sitecore.Data.ID mediaId = Sitecore.Data.ID.Parse(prd.ThumbnailImageMediaId);
                    Item mediaItem = Sitecore.Context.Database.GetItem(mediaId);
                    s.mediaImage = new code.Model.Image();
                    s.mediaImage.MediaItem = mediaItem;
                }
                s.url = CatalogLibrary.GetNiceUrlForProduct(prd, SiteContext.Current.CatalogContext.CurrentCategory, SiteContext.Current.CatalogContext.CurrentCatalog);
                products.Add(s);
            }
            this.lvPrd.DataSource = products;
            this.lvPrd.DataBind();
        }
    }
}