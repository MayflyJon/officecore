﻿using Officecore.Website.code.Controls;
using Officecore.Website.code.Service;
using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Officecore.Website.layouts.Officecore.Controls.Desktop.Products
{   
    public partial class Desktop_Promotion_Spot : DatasourceControl
    {
        private ProductService ps = new ProductService();
        public Boolean ActivateCommerce = true;
        protected void Page_Load(object sender, EventArgs e)
        {
            ActivateCommerce = ps.ActivateCommerce();
            ThumbnailImage.Item = this.Item;
            DisplayName.Item = this.Item;
            ShortDescription.Item = this.Item;
            if (ActivateCommerce)
            {
                Price.Item = this.Item;
            }
        }
    }
}