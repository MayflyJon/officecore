﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Desktop_Product_Recently_Viewed.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Desktop.Products.Desktop_Product_Recently_Viewed" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register Src="~/layouts/Officecore/Controls/Desktop/Products/Desktop-Product-Teaser.ascx" TagPrefix="model" TagName="Product" %>

<div class="row-fluid" id="recentlyViewed" runat="server">
    <h2 style="padding-left: 20px;">
        <asp:Literal runat="server" ID="title" /></h2>

    <div class="span12 product-list panel recentlyViewedHolder">
        <span class="glyphicon glyphicon-chevron-left buttonBack"></span>
        <span class="glyphicon glyphicon-chevron-right buttonForward"></span>
        <div class="recentlyViewedGroup" id="recentlyViewedGroup" runat="server">
            <asp:ListView runat="server" ID="lvProducts" ItemType="Officecore.Website.code.Model.Product">
                <ItemTemplate>
                    <div class="panel panel-default" style="float:left">
                        <model:product runat="server" id="Product" currentproduct="<%# Item %>" />
                    </div>
                </ItemTemplate>
            </asp:ListView>
        </div>
    </div>
</div>

<script type="text/javascript" >
    var maxOffset = (Math.ceil(document.querySelectorAll('.recentlyViewedGroup>div').length / 3) - 1) * -452;
    var minOffset = 0;
    var moving = false;

    document.getElementsByClassName('buttonForward')[0].onclick = function () {
        if (jQuery(".recentlyViewedGroup").children().length > 3 && !moving) {
            moving = true;
            var boxOne = document.getElementsByClassName('recentlyViewedGroup')[0];
            boxOne.classList.add('horizTranslate');
            var left = parseInt(boxOne.style.left.replace("px", ""));
            if (left > maxOffset || isNaN(left)) {
                boxOne.style.left = boxOne.offsetLeft - 625 + "px";
            }
            else {
                boxOne.style.left = "0px";
            }
            setTimeout(function () { moving = false; }, 3000);
        }
        return false;
    }

    document.getElementsByClassName('buttonBack')[0].onclick = function () {
        if (jQuery(".recentlyViewedGroup").children().length > 3 && !moving) {
            moving = true;
            var boxOne = document.getElementsByClassName('recentlyViewedGroup')[0];
            boxOne.classList.add('horizTranslate');
            var left = parseInt(boxOne.style.left.replace("px", ""));

            if (left < minOffset) {
                boxOne.style.left = boxOne.offsetLeft + 625 + "px";
            }
            else {
                boxOne.style.left = maxOffset + "px";
            }
            setTimeout(function () { moving = false; }, 3000);
        }
        return false;
    }
</script>
