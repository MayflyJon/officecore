﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Desktop-Search-Facets.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Desktop.Desktop_Search_Facets" %>
<% if(ActivateCommerce){ %>
<div class="panel-group" id="accordion">
    <div id="facetsDiv" runat="server">
        <asp:Repeater ID="rptFacets" runat="server" OnItemDataBound="rptFacets_ItemDataBound">
            <HeaderTemplate>
            </HeaderTemplate>
            <ItemTemplate>
                <div class="panel panel-default" style="margin-top: 10px;">
                    <div id='h<%# DataBinder.Eval(Container, "ItemIndex") %>' class="panel-heading clearfix" style="background-color: white;" data-toggle="collapse" data-parent="#accordion" href='#h<%# DataBinder.Eval(Container, "ItemIndex") %>h' onclick="myFunction(id)">
                        <h4 class="panel-title pull-left" style="margin-top:2px;">
                            <asp:Literal ID="litHeadline" runat="server" />
                        </h4>
                        <div class="pull-right">
                            <img src="/assets/images/plus-icon-sml.gif" />
                        </div>
                    </div>
                    <div id='h<%# DataBinder.Eval(Container, "ItemIndex") %>h' class="panel-body twocolumnform" style="padding-top: 0px">
                        <asp:Repeater ID="rptCheckBoxes" runat="server" OnItemDataBound="rptCheckBoxes_ItemDataBound">
                            <ItemTemplate>
                                <div class="row" style="margin-left:0px;">
                                    <div class="col-sm-2">
                                        <asp:Button ID="btnCheckBox" runat="server" />
                                    </div>
                                    <div class="col-sm-10" style="padding-top:12px">
                                        <asp:Literal ID="rating" runat="server" />
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </ItemTemplate>
            <FooterTemplate>
            </FooterTemplate>
        </asp:Repeater>
    </div>
</div>
<!--
<script type="text/javascript" >

    //get all the child names for the containing div of the input tickboxs, use a loop
    //within the loop get all the names of them tickboxs
    //check to see if that item has been checked


    function myFunction(id1) {
        var headDiv = document.getElementById(id1);

        if (headDiv.getAttribute("class") == "panel-heading clearfix") {
            headDiv.children[1].children[0].setAttribute("src", "/assets/images/plus-icon-sml.gif");
        }
        else if (headDiv.getAttribute("class") == "panel-heading clearfix collapsed") {
            headDiv.children[1].children[0].setAttribute("src", "/assets/images/minus-icon-sml.gif");
        }

    }


    var facets = '<%=facetSize%>';//amount of facets on the page
    for (i = 0; i < facets; i++) {//loop through them
        var headDiv = document.getElementById("h" + i);//get the current facet heading
        var childDiv = document.getElementById("h" + i + "h");//get the current facet
        var children = childDiv.children.length;//count the children in the facet
        for (x = 0; x < children; x++) {//loop through the children
            if (childDiv.children[x].getAttribute("class") == "checkbox checked") {
                //therefore stop the loop and set the explanded class
                x = children;
                headDiv.setAttribute("class", "panel-heading clearfix");
                childDiv.setAttribute("class", "panel-body panel-collapse twocolumnform in");
                if (headDiv.getAttribute("class") == "panel-heading clearfix") {
                    headDiv.children[1].children[0].setAttribute("src", "/assets/images/minus-icon-sml.gif");
                }
            }
        }
    }

</script>
-->
<%} %>
