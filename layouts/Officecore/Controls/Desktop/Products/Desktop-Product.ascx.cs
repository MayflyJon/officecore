﻿using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using UCommerce.Api;
using UCommerce.EntitiesV2;
using UCommerce.Pipelines;
using UCommerce.Runtime;
using UCommerce.Extensions;
using Officecore.Website.code.DataRepositories;
using Sitecore.Globalization;
using Officecore.Website.code.Helpers;
using Sitecore.Web;
using Sitecore.Text;
using Sitecore.Resources.Media;
using Officecore.Website.code.Service;

namespace Officecore.Website.layouts.Officecore.Controls
{
    public partial class Desktop_Product : System.Web.UI.UserControl
    {
        public code.Model.Product CurrentProduct { get; set; }
        protected ProductService ps = new ProductService();
        public Boolean ActivateCommerce = true;

        private void Page_Load(object sender, EventArgs e)
        {
            ActivateCommerce = ps.ActivateCommerce();
            if (ActivateCommerce)
            {
                if (SiteContext.Current.CatalogContext.CurrentProduct == null)
                {
                    CurrentProduct = new code.Model.Product(ps.GetUcommerceProduct(Sitecore.Context.Item));
                }
                else
                {
                    CurrentProduct = new code.Model.Product(SiteContext.Current.CatalogContext.CurrentProduct);
                }
            }
            else
            {
                CurrentProduct = new code.Model.Product(Sitecore.Context.Item);
            }
            var ignoreReviewedMessage = WebUtil.GetQueryString("reviewed", "1");

            if (Session[CurrentProduct.Name + "_REVIEWED"] != null && IsPostBack)
            {
                if (!string.IsNullOrWhiteSpace(ignoreReviewedMessage))
                {
                    reviewPlaced.Visible = true;
                }
                Session.Remove(CurrentProduct.Name + "_REVIEWED");
            }
            else
            {
                reviewPlaced.Visible = false;
            }
            
            homeHeading.Text = DictionaryHelper.GetText("Detail tab heading");
            shippingHeading.Text = DictionaryHelper.GetText("Shipping tab heading");
            ratingHeading.Text = DictionaryHelper.GetText("Ratings tab heading");
            NoReviews.Text = DictionaryHelper.GetText("No reviews");

            RatingLabel.Text = DictionaryHelper.GetText("Review Form Your Rating");
            NameLabel.Text = DictionaryHelper.GetText("Review Form Your Name");
            EmailLabel.Text = DictionaryHelper.GetText("Review Form Your Email");
            TitleLabel.Text = DictionaryHelper.GetText("Review Form Your Title");
            ReviewLabel.Text = DictionaryHelper.GetText("Review Form Your Review");
            btnSubmitReview.Text = DictionaryHelper.GetText("Review Submit");


            if (CurrentProduct.ProductReviews == null || CurrentProduct.ProductReviews.Count < 1)
            {
                NoReviewsHolder.Visible = true;
            }
            else
            {
                productratings.DataSource = CurrentProduct.ProductReviews;
                
            }
            DataBind();
        }      

        public void btnAddToBasket_Click(object sender, EventArgs e)
        {
            var variant = ps.GetVariantFromPostData(SiteContext.Current.CatalogContext.CurrentProduct, "variation-");

            if (variant == null)
            {
                return;
            }
            TransactionLibrary.AddToBasket(1, variant.Sku, variant.VariantSku);
            Response.Redirect(Request.RawUrl);
        }

        

        public void btnSubmitReview_Click(object sender, EventArgs e)
        {
            Session.Add(string.Format("{0}_REVIEWED", CurrentProduct.Name), "1");
            ps.LeaveReview(reviewName.Value, reviewEmail.Value, reviewHeadline.Value, reviewText.Value);
        }

        public void IgnoreReviewedMessage_Click(object sender, EventArgs e)
        {
            UrlString newUrl = new UrlString(Request.RawUrl);
            newUrl.Add("reviewed", "1");
            Session.Remove(string.Format("{0}_REVIEWED", CurrentProduct.Name));
            Response.Redirect(newUrl.ToString());
        }
    }
}