﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Desktop-Product-Categories.ascx.cs" Inherits="Officecore.Website.layouts.Officecore.Controls.Desktop_Product_Categories" %>
<%@ Import Namespace="Officecore.Website.code.Model" %>
<asp:ListView runat="server" ID="lvCat">
    <ItemTemplate>
        <div style="font-size: xx-large; text-align: center; line-height: 40px; float: left; margin: 2px; background-color: white;">
            <div><a href="<%# ((StoreCategory)Container.DataItem).url %>"><%# ((StoreCategory)Container.DataItem).category.Name%></a></div>
            <div><a href="<%# ((StoreCategory)Container.DataItem).url %>">
                <store:image runat="server" id="imgProduct" mediaitem="<%# ((StoreCategory)Container.DataItem).mediaImage.MediaItem %>" height="290" width="290" />
            </a></div>
        </div>
    </ItemTemplate>
</asp:ListView>
<asp:ListView runat="server" ID="lvPrd">
    <ItemTemplate>
        <div style="font-size: xx-large; text-align: center; line-height: 40px; float: left; margin: 2px; background-color: white;">
            <div><a href="<%# ((StoreCategory)Container.DataItem).url %>"><%# ((StoreCategory)Container.DataItem).product.Name%></a></div>
            <div><a href="<%# ((StoreCategory)Container.DataItem).url %>">
                <store:image runat="server" id="imgProduct" mediaitem="<%# ((StoreCategory)Container.DataItem).mediaImage.MediaItem %>" height="290" width="290" />
            </a></div>
        </div>
    </ItemTemplate>
</asp:ListView>