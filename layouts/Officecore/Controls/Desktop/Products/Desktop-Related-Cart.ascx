﻿<%@ control language="C#" autoeventwireup="true" codebehind="Desktop-Related-Cart.ascx.cs" inherits="Officecore.Website.layouts.Officecore.Controls.Mobile.Desktop_Related_Cart" %>
<%@ register src="~/layouts/Officecore/Controls/Desktop/Products/Desktop-Product-Teaser.ascx" tagprefix="store" tagname="Product" %>
<div class="row-fluid">
    <div class="col-md-12">
            <asp:listview runat="server" id="lvProducts" itemtype="Officecore.Website.code.Model.Product" groupitemcount="3">
            <GroupTemplate>
                <div class="row">
                    <div id="itemPlaceholder" runat="server"></div>
                </div>
            </GroupTemplate>
            <ItemTemplate>
                <div class="col-xs-12 col-md-4">
                    <store:product runat="server" id="Product" currentproduct="<%# Item %>" />
                </div>
            </ItemTemplate>
        </asp:ListView>
        </div>
    </div>
