﻿<%@ control language="C#" autoeventwireup="true" codebehind="Desktop-Product.ascx.cs" inherits="Officecore.Website.layouts.Officecore.Controls.Desktop_Product" %>
<%@ register tagprefix="model" namespace="Officecore.Website.code.Model" assembly="Officecore.Website" %>
<%@ register src="~/layouts/Officecore/Controls/Desktop/Products/Desktop-ProductReview.ascx" tagprefix="officecore" tagname="ProductReview" %>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="row-fluid">
            <div class="col-md-12">
                <h1>
                    <asp:Literal runat="server" ID="productTitle" Text="<%# CurrentProduct.DisplayName %>"/>
                </h1>
                <img id="imgProduct" src="<%# CurrentProduct.PrimaryImageUrl %>" width="100" class="product-image center-block" data-zoom-image="<%# CurrentProduct.PrimaryImageUrl %>" />
                <div class="row">&nbsp;</div>
                <div class="alert alert-success col-md-12" visible="false" runat="server" id="reviewPlaced">
                    <asp:Literal ID="Literal1" runat="server" Text="Your review has been received and will appear on the website as soon as it has been approved. Thank you." />
                </div>
                <!-- Nav tabs -->
                <ul id="product" class="nav nav-tabs">
                    <li>
                        <a href="#home" data-toggle="tab">
                            <asp:Literal ID="homeHeading" runat="server" />
                        </a>
                    </li>
                    <% if(ActivateCommerce) {%>
                    <li>
                        <a href="#shipping" data-toggle="tab">
                            <asp:Literal ID="shippingHeading" runat="server" />
                        </a>
                    </li>
                    <li>
                        <a href="#ratings" data-toggle="tab">
                            <asp:Literal ID="ratingHeading" runat="server" />
                        </a>
                    </li>
                    <%} %>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="home">
                        <sc:text field="Long description" item="<%# Sitecore.Context.Item %>" runat="server" />
                    </div>
                    <div class="tab-pane" id="shipping">
                        <div style="padding: 10px;">
                            <h4><span class="glyphicon glyphicon-gift">&nbsp;</span>Click &amp; collect from 2pm tomorrow</h4>
                            <p>
                                <span>Order before <strong>9 PM</strong><br />
                                    Free from Officecore shops</span>
                            </p>
                            <h4><span class="glyphicon glyphicon-road">&nbsp;</span>UK delivery</h4>
                            <p>
                                <span class="price">
                                    <strong>£3.00</strong>
                                </span>
                                Standard UK delivery within 5 working days
                            </p>
                            <h4>
                                <span class="glyphicon glyphicon-plane">&nbsp;</span>
                                International delivery not available.
                            </h4>
                            <p>
                                See our <a href="/About-Us.aspx">terms and conditions</a> for how we might still be able to assist.
                            </p>
                            <h4><span class="glyphicon glyphicon-share-alt">&nbsp;</span>Returns are free</h4>
                            See our <a href="/About-Us.aspx">terms and conditions</a> about returning this product.
                        </div>
                    </div>
                    <div class="tab-pane" id="ratings">
                        <div class="row">&nbsp;</div>
                        <div class="panel panel-default">
                            <asp:Repeater ID="productratings" runat="server" ItemType="UCommerce.EntitiesV2.ProductReview">
                                <ItemTemplate>
                                    <officecore:productreview runat="server" id="ProductReview" review="<%# Item %>" />
                                </ItemTemplate>
                            </asp:Repeater>
                            <div runat="server" id="NoReviewsHolder" visible="false">
                                <asp:Literal runat="server" ID="NoReviews" />
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="review-rating">
                                <asp:Literal runat="server" ID="RatingLabel" />
                            </label>
                            <div class="controls rating">
                                <label class="glyphicon glyphicon-star-empty">
                                    <input type="radio" name="review-rating" value="1" /></label>
                                <label class="glyphicon glyphicon-star-empty">
                                    <input type="radio" name="review-rating" value="2" /></label>
                                <label class="glyphicon glyphicon-star-empty">
                                    <input type="radio" name="review-rating" value="3" /></label>
                                <label class="glyphicon glyphicon-star-empty">
                                    <input type="radio" name="review-rating" value="4" /></label>
                                <label class="glyphicon glyphicon-star-empty">
                                    <input type="radio" name="review-rating" value="5" /></label>
                            </div>
                        </div>
                        <div class="row">&nbsp;</div>
                        <div id="review-form">

                            <form role="form">

                                <div class="form-group">
                                    <label for="review-name">
                                        <asp:Literal runat="server" ID="NameLabel" />
                                    </label>
                                    <div class="controls">
                                        <input type="text" id="reviewName" runat="server" name="review-name" placeholder="Name" class="required form-control" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                            ControlToValidate="reviewName"
                                            ValidationGroup="Review"
                                            ErrorMessage="Name required"
                                            ForeColor="Red"
                                            Display="dynamic">
                                        </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="review-email">
                                        <asp:Literal runat="server" ID="EmailLabel" />
                                    </label>
                                    <div class="controls">
                                        <input type="text" id="reviewEmail" runat="server" name="review-email" placeholder="Email" class="required form-control" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                                            runat="server"
                                            ValidationExpression=".+@.+\..*"
                                            ControlToValidate="reviewEmail"
                                            ValidationGroup="Review"
                                            Display="Dynamic"
                                            ErrorMessage="Email not valid"
                                            ForeColor="Red">
                                        </asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server"
                                            ControlToValidate="reviewEmail"
                                            ErrorMessage="Email Required"
                                            Display="Dynamic"
                                            ValidationGroup="Review"
                                            ForeColor="Red">
                                        </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="review-headline">
                                        <asp:Literal runat="server" ID="TitleLabel" />
                                    </label>
                                    <div class="controls">
                                        <input type="text" id="reviewHeadline" clientidmode="static" runat="server" name="review-headline" placeholder="Title" class="required form-control" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                            ControlToValidate="reviewHeadline"
                                            ValidationGroup="Review"
                                            ErrorMessage="Review headline required"
                                            ForeColor="Red"
                                            Display="Dynamic">
                                        </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="review-text">
                                        <asp:Literal runat="server" ID="ReviewLabel" />
                                    </label>
                                    <div class="controls">
                                        <textarea rows="3" id="reviewText" runat="server" name="review-text" placeholder="Your review" class="required form-control"></textarea>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                                            ControlToValidate="reviewText"
                                            ValidationGroup="Review"
                                            ErrorMessage="Review Required"
                                            ForeColor="Red"
                                            Display="Dynamic">
                                        </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="controls">
                                        <asp:Button runat="server" ID="btnSubmitReview" name="review-product" class="btn btn-success pull-right saveButton" OnClick="btnSubmitReview_Click" ValidationGroup="Review" />
                                        <input type="hidden" id="reviewProductSku" name="reviewed-product-sku" />
                                        <input type="hidden" name="review-product" />
                                    </div>
                                </div>


                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
