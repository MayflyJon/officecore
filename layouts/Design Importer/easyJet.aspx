﻿<%@ Page Language="C#" Inherits="System.Web.UI.Page" CodePage="65001" %><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>easyJet</title>
<style type="text/css">
.ReadMsgBody {
 width: 100%;
}
.ExternalClass {
 width: 100%;
}
body, table, tr, td, br, strong, span, a, img {
 line-height:normal;
 font-family: Verdana, Geneva, sans-serif;
 border-collapse:collapse;
 -webkit-text-size-adjust:none;
}
img {
 display:block;
}</style>
</head>

<body bgcolor="#cddce1"><form runat="server" id="Sitecore_DesignImporter_MainForm"></form>

<img src="https://pixel.monitor1.returnpath.net/pixel.gif?r=307971c69f761750a3cb9f251be82c22ad1c3035&amp;c=easyjetri5.1754&amp;campaignname=APIS&amp;lang=EN" width="1" height="1" style="display:none">

<table bgcolor="#cddce1" border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody><tr>
<td>
<br>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="615">
	<tbody><tr>
    	<td>
            

<table align="center" border="0" cellpadding="0" cellspacing="0" width="601">
                <tbody><tr>
                    <td width="375" style="font-family:Verdana, Geneva, sans-serif; font-size:10px; color:#063342; text-align:left" valign="top">Hi Chris  |  Booking Ref: EKCQ1C5</td>
                    <td width="226" style="font-family:Verdana, Geneva, sans-serif; font-size:10px; color:#063342; text-align:right" valign="top"><a href="https://email.easyjet.com/pub/cc?_ri_=X0Gzc2X%3DWQpglLjHJlYQGow9zfCtlvMR1Mn9zf2EzaDADzfCOv37stzczeDnVXtpKX%3DCAAR&amp;_ei_=EolaGGF4SNMvxFF7KucKuWOdNMWbLk1yC3yUuUQI1eQU2_RBjhVfsFRH4w5CXE5ujRf_ttxusoYpmCk8uvpsOpocnqPN1Yes1_PM4jQyoEprTBb7WOmcxDTEgBoob687vmIoU7I-h4BJ7DC1IptdmGzvvroOZxaKJbhqXXM2TvrfTiNtCk6hXjJ4IGAx2OehEM." target="_blank" style="color:#063342; text-decoration:underline">View email online</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="https://email.easyjet.com/pub/cc?_ri_=X0Gzc2X%3DWQpglLjHJlYQGow9zfCtlvMR1Mn9zf2EzaDADzfCOv37stzczeDnVXtpKX%3DCAAS&amp;_ei_=EnEcSDhDsm6ckg2791wHueHWVtMGCwzdNkjB0cEZactXPRYJPo9sF1NE3-p09uhJpE1gWOy6IkrPBsWOPB_ct-Upmy3LWBwhuCCC8dW9MEwy." target="_blank" style="color:#063342; text-decoration:underline">easyJet.com</a></td>
                </tr>
            </tbody></table>
        </td>
    </tr>
    <tr>
    	<td height="8"><img src="https://a248.e.akamai.net/f/248/112531/14d/ig.rsys5.net/responsysimages/easyjetri5/z_APIS/APIS_Shell/spacer.gif" style="display:block" border="0" width="1" height="1"></td>
    </tr>
    <tr>
    	<td><img src="https://a248.e.akamai.net/f/248/112531/14d/ig.rsys5.net/responsysimages/easyjetri5/z_APIS/APIS_Shell/top_gradient.jpg" style="display:block" border="0" width="615" height="6"></td>
    </tr>
</tbody></table>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="615">
	<tbody><tr>
    	<td width="7" background="https://a248.e.akamai.net/f/248/112531/14d/ig.rsys5.net/responsysimages/easyjetri5/z_APIS/APIS_Shell/left_gradient.gif"></td>
    	<td width="601">
        	<table align="center" border="0" cellpadding="0" cellspacing="0" width="601" bgcolor="#ffffff">
            	<tbody><tr>
                	<td>
                    	<table align="center" border="0" cellpadding="0" cellspacing="0" width="601" bgcolor="#ef662d">
                        	<tbody><tr>
                            	<td width="151"><a href="https://email.easyjet.com/pub/cc?_ri_=X0Gzc2X%3DWQpglLjHJlYQGow9zfCtlvMR1Mn9zf2EzaDADzfCOv37stzczeDnVXtpKX%3DCAYC&amp;_ei_=EnEcSDhDsm6ckg2791wHueHWVtMGCwzdNkjB0cEZactXPRYJPo9sF1NE3-p09uhJpE1gWOy6IkrPBsWOPB_ct-Upmy3LWBwhuCCC8dW9MEwy." target="_blank"><img src="https://a248.e.akamai.net/f/248/112531/14d/ig.rsys5.net/responsysimages/easyjetri5/z_APIS/APIS_Shell/logo.jpg" style="display:block" border="0" width="151" height="67" alt="europe by easyJet" title="europe by easyJet"></a></td>
                                <td width="450"><img src="https://a248.e.akamai.net/f/248/112531/14d/ig.rsys5.net/responsysimages/easyjetri5/z_APIS/APIS_Shell/top_border_pattern.jpg" style="display:block" border="0" width="450" height="67"></td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
                <tr>
                	<td>
                    	

<table align="center" border="0" cellpadding="0" cellspacing="0" width="601">
                        	<tbody><tr>
                            	<td height="25"><img src="https://a248.e.akamai.net/f/248/112531/14d/ig.rsys5.net/responsysimages/easyjetri5/z_APIS/APIS_EN_body/spacer.gif" style="display:block" border="0" width="1" height="1"></td>
                            </tr>
                            <tr>
                            	<td>
                                	<table align="left" border="0" cellpadding="0" cellspacing="0" width="576" bgcolor="#fef0e7">
                                    	<tbody><tr>
                                        	<td height="97">
                                            	<table align="right" border="0" cellpadding="0" cellspacing="0" width="551">
                                            		<tbody><tr>
                                                        <td style="font-family:Verdana, Geneva, sans-serif; font-size:15px; color:#336666; text-align:left">easyJet booking ref: EKCQ1C5</td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"><img src="https://a248.e.akamai.net/f/248/112531/14d/ig.rsys5.net/responsysimages/easyjetri5/z_APIS/APIS_EN_body/spacer.gif" style="display:block" border="0" width="1" height="1"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-family:Verdana, Geneva, sans-serif; font-size:30px; color:#336666; text-align:left"><sc:fieldrenderer runat="server" id="FieldRenderer1" fieldname="Header 1"></sc:fieldrenderer></td>
                                                    </tr>
                                                 </tbody></table>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                </td>
                            </tr>
                            <tr>
                            	<td height="14"><img src="https://a248.e.akamai.net/f/248/112531/14d/ig.rsys5.net/responsysimages/easyjetri5/z_APIS/APIS_EN_body/spacer.gif" style="display:block" border="0" width="1" height="1"></td>
                            </tr>
                            <tr>
                            	<td>
                                	<table align="center" border="0" cellpadding="0" cellspacing="0" width="551">
                                    	<tbody><tr>
                                        	<td style="font-family:Verdana, Geneva, sans-serif; font-size:14px; color:#000001; text-align:left"><strong>Travel document information required</strong></td>
                                        </tr>
                                        <tr>
                                        	<td height="20"><img src="https://a248.e.akamai.net/f/248/112531/14d/ig.rsys5.net/responsysimages/easyjetri5/z_APIS/APIS_EN_body/spacer.gif" style="display:block" border="0" width="1" height="1"></td>
                                        </tr>
                                        <tr>
                                        	<td style="font-family:Verdana, Geneva, sans-serif; font-size:13px; color:#666666; text-align:left">Before you travel you are <strong>legally</strong> required to provide us with details of the passport or identity card that you intend to use for your journey. If you have not already done so, please visit <a href="https://email.easyjet.com/pub/cc?_ri_=X0Gzc2X%3DWQpglLjHJlYQGow9zfCtlvMR1Mn9zf2EzaDADzfCOv37stzczeDnVXtpKX%3DCAAT&amp;_ei_=EnEcSDhDsm6ckg2791wHueHWVtMGCwzdNkjB0cEZactXko1lV2UcAsQzRIHugLuQnYZvbsS2nagsq0_Cg2k." target="_blank" style="font-family:Verdana, Geneva, sans-serif; font-size:13px; color:#ff6600; text-decoration:underline">easyJet.com</a> to enter this information for all passengers travelling with you.<br><br>

If you have already provided us with this information then you need take no further action.<br><br>

More information about these requirements can be found on our help site.</td>
                                        </tr>
                                        <tr>
                                        	<td height="15"><img src="https://a248.e.akamai.net/f/248/112531/14d/ig.rsys5.net/responsysimages/easyjetri5/z_APIS/APIS_EN_body/spacer.gif" style="display:block" border="0" width="1" height="1"></td>
                                        </tr>
                                        <tr>
                                        	<td height="15">
                                            	
                                            	<table align="left" border="0" cellpadding="0" cellspacing="0" bgcolor="#858585">
                                                	<tbody><tr>
                                                    	<td width="6"><img src="https://a248.e.akamai.net/f/248/112531/14d/ig.rsys5.net/responsysimages/easyjetri5/z_APIS/APIS_EN_body/cta_left.jpg" style="display:block" border="0" width="6" height="29"></td>
                                                        <td valign="bottom">
                                                        	<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            	<tbody><tr>
                                                        			<td valign="bottom" height="22" background="https://a248.e.akamai.net/f/248/112531/14d/ig.rsys5.net/responsysimages/easyjetri5/z_APIS/APIS_EN_body/cta_bg.jpg"><a href="https://email.easyjet.com/pub/cc?_ri_=X0Gzc2X%3DWQpglLjHJlYQGow9zfCtlvMR1Mn9zf2EzaDADzfCOv37stzczeDnVXtpKX%3DCAAU&amp;_ei_=EnEcSDhDsm6ckg2791wHueHWVtMGCwzdNkjB0cEZactXko1lV2UcAsQzRIHugLuQnYZvbsS2nagsq0_Cg2k." target="_blank" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#fefefe; text-decoration:none"> FIND OUT MORE </a></td>
                                                                </tr>
                                                                <tr>
                                                                	<td width="6"><img src="https://a248.e.akamai.net/f/248/112531/14d/ig.rsys5.net/responsysimages/easyjetri5/z_APIS/APIS_EN_body/cta_bottom.jpg" style="display:block" border="0" width="122" height="7"></td>
                                                                </tr>
                                                            </tbody></table>
                                                        </td>
                                                        <td width="27"><img src="https://a248.e.akamai.net/f/248/112531/14d/ig.rsys5.net/responsysimages/easyjetri5/z_APIS/APIS_EN_body/cta_right.jpg" style="display:block" border="0" width="27" height="29"></td>
                                                    </tr>
                                                </tbody></table>
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                        	<td height="15"><img src="https://a248.e.akamai.net/f/248/112531/14d/ig.rsys5.net/responsysimages/easyjetri5/z_APIS/APIS_EN_body/spacer.gif" style="display:block" border="0" width="1" height="1"></td>
                                        </tr>
                                    </tbody></table>
                                </td>
                            </tr>
                            <tr>
                            	<td>
                                	<table align="left" border="0" cellpadding="0" cellspacing="0" width="576" bgcolor="#fef0e7">
                                    	<tbody><tr>
                                        	<td height="80">
                                            	<table align="right" border="0" cellpadding="0" cellspacing="0" width="551">
                                                	<tbody><tr>
                                                        <td height="10"><img src="https://a248.e.akamai.net/f/248/112531/14d/ig.rsys5.net/responsysimages/easyjetri5/z_APIS/APIS_EN_body/spacer.gif" style="display:block" border="0" width="1" height="1"></td>
                                                    </tr>
                                            		<tr>
                                                        <td style="font-family:Verdana, Geneva, sans-serif; font-size:11px; color:#000000; text-align:left">easyJet Airline Company Limited Registered in England with Registered number: 3034606 Subsidiary of easyJet Plc Registered in England with registered number: 3959649. <br>Registered Office: Hangar 89, London Luton Airport, Luton, Bedfordshire, LU2 9PF.</td>
                                                    </tr>
                                                    <tr>

                                                        <td height="10"><img src="https://a248.e.akamai.net/f/248/112531/14d/ig.rsys5.net/responsysimages/easyjetri5/z_APIS/APIS_EN_body/spacer.gif" style="display:block" border="0" width="1" height="1"></td>
                                                    </tr>
                                                 </tbody></table>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                </td>
                            </tr>
                            <tr>
                                <td height="25"><img src="https://a248.e.akamai.net/f/248/112531/14d/ig.rsys5.net/responsysimages/easyjetri5/z_APIS/APIS_EN_body/spacer.gif" style="display:block" border="0" width="1" height="1"></td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
                <tr>
                	<td bgcolor="#ef662d"><img src="https://a248.e.akamai.net/f/248/112531/14d/ig.rsys5.net/responsysimages/easyjetri5/z_APIS/APIS_Shell/bottom_border_pattern.jpg" style="display:block" border="0" width="601" height="55"></td>
                </tr>
            </tbody></table>
        </td>
    	<td width="7" background="https://a248.e.akamai.net/f/248/112531/14d/ig.rsys5.net/responsysimages/easyjetri5/z_APIS/APIS_Shell/right_gradient.jpg"><img src="https://a248.e.akamai.net/f/248/112531/14d/ig.rsys5.net/responsysimages/easyjetri5/z_APIS/APIS_Shell/spacer.gif" style="display:block" border="0" width="1" height="1" alt=""></td>
    </tr>
</tbody></table>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="615">
    <tbody><tr>
    	<td><img src="https://a248.e.akamai.net/f/248/112531/14d/ig.rsys5.net/responsysimages/easyjetri5/z_APIS/APIS_Shell/bottom_gradient.jpg" style="display:block" border="0" width="615" height="6"></td>
    </tr>
</tbody></table>

</td>
</tr>
</tbody></table>

</body></html>