﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Diagnostics;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Web.UI.WebControls;

namespace Dms.Jump.Presentation.Modules.CMS.Metadata
{
    public partial class ShowDecendantsSublayout : CustomUserControl
    {

        #region Tag
        public string TagID
        {
            get { return GetProperty("Tags"); } 
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                rptDecendants.DataSource = GetDataSource(this.ItemInstance);
                rptDecendants.DataBind(); 
            }
        }

        private object GetDataSource( Item currentItem ) 
        {
            Debug.ArgumentNotNull(currentItem, "currentItem"); 
            const string queryExpression = ".//*[@templatename='Course Description'"; 
            var items = currentItem.Axes.SelectItems(queryExpression); 
            if( string.IsNullOrEmpty( TagID ) ) {
                return items; 
            }
            var splitIDs = TagID.Split('|'); 
            return items.Where( i => splitIDs.Any( id => i["tags"].Contains(id))); 
        }

        protected void rptDecendants_ItemDataBound( object sender, RepeaterItemEventArgs e ) {

            if( e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem ) {
                var subItem = e.Item.DataItem as Item;

                if( subItem != null ) 
                {
                    var subItemUrl = LinkManager.GetItemUrl(subItem); 
                    var hlMenuPoint = e.Item.FindControl("hlMenuPoint") as HyperLink; 

                    if( hlMenuPoint != null ) 
                    {
                        hlMenuPoint.Text = subItem["title"]; 
                        hlMenuPoint.NavigateUrl = subItemUrl; 
                    }
                }

                if( Sitecore.Context.PageMode.IsPageEditorEditing) 
                {
                    var editFrame = e.Item.FindControl("efMenuPoint") as EditFrame; 

                    if( editFrame != null ) 
                    {
                        editFrame.DataSource = subItem.ID.ToString(); 
                        editFrame.Buttons = "/sitecore/content/Applications/WebEdit/Edit Frame Buttons/Tags"; 
                    }

                }
            }
        }
    }
}