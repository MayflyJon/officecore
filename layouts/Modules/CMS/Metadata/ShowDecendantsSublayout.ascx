﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShowDecendantsSublayout.ascx.cs"
    Inherits="Dms.Jump.Presentation.Modules.CMS.Metadata.ShowDecendantsSublayout" %>
<asp:Repeater ID="rptDecendants"  OnItemDataBound="rptDecendants_ItemDataBound" runat="server">
    <HeaderTemplate>
        <div id="whats_new" class="module rightColumnSpot">
            <div class="top">
            </div>
            <div class="middle">
                <div class="middle_inner">
                    <h2>
                        What's New
                    </h2>
                    <ul>
    </HeaderTemplate>
    <ItemTemplate>
                <li>
                    <sc:EditFrame ID="efMenuPoint" runat="server" Title="Edit SubItem">
                        <asp:HyperLink ID="hlMenuPoint" runat="server"/>
                    </sc:EditFrame>
                </li>
    </ItemTemplate>
    <FooterTemplate>
                    </ul>
                    <div id="rss_icon">
                        <a target="_blank" href="//.aspx">rss </a>
                    </div>
                    <div class="clear">
                    </div>
                </div>
            </div>
            <div class="bottom">
            </div>
        </div>
    </FooterTemplate>
</asp:Repeater>
