﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.Collections.Specialized;
using System.IO;
using System.Data;
using Dms.Jump.Business.Entities;
using Dms.Jump.Business.Helper;
using System.Web.UI.HtmlControls;
using Dms.Jump.Business.Utilities.Tools;
using System.Threading;
using Dms.Jump.Business.Entities.Requests;

namespace Dms.Jump.Presentation.Modules.DMS.TrafficEmulator
{
    public partial class DmsTrafficEmulatorSublayout : System.Web.UI.UserControl
    {
        #region Properties 

        private List<TrafficParameter> TrafficParameters
        {
            get
            {
                return 
                    Session["SC_TrafficParameters"] != null 
                    ?  Session["SC_TrafficParameters"] as List<TrafficParameter> 
                    : null; 
            }
            set
            {
                Session["SC_TrafficParameters"] = value; 
            }

        }

        private int Repeat
        {
            get
            {
                return
                    Session["SC_Repeat"] != null 
                    ? (int)Session["SC_Repeat"]
                    : 1;
            }
            set
            {
                Session["SC_Repeat"] = value;
            }

        }

        #endregion 

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ResetWorkflow();
            }

        }

        protected void btnGenerateTraffic_Click(object sender, EventArgs e)
        {
            StringBuilder requestsStringBuilder = new StringBuilder();
            StringBuilder confirmationStringBuilder = new StringBuilder(); 
            int requestCounter = 0;
            int numberOfErrors = 0;
            DateTime start = DateTime.Now;

            for( int rep = 0; rep < this.Repeat; rep++) {

                requestsStringBuilder.Append("<b>Iteration: " + rep.ToString() + @"</b><br />");
                foreach (var trafficParameter in this.TrafficParameters)
                {
                    var uris = trafficParameter.GetUris();
                    requestsStringBuilder.Append("<i>From: " + trafficParameter.Traffic.UrlReferrer + @"</i><br />");

                    foreach (Uri uri in uris)
                    {
                        requestsStringBuilder.Append(requestCounter + " \t");
                        try
                        {
                            //WebClient client = new WebClient();
                            //Stream data = client.OpenRead(uri.ToString());

                            //WebRequest request = WebRequest.Create(uri);
                            //request.Timeout = 200; 
                            //WebResponse response = request.GetResponse();

                            // Wait one sec to allow http to digest requests
                            if (requestCounter % 100 == 0)
                                Thread.Sleep(2500);

                            ClientAsync.PerformRequest(uri);
                            requestsStringBuilder.Append(uri.ToString() + "<br />");
                        }   
                        catch (Exception exp)
                        {
                            numberOfErrors++;
                            requestsStringBuilder.Append("Error in: <font color=red>" + uri.ToString() + "</font> " + exp.Message + "<br />");
                        }
                        requestCounter++;
                    }

                    Thread.Sleep(5000);
                }
            }

            TimeSpan tts = DateTime.Now - start;
            lblConfirmation.Text = "Completed " + this.TrafficParameters.Count() + " imports<br />";
            lblConfirmation.Text += requestCounter + " requests processed in " + tts.TotalSeconds + " sec. ";
            lblConfirmation.Text += "Avg req/sec. " + requestCounter / tts.TotalSeconds + "<br />";
            lblConfirmation.Text += "<font color=red>Detected number of errors = " + numberOfErrors + "</font><br /><br />";
            lblConfirmation.Text += "Imports:<br />";
            lblConfirmation.Text += confirmationStringBuilder.ToString(); 


            lblConfirmationRequests.Text = requestsStringBuilder.ToString(); 

            pnlUploadFile.Visible = false;
            pnlGenerateTraffic.Visible = false;
            pnlConfirmation.Visible = true;

            this.TrafficParameters = null;
            rptData.DataSource = this.TrafficParameters;
            rptData.DataBind();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.ResetWorkflow();   
        }

        protected void btnDone_Click(object sender, EventArgs e)
        {
            this.ResetWorkflow(); 
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            //check to make sure a file is selected    
            if (fuCsvFile.HasFile)
            {
                try
                {
                    lblGenerateTraffic.Text = "Loaded filename: " + fuCsvFile.PostedFile.FileName + "<br/>" +
                         "File size; " + fuCsvFile.PostedFile.ContentLength + " kb" + "<br/>" +
                         "Content type:: " + fuCsvFile.PostedFile.ContentType + "<br/><br/>";

                    using (StreamReader sr = new StreamReader(fuCsvFile.PostedFile.InputStream))
                    {
                        CsvHelper csvHelper = new CsvHelper();
                        this.TrafficParameters = csvHelper.GetTrafficParameters(sr); 
                    }

                    int repeat = 1;
                    if (Int32.TryParse(this.tbRepeat.Text, out repeat))
                        this.Repeat = repeat; 

                    rptData.DataSource = this.TrafficParameters;
                    rptData.DataBind();

                    int numberOfRequests = 0; 
                    foreach (var trafficParameter in this.TrafficParameters)
                        numberOfRequests += trafficParameter.GetUris().Count();

                    numberOfRequests = numberOfRequests * repeat; 

                    double estimatedRequestTime = 0;
                    if (numberOfRequests > 10 && cbEstimateTime.Checked)
                    {
                        var url = HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.AbsolutePath, "");

                        if (!String.IsNullOrEmpty(url))
                        {
                            Uri uri = new Uri(url);

                            var start = DateTime.Now;
                            for (int i = 0; i < 10; i++)
                                ClientAsync.PerformRequest(uri);

                            TimeSpan ts = DateTime.Now - start;
                            estimatedRequestTime = ts.TotalSeconds; 
                        }
                    }

                    lblGenerateTraffic.Text += "Number of requests = " + numberOfRequests  + " <br />";
                    if( estimatedRequestTime > 0 )
                        lblGenerateTraffic.Text += "Estimated time to process = " + (int)(numberOfRequests / 10  * estimatedRequestTime) + " sec. <br />";

                    lblGenerateTraffic.Text += "<br />"; 

                    pnlUploadFile.Visible = false;
                    pnlGenerateTraffic.Visible = true;
                    pnlConfirmation.Visible = false;
                }
                catch (Exception ex)
                {
                    lblUploadFile.Text = "ERROR: " + ex.Message.ToString();
                }
            }
        }

        protected void rptData_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item ||
                e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var lblDateFrom = e.Item.FindControl("lblDateFrom") as Label;
                var lblDateTo = e.Item.FindControl("lblDateTo") as Label;
                var lblTrafficType = e.Item.FindControl("lblTrafficType") as Label;
                var lblKeyword = e.Item.FindControl("lblKeyword") as Label;
                var lblCampaign = e.Item.FindControl("lblCampaign") as Label; 
                var lblTrafficNumberOfRequests = e.Item.FindControl("lblTrafficNumberOfRequests") as Label;
                var lblTrafficMode = e.Item.FindControl("lblTrafficMode") as Label;
                var lblTrafficUrls = e.Item.FindControl("lblTrafficUrls") as Label;
                var lblReferrer = e.Item.FindControl("lblReferrer") as Label;
                var lblEngagementIp = e.Item.FindControl("lblEngagementIp") as Label;
                var lblEngagementPercentage = e.Item.FindControl("lblEngagementPercentage") as Label;
                var lblEngagementMode = e.Item.FindControl("lblEngagementMode") as Label;
                var lblEngagementGoals = e.Item.FindControl("lblEngagementGoals") as Label;

                var trafficParameter = e.Item.DataItem as TrafficParameter;

                lblCampaign.Text = !String.IsNullOrEmpty(trafficParameter.Traffic.Campaign) ? trafficParameter.Traffic.Campaign : "-";
                lblDateFrom.Text = trafficParameter.DateRange.DateFrom.ToString();
                lblDateTo.Text = trafficParameter.DateRange.DateTo.ToString();
                lblTrafficType.Text = trafficParameter.Traffic.TrafficType.ToString();
                lblTrafficNumberOfRequests.Text = trafficParameter.Traffic.NumberOfRequests.ToString();
                lblTrafficMode.Text = trafficParameter.Traffic.Mode.ToString();
                lblKeyword.Text = !String.IsNullOrEmpty(trafficParameter.Traffic.Keyword) ? trafficParameter.Traffic.Keyword : "-";
                lblTrafficUrls.Text = trafficParameter.Traffic.Urls.Count > 0 ? trafficParameter.Traffic.Urls.Count.ToString() : "-";
                lblReferrer.Text = trafficParameter.Traffic.UrlReferrer;
                lblEngagementIp.Text = trafficParameter.Engagement.IP;
                lblEngagementPercentage.Text = trafficParameter.Engagement.Percentage.ToString();
                lblEngagementMode.Text = trafficParameter.Engagement.Mode.ToString();
                lblEngagementGoals.Text = trafficParameter.Engagement.SpecificGoalNames.Count() > 0 ? trafficParameter.Engagement.SpecificGoalNames.Count().ToString() : "-";
            }
            else if (e.Item.ItemType == ListItemType.Header)
            {       
                // Do nothing
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                // Do nothing
            }
        }


        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }


        #region Private Helpers
        private void ResetWorkflow()
        {
            pnlUploadFile.Visible = true;
            pnlGenerateTraffic.Visible = false;
            pnlConfirmation.Visible = false;

            this.TrafficParameters = null;
            rptData.DataSource = this.TrafficParameters;
            rptData.DataBind();

            cbEstimateTime.Checked = false; 
        }
        #endregion 

    }
}
