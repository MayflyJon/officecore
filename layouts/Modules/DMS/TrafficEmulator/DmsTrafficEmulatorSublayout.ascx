﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DmsTrafficEmulatorSublayout.ascx.cs" Inherits="Dms.Jump.Presentation.Modules.DMS.TrafficEmulator.DmsTrafficEmulatorSublayout" %>


    <div class="trafficGenerator">

         <h2>Traffic Generator</h2>
         <br />

        <asp:Panel ID="pnlUploadFile" runat="server">
         <h2><font color="red">Upload File</font><font color="#D0D0D0"> >> Start Generation >> Confirmation </font> </h2>
         <i>Lorem ipsum dolor est sit amtet, lorem ipsum dolor est sit amtet.Lorem ipsum dolor est sit amtet. Lorem ipsum dolor est sit amtet, lorem ipsum dolor est sit amtet.Lorem ipsum dolor est sit amtet</i>
         <br /><br /><br />
         <table>
            <tr><td>
                 Upload .csv file: &nbsp;&nbsp;&nbsp;
                 </td><td>
              <div class="tgfileupload">
                 <asp:FileUpload ID="fuCsvFile"  Width="500" runat="server" />&nbsp;&nbsp;&nbsp;
                 <asp:Label ID="lblUploadFile" runat="server"/> <br /><br />
              </div>
             </td></tr><tr><td>
                 Repeat: 
                 </td><td>
                 <asp:TextBox ID="tbRepeat" runat="server"></asp:TextBox><br /><br />
             </td></tr><tr><td>
                <asp:CheckBox ID="cbEstimateTime" runat="server" /> 
                </td><td>Estimate time to generate traffic<br />
                <i>Note: This generates 10 addition requests</i>
             </td></tr>
             <tr><td>
             <p align="right">
                <asp:Button ID="btnUpload" runat="server" Text="Upload" onclick="btnUpload_Click" />    
             </p>
             </td></tr>
           </table>
        </asp:Panel>

        <asp:Panel ID="pnlGenerateTraffic" runat="server">
         <h2><font color="#D0D0D0"> Upload File >> </font><font color="red">Generate Traffic</font><font color="#D0D0D0"> >> Confirmation </font> </h2>
         <i>Lorem ipsum dolor est sit amtet, lorem ipsum dolor est sit amtet.Lorem ipsum dolor est sit amtet. Lorem ipsum dolor est sit amtet, lorem ipsum dolor est sit amtet.Lorem ipsum dolor est sit amtet</i>
         <br /><br /><br />
         <asp:Label ID="lblGenerateTraffic" runat="server"/> 
         

        <asp:Repeater ID="rptData" runat="server" OnItemDataBound="rptData_ItemDataBound">
             <HeaderTemplate>
                 <p align="center">
                 <table  border="1" style="border-style: solid; border-color: #D0D0D0;font-size:8px" cellpadding="2" cellspacing="0" >
                     <tr>
                         <td colspan="5">File content:</td>
                         <td colspan="2">Date:</td>
                         <td colspan="3" align="center">Traffic</td>
                         <td colspan="3" align="center">Engagement</td>
                     </tr>
                     <tr>
                         <td>Url</td>
                         <td>Ip</td>
                         <td>Type</td>
                         <td>Campaign</td>
                         <td>Keyword</td>
                         <td>From</td>
                         <td>To</td>
                         <td># Requests</td>
                         <td>Mode</td>
                         <td>Urls</td>
                         <td>Percentage</td>
                         <td>Type</td>
                         <td>Goals</td>
                     </tr>
             </HeaderTemplate>
             <ItemTemplate>
                     <tr>
                         <td><asp:label ID="lblReferrer" runat="server"/></td>
                         <td><asp:label ID="lblEngagementIp" runat="server"/></td>
                         <td><asp:label ID="lblTrafficType" runat="server"/></td>
                         <td><asp:label ID="lblCampaign" runat="server"/></td>
                         <td><asp:label ID="lblKeyword" runat="server"/></td>
                         <td><asp:label ID="lblDateFrom" runat="server"/></td>
                         <td><asp:label ID="lblDateTo" runat="server"/></td>
                         <td><asp:label ID="lblTrafficNumberOfRequests" runat="server"/></td>
                         <td><asp:label ID="lblTrafficMode" runat="server"/></td>
                         <td><asp:label ID="lblTrafficUrls" runat="server"/></td>
                         <td><asp:label ID="lblEngagementPercentage" runat="server"/></td>
                         <td><asp:label ID="lblEngagementMode" runat="server"/></td>
                         <td><asp:label ID="lblEngagementGoals" runat="server"/></td>
                     </tr>
             </ItemTemplate>
             <FooterTemplate>
             </table>
             </p>
             <br />
             <p align="right">
             <asp:Button ID="btnCancel" runat="server" Text="Cancel" onclick="btnCancel_Click"/>
             <asp:Button ID="btnGenerateTraffic" runat="server" Text="Generate Traffic" onclick="btnGenerateTraffic_Click"/>

             </p>
            </FooterTemplate>
            
         </asp:Repeater>
         </asp:Panel>

         <asp:Panel ID="pnlConfirmation" runat="server">
         <h2><font color="#D0D0D0"> Upload File >> Start Generation >> </font><font color="red">Confirmation</font> </h2>
             <i>Lorem ipsum dolor est sit amtet, lorem ipsum dolor est sit amtet.Lorem ipsum dolor est sit amtet. Lorem ipsum dolor est sit amtet, lorem ipsum dolor est sit amtet.Lorem ipsum dolor est sit amtet</i>
             <br /><br /><br />
             Report:
             <div style="font-size:8px">
                 <asp:Label ID="lblConfirmation" runat="server"/> <br />
             </div>
             <p align="right">
                 <asp:Button ID="btnDone" runat="server" Text="Done" onclick="btnDone_Click"/>
             </p>
             Detailed Requests:
             <div style="font-size:8px">
                <asp:Label ID="lblConfirmationRequests" runat="server"/> <br /><br />
             </div>
         </asp:Panel>


     </div>

     <br /><br /><br /><br />
    