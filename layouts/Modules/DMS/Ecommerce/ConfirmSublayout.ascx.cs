﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.Collections.Specialized;
using System.IO;
using System.Data;
using Dms.Jump.Business.Entities;
using Dms.Jump.Business.Helper;
using System.Web.UI.HtmlControls;
using Dms.Jump.Business.Utilities.Tools;
using System.Threading;
using Dms.Jump.Business.Entities.Requests;
using Sitecore.Analytics;
using Sitecore.Analytics.Data.Items;
using Sitecore.Analytics.Data;
using Sitecore.Data.Items;
using Dms.Jump.Business.Utilities.Extension; 

namespace Dms.Jump.Presentation.Modules.DMS.Ecommerce
{
    public partial class ConfirmSublayout : EcommerceBaseSublayout
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                phConfirm.Visible = true;

                // empty basket
                base.Cart = null; 
            }
        }
    }
}
