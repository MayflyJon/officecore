﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConfirmSublayout.ascx.cs" Inherits="Dms.Jump.Presentation.Modules.DMS.Ecommerce.ConfirmSublayout" %>


<asp:PlaceHolder id="phConfirm" runat="server">
    <h2><font color="#D0D0D0">Find Product - Check Out - </font><font color="red">Confirmation</font></h2><br /><br />
    <h2>Thank You for buying at our Online Store!</h2>
    <br /><br />
</asp:PlaceHolder>