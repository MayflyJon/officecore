﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.Collections.Specialized;
using System.IO;
using System.Data;
using Dms.Jump.Business.Entities;
using Dms.Jump.Business.Helper;
using System.Web.UI.HtmlControls;
using Dms.Jump.Business.Utilities.Tools;
using System.Threading;
using Dms.Jump.Business.Entities.Requests;
using Sitecore.Analytics;
using Sitecore.Analytics.Data.Items;
using Sitecore.Analytics.Data;
using Sitecore.Data.Items;
using Dms.Jump.Business.Utilities.Extension; 

namespace Dms.Jump.Presentation.Modules.DMS.Ecommerce
{
    public partial class AddToCartSublayout : EcommerceBaseSublayout
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                var isProduct = this.ItemInstance.HasBaseTemplate("Product Description");

                if (isProduct)
                {
                    PageEventData pageEventData = new PageEventData("View Product")
                    {
                        Data = this.ItemInstance.ID.ToString(),
                        DataKey = "Dms.Jump.PageEventData.ViewProduct",
                        Text = string.Format("Viewing Product {0}", this.ItemInstance.Name),
                    };

                    Tracker.CurrentPage.Register(pageEventData);
                }

                phAddToCart.Visible = isProduct;
                phAddedToCart.Visible = !phAddToCart.Visible;
            }
        }

        protected void imgbtnAddToCart_Click(object sender, EventArgs e)
        {
            this.AddToBasket(); 
            
            PageEventData pageEventData = new PageEventData("Add Product")
            {
                Data = this.ItemInstance.ID.ToString(),
                DataKey = "Dms.Jump.PageEventData.AddProduct",
                Text = string.Format("Add Product {0} to Cart Quantity {1}", this.ItemInstance.Name, tbQuantity.Text),
            };

            Tracker.CurrentPage.Register(pageEventData);

            phAddedToCart.Visible = true;
            phAddToCart.Visible = false;
        }

        private void AddToBasket()
        {
            int quantity = 0;
            Int32.TryParse(tbQuantity.Text, out quantity);

            double unitPrice = 0;
            if (this.ItemInstance.HasField("Price"))
            {
                string itemPrice = this.ItemInstance.GetFieldValue("Price");
                var strippedPrice = itemPrice.Replace("$", "").Split('.');
                Double.TryParse(strippedPrice[0], out unitPrice);
            }

            base.Cart.Add(this.ItemInstance.ID, quantity, (int)unitPrice); 
        }
    }
}
