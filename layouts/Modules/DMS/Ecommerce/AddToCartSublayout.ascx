﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddToCartSublayout.ascx.cs" Inherits="Dms.Jump.Presentation.Modules.DMS.Ecommerce.AddToCartSublayout" %>



<asp:PlaceHolder id="phAddToCart" runat="server">
        <table cellpadding="2px" style="background-color: #F5F5FA">
            <tr>
                <td valign="top" width="220">
                    <b>Quantity:</b>&nbsp;&nbsp;&nbsp;<asp:TextBox ID="tbQuantity" onfocus="doClear(this)" value="1" style="color: black; width: 30px; text-align: right" runat="server"/>
                </td>
                <td valign="middle" align="left" width="90">
                    <asp:ImageButton ID="imgbtnAddToCart"  ImageUrl="~/images/ecommerce/add_to_basket.gif" OnClick="imgbtnAddToCart_Click" runat="server" />
                </td>
            </tr>
        </table>
</asp:PlaceHolder>

<asp:PlaceHolder ID="phAddedToCart" runat="server">
    <table cellpadding="2px" style="background-color: #F5F5FA">
        <tr>
            <td valign="top" width="310">
                <b>Product Added to Cart</b>
            </td>
        </tr>
    </table>

</asp:PlaceHolder>