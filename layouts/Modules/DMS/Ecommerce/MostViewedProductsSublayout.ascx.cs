﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.Collections.Specialized;
using System.IO;
using System.Data;
using Dms.Jump.Business.Entities;
using Dms.Jump.Business.Helper;
using System.Web.UI.HtmlControls;
using Dms.Jump.Business.Utilities.Tools;
using System.Threading;
using Dms.Jump.Business.Entities.Requests;
using Sitecore.Analytics;
using Sitecore.Analytics.Data.Items;
using Sitecore.Analytics.Data;
using Sitecore.Data.Items;
using Dms.Jump.Business.Utilities.Extension;
using Sitecore.Resources.Media;
using Dms.Jump.Business.Data.Retrievers;
using Sitecore.Web;
using Sitecore.Links;
using System.Text.RegularExpressions; 

namespace Dms.Jump.Presentation.Modules.DMS.Ecommerce
{
    public partial class MostViewedProductsSublayout : EcommerceBaseSublayout
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                var pageEventsDataRetriever = new PageEventsDataRetriever();
                
                var items = pageEventsDataRetriever.GetMostViewedProducts(5);
                rptData.DataSource = items;
                rptData.DataBind(); 
            }
        }

        protected void rptData_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item ||
                e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var lblProductTitle = e.Item.FindControl("lblProductTitle") as Label;
                var lblProductDetails = e.Item.FindControl("lblProductDetails") as Label;
                var hlinkReadMore = e.Item.FindControl("hlinkReadMore") as HyperLink;
                var imgProduct = e.Item.FindControl("imgProduct") as Image;

                var item = e.Item.DataItem as Item;

                if( item.Fields["Title"] != null )
                    lblProductTitle.Text = item.Fields["Title"].ToString();

                if (item.Fields["Text"] != null) 
                {
                    string text = item.Fields["Text"].ToString();
                    text = Regex.Replace(text, @"<[^>]*>", String.Empty); 
                    var maxLength = 140; 
                    var lenght = Math.Min( text.Length, maxLength );
                    lblProductDetails.Text = text.Substring(0, lenght);
                    if (text.Length > maxLength)
                        lblProductDetails.Text += "..."; 
                }

                UrlOptions urlOptions = new UrlOptions(); 
                urlOptions.AddAspxExtension = true;
                string navigateUrl = LinkManager.GetItemUrl(item, urlOptions);
                hlinkReadMore.NavigateUrl = navigateUrl; 
                
                 var mediaUrl = string.Empty;
                 if (item.HasField("Image"))
                 {
                     var imageField = (Sitecore.Data.Fields.ImageField)item.Fields["Image"];
                     var mediaItem = imageField.MediaItem;

                     if (mediaItem != null)
                     {
                         var options = new MediaUrlOptions();
                         options.Width = 75;
                         options.MaxHeight = 75;
                         options.Thumbnail = true;
                         options.UseItemPath = true;
                         mediaUrl = MediaManager.GetMediaUrl(mediaItem, options);
                     }
                 }

                 if (!String.IsNullOrEmpty(mediaUrl))
                     imgProduct.ImageUrl = "~/" + mediaUrl;
            }

        }
    }
}
