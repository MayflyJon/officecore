﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CheckoutProcessSublayout.ascx.cs" Inherits="Dms.Jump.Presentation.Modules.DMS.Ecommerce.CheckoutProcessSublayout" %>



<asp:PlaceHolder id="phEcommerce" runat="server">

<asp:PlaceHolder id="phCartEmpty" runat="server">
<b>There are currently no items in your shopping bag.</b>
</asp:PlaceHolder>


<asp:PlaceHolder id="phCart" runat="server">
    <h2><font color="#D0D0D0">Find Products - </font><font color="red">Check Out</font><font color="#D0D0D0"> - Confirmation</font></h2><br />

        <asp:Repeater ID="rptData" runat="server" OnItemDataBound="rptData_ItemDataBound">
            <HeaderTemplate>
               <table  border="1" style="border-style: solid; border-color: #D0D0D0" cellpadding="2" cellspacing="0" width="100%">
                    <tr>
                         <td colspan="3" style="background-color: #F5F5FA; font-style:italic">Invoice: <asp:label ID="lblInvoice" runat="server"/></td>
                    </tr>
                    <tr>
                         <td width="550"><b>Product Details</b></td>
                         <td width="100"><div style="text-align: center"><b>Quantity</b></div></td>
                         <td width="100"><div style="text-align: center"><b>Price</b></div></td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                    <tr>
                        <td><div style="vertical-align: top"><asp:Image ID="imgProduct" runat="server"/>&nbsp;&nbsp;<asp:label ID="lblProductDetails" runat="server"/></td>
                        <td><div style="text-align: center"><asp:label ID="lblTotalQuantity" runat="server"/></div></td>
                        <td><div style="text-align: center">$<asp:label ID="lblTotalPrice" runat="server"/></div></td>
                    </tr>
            </ItemTemplate>
            <FooterTemplate>
                    <tr>
                        <td></td>
                        <td style="background-color: #F5F5FA">Subtotal</td>
                        <td style="background-color: #F5F5FA"><div style="text-align: center">$<asp:label ID="lblSubTotal" runat="server"/></div></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="background-color: #F5F5FA">Shipping</td>
                        <td style="background-color: #F5F5FA"><div style="text-align: center"><font color="red">FREE</font></div></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="background-color: #F5F5FA"><h3>Total</h3></td>
                        <td style="background-color: #F5F5FA"><div style="text-align: center">$<asp:label ID="lblTotal" runat="server"/></div></td>
                    </tr>
                </table>
                <div style="font-size: 9px; color:#D0D0D0; font-style:italic">* Estimated tax will be calculated during checkout.
                  Items in your shopping bag will remain for the duration of the Session and will always reflect the most current price and availability.
                  </div>
            </FooterTemplate>
        </asp:Repeater>
        <br /><br />
        <div style="text-align: right">
            <asp:Button ID="btnClearCart" runat="server" Text="Empty Cart" OnClick="btnEmptyCart_Click" />
            <asp:Button ID="btnConfirm" runat="server" Text="Confirm" OnClick="btnConfirm_Click" />
        </div>
</asp:PlaceHolder>

</asp:PlaceHolder>