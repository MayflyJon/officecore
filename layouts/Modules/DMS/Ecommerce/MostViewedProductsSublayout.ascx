﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MostViewedProductsSublayout.ascx.cs" Inherits="Dms.Jump.Presentation.Modules.DMS.Ecommerce.MostViewedProductsSublayout" %>



<asp:PlaceHolder id="phMostViewedProducts" runat="server">

    <h2>WHAT'S <font color="red">HOT</font></h2>
    <br />
    <asp:Repeater ID="rptData" runat="server" OnItemDataBound="rptData_ItemDataBound">
         <HeaderTemplate>
            <table cellpadding="2" cellspacing="0" width="100%">
         </HeaderTemplate>
         <ItemTemplate>
                <tr>
                    <td rowspan="2"><div style="vertical-align: top"><asp:Image ID="imgProduct" runat="server" /></div></td>
                    <td><div style="text-align: left"><b><asp:label ID="lblProductTitle" runat="server"/></b><br />
                    <asp:label ID="lblProductDetails" runat="server"/></div></td>
                </tr>
                <tr>
                    <td><div style="text-align: right"><asp:HyperLink ID="hlinkReadMore" Text="Read more" runat="server"/></td>
                </tr>
         </ItemTemplate>
         <FooterTemplate>
            </table>
         </FooterTemplate>
     </asp:Repeater>

</asp:PlaceHolder>

