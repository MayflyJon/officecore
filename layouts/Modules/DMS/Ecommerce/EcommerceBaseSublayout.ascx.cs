﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.Collections.Specialized;
using System.IO;
using System.Data;
using Dms.Jump.Business.Entities;
using Dms.Jump.Business.Helper;
using System.Web.UI.HtmlControls;
using Dms.Jump.Business.Utilities.Tools;
using System.Threading;
using Dms.Jump.Business.Entities.Requests;
using Sitecore.Analytics;
using Sitecore.Analytics.Data.Items;
using Sitecore.Analytics.Data;
using Sitecore.Data.Items;
using Dms.Jump.Business.Utilities.Extension;
using Dms.Jump.Ecommerce.Entities;

namespace Dms.Jump.Presentation.Modules.DMS.Ecommerce
{
    public partial class EcommerceBaseSublayout : System.Web.UI.UserControl
    {
        public Item ItemInstance 
        {
            get
            {
                return Sitecore.Context.Item; 
            }
        }

        protected Order Cart
        {
            get
            {
                lock (this)
                {
                    if (Session["SC_OrderEngine_Order"] == null)
                        Session["SC_OrderEngine_Order"] = new Order();
                }

                return Session["SC_OrderEngine_Order"] as Order;
            }
            set
            {
                Session["SC_OrderEngine_Order"] = value;
            }

        }
    }
}
