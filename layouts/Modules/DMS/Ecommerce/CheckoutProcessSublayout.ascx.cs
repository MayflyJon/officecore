﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.Collections.Specialized;
using System.IO;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Threading;

using Sitecore.Analytics;
using Sitecore.Analytics.Data.Items;
using Sitecore.Analytics.Data;
using Sitecore.Data.Fields;
using Sitecore.Resources.Media;

using Dms.Jump.Business.Entities;
using Dms.Jump.Business.Entities.Requests;
using Dms.Jump.Business.Helper;
using Dms.Jump.Business.Utilities.Tools;
using Dms.Jump.Business.Utilities.Extension;

using Dms.Jump.Ecommerce.Entities;


namespace Dms.Jump.Presentation.Modules.DMS.Ecommerce
{
    public partial class CheckoutProcessSublayout : EcommerceBaseSublayout
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                phEcommerce.Visible = true;

                phCartEmpty.Visible = this.Cart.IsEmpty();
                phCart.Visible = !phCartEmpty.Visible;
            }

            rptData.DataSource = base.Cart.OrderLines;
            rptData.DataBind(); 
        }

        protected void btnEmptyCart_Click(object sender, EventArgs e)
        {
            PageEventData pageEventData = new PageEventData("Empty Cart")
            {
                Data = this.Cart.Invoice,
                DataKey = "Dms.Jump.PageEventData.EmptyCart",
                Text = string.Format("Empty Cart with {0} products", this.Cart.GetTotalQuantity()),
            };

            Tracker.CurrentPage.Register(pageEventData);

            this.Cart = null; 

            phCartEmpty.Visible = this.Cart.IsEmpty();
            phCart.Visible = !phCartEmpty.Visible;
        }

        protected void rptData_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item ||
                e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var lblProductDetails = e.Item.FindControl("lblProductDetails") as Label;
                var lblTotalQuantity = e.Item.FindControl("lblTotalQuantity") as Label;
                var lblTotalPrice = e.Item.FindControl("lblTotalPrice") as Label;
                var imgProduct = e.Item.FindControl("imgProduct") as Image;

                var orderLine = e.Item.DataItem as OrderLine;

                lblProductDetails.Text = orderLine.Name;
                lblTotalQuantity.Text = orderLine.Quantity.ToString();
                lblTotalPrice.Text = orderLine.Price.ToString(); 


                var item = orderLine.ItemInstance;
                var mediaUrl = string.Empty; 
                if (item.HasField("Image"))
                {
                    var imageField = (Sitecore.Data.Fields.ImageField)item.Fields["Image"];
                    var mediaItem = imageField.MediaItem;

                    if (mediaItem != null)
                    {
                        var options = new MediaUrlOptions();
                        options.Width = 25;
                        options.MaxHeight = 25;
                        options.Thumbnail = true;
                        options.UseItemPath = true;
                        mediaUrl = MediaManager.GetMediaUrl(mediaItem, options); 
                    }
                }

                if (!String.IsNullOrEmpty(mediaUrl))
                    imgProduct.ImageUrl = "~/" + mediaUrl;

            }
            else if (e.Item.ItemType == ListItemType.Header)
            {
                var lblInvoice = e.Item.FindControl("lblInvoice") as Label;

                lblInvoice.Text = this.Cart.Invoice; 
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                var lblSubTotal = e.Item.FindControl("lblSubTotal") as Label;
                var lblTotal = e.Item.FindControl("lblTotal") as Label;

                lblSubTotal.Text = this.Cart.GetTotalPrice().ToString();
                lblTotal.Text = this.Cart.GetTotalPrice().ToString(); 
            }
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            foreach (var orderline in this.Cart.OrderLines)
            {
                PageEventData pageEventData = new PageEventData("Buy Product")
                {
                    Data = orderline.ItemId.ToString(),
                    DataKey = "Dms.Jump.PageEventData.BuyProduct",
                    Text = string.Format("Buy Product {0}", orderline.Name)
                };

                Tracker.CurrentPage.Register(pageEventData);

                this.UpdateVisitorPhase("Confirms"); 
            }


            if (this.ItemInstance.HasField("Cart Confirmation Page"))
            {
                var interalLink = (InternalLinkField) this.ItemInstance.Fields["Cart Confirmation Page"];
                var targetItem = interalLink.TargetItem;

                if( targetItem != null ) 
                    Response.Redirect(targetItem.GetUrlOfItem()); 
            }


        }

        private void UpdateVisitorPhase(string phase)
        {
            var privileges = Tracker.Visitor.Tags.Find("Phase");

            if (privileges != null)
                privileges.TagValue = "Confirms";
            else
                Tracker.Visitor.Tags.Add("Phase", "Confirms"); 
        }
    }
}
