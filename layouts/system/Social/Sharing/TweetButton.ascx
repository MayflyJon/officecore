﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Sitecore.Social.Twitter.Client.Sharing.Controls.TweetButton" %>
<%@ Import Namespace="Sitecore.Globalization" %>

<div style="float: right;" runat="server" id="TButton">
    <a href="http://twitter.com/share" data-url="<%= HttpUtility.UrlPathEncode(GetCampaignQS())%>" data-counturl="<%= this.GetSharePageUrl() %>" class="twitter-share-button" data-count="horizontal">
        <%= Translate.Text(Sitecore.Social.Twitter.Common.Texts.Tweet)%></a>
    <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
</div>
