﻿using Sitecore;
using Sitecore.Text;
using Sitecore.Web.UI.Sheer;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Shell.Framework.Commands;

namespace Officecore.Website.code.Sheer
{
    public class Editor : Command
    {
        public override void Execute([NotNull] CommandContext context)
        {
            var url = new UrlString("/sitecore/shell/Applications/Content%20editor.aspx");
            SheerResponse.Eval("open('" + url + "')");
        }
    }
}