﻿using Sitecore;
using Sitecore.Data.Items;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Officecore.Website.code
{
    public class DateComparer : IComparer<Item>
    {
        public int Compare(Item x, Item y)
        {
            if (x.Fields["Date"] != null && y.Fields["Date"] != null)
            {
                DateTime time1 = DateUtil.ParseDateTime(x.Fields["Date"].Value, DateTime.Now);
                DateTime time2 = DateUtil.ParseDateTime(y.Fields["Date"].Value, DateTime.Now);
                return DateTime.Compare(time1, time2) * -1;
            }
            return -1;
        }
    }
}