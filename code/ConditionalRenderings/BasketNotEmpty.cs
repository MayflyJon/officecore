﻿using Sitecore.Rules;
using Sitecore.Rules.Conditions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UCommerce.Api;

namespace Officecore.Website.code.ConditionalRenderings
{
    public class BasketNotEmpty<T> : WhenCondition<T> where T : RuleContext
    {

        protected override bool Execute(T ruleContext)
        {
            return TransactionLibrary.GetBasket(true).PurchaseOrder.OrderLines.Count > 0;
        }
    }
}