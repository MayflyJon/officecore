﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using Officecore.Website.code.Constants;
using Sitecore.Data.Items;
using UCommerce.Api;
using UCommerce.EntitiesV2;
using UCommerce.Runtime;
using UCommerce.Search.Facets;
using Officecore.Website.code.Helpers;
using UCommerce;

namespace Officecore.Website.code.Helpers
{
    public static class UcommerceHelper
    {
        /// <summary>
        /// The product relation types
        /// </summary>
        public enum ProductRelationType
        {
            Default = 1,
            Upsell = 2,
            CrossSell = 3
        }

        /// <summary>
        /// Get the product relation type from Sitecore
        /// </summary>
        /// <param name="item">The item containing the ID</param>
        /// <returns>The ProductRelationType. Returns ProductRelationType.Default by default</returns>
        public static ProductRelationType GetProductRelationType(Item item)
        {
            var type = ProductRelationType.Default;
            if (item != null)
            {
                int id = 0;
                if (int.TryParse(item[FieldConstants.Ucommerce.ProductRelationTypeId], out id))
                {
                    type = (ProductRelationType)id;
                }
            }

            return type;
        }

        /// <summary>
        /// Get the facets from the querystring
        /// </summary>
        /// <param name="qs">The querystring</param>
        /// <returns>The list of facets</returns>
        public static IList<Facet> GetFacets(NameValueCollection qs)
        {
            var parameters = new Dictionary<string, string>();
            foreach (var queryString in qs.AllKeys)
            {
                if (queryString == null) continue;
                if (queryString.StartsWith("sc_")) continue;

                parameters[queryString] = qs[queryString];
            }

            if (parameters.ContainsKey("product"))
            {
                parameters.Remove("product");
            }

            if (parameters.ContainsKey("category"))
            {
                parameters.Remove("category");
            }

            if (parameters.ContainsKey("catalog"))
            {
                parameters.Remove("catalog");
            }

            if (parameters.ContainsKey("SearchTermHeader"))
            {
                parameters.Remove("SearchTermHeader");
            }

            var facetsForQuerying = new List<Facet>();

            foreach (var parameter in parameters)
            {
                var facet = new Facet();
                facet.FacetValues = new List<FacetValue>();
                facet.Name = parameter.Key;
                foreach (var value in parameter.Value.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    facet.FacetValues.Add(new FacetValue() { Value = value });
                }
                facetsForQuerying.Add(facet);
            }

            return facetsForQuerying;
        }

        

        /// <summary>
        /// Get the rating from a single review
        /// </summary>
        /// <param name="review">The review to get the rating of</param>
        /// <returns>A string with the amount of stars</returns>
        /// <remarks>This does not take into account the 'reviews need to be approved' tickbox on the catalog</remarks>
        public static string GetReviewRating(ProductReview review)
        {
            var returnValue = string.Empty;

            if (review.Approved())
            {
                var rating = review.Rating;
                returnValue = GetRating(rating);
            }
            else
            {
                returnValue = GetRating(0);
            }

            return returnValue;
        }

        /// <summary>
        /// Get the average product rating of approved products
        /// </summary>
        /// <param name="currentProduct">The product</param>
        /// <param name="url">The URL</param>
        /// <returns>The HTML displaying the rating</returns>
        /// <remarks>This does not take into account the 'reviews need to be approved' tickbox on the catalog</remarks>
        public static string GetProductRating(code.Model.Product currentProduct, String url)
        {
            // Can't use currentProduct.Rating as that includes unapproved reviews.
            var rating = 0;
            var numberOfApprovedReviews = 0;
            var totalRating = 0;
            if (currentProduct.ProductReviews != null)
            {
                foreach (ProductReview review in currentProduct.ProductReviews)
                {
                    if (review.Approved())
                    {
                        numberOfApprovedReviews++;
                        if (review.Rating.HasValue)
                        {
                            totalRating += review.Rating.Value;
                        }
                    }
                }

                if (numberOfApprovedReviews > 0 && totalRating > 0)
                {
                    rating = totalRating / numberOfApprovedReviews;
                }               
            }
            var returnValue = GetRating(rating);
            if (url != string.Empty)
            {
                returnValue += "&nbsp;<a class=\"rating-overview\" href=\"" + url + "#ratings" + "\">(" + numberOfApprovedReviews + " reviews)</a>";
            }

            return returnValue;
        }

        /// <summary>
        /// Get the rating string
        /// </summary>
        /// <param name="rating">The rating</param>
        /// <returns>The html displaying (empty) stars</returns>
        public static string GetRating(Double? rating)
        {
            var returnValue = "<span class=\"star-rating\">";
            if (rating.HasValue)
            {
                for (int i = 20; i <= 100; i = i + 20)
                {
                    if (rating >= i)
                    {
                        returnValue += "<i class=\"" + CssConstants.GlyphStar + "\"></i>";
                    }
                    else
                    {
                        returnValue += "<i class=\"" + CssConstants.GlyphEmptyStar + "\"></i>";
                    }
                }
            }
            else
            {
                for (int i = 20; i <= 100; i = i + 20)
                {
                    returnValue += "<i class=\"" + CssConstants.GlyphEmptyStar + "\"></i>";
                }
            }

            returnValue += "</span>";

            return returnValue;
        }    

        public static string GetBasketTotal(PurchaseOrder basket)
        {
            Money total = new Money(new decimal(), SiteContext.Current.CatalogContext.CurrentPriceGroup.Currency);
            if (basket.SubTotal.HasValue)
            {
                total = new Money(basket.SubTotal.Value, SiteContext.Current.CatalogContext.CurrentPriceGroup.Currency);
            }
            
            return total.ToString();
        }
    }
}