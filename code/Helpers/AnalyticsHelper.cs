﻿namespace Officecore.Website.code.Helpers
{
    using Sitecore.Analytics;
    using Sitecore.Analytics.Data.Items;
    using Sitecore.Data;

    public static class AnalyticsHelper
    {
        public static bool TriggerGoal(ID goalID)
        {
            if (goalID == (ID)null)
            {
                return false;
            }

            if (Tracker.CurrentPage == null)
            {
                return false;
            }

            var goalItem = Sitecore.Context.Database.GetItem(goalID);
            if (goalItem == null)
            {
                return false;
            }

            if (!Tracker.IsActive)
            {
                Tracker.StartTracking();
            }

            var goal = new PageEventItem(goalItem);
            Tracker.CurrentPage.Register(goal);
            Tracker.Submit();

            return true;
        }
    }
}