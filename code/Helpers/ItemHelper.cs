﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Officecore.Website.code.Helpers
{
    public static class ItemHelper
    {
        /// <summary>
        /// Check whether a specific field has to be wrapped around with paragraph tags
        /// </summary>
        /// <param name="item">The item</param>
        /// <param name="fieldname">The field</param>
        /// <returns>True if the string contained in the field does not start with a paragraph tag. False otherwise</returns>
        public static bool PrefixWithParagraphTag(Sitecore.Data.Items.Item item, String fieldname)
        {
            if (item[fieldname].StartsWith("<p>", StringComparison.InvariantCultureIgnoreCase))
            {
                return false;
            }

            return true;
        }
    }
}