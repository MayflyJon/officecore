﻿using System.Collections.Generic;
using Sitecore.Data;

namespace Officecore.Website.code.Helpers
{
    public static class ProfileHelper
    {
        public static IEnumerable<ID> GetInterests()
        {
            if (!Sitecore.Context.IsLoggedIn) yield break;

            foreach (var tag in Sitecore.Context.User.Profile.GetCustomProperty("Interests").Split('|'))
            {
                ID tagID;

                if (ID.TryParse(tag, out tagID))
                {
                    yield return tagID;
                }
            }
        }
    }
}