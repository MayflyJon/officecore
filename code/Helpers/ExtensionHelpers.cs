﻿using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Data.Templates;
using Sitecore.Resources.Media;
using UCommerce.EntitiesV2;

namespace Officecore.Website.code.Helpers
{
    public static class ExtensionHelpers
    {
        public static Item GetMediaField(this Item item, string fieldName)
        {
            if (item == null) return null;

            var imageField = (ImageField)item.Fields[fieldName];
            return imageField != null ? imageField.MediaItem : null;
        }

        public static string GetMediaFieldUrl(this Item item, string fieldName, string defaultUrl)
        {
            var mediaItem = item.GetMediaField(fieldName);
            var url = mediaItem != null ? MediaManager.GetMediaUrl(mediaItem) : defaultUrl;
            return StringUtil.EnsurePrefix('/', url);
        }

        /// <summary>
        /// Check if the ReviewStatus is approved.
        /// </summary>
        /// <param name="review">The review</param>
        /// <returns>True if the reviewstatus is approved.</returns>
        public static bool Approved(this ProductReview review)
        {
            return review.ProductReviewStatus.Id == 2000;
        }
    }
}