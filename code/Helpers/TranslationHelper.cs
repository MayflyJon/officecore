﻿namespace Officecore.Website.code.Helpers
{
    using Sitecore.Form.Web.UI.Controls;

    public static class TranslationHelper
    {
        // TODO: Would like to use a prefetch here which is cleared on publish.
        public static string Translate(string path, bool allowEdit = true)
        {
            if (string.IsNullOrEmpty(path))
            {
                return null;
            }

            if (Sitecore.Context.Language == null)
            {
                return "[Sitecore.Context.Language null]";
            }

            // TODO: Site specific folders, and then Global
            const string RootPath = "/sitecore/content/Global/Translations/";
            var fullPath = RootPath + path;

            var item = Sitecore.Context.Database.GetItem(fullPath, Sitecore.Context.Language);
            if (item == null)
            {
                return string.Format("[{0}:{1}:{2}]", Sitecore.Context.Database.Name, Sitecore.Context.Language.Name, path);
            }

            return FieldRenderer.Render(item, "Translation", string.Empty, !allowEdit);
        }
    }
}