﻿using Sitecore.Forms.Core.Rules;

namespace Officecore.Website.code.Rules.Actions
{
    public class ReadFromContextItemField<T> : ReadValue<T> where T : ConditionalRuleContext
    {
        protected override object GetValue()
        {
            return Sitecore.Context.Item != null ? Sitecore.Context.Item[Name] : string.Empty;
        }
    }
}