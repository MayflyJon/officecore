﻿using Sitecore.Rules;
using Sitecore.Rules.Conditions;
using Sitecore.Diagnostics;
using Sitecore.Data;

namespace Officecore.Website.code.Rules.Conditions
{
    using System.Linq;
    using Officecore.Website.code.Helpers;

    public class HasProfileInterest<T> : OperatorCondition<T> where T : RuleContext
    {
        public ID InterestID { get; set; }

        public string QueryStringValue { get; set; }

        protected override bool Execute(T ruleContext)
        {
            Assert.ArgumentNotNull(ruleContext, "ruleContext");
            if (!Sitecore.Context.IsLoggedIn || Sitecore.Context.User.Profile == null) return false;

            var interests = ProfileHelper.GetInterests();
            return interests.Contains(InterestID);
        }
    }
}