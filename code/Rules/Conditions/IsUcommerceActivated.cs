﻿using Sitecore.Rules;
using Sitecore.Rules.Conditions;
using Sitecore.Diagnostics;

namespace Officecore.Website.code.Rules.Conditions
{
    using Officecore.Website.code.DataRepositories;

    public class IsUcommerceActivated<T> : WhenCondition<T> where T : RuleContext
    {
        public string Value { get; set; }

        protected override bool Execute(T ruleContext)
        {
            Assert.ArgumentNotNull(ruleContext, "ruleContext");

            var repo = new ProductRepository();
            return repo.ActivateCommerce();
        }
    }
}