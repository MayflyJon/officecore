﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Officecore.Website.code.Constants
{
    public static class ItemConstants
    {
        /// <summary>
        /// The ID for the product content item
        /// </summary>
        public const string ProductId = "{07D9A696-A2FE-4A59-88FB-A57FE386B8AD}";
    }
}