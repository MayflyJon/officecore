﻿using UCommerce.EntitiesV2;
using System.Collections.Generic;
using System.Web.Security;
using Sitecore.Security.Authentication;

namespace Officecore.Website.code.Service
{
    using System;
    using Sitecore.Analytics;

    public class CustomerService
    {
        public bool Logon(string username, string password)
        {
            var userName = "extranet\\" + username.ToLower();
            return AuthenticationManager.Login(userName, password);
        }

        public void Logout()
        {
            AuthenticationManager.Logout();
            Sitecore.Web.WebUtil.Redirect("/");
        }

        public Sitecore.Security.Accounts.User CreateUser(string username, string password, string title, string firstname, string lastname, string phone, string mobile, string email)
        {
            var userName = Sitecore.Context.Domain.Name + "\\" + username.ToLower();

            var user = Sitecore.Security.Accounts.User.Create(userName, password);
            user.Roles.Add(Sitecore.Security.Accounts.Role.FromName("extranet\\Customer"));
            user.Profile.FullName = string.Format("{0} {1}", firstname, lastname);
            user.Profile.Email = email;
            user.Profile.Save();
            return user;
        }

        public void DeleteUser(string username, bool deleteAllRelatedData)
        {
            var user = Sitecore.Security.Accounts.User.FromName(username, false);
            if (user != null)
            {
                user.Delete();
            }
        }

        public IEnumerable<PurchaseOrder> GetCustomerOrders()
        {
            if (Sitecore.Context.User.IsAuthenticated)
            {
                //Then we have a user with an email to search to uCommerce customer database
                var user = Sitecore.Context.User;

                //Get the ID for a match between the Ucommerce customer table email and the User.Profile.Email
                var customer = Customer.FirstOrDefault(x => x.EmailAddress == user.Profile.Email);
                if (customer != null)
                {
                    int customerId = customer.CustomerId;
                    //Get a list of orders where the CustomerId in the PurchaseOrder table matches the ID found above from the customer table
                    return PurchaseOrder.Find(x => x.Customer.CustomerId == customerId);
                }
            }

            return new List<PurchaseOrder>();
        }

        public void UpdateCustomer(string type, string title = null, string firstname = null, string lastname = null, string phone = null, string mobile = null, string email = null, string house = null, string street = null, string city = null, string county = null, string country = null, string postcode = null)
        {
            if (Sitecore.Context.User == null || Sitecore.Context.User.Profile == null) return;
            var profile = Sitecore.Context.User.Profile;

            // TODO: Check new email doesn't exist elsewhere
            if (string.IsNullOrEmpty(profile.Email) && email != null)
            {
                profile.Email = email;
            }

            if (title != null)
            {
                profile.SetCustomProperty(type + "Title", title);
            }

            if (firstname != null)
            {
                profile.SetCustomProperty(type + "FirstName", firstname);
            }

            if (lastname != null)
            {
                profile.SetCustomProperty(type + "LastName", lastname);
            }

            if (firstname != null && lastname != null)
            {
                profile.FullName = string.Format("{0} {1}", firstname, lastname);
            }

            if (phone != null)
            {
                profile.SetCustomProperty(type + "Phone", phone);
            }

            if (mobile != null)
            {
                profile.SetCustomProperty(type + "Mobile", mobile);
            }

            if (house != null)
            {
                profile.SetCustomProperty(type + "House", house);
            }

            if (street != null)
            {
                profile.SetCustomProperty(type + "Street", street);
            }

            if (city != null)
            {
                profile.SetCustomProperty(type + "City", city);
            }

            if (county != null)
            {
                profile.SetCustomProperty(type + "County", county);
            }

            if (postcode != null)
            {
                profile.SetCustomProperty(type + "Postcode", postcode);
            }

            if (country != null)
            {
                profile.SetCustomProperty(type + "Country", country);
            }

            profile.Save();
        }

        public bool UserNameExists(string username)
        {
            return Sitecore.Security.Accounts.User.Exists(username);
        }

        // TODO: Clean this up
        public bool UserEmailExists(string email)
        {
            return this.GetUserByEmailAddress(email) != null;
        }

        // TODO: Clean this up
        public Sitecore.Security.Accounts.User GetUserByEmailAddress(string email)
        {
            if (email == null)
            {
                throw new ArgumentNullException("email");
            }

            var username = Membership.GetUserNameByEmail(email);
            if (!string.IsNullOrEmpty(username))
            {
                return Sitecore.Security.Accounts.User.FromName(username, true);
            }

            username = Sitecore.Context.Domain + "\\" + this.EscapeEmailToUsername(email);
            if (Sitecore.Security.Accounts.User.Exists(username))
            {
                return Sitecore.Security.Accounts.User.FromName(username, true);    
            }

            return null;
        }

        protected bool UpdatePassword(string username, string newpassword, string oldpassword)
        {
            //if the old password and the newpassword string differ then change the password.
            if (oldpassword.Equals(newpassword)) return false;

            return Membership.Provider.ChangePassword(username, newpassword, oldpassword);
        }

        public void ResetPassword(string username)
        {
            //TODO: This should trigger an automated email taking the customer to a page to setup a new password
        }

        public void FixAutomationStates()
        {
            if (!Sitecore.Context.IsLoggedIn)
            {
                return;
            }

            if (!Tracker.IsActive)
            {
                Tracker.StartTracking();
            }

            Tracker.Visitor.ExternalUser = Sitecore.Context.GetUserName();
            Tracker.Visitor.Submit();

            const string Query = "UPDATE {0}AutomationStates{1} SET {0}UserName{1}={2}UserName{3} WHERE {0}VisitorID{1}={2}VisitorID{3}";
            var parameters = new object[] { "UserName", Tracker.Visitor.ExternalUser, "VisitorID", Tracker.CurrentVisit.VisitorId };
            Sitecore.Analytics.Data.DataAccess.DataAdapters.DataAdapterManager.Sql.Execute(Query, parameters);
        }

        public string EscapeEmailToUsername(string email)
        {
            if (email == null)
            {
                throw new ArgumentNullException("email");
            }

            return email.Replace("@", "_").Replace(".", "_").Replace("-", "_").Replace("+", "_");
        }
    }
}