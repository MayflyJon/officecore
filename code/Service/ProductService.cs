﻿using Officecore.Website.code.Constants;
using Officecore.Website.code.DataRepositories;
using Officecore.Website.code.Helpers;
using Officecore.Website.code.Model;
using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UCommerce.EntitiesV2;
using UCommerce.Runtime;

namespace Officecore.Website.code.Service
{
    using Sitecore.ContentSearch;
    using Sitecore.ContentSearch.Linq;
    using Sitecore.ContentSearch.Linq.Utilities;

    public class ProductService
    {
        private ProductRepository repo = new ProductRepository();

        public static HashSet<ID> SearchTemplates = 
            Sitecore.ContentSearch.Utilities.EnumerableExtensions.ToHashSet("{8104A35C-485D-4241-B80D-83E0C1926D9F}|{C0A59B39-8C63-4FF1-B7CB-75457DB35FA6}|{1D578EDE-4723-4E21-B38E-57DAA0F017B1}|{EF3E2C78-551D-4310-8A15-5F53470DA8D3}|{23CFC180-AF56-4F12-B1DB-F26534BCD218}|{0418A650-AE57-4DD3-BDB4-CF8E6C7F89BA}|{34A5EDED-6BD2-4DF5-ADDB-163F4655BEC7}|{D89BDC0E-FF2B-4AA9-BA39-8F863A6056E9}|{74A07E7C-B826-4B23-B87C-11398E9D6FEF}|{1A7B1DFF-E172-4DCF-8F01-B34571075756}".Split('|').Select(x => new ID(x)));

        public static HashSet<ID> SearchTemplatesJustProducts =
            Sitecore.ContentSearch.Utilities.EnumerableExtensions.ToHashSet("{1A7B1DFF-E172-4DCF-8F01-B34571075756}".Split('|').Select(x => new ID(x)));

        private Item activateCommerce = Sitecore.Data.Database.GetDatabase("web").GetItem(ProductConstants.ACTIVATE_COMMERCE);
        
        // this needs to be master db as we edit in page editor
        private Item promoSpotSublayout = Sitecore.Data.Database.GetDatabase("master").GetItem("{5594F1B0-4E8A-4331-ADE4-76BE89286AC9}");

        public List<ProductReview> GetProductReviews(UCommerce.EntitiesV2.Product currentProduct)
        {
            return repo.GetApprovedProductReviews(currentProduct);
        }

        public Boolean ActivateCommerce()
        {
            return repo.ActivateCommerce();
        }

        public void UpdateCommerceToggle(String value)
        {
            string domainUser = @"sitecore\admin";
            Sitecore.Security.Accounts.User user =
                 Sitecore.Security.Accounts.User.FromName(domainUser, false);
            if (Sitecore.Security.Accounts.User.Exists(domainUser))
            {
                using (new Sitecore.Security.Accounts.UserSwitcher(user))
                {

                    //Begin editing the activeCommerce item in the content-home-standardItems
                    activateCommerce.Editing.BeginEdit();
                    //Set the ActivateCommerce field value to the toggleState
                    activateCommerce.Fields["ActivateCommerce"].Value = value;
                    //Set the Store field value to the value of a StoreConstant property
                    activateCommerce.Fields["Store"].Value = GetStore(value).ToString();
                    //End editing the activeCommerce item
                    activateCommerce.Editing.EndEdit();
                }
            }
        }

        public String GetStore(String value)
        {
            if (value == "false")
            {
                UpdatePromoSubLayoutDatasource("/sitecore/Content/Home/Products");
                return ProductConstants.FINANCIAL;
            }
            else
            {
                UpdatePromoSubLayoutDatasource("/sitecore/uCommerce/Stores/Officecore");
                return ProductConstants.DEFAULT;
            }
        }

        private void UpdatePromoSubLayoutDatasource(String source)
        {
            //Set the promotion spot datasource to finance
            using (new Sitecore.SecurityModel.SecurityDisabler())
            {
                promoSpotSublayout.Editing.BeginEdit();
                promoSpotSublayout.Fields["Datasource Location"].Value = source;
                promoSpotSublayout.Editing.EndEdit();
            }
        }

        public UCommerce.EntitiesV2.Product GetVariantFromPostData(UCommerce.EntitiesV2.Product product, string variantPrefix)
        {
            var request = HttpContext.Current.Request;
            var keys = request.Form.AllKeys.Where(k => k.StartsWith(variantPrefix, StringComparison.InvariantCultureIgnoreCase));
            var properties = keys.Select(k => new { Key = k.Replace(variantPrefix, string.Empty), Value = request.Form[k] }).ToList();

            UCommerce.EntitiesV2.Product variant = null;

            // If there are variant values we'll need to find the selected variant
            if (product.Variants.Any() && properties.Any())
            {
                variant = product.Variants.FirstOrDefault(v => v.ProductProperties
                    .Where(pp => pp.ProductDefinitionField.DisplayOnSite
                        && pp.ProductDefinitionField.IsVariantProperty
                        && !pp.ProductDefinitionField.Deleted)
                    .All(p => properties.Any(kv => kv.Key.Equals(p.ProductDefinitionField.Name, StringComparison.InvariantCultureIgnoreCase) && kv.Value.Equals(p.Value, StringComparison.InvariantCultureIgnoreCase))));
            }
            // Only use the current product where there are no variants
            else if (!product.Variants.Any())
            {
                variant = product;
            }

            return variant;
        }

        public Item GetProductPrimaryImage(UCommerce.EntitiesV2.Product currentProduct)
        {
            Sitecore.Data.ID mediaId = Sitecore.Data.ID.Parse(currentProduct.PrimaryImageMediaId);
            Item mediaItem = Sitecore.Context.Database.GetItem(mediaId);
            return mediaItem;
        }

        public void LeaveReview(String reviewName, String reviewEmail, String reviewHeadline, String reviewText)
        {
            repo.LeaveReview(SiteContext.Current.CatalogContext.CurrentProduct, reviewName, reviewEmail, Convert.ToInt32(HttpContext.Current.Request.Form["review-rating"]) * 20, reviewHeadline, reviewText, HttpContext.Current.Request.UserHostName);
        }

        public List<code.Model.Product> GetProducts(string searchterm, System.Collections.Specialized.NameValueCollection requestQueryString)
        {
            if (repo.ActivateCommerce())
            {
                IList<UCommerce.Search.Facets.Facet> facetList = UcommerceHelper.GetFacets(requestQueryString);
                List<code.Model.Product> productResults;

                var list = repo.SearchProducts(searchterm, facetList, out productResults).ToList();
                productResults.RemoveAll(x => !list.Exists(y => y.Sku == x.Sku));
                productResults = productResults.OrderBy(p => p.Name).ToList();
                return productResults;
            }

            var helper = new SearchHelper();


            using (var context = ContentSearchManager.GetIndex(helper.Index).CreateSearchContext())
            {
                var predicate = PredicateBuilder.True<ResultItem>();

                if (HttpContext.Current.Request.QueryString["SearchTermHeader"] != null)
                {
                    foreach (ID template in SearchTemplates)
                    {
                        predicate = predicate.Or(item => item.TemplateId == template);
                    }
                }
                else
                {
                    foreach (ID template in SearchTemplatesJustProducts)
                    {
                        predicate = predicate.Or(item => item.TemplateId == template);
                    }
                }

                var q = context.GetQueryable<ResultItem>()
                        .Where(item => item.Content.Contains(searchterm))
                        .Where(item => item.Name != "__Standard Values")
                        .Where(item => item.Language == Sitecore.Context.Language.Name)
                        .Where(item => item.IsLatestVersion)
                        .Where(predicate)
                        .FacetOn(item => item.TemplateId);

                var res = q.GetResults();

                Sitecore.Context.Items["oc_FacetCategories"] = res.Facets.Categories;

                ID facetTemplateId;
                if (ID.TryParse(HttpContext.Current.Request.QueryString["templateID"], out facetTemplateId))
                {
                    q = q.Where(x => x.TemplateId == facetTemplateId);
                }

                res = q.GetResults();
                var items = SearchHelper.GetItemsFromSearchResults(res.Hits.Select(x => x.Document));

                return items.Select(p => new Model.Product(p)).ToList();
            }
        }

        public List<code.Model.Product> GetProductsByCategory(Item category)
        {
            IEnumerable<Item> items = category.Axes.GetDescendants().Where(x => x.TemplateID.ToString() == "{1A7B1DFF-E172-4DCF-8F01-B34571075756}");
            return items.Select(x => new code.Model.Product(x)).ToList();
        }

        public UCommerce.EntitiesV2.Product GetUcommerceProduct(Item i)
        {
            return UCommerce.EntitiesV2.Product.FirstOrDefault(x => x.Sku == i.Fields["SKU"].Value);
        }

        public UCommerce.EntitiesV2.Category GetUcommerceCategory(Item i)
        {
            return UCommerce.EntitiesV2.Category.FirstOrDefault(x => x.Name == i.Fields["Name"].Value);
        }
    }
}