﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UCommerce.Api;
using UCommerce.Extensions;
using UCommerce.EntitiesV2;
using Sitecore.Data.Items;
using Officecore.Website.code.Helpers;
using Sitecore.Resources.Media;
using Sitecore.Links;

namespace Officecore.Website.code.Model
{
    using Sitecore.Data;

    public class Product
    {
        public String Name { get; set; }
        public String ThumbnailImage { get; set; }
        public Item PrimaryImage { get; set; }
        public String PrimaryImageUrl { get; set; }
        public String DisplayName { get; set; }
        public String Url { get; set; }
        public String ShortDescription { get; set; }
        public String LongDescription { get; set; }
        public ICollection<ProductReview> ProductReviews { get; set; }
        public String Price { get; set; }
        public String Tax { get; set; }
        public String Sku { get; set; }
        public String VariantSku { get; set; }
        public String Rating { get; set; }
        public String Submit = DictionaryHelper.GetText("Add to cart");
        
        public Product(ResultItem resultItem)
        {
            Name = resultItem.Name;            
            ThumbnailImage = resultItem.ThumbnailImage;            
            DisplayName = resultItem.DisplayName;
            Url = resultItem.ItemUrl;
            ShortDescription = resultItem.ShortDescription;
        }

        public Product(Item item)
        {
            Name = item.Name;
            DisplayName = item.DisplayName;
            Url = LinkManager.GetItemUrl(item);

            var financeTemplateID = new ID("{1A7B1DFF-E172-4DCF-8F01-B34571075756}");
            if (item.TemplateID == financeTemplateID)
            {
                PrimaryImageUrl = item.GetMediaFieldUrl("Primary Image", "");
                ThumbnailImage = item.GetMediaFieldUrl("Thumbnail Image", "");
                ShortDescription = item["Overview Abstract"];
                LongDescription = item["Text"];
            }
            else
            {
                PrimaryImageUrl = item.GetMediaFieldUrl("Image", "");
                ThumbnailImage = item.GetMediaFieldUrl("Image", "");
                ShortDescription = item["Short Description"];
                LongDescription = item["Long Description"];
            }
        }

        public Product(UCommerce.EntitiesV2.Product product)
        {
            SetProduct(product);
        }

        private void SetProduct(UCommerce.EntitiesV2.Product product)
        {
            Name = product.Name;
            if (!string.IsNullOrWhiteSpace(product.ThumbnailImageMediaId))
            {
                Sitecore.Data.ID mediaId = Sitecore.Data.ID.Parse(product.ThumbnailImageMediaId);                
                ThumbnailImage = MediaManager.GetMediaUrl(Sitecore.Context.Database.GetItem(mediaId));
            }
            if (!string.IsNullOrWhiteSpace(product.PrimaryImageMediaId))
            {
                Sitecore.Data.ID mediaId = Sitecore.Data.ID.Parse(product.PrimaryImageMediaId);
                PrimaryImage = Sitecore.Context.Database.GetItem(mediaId);
                PrimaryImageUrl = MediaManager.GetMediaUrl(PrimaryImage);
            }
            DisplayName = product.DisplayName();
            Url = CatalogLibrary.GetNiceUrlForProduct(product, null);
            ShortDescription = product.ShortDescription();
            LongDescription = product.LongDescription();
            PriceCalculation priceCalculation = CatalogLibrary.CalculatePrice(product);
            if (priceCalculation.YourPrice != null)
            {
                Price = priceCalculation.YourPrice.Amount.ToString();
                Tax = priceCalculation.YourTax.ToString();
            }
            else
            {
                Price = "0";
                Tax = "0";
            }
            ProductReviews = product.ProductReviews;
            Sku = product.Sku;
            VariantSku = product.VariantSku;
            Rating = UcommerceHelper.GetProductRating(this, Url);
        }
    }
}

 