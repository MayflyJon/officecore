﻿using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Officecore.Website.code.Model
{
    public class SubItem : SearchResultItem
    {
        [IndexField("_latestversion")]
        public bool IsLatestVersion { get; set; }
    }
}