﻿using Sitecore.Links;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UCommerce.Api;

namespace Officecore.Website.code.Model
{
    public class Category
    {
        public String Url { get; set; }
        public String Name { get; set; }
        public UCommerce.EntitiesV2.Category CommCategory { get; set; }
        public Sitecore.Data.Items.Item Item { get; set; }

        public Category(UCommerce.EntitiesV2.Category category)
        {
            Name = category.Name;           
            Url = CatalogLibrary.GetNiceUrlForCategory(category);
            CommCategory = category;
        }
        public Category(Sitecore.Data.Items.Item category)
        {
            Name = category.DisplayName;
            Url = LinkManager.GetItemUrl(category);
            Item = category;
        }
    }
}