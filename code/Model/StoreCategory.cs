﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UCommerce.EntitiesV2;

namespace Officecore.Website.code.Model
{
    public class StoreCategory
    {
        public Officecore.Website.code.Model.Image mediaImage { get; set; }
        public UCommerce.EntitiesV2.Category category { get; set; }
        public UCommerce.EntitiesV2.Product product { get; set; }
        public String url { get; set; }
        
    }
}