﻿
namespace Officecore.Website.code.Controls
{
    using code.DataRepositories;

    public class ContactUsPage : System.Web.UI.UserControl
    {
        internal ContactUsRepository repo = new ContactUsRepository();
        public virtual string LatLonList { get { return repo.GetLatLonListGlobal(); } }
    }
}