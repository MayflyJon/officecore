﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;
using Sitecore.Web;
using Sitecore.Web.UI.WebControls;
using Sitecore.Data.Fields;
using Officecore.Website.code.Helpers;

namespace Officecore.Website.code.Controls
{
    public class DatasourceControl : System.Web.UI.UserControl
    {
        private NameValueCollection properties;
        private Item item;

        protected string DataSource { get; set; }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            var sublayout = (Sublayout)Parent;
            if (sublayout != null)
            {
                DataSource = sublayout.DataSource;
                string paramaeters = Attributes["sc_parameters"];
                if (string.IsNullOrEmpty(paramaeters))
                {
                    paramaeters = sublayout.Parameters;
                }
                properties = WebUtil.ParseUrlParameters(paramaeters);
            }
        }

        /// <summary>
        /// Either retrive the datasource item if used, or return the current item
        /// </summary>
        public Item Item
        {
            get
            {
                if (item == null)
                {
                    if (!string.IsNullOrEmpty(DataSource))
                    {
                        item = Sitecore.Context.Database.GetItem(DataSource) ?? Sitecore.Context.Item;
                    }
                    else
                    {
                        item = Sitecore.Context.Item;
                    }
                }
                return item;
            }
        }

        public string GetProperty(string property)
        {
            return properties[property];
        }

        /// <summary>
        /// Get a URL for a Media item when referenced by a field.
        /// </summary>
        /// <param name="currentItem"></param>
        /// <param name="field"></param>
        /// <returns>string</returns>
        public static string GetImageURL(Item currentItem, string field)
        {
            Item item = currentItem;
            return item.GetMediaFieldUrl(field, "/sitecore/shell/Themes/Standard/Images/WebEdit/default_image.png");
        }
    }
}