﻿using System.Linq;
using Raven.Client.Linq;
using System;
using System.Collections.Generic;
using System.Web;
using Officecore.Website.code.Constants;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using UCommerce.EntitiesV2;
using UCommerce.Pipelines;
using UCommerce.Runtime;
using Officecore.Website.code.Helpers;
using UCommerce.Api;
using Sitecore.Data;
using Sitecore.ContentSearch.Linq;
using Officecore.Website.code.Search;
using UCommerce.Search;
using UCommerce.Extensions;
using UCommerce.Search.Facets;
using Sitecore.Analytics;
using Sitecore.Analytics.Data.DataAccess;
using Sitecore.Data.Managers;
using System.Globalization;
using UCommerce.EntitiesV2.Queries;

namespace Officecore.Website.code.DataRepositories
{
    public class ProductRepository : ItemRepositoryBase
    {
        private Item activateCommerce = Sitecore.Data.Database.GetDatabase("web").GetItem(ProductConstants.ACTIVATE_COMMERCE);

        public Boolean ActivateCommerce()
        {
            return activateCommerce.Fields["ActivateCommerce"].ToString() == "true";
        }

        public ProductCatalogGroup GetProductCatalogGroup() {
            return ProductCatalogGroup.SingleOrDefault(x => x.Name == activateCommerce.Fields["Store"].ToString());
        }
        /// <summary>
        /// Get all sub categories of a category
        /// </summary>
        /// <param name="item">The category</param>
        /// <returns>A list of categories</returns>
        public List<code.Model.Category> GetCategories(code.Model.Category item)
        {
            if (ActivateCommerce())
            {
                if (item == null)
                {
                    var catalog = ProductCatalog.SingleOrDefault(x => x.Name == activateCommerce.Fields["Store"].ToString());
                    var categories = catalog.Categories.Where(x => x.ParentCategory == null);
                    return categories.Select(x=>new code.Model.Category(x)).ToList();
                }
                return item.CommCategory.Categories.Select(x=>new code.Model.Category(x)).ToList();
            }
            else
            {
                IEnumerable<Item> cats;
                if (item == null)
                {
                    Item parentCat = Sitecore.Context.Database.GetItem("{07D9A696-A2FE-4A59-88FB-A57FE386B8AD}");
                    cats = parentCat.Axes.GetDescendants().Where(x => x.TemplateID.ToString() == "{7633B9E6-A44F-4318-85AE-C27D12D6DF11}").ToList();
                    return cats.Select(x => new code.Model.Category(x)).ToList();
                }
                cats = item.Item.Axes.GetDescendants().Where(x => x.TemplateID.ToString() == "{7633B9E6-A44F-4318-85AE-C27D12D6DF11}");
                return cats.Select(x => new code.Model.Category(x)).ToList();
            }           
        }

        /// <summary>
        /// Get teasers for the related products
        /// </summary>
        /// <param name="item"></param>
        /// <param name="productRelationType">The type of the product relation</param>
        /// <returns>A list with related products of the selected type</returns>
        public List<code.Model.Product> GetRelatedProducts(Product item, Helpers.UcommerceHelper.ProductRelationType productRelationType)
        {
            List<Product> products = item.GetRelatedProducts().First(prt => (int)productRelationType == prt.Key.Id).Value;
            return products.Select(x => new code.Model.Product(x)).ToList();
        }

        /// <summary>
        /// Post a review for a product
        /// </summary>
        /// <param name="product">The product to leave a review on</param>
        /// <param name="name">The (first) name of the reviewer</param>
        /// <param name="email">The email address of the reviewer</param>
        /// <param name="rating">The rating of the product</param>
        /// <param name="reviewHeadline">The title of the review</param>
        /// <param name="reviewText">The review</param>
        /// <param name="ip">the hostname of the reviewer</param>
        public void LeaveReview(UCommerce.EntitiesV2.Product product, string name, string email, int rating, string reviewHeadline, string reviewText, string ip)
        {
            var basket = SiteContext.Current.OrderContext.GetBasket();

            if (basket.PurchaseOrder.Customer == null)
            {
                basket.PurchaseOrder.Customer = new Customer() { FirstName = name, LastName = String.Empty, EmailAddress = email };
            }
            else
            {
                basket.PurchaseOrder.Customer.FirstName = name;
                if (basket.PurchaseOrder.Customer.LastName == null)
                {
                    basket.PurchaseOrder.Customer.LastName = String.Empty;
                }
                basket.PurchaseOrder.Customer.EmailAddress = email;
            }

            basket.PurchaseOrder.Customer.Save();

            var review = new ProductReview()
            {
                ProductCatalogGroup = SiteContext.Current.CatalogContext.CurrentCatalogGroup,
                ProductReviewStatus = ProductReviewStatus.SingleOrDefault(s => s.Name == "New"),
                CreatedOn = DateTime.Now,
                CreatedBy = "System",
                Product = product,
                Customer = basket.PurchaseOrder.Customer,
                Rating = rating,
                ReviewHeadline = reviewHeadline,
                ReviewText = reviewText,
                Ip = ip
            };

            product.AddProductReview(review);

            PipelineFactory.Create<ProductReview>("ProductReview").Execute(review);
        }

        /// <summary>
        /// Get all product reviews for a specific product
        /// </summary>
        /// <param name="product">The product to get the reviews for</param>
        /// <returns>A list with productreviews</returns>
        public List<ProductReview> GetApprovedProductReviews(Product product)
        {
            var list = new List<ProductReview>();
            var approvedReviews = product.ProductReviews.Where(review => review.Approved());
            if (approvedReviews.Any())
            {
                list = approvedReviews.ToList();
            }

            return list;
        }
        
        public List<Product> GetDeepProducts(Category category,  List<Product> products)
        {           
            products.AddRange(category.Products);
            foreach (Category c in category.GetCategories())
            {
                GetDeepProducts(c, products);
            }
            return products;
        }

        /// <summary>
        /// Get the products with faceting options from a specific category
        /// </summary>
        /// <param name="category">The category</param>
        /// <returns>A list of products</returns>
        public List<code.Model.Product> GetProductsWithFacets(Category category)
        {
            List<Product> allProducts = new List<Product>();
            allProducts = GetDeepProducts(category, allProducts);
            var facetsForQuerying = UcommerceHelper.GetFacets(HttpContext.Current.Request.QueryString);
            var productsFiltered = SearchLibrary.GetProductsFor(category, facetsForQuerying);
            var productList = new List<code.Model.Product>();

            if (productsFiltered.Count == 0)
            {
                return allProducts.Select(x => new code.Model.Product(x)).ToList(); 
            }
            else
            {
                foreach (var product in productsFiltered)
                {
                    productList.Add(new code.Model.Product(allProducts.FirstOrDefault(x => x.Sku == product.Sku && x.VariantSku == product.VariantSku)));
                }

                return productList;
            }
        }

        public IEnumerable<Facet> SearchFacets(string searchterm, IList<Facet> facetList, out List<code.Model.Product> products)
        {
            return SearchProducts(searchterm, facetList, out products).ToFacets();
        }

        public IFacetedQueryable<UCommerce.Documents.Product> SearchProducts(string searchterm, IList<Facet> facetList, out List<code.Model.Product> products)
        {
            products =  new List<Model.Product>();
            Product[] matchingProducts;
            ProductCatalogGroup pcg = GetProductCatalogGroup();
            matchingProducts = Product.Find(p => (p.CategoryProductRelations.Any(y => y.Category.ProductCatalog.ProductCatalogGroup == pcg)) && p.DisplayOnSite
                                                       && (p.Name.Contains(searchterm)
                                                        || p.Sku.Contains(searchterm)
                                                        || p.ProductDescriptions.Any(description => description.CultureCode == CultureInfo.CurrentCulture.Name && description.LongDescription.Contains(searchterm))
                                                        || p.ProductDescriptions.Any(description => description.CultureCode == CultureInfo.CurrentCulture.Name && description.ShortDescription.Contains(searchterm)))).ToArray();
            string[] skus = new string[matchingProducts.Length];

            for (int i = 0; i < matchingProducts.Length; i++)
            {
                matchingProducts[i].GetCategories();
                skus[i] = matchingProducts[i].Sku;
                products.Add(new code.Model.Product(matchingProducts[i]));
            }
            IFacetedQueryable<UCommerce.Documents.Product> query = SearchLibrary.FacetedQuery().Where
                                            (
                                                product => product.Sku.In<string>(skus)
                                            ).WithFacets(facetList);

            return query;
        }

        public IEnumerable<Facet> SearchFacets(Category category, IList<Facet> facetList, out List<code.Model.Product> products)
        {
            return SearchProducts(category, facetList, out products).ToFacets();
        }

        public IFacetedQueryable<UCommerce.Documents.Product> SearchProducts(Category category, IList<Facet> facetList, out List<code.Model.Product> products)
        {
            products = new List<code.Model.Product>();
            ProductCatalogGroup pcg = GetProductCatalogGroup();
            foreach (var product in Product.Find(p => (p.CategoryProductRelations.Any(y => y.Category.ProductCatalog.ProductCatalogGroup == pcg)) && p.DisplayOnSite))
            {
                if (product.GetCategories().Contains(category))
                {
                    products.Add(new code.Model.Product(product));
                }
            }

            string[] skus = new string[products.Count];
            for (int i = 0; i < products.Count; i++)
            {
                skus[i] = products[i].Sku;
            }

            IFacetedQueryable<UCommerce.Documents.Product> query = SearchLibrary.FacetedQuery().Where
                                            (
                                                product => product.Sku.In<string>(skus)
                                            ).WithFacets(facetList);

            return query;
        }

        public List<code.Model.Product> GetRecentlyViewedProduct()
        {
            var list = new List<Product>();
            Visitor visitor = Tracker.Visitor;
            visitor.LoadAll();
            ProductCatalogGroup pcg = GetProductCatalogGroup();
            foreach (var page in visitor.DataSet.Pages)
            {
                var item = Sitecore.Context.Database.GetItem(page.ItemId.ToString());
                if (item != null && Sitecore.Context.Item.ID != item.ID)
                {                   
                    var product = Product.SingleOrDefault(p => p.CategoryProductRelations.Any(y => y.Category.ProductCatalog.ProductCatalogGroup == pcg) && p.Sku == item[FieldConstants.Ucommerce.Sku]);
                    if (!list.Contains(product) && product != null)
                    {
                        list.Add(product);
                    }
                }
            }
            return list.Select(x => new code.Model.Product(x)).Reverse().ToList();           
        }

        public List<Product> GetProductsFromBasket(PurchaseOrder basket)
        {
            var list = new List<Product>();
            foreach (OrderLine orderline in basket.OrderLines)
            {
                var product = Product.SingleOrDefault(x => x.Sku == orderline.Sku);
                if (product != null)
                {
                    list.Add(product);
                }
            }

            return list;
        }

        public void UpdateCart(Product product, int quantity)
        {
            var basket = TransactionLibrary.GetBasket().PurchaseOrder;
            var orderline = basket.OrderLines.SingleOrDefault(x => x.Sku == product.Sku);

            TransactionLibrary.UpdateLineItem(orderline.Id, quantity);
            TransactionLibrary.ExecuteBasketPipeline();
        }
    }
}