﻿using System.Collections.Generic;
using Officecore.Website.code.Constants;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;

namespace Officecore.Website.code.DataRepositories
{
    public class ItemRepositoryBase
    {
        public virtual IEnumerable<Item> GetItems(string queryString)
        {
            return Sitecore.Context.Database.SelectItems(SitecoreSiteRoot + queryString);
        }

        public IEnumerable<Item> GetMainMenu(string id)
        {
            var main = Sitecore.Context.Database.GetItem(id);
            return GetMainMenu(main);
        }

        public IEnumerable<Item> GetMainMenu(Item main)
        {
            if (main != null)
            {
                foreach (Item child in main.Children)
                {
                    var lf = (LinkField)child.Fields["Link"];

                    if (lf != null && (!lf.IsInternal || lf.TargetItem != null))
                    {
                        yield return child;
                    }
                }
            }
        }

        public string SitecoreSiteRoot
        {
            get
            {
                var current = Sitecore.Context.Item; ;
                var root = current.Axes.SelectSingleItem("ancestor-or-self::*[@@templateid='" + TemplateConstants.SiteRootId + "']");
                if (root != null)
                {
                    return root.Paths.Path;
                }

                return Sitecore.Context.Site.StartPath;
            }
        }

        public Database Database
        {
            get
            {
                if (Sitecore.Context.ContentDatabase != null)
                {
                    return Sitecore.Context.ContentDatabase;
                }

                return Sitecore.Context.Database;
            }
        }
    }
}