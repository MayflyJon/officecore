﻿using System.Web;

// http://www.sitecore.net/unitedkingdom/Community/Technical-Blogs/John-West-Sitecore-Blog/Posts/2013/04/Handling-Errors-in-the-Sitecore-ASPNET-CMS.aspx
namespace Officecore.Website.code.Pipelines.HttpRequest
{
    public class ExecuteRequest : Sitecore.Pipelines.HttpRequest.ExecuteRequest
    {
        protected override void RedirectOnNoAccess(string url)
        {
            Sitecore.Diagnostics.Log.Warn(this + " : No access for " + HttpContext.Current.Request.RawUrl + " (" + Sitecore.Context.User.Name + ")", this);
            Sitecore.Web.WebUtil.Redirect("/?oc_loginError=1");
        }

        protected override void RedirectOnSiteAccessDenied(string url)
        {
            Sitecore.Diagnostics.Log.Warn(
              this + " : No site access for " + HttpContext.Current.Request.RawUrl + " (" + Sitecore.Context.User.Name + ")",
              this);

            Sitecore.Diagnostics.Log.Warn(this + " : No access for " + HttpContext.Current.Request.RawUrl + " (" + Sitecore.Context.User.Name + ")", this);
            Sitecore.Web.WebUtil.Redirect("/?oc_loginError=2");
        }
    }
}