﻿// Decompiled with JetBrains decompiler
// Type: Sitecore.Labs.AnalyticsExtensions.RenderFieldPipeline.ExpandAnalyticsTokens
// Assembly: Sitecore.Labs.AnalyticsExtensions, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C8A60C0F-94BA-4FE5-B45B-16523B7C76EC
// Assembly location: C:\Websites\_dev\officecore\Website\bin\Sitecore.Labs.AnalyticsExtensions.dll

using System.Text.RegularExpressions;
using Sitecore.Analytics;
using Sitecore.Analytics.Data.DataAccess.DataSets;
using Sitecore.Pipelines.RenderField;

namespace Officecore.Website.code.Pipelines.RenderField
{
    public class ExpandAnalyticsTokens
    {
        public void Process(RenderFieldArgs args)
        {
            if (!Tracker.IsActive)
                return;

            string str = new Regex("\\[\\[([^\\]]*)\\]\\]").Replace(args.Result.FirstPart, Evaluator);
            args.Result.FirstPart = str;
        }

        private string Evaluator(Match match)
        {
            var field = match.Groups[1].Value;
            VisitorDataSet.VisitorTagsRow visitorTagsRow = Tracker.Visitor.Tags.Find(field);

            if (visitorTagsRow != null)
            {
                return visitorTagsRow.TagValue;
            }

            if (Sitecore.Context.User == null || Sitecore.Context.User.Profile == null)
            {
                return match.Groups[0].Value;
            }

            var profileValue = Sitecore.Context.User.Profile[field];
            return string.IsNullOrWhiteSpace(profileValue) ? match.Groups[0].Value : profileValue;
        }
    }
}